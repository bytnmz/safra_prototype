﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Domain.Impl
{
    public class FilterContentService : IFilterContentService
    {
        private readonly TaxonomyManager taxonManager;

        public FilterContentService()
        {
            taxonManager = TaxonomyManager.GetManager();
        }

        public DynamicContent GetLiveContent(Guid masterId, string itemType)
        {
            var dynamicModuleManager = DynamicModuleManager.GetManager();
            var type = TypeResolutionService.ResolveType(itemType);

            return dynamicModuleManager.GetDataItems(type)
                    .FirstOrDefault(c => c.OriginalContentId == masterId &&
                                    c.Status == ContentLifecycleStatus.Live && c.Visible);
        }

        public IQueryable<DynamicContent> FilterContent(string itemType,
            string category = "", string location = "", string persona = "",
            string keyword = "")
        {
            var dynamicModuleManager = DynamicModuleManager.GetManager();
            var type = TypeResolutionService.ResolveType(itemType);

            var queryData = dynamicModuleManager.GetDataItems(type)
                    .Where(c => c.Status == ContentLifecycleStatus.Live && c.Visible);

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                if (itemType == ContentTypeConstants.Promotion)
                {
                    queryData = queryData.Where(c => c.GetValue<string>("Title").Contains(keyword) ||
                    c.GetValue<string>("MerchantName").Contains(keyword));
                }
                else
                {
                    queryData = queryData.Where(c => c.GetValue<string>("Title").Contains(keyword));
                }
            }

            ApplyFilter(ref queryData, category, "Categories", "Category");
            ApplyFilter(ref queryData, location, "locations", "locations");
            ApplyFilter(ref queryData, persona, "personas", "personas");

            return queryData;
        }

        public void ApplyFilter(ref IQueryable<DynamicContent> queryData, string filter,
            string taxonName, string taxonFieldName)
        {
            if (!filter.IsNullOrWhitespace() && filter != "all")
            {
                var taxon = taxonManager.GetTaxonomies<HierarchicalTaxonomy>()
                    .FirstOrDefault(t => t.Name == taxonName);

                if (taxon != null)
                {
                    var categoryIds = GetTaxonIdsFromFilter(taxon, filter);

                    queryData = queryData.Where(c => c.GetValue<IList<Guid>>(taxonFieldName).Any(t => categoryIds.Contains(t.ToString())));
                }
            }
        }

        public bool IsContentLive(string urlName, string itemType)
        {
            var dynamicModuleManager = DynamicModuleManager.GetManager();
            var type = TypeResolutionService.ResolveType(itemType);

            return dynamicModuleManager.GetDataItems(type)
                    .Any(c => c.UrlName == urlName &&
                                    c.Status == ContentLifecycleStatus.Live && c.Visible);
        }

        private string GetTaxonIdsFromFilter(HierarchicalTaxonomy taxon, string taxonText)
        {
            var taxonIds = new List<Guid>();

            foreach (var token in taxonText.Split('|'))
            {
                var cat = token.Trim();
                if (!string.IsNullOrWhiteSpace(cat))
                {
                    var id = taxon.Taxa.FirstOrDefault(t => t.Name == cat)?.Id;

                    if (id.HasValue)
                    {
                        taxonIds.Add(id.Value);
                    }
                }
            }

            return string.Join(",", taxonIds.Select(t => t.ToString()));
        }
    }
}