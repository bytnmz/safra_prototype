﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Lists.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Lists;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SAFRA.Domain.Impl
{
    public class CategoriesService : ICategoriesService
    {
        private readonly ListsManager listsManager;

        public CategoriesService()
        {
            listsManager = ListsManager.GetManager();
        }

        public List<Category> GetCategories(Guid listId, string categoryName = "Categories")
        {
            var list = listsManager.GetLists().FirstOrDefault(l => l.Id == listId);

            var categories = list != null ? GetCategoryInfo(list, categoryName) : new List<Category>();

            return categories;
        }

        public List<Category> GetParentCategories(Guid listId, string categoryName = "Categories")
        {
            var list = listsManager.GetLists().FirstOrDefault(l => l.Id == listId);

            var categories = list != null ? GetParentCategoryInfo(list, categoryName) : new List<Category>();

            return categories;
        }

        private List<Category> GetCategoryInfo(List list, string categoryName)
        {
            var categories = new List<Category>();
            var listItems = list.ListItems.Where(i => i.Visible && i.Status == ContentLifecycleStatus.Live).OrderBy(i => i.Ordinal);

            var taxonomyManager = TaxonomyManager.GetManager();
            var categoriesTaxon = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>()
                .SingleOrDefault(t => t.Name == categoryName);

            foreach (var item in listItems)
            {
                var category = new Category
                {
                    DisplayName = item.Title
                };

                var catFieldName = categoryName == "Categories" ? "Category" : categoryName;

                var catGuid = item.GetValue<IList<Guid>>(catFieldName)?.FirstOrDefault();

                if (catGuid != null && categoriesTaxon != null)
                {
                    var cat = categoriesTaxon.Taxa.FirstOrDefault(t => t.Id == catGuid);

                    if (cat != null)
                    {
                        category.CategoryName = cat.Title;
                        category.CategoryId = cat.Name;
                    }
                }

                categories.Add(category);
            }

            return categories;
        }

        private List<Category> GetParentCategoryInfo(List list, string categoryName)
        {
            var categories = new List<Category>();
            var listItems = list.ListItems.Where(i => i.Visible && i.Status == ContentLifecycleStatus.Live).OrderBy(i => i.Ordinal);

            var taxonomyManager = TaxonomyManager.GetManager();
            var categoriesTaxon = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>()
                .SingleOrDefault(t => t.Name == categoryName);

            foreach (var item in listItems)
            {
                var catFieldName = categoryName == "Categories" ? "Category" : categoryName;

                var catGuid = item.GetValue<IList<Guid>>(catFieldName)?.FirstOrDefault();

                if (catGuid != null && categoriesTaxon != null)
                {
                    var cat = categoriesTaxon.Taxa.FirstOrDefault(t => t.Id == catGuid);

                    if (cat != null)
                    {
                        var parent = cat.Parent ?? cat;

                        if (!categories.Any(c => c.CategoryName == parent.Title))
                        {
                            categories.Add(new Category
                            {
                                DisplayName = parent.Title,
                                CategoryName = parent.Title,
                                CategoryId = parent.Name
                            });
                        }
                    }
                }
            }

            return categories;
        }
    }
}