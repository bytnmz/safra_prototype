﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Domain.Impl
{
    public class PromotionsService : IPromotionsService
    {
        public List<DynamicContent> GetFeaturedOfTheMonth(int count)
        {           
            var dynamicModuleManager = DynamicModuleManager.GetManager();
            var type = TypeResolutionService.ResolveType(ContentTypeConstants.Promotion);

            var dateTime = DateTime.Now;
            var monthStart = new DateTime(dateTime.Year, dateTime.Month, 1);
            var monthEnd = monthStart.AddMonths(1).AddSeconds(-1);

            var queryData = dynamicModuleManager.GetDataItems(type)
                    .Where(d => d.Status == ContentLifecycleStatus.Live && d.Visible 
                    && d.GetValue<bool>("Featured")
                    && (!d.GetValue<DateTime?>("EndDate").HasValue || d.GetValue<DateTime?>("EndDate") > monthStart.ToUniversalTime())
                    && d.GetValue<DateTime?>("StartDate") < monthEnd.ToUniversalTime())
                    .OrderBy(d => d.GetValue<DateTime?>("StartDate"))
                    .Take(count);

            return queryData.ToList();
        }
    }
}