﻿using System;
using System.Collections.Generic;

namespace SAFRA.Domain
{
    public interface ICategoriesService
    {
        List<Category> GetCategories(Guid listId, string categoryName = "Categories");
        List<Category> GetParentCategories(Guid listId, string categoryName = "Categories");
    }
}
