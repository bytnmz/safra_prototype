﻿using System;
using System.Linq;
using Telerik.Sitefinity.DynamicModules.Model;

namespace SAFRA.Domain
{
    public interface IFilterContentService
    {
        DynamicContent GetLiveContent(Guid masterId, string itemType);

        IQueryable<DynamicContent> FilterContent(string itemType,
            string category = "", string location = "", string persona = "",
            string keyword = "");

        void ApplyFilter(ref IQueryable<DynamicContent> query, string filter,
            string taxonName, string taxonFieldName);

        bool IsContentLive(string urlName, string itemType);
    }
}