﻿using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;

namespace SAFRA.Domain
{
    public interface IPromotionsService
    {
        List<DynamicContent> GetFeaturedOfTheMonth(int count);
    }
}