﻿namespace SAFRA.Domain
{
    public class Category
    {
        public string DisplayName { get; set; }
        public string CategoryName { get; set; }
        public string CategoryId { get; set; }

        public Category()
        {
            DisplayName = string.Empty;
            CategoryName = string.Empty;
            CategoryId = string.Empty;
        }
    }
}