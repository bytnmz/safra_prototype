﻿using System;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Publishing;
using Telerik.Sitefinity.Publishing.Pipes;

namespace SAFRA
{
    public class CustomPageInboundPipe : PageInboundPipe
    {
        protected override void SetWrapperObjectProperties(WrapperObject wrapperObject, PageNode node)
        {
            base.SetWrapperObjectProperties(wrapperObject, node);

            var pageData = node.GetPageData();
            if (pageData != null)
            {
                wrapperObject.AddProperty("Description", pageData.Description.ToString());
            }
            else
            {
                wrapperObject.AddProperty("Description", String.Empty);
            }
        }
    }
}
