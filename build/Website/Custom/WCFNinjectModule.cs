﻿using Ninject.Modules;
using SAFRA.Custom.Services.Service;

namespace SAFRA.Custom
{
    public class WCFNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IActivityService>().To<ActivityService>();
            Bind<ICourseService>().To<CourseService>();
            Bind<IPromotionService>().To<PromotionService>();
            Bind<IFacilityService>().To<FacilityService>();
            Bind<IAmenityService>().To<AmenityService>();
            Bind<IDiscountGuideService>().To<DiscountGuideService>();
            Bind<IBusinessUnitService>().To<BusinessUnitService>();
            Bind<IAnnouncementService>().To<AnnouncementService>();
            Bind<ISafraClubhouseService>().To<SafraClubhouseService>();
            Bind<IMerchantService>().To<MerchantService>();
        }
    }
}