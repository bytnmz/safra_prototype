﻿using SAFRA.Custom.Services.Model;
using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace SAFRA.Custom.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISAFRA_CMS_Service" in both code and config file together.
    [ServiceContract]
    public interface ISAFRA_CMS_Service
    {
        #region Activities Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllActivities/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<Activity>> GetAllActivities(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetActivity/{ActivityId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Activity> GetActivity(String ActivityId, String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetActivityAdminCharge/{ActivityId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<ActivityAdminCharge>> GetActivityAdminCharge(String ActivityId, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetActivityRate/{ActivityId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<ActivityRate>> GetActivityRate(String ActivityId, String Username, String Pass);

        #endregion

        #region Courses Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllCourse/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<Course>> GetAllCourse(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetCourse/{CourseId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Course> GetCourse(String CourseId, String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetCourseSchedule/{CourseId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<CourseSchedule>> GetCourseSchedule(String CourseId, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetCourseRate/{CourseId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<CourseRate>> GetCourseRate(String CourseId, String Username, String Pass);

        #endregion

        #region Promotions Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllPromotions/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<Promotions>> GetAllPromotions(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetPromotionsbyId/{PromotionId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Promotions> GetPromotionsbyId(String PromotionId, String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetPromotionsbyClubId/{ClubId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Promotions> GetPromotionsbyClubId(String ClubId, String Username, String Pass);

        #endregion

        #region Facilities Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllFacilities/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<Facility>> GetAllFacilities(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetFacilitybyId/{FacilityId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Facility> GetFacilitybyId(String FacilityId, String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetFacilityRate/{FacilityId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<FacilityRate>> GetFacilityRate(String FacilityId, String Username, String Pass);

        #endregion

        #region Amenity Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllAmenity/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<Amenity>> GetAllAmenity(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAmenitybyId/{AmenityId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Amenity> GetAmenitybyId(String AmenityId, String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAmenityRate/{AmenityId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<AmenityRate>> GetAmenityRate(String AmenityId, String Username, String Pass);

        #endregion

        #region Discount Guide Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllDiscountGuides/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<DiscountGuide>> GetAllDiscountGuides(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetDiscountGuidebyId/{DiscountGuideId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<DiscountGuide> GetDiscountGuidebyId(String DiscountGuideId, String IsSimpleReturn, String Username, String Pass);

        #endregion

        #region Business Unit Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllBusinessUnits/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<BusinessUnit>> GetAllBusinessUnits(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetBusinessUnitbyId/{BusinessUnitsId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<BusinessUnit> GetBusinessUnitbyId(String BusinessUnitsId, String IsSimpleReturn, String Username, String Pass);

        #endregion

        #region Announcement Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllAnnouncement/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<Announcement>> GetAllAnnouncement(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAnnouncementbyId/{AnnouncementId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Announcement> GetAnnouncementbyId(String AnnouncementId, String IsSimpleReturn, String Username, String Pass);

        #endregion

        #region Safra Clubhouse Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllSafraClubhouse/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<SafraClubhouse>> GetAllSafraClubhouse(String IsSimpleReturn, String Username, String Pass);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetSafraClubhousebyId/{SafraClubhouseId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<SafraClubhouse> GetSafraClubhousebyId(String SafraClubhouseId, String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetCarParkRatebyClubId/{SafraClubhouseId}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<CarparkRate>> GetCarParkRatebyClubId(String SafraClubhouseId, String Username, String Pass);

        #endregion

        #region Merchant Methods

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllMerchants/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<List<Merchants>> GetAllMerchants(String IsSimpleReturn, String Username, String Pass);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetMerchantByID/{MerchantId}/{IsSimpleReturn}/{Username}/{Pass}",
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        GetResponseModel<Merchants> GetMerchantByID(String MerchantId, String IsSimpleReturn, String Username, String Pass);

        #endregion
    }
}
