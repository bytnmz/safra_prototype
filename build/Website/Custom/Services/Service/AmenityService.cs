﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Map;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class AmenityService : IAmenityService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String AmenityContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.Amenity";
        private readonly String AmentiyRateContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.AmenityRate";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _AmentiyContentType;
        private readonly Type _AmentiyRateContentType;

        public AmenityService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _AmentiyContentType = TypeResolutionService.ResolveType(AmenityContentTypeRef);
            _AmentiyRateContentType = TypeResolutionService.ResolveType(AmentiyRateContentTypeRef);
        }

        public List<Amenity> GetAllAmenity(String IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var amenityItems = _DynamicModuleManager.GetDataItems(_AmentiyContentType).ToList()
                .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true).ToList();

            if (amenityItems != null & amenityItems.Count() > 0)
            {
                List<Amenity> amenityModelList = new List<Amenity>();

                foreach (var amenityItem in amenityItems)
                {
                    amenityModelList.Add(MapAmenityContentToModel(amenityItem, isSimpleReturnInBoolean));
                }

                return amenityModelList;
            }

            return new List<Amenity>();
        }

        public Amenity GetAmenitybyId(String AmenityId, String IsSimpleReturn)
        {
            Boolean isAmenityIdValid = Guid.TryParse(AmenityId, out Guid amenityIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isAmenityIdValid)
            {
                var amenityItem = _DynamicModuleManager.GetDataItems(_AmentiyContentType)
                    .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true 
                        && i.GetValue<Guid>(AmenityConstant.AmenityId) == amenityIdInGuid || i.Id == amenityIdInGuid).FirstOrDefault();

                if (amenityItem != null)
                {
                    return MapAmenityContentToModel(amenityItem, isSimpleReturnInBoolean);
                }
            }

            return new Amenity();
        }

        public List<AmenityRate> GetAmenityRate(String AmenityId)
        {
            Boolean isAmenityIdValid = Guid.TryParse(AmenityId, out Guid amenityIdInGuid);

            if (isAmenityIdValid)
            {
                var amenityItem = _DynamicModuleManager.GetDataItems(_AmentiyContentType)
                    .FirstOrDefault(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true &&
                         i.GetValue<Guid>(AmenityConstant.AmenityId) == amenityIdInGuid || i.Id == amenityIdInGuid);

                if (amenityItem != null)
                {
                    var amenityRateItems = amenityItem.GetChildItems(_AmentiyRateContentType)
                        .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true).ToList();

                    if (amenityRateItems != null && amenityRateItems.Count() > 0)
                    {
                        List<AmenityRate> amentiyRateModelList = new List<AmenityRate>();

                        foreach (var amenityRateItem in amenityRateItems)
                        {
                            amentiyRateModelList.Add(MapAmenityRateContentToModel(amenityRateItem));
                        }

                        return amentiyRateModelList;
                    }
                }
            }

            return new List<AmenityRate>();
        }

        private Amenity MapAmenityContentToModel(DynamicContent AmenityItem, Boolean IsSimpleReturn)
        {
            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);

            Amenity amenities = null;

            if (IsSimpleReturn)
            {
                amenities = new Amenity()
                {
                    Amenity_ID = DynamicContentUtility.ToGuid(AmenityItem, AmenityConstant.AmenityId),
                    Name = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.Name),
                };
            }
            else
            {
                amenities = new Amenity()
                {
                    Title = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.Title),
                    Amenity_ID = DynamicContentUtility.ToGuid(AmenityItem, AmenityConstant.AmenityId),
                    Name = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.Name),
                    Type = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.Type),
                    Description = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.Description),//.Replace(LocalhostUrl, WebServerUrl)
                    SpecialRemarks = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.SpecialRemarks),//.Replace(LocalhostUrl, WebServerUrl)
                    Address = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.Address),
                    PostalCode = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.PostalCode),
                    TermsAndConditions = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.TermsAndConditions),
                    Url = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.Url),
                    BannerUrl = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.BannerUrl),
                    ImageUrl = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.ImageUrl),
                    UrlName = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.UrlName),
                    RegistrationUrl = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.RegistrationUrl),
                    OverviewDescription = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.OverviewDescription),//.Replace(LocalhostUrl, WebServerUrl)
                    UnitNo = DynamicContentUtility.ToString(AmenityItem, AmenityConstant.UnitNo),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(AmenityItem, AmenityConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl)
                    Locations = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.Location, locations),
                    Categories = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.DSSCategory, categories),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.DSSLifestyle, lifestyles),
                    Department = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.DSSSector, sectors),
                    Fabs = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(AmenityItem, AmenityConstant.DSSClubCategory, clubCategories),
                };

                var merchantOutletItems = AmenityItem.GetRelatedItems(AmenityConstant.OutletRelatedData).ToList();

                if (merchantOutletItems != null && merchantOutletItems.Count() > 0)
                {
                    foreach (var merchantOutletItem in merchantOutletItems)
                    {
                        DynamicContent merchantOutletItemAsDynContent = merchantOutletItem as DynamicContent;

                        amenities.MerchantOutlets.Add(MerchantOutletMap.MapMerchantOutletContentToModel(merchantOutletItemAsDynContent,
                            locations, categories, lifestyles, departments, clubs, sectors, clubCategories));
                    }
                }
            }

            return amenities;
        }

        private AmenityRate MapAmenityRateContentToModel(DynamicContent AmenityRateItem)
        {
            AmenityRate amenityRate = new AmenityRate()
            {
                Title = DynamicContentUtility.ToString(AmenityRateItem, AmenityRateConstant.Title),
                CustomerType = DynamicContentUtility.ToString(AmenityRateItem, AmenityRateConstant.CustomerType),
                ChargesFees = DynamicContentUtility.ToFloat(AmenityRateItem, AmenityRateConstant.ChargesFees),
                ChargesType = DynamicContentUtility.ToString(AmenityRateItem, AmenityRateConstant.ChargesType),
            };

            return amenityRate;
        }
    }
}