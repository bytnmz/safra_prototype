﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Map;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class FacilityService : IFacilityService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String FacilityContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.Facility";
        private readonly String FacilityRateContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.FacilityRate";


        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _FacilityContentType;
        private readonly Type _FacilityRateContentType;

        public FacilityService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _FacilityContentType = TypeResolutionService.ResolveType(FacilityContentTypeRef);
            _FacilityRateContentType = TypeResolutionService.ResolveType(FacilityRateContentTypeRef);
        }

        public List<Facility> GetAllFacilities(String IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var facilityItems = _DynamicModuleManager.GetDataItems(_FacilityContentType)
                .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true).ToList();
            
            if (facilityItems != null && facilityItems.Count() > 0)
            {
                List<Facility> facilityModelList = new List<Facility>();

                foreach (var facilityItem in facilityItems)
                {
                    facilityModelList.Add(MapFacilityContentToModel(facilityItem, isSimpleReturnInBoolean));
                }

                return facilityModelList;
            }

            return new List<Facility>();
        }

        public Facility GetFacilitybyId(String FacilityId, String IsSimpleReturn)
        {
            Boolean isFacilityIdValid = Guid.TryParse(FacilityId, out Guid facilityIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isFacilityIdValid)
            {
                var facilityItem = _DynamicModuleManager.GetDataItems(_FacilityContentType)
                    .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true 
                        && i.GetValue<Guid>(FacilityConstant.FacilityId) == facilityIdInGuid || i.Id == facilityIdInGuid).FirstOrDefault();

                if (facilityItem != null)
                {
                    return MapFacilityContentToModel(facilityItem, isSimpleReturnInBoolean);
                }
            }

            return new Facility();
        }

        public List<FacilityRate> GetFacilityRate(String FacilityId)
        {
            Boolean isFacilityIdValid = Guid.TryParse(FacilityId, out Guid facilityIdInGuid);

            if (isFacilityIdValid)
            {
                var facilityItem = _DynamicModuleManager.GetDataItems(_FacilityContentType)
                    .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                        && i.GetValue<Guid>(FacilityConstant.FacilityId) == facilityIdInGuid || i.Id == facilityIdInGuid).FirstOrDefault();
                
                if (facilityItem != null)
                {
                    var facilityRateItems = facilityItem.GetChildItems(_FacilityRateContentType).ToList();

                    if (facilityRateItems != null && facilityRateItems.Count() > 0)
                    {
                        List<FacilityRate> facilityRateModelList = new List<FacilityRate>();

                        foreach (var facilityRateItem in facilityRateItems)
                        {
                            facilityRateModelList.Add(MapFacilityRateContentToModel(facilityRateItem));
                        }

                        return facilityRateModelList;
                    }
                }
            }

            return new List<FacilityRate>();
        }

        private Facility MapFacilityContentToModel(DynamicContent FacilityItem, Boolean IsSimpleReturn)
        {
            List<Facility> facilityModelList = new List<Facility>();

            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);


            Facility facilities = null;

            if (IsSimpleReturn)
            {
                facilities = new Facility()
                {
                    Facility_ID = DynamicContentUtility.ToGuid(FacilityItem, FacilityConstant.FacilityId),
                    Name = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.Name),
                };
            }
            else
            {
                facilities = new Facility()
                {
                    Title = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.Title),
                    Facility_ID = DynamicContentUtility.ToGuid(FacilityItem, FacilityConstant.FacilityId),
                    Name = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.Name),
                    Description = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.Description),//.Replace(LocalhostUrl, WebServerUrl)
                    SpecialRemarks = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.SpecialRemarks),//.Replace(LocalhostUrl, WebServerUrl)
                    TermsAndConditions = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.TermsAndConditions),
                    Url = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.Url),
                    BannerUrl = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.BannerUrl),
                    ImageUrl = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.ImageUrl),
                    OperatingHours = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.OperatingHours),
                    UrlName = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.UrlName),
                    PostalCode = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.PostalCode),
                    RegistrationUrl = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.RegistrationUrl),
                    ContactInfo = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.ContactInfo),
                    OverviewDescription = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.OverviewDescription),
                    UnitNo = DynamicContentUtility.ToString(FacilityItem, FacilityConstant.UnitNo),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(FacilityItem, FacilityConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl)
                    Locations = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.Location, locations),
                    Categories = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.DSSCategory, categories),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.DSSLifestyle, lifestyles),
                    Department = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.DSSSector, sectors),
                    Fabs = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(FacilityItem, FacilityConstant.DSSClubCategory, clubCategories),
                };

                var merchantOutletItems = FacilityItem.GetRelatedItems(FacilityConstant.MerchantOutlets).ToList();

                if (merchantOutletItems != null && merchantOutletItems.Count() > 0)
                {
                    foreach (var merchantOutletItem in merchantOutletItems)
                    {
                        DynamicContent merchantOutletItemAsDynContent = merchantOutletItem as DynamicContent;

                        facilities.MerchantOutlets.Add(MerchantOutletMap.MapMerchantOutletContentToModel(merchantOutletItemAsDynContent,
                            locations, categories, lifestyles, departments, clubs, sectors, clubCategories));
                    }
                }
            }

            return facilities;
        }
        
        private FacilityRate MapFacilityRateContentToModel(DynamicContent FacilityRateItem)
        {
            FacilityRate facilityRate = new FacilityRate()
            {
                Title = DynamicContentUtility.ToString(FacilityRateItem, FacilityRateConstant.Title),
                CustomerType = DynamicContentUtility.ToString(FacilityRateItem, FacilityRateConstant.CustomerType),
                ChargesFees = DynamicContentUtility.ToFloat(FacilityRateItem, FacilityRateConstant.ChargesFees),
                ChargesType = DynamicContentUtility.ToString(FacilityRateItem, FacilityRateConstant.ChargesType),
            };

            return facilityRate;
        }
    }
}