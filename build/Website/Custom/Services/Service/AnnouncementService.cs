﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String AnnouncementContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.Announcement";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _AnnouncementContentType;

        public AnnouncementService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _AnnouncementContentType = TypeResolutionService.ResolveType(AnnouncementContentTypeRef);
        }

        public List<Announcement> GetAllAnnouncement(string IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var announcementItems = _DynamicModuleManager.GetDataItems(_AnnouncementContentType).ToList()
                .FindAll(b => b.Status == ContentLifecycleStatus.Live && b.Visible == true).ToList();

            if (announcementItems != null && announcementItems.Count() > 0)
            {
                List<Announcement> announcementModelList = new List<Announcement>();

                foreach (var announcementItem in announcementItems)
                {
                    announcementModelList.Add(MapAnnouncementContentToModel(announcementItem, isSimpleReturnInBoolean));
                }

                return announcementModelList;
            }

            return new List<Announcement>();
        }

        public Announcement GetAnnouncementbyId(string AnnouncementId, string IsSimpleReturn)
        {
            Boolean isAnnouncementIdValid = Guid.TryParse(AnnouncementId, out Guid announcementIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isAnnouncementIdValid)
            {
                var announcementItem = _DynamicModuleManager.GetDataItems(_AnnouncementContentType)
                    .Where(b => b.Status == ContentLifecycleStatus.Live && b.Visible == true
                        && b.GetValue<Guid>(AnnouncementConstant.AnnouncementId) == announcementIdInGuid || b.Id == announcementIdInGuid).FirstOrDefault();

                if (announcementItem != null)
                {
                    return MapAnnouncementContentToModel(announcementItem, isSimpleReturnInBoolean);
                }
            }

            return new Announcement();
        }

        private Announcement MapAnnouncementContentToModel(DynamicContent AnnouncementItem, Boolean IsSimpleReturn)
        {
            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);

            Announcement announcement = null;

            if (IsSimpleReturn)
            {
                announcement = new Announcement()
                {
                    Announcement_ID = DynamicContentUtility.ToGuid(AnnouncementItem, AnnouncementConstant.AnnouncementId),
                    Name = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.Name),
                };
            }
            else
            {
                announcement = new Announcement()
                {
                    Title = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.Title),
                    Announcement_ID = DynamicContentUtility.ToGuid(AnnouncementItem, AnnouncementConstant.AnnouncementId),
                    Name = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.Name),
                    Description = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.Description),//.Replace(LocalhostUrl, WebServerUrl)
                    AnnouncementDate = DynamicContentUtility.ToDateTime(AnnouncementItem, AnnouncementConstant.AnnouncementDate),
                    Url = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.Url),
                    ImageUrl = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.ImageUrl),
                    UrlName = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.UrlName),
                    PostalCode = DynamicContentUtility.ToString(AnnouncementItem, AnnouncementConstant.PostalCode),
                    StartDate = DynamicContentUtility.ToDateTime(AnnouncementItem, AnnouncementConstant.StartDate),
                    EndDate = DynamicContentUtility.ToDateTime(AnnouncementItem, AnnouncementConstant.EndDate),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(AnnouncementItem, AnnouncementConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl)
                    Locations = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.Location, locations),
                    Categories = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.DSSCategory, categories),
                    Department = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.DSSSector, sectors),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.DSSLifestyle, lifestyles),
                    Fabs = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(AnnouncementItem, AnnouncementConstant.DSSClubCategory, clubCategories)
                };
            }

            return announcement;
        }
    }
}