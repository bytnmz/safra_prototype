﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface ISafraClubhouseService
    {
        List<SafraClubhouse> GetAllSafraClubhouse(String IsSimpleReturn);
        
        SafraClubhouse GetSafraClubhousebyId(String SafraClubhouseId, String IsSimpleReturn);
        
        List<CarparkRate> GetCarParkRatebyClubId(String SafraClubhouseId);
    }
}
