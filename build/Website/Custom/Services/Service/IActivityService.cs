﻿
using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IActivityService
    {
        List<Activity> GetAllActivities(String IsSimpleReturn);

        Activity GetActivity(String ActivityId, String IsSimpleReturn);

        List<ActivityAdminCharge> GetActivityAdminCharge(String ActivityId);

        List<ActivityRate> GetActivityRate(String ActivityId);
    }
}
