﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IBusinessUnitService
    {
        List<BusinessUnit> GetAllBusinessUnits(String IsSimpleReturn);

        BusinessUnit GetBusinessUnitbyId(String BusinessUnitsId, String IsSimpleReturn);
    }
}
