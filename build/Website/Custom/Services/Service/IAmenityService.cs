﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IAmenityService
    {
        List<Amenity> GetAllAmenity(String IsSimpleReturn);

        Amenity GetAmenitybyId(String AmenityId, String IsSimpleReturn);
        
        List<AmenityRate> GetAmenityRate(String AmenityId);
    }
}
