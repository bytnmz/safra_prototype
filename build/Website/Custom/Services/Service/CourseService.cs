﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class CourseService : ICourseService
    {   
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String CourseContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.Course";
        private readonly String CourseRateContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.CourseRate";
        private readonly String CourseScheduleContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.CourseSchedule";


        private readonly Telerik.Sitefinity.DynamicModules.DynamicModuleManager _DynamicModuleManager;
        private readonly Type _CourseContentType;
        private readonly Type _CourseRateContentType;
        private readonly Type _CourseScheduleContentType;

        public CourseService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _CourseContentType = TypeResolutionService.ResolveType(CourseContentTypeRef);
            _CourseRateContentType = TypeResolutionService.ResolveType(CourseRateContentTypeRef);
            _CourseScheduleContentType = TypeResolutionService.ResolveType(CourseScheduleContentTypeRef);
        }

        public List<Course> GetAllCourse(string IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);
       
            var coursesItems = _DynamicModuleManager.GetDataItems(_CourseContentType)
                .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true).ToList();
            
            if (coursesItems != null && coursesItems.Count() > 0)
            {
                List<Course> courseModelList = new List<Course>();

                foreach (var coursesItem in coursesItems)
                {
                    courseModelList.Add(MapCourseContentToModel(coursesItem, isSimpleReturnInBoolean));
                }

                return courseModelList;
            }

            return new List<Course>();
        }

        public Course GetCourse(string CourseId, string IsSimpleReturn)
        {
            Boolean isCourseIdValid = Guid.TryParse(CourseId, out Guid courseIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isCourseIdValid)
            {
                var courseItem = _DynamicModuleManager.GetDataItems(_CourseContentType)
                    .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true 
                    && i.GetValue<Guid>(CourseConstant.CourseId) == courseIdInGuid || i.Id == courseIdInGuid).FirstOrDefault();

                if (courseItem != null)
                {
                    return MapCourseContentToModel(courseItem, isSimpleReturnInBoolean);
                }
            }

            return new Course();
        }

        public List<CourseRate> GetCourseRate(string CourseId)
        {
            Boolean isCourseIdValid = Guid.TryParse(CourseId, out Guid courseIdInGuid);

            if (isCourseIdValid)
            {
                var courseItem = _DynamicModuleManager.GetDataItems(_CourseContentType)
                    .FirstOrDefault(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true 
                    && i.GetValue<Guid>(CourseConstant.CourseId) == courseIdInGuid || i.Id == courseIdInGuid);
                
                if (courseItem != null)
                {
                    var courseRateItems = courseItem.GetChildItems(_CourseRateContentType).ToList();

                    if (courseRateItems != null && courseRateItems.Count() > 0)
                    {
                        List<CourseRate> courseRateModelList = new List<CourseRate>();

                        foreach (var courseRateItem in courseRateItems)
                        {
                            courseRateModelList.Add(MapCourseRateContentToModel(courseRateItem));
                        }

                        return courseRateModelList;
                    }
                }
            }
            
            return new List<CourseRate>();
        }

        public List<CourseSchedule> GetCourseSchedule(string CourseId)
        {
            Boolean isCourseIdValid = Guid.TryParse(CourseId, out Guid courseIdInGuid);

            if (isCourseIdValid)
            {
                var courseItem = _DynamicModuleManager.GetDataItems(_CourseContentType)
                    .FirstOrDefault(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                    && i.GetValue<Guid>(CourseConstant.CourseId) == courseIdInGuid || i.Id == courseIdInGuid);

                if (courseItem != null)
                {
                    var courseScheduleItems = courseItem.GetChildItems(_CourseScheduleContentType).ToList();

                    if (courseScheduleItems != null && courseScheduleItems.Count() > 0)
                    {
                        List<CourseSchedule> courseScheduleModelList = new List<CourseSchedule>();

                        foreach (var courseScheduleItem in courseScheduleItems)
                        {
                            courseScheduleModelList.Add(MapCourseScheduleContentToModel(courseScheduleItem));
                        }

                        return courseScheduleModelList;
                    }
                }
            }

            return new List<CourseSchedule>();
        }

        private Course MapCourseContentToModel(DynamicContent CourseItem, Boolean IsSimpleReturn)
        {
            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> applicablecustomertypes = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSApplicableCustomerTypes);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);

            Course course = null;

            if (IsSimpleReturn)
            {
                course = new Course()
                {
                    Course_ID = DynamicContentUtility.ToGuid(CourseItem, CourseConstant.CourseId),
                    Name = DynamicContentUtility.ToString(CourseItem, CourseConstant.Name),
                };
            }
            else
            {
                course = new Course()
                {
                    Course_ID = DynamicContentUtility.ToGuid(CourseItem, CourseConstant.CourseId),
                    Title = DynamicContentUtility.ToString(CourseItem, CourseConstant.Title),
                    Name = DynamicContentUtility.ToString(CourseItem, CourseConstant.Name),
                    Type = DynamicContentUtility.ToString(CourseItem, CourseConstant.Type),
                    ApplicableAgeFrom = DynamicContentUtility.ToInteger(CourseItem, CourseConstant.ApplicableAgeFrom),
                    ApplicableAgeTo = DynamicContentUtility.ToInteger(CourseItem, CourseConstant.ApplicableAgeTo),
                    NoOfLesson = DynamicContentUtility.ToInteger(CourseItem, CourseConstant.NoOfLesson),
                    CourseOrganizer = DynamicContentUtility.ToString(CourseItem, CourseConstant.CourseOrganizer),
                    PublishDate = DynamicContentUtility.ToDateTime(CourseItem, CourseConstant.PublishDate),
                    StartDate = DynamicContentUtility.ToDateTime(CourseItem, CourseConstant.StartDate),
                    EndDate = DynamicContentUtility.ToDateTime(CourseItem, CourseConstant.EndDate),
                    CourseTerm = DynamicContentUtility.ToString(CourseItem, CourseConstant.CourseTerm),
                    RegStartDate = DynamicContentUtility.ToDateTime(CourseItem, CourseConstant.RegStartDate),
                    RegEndDate = DynamicContentUtility.ToDateTime(CourseItem, CourseConstant.RegEndDate),
                    MaxCapacity = DynamicContentUtility.ToInteger(CourseItem, CourseConstant.MaxCapacity),
                    InstructorName = DynamicContentUtility.ToString(CourseItem, CourseConstant.InstructorName),
                    Description = DynamicContentUtility.ToString(CourseItem, CourseConstant.Description),//.Replace(LocalhostUrl, WebServerUrl)
                    SpecialRemarks = DynamicContentUtility.ToString(CourseItem, CourseConstant.SpecialRemarks),//.Replace(LocalhostUrl, WebServerUrl)
                    ContactPersonInfo = DynamicContentUtility.ToString(CourseItem, CourseConstant.ContactPersonInfo),
                    IndemnityWaiver = DynamicContentUtility.ToString(CourseItem, CourseConstant.IndemnityWaiver),
                    TermsAndConditions = DynamicContentUtility.ToString(CourseItem, CourseConstant.TermsAndConditions),
                    Url = DynamicContentUtility.ToString(CourseItem, CourseConstant.Url),
                    BannerUrl = DynamicContentUtility.ToString(CourseItem, CourseConstant.BannerUrl),
                    ImageUrl = DynamicContentUtility.ToString(CourseItem, CourseConstant.ImageUrl),
                    UrlName = DynamicContentUtility.ToString(CourseItem, CourseConstant.UrlName),
                    PostalCode = DynamicContentUtility.ToString(CourseItem, CourseConstant.PostalCode),
                    RegistrationUrl = DynamicContentUtility.ToString(CourseItem, CourseConstant.RegistrationUrl),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(CourseItem, CourseConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl)
                    Locations = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.Location, locations),
                    Categories = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSCategory, categories),
                    Department = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSDepartment, departments),
                    Sectors = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSSector, sectors),
                    Clubs = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSClub, clubs),
                    ApplicableCustomerTypes = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSApplicableCustomerTypes, applicablecustomertypes),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSLifestyle, lifestyles),
                    Fabs = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(CourseItem, CourseConstant.DSSClubCategory, fabs),
                };
            }

            return course;
        }

        private CourseRate MapCourseRateContentToModel(DynamicContent CourseRateItem)
        {
            CourseRate courseRate = new CourseRate()
            {
                Title = DynamicContentUtility.ToString(CourseRateItem, CourseRateConstant.Title),
                CustomerType = DynamicContentUtility.ToString(CourseRateItem, CourseRateConstant.CustomerType),
                ChargesFees = DynamicContentUtility.ToFloat(CourseRateItem, CourseRateConstant.ChargesFees),
                ChargesType = DynamicContentUtility.ToString(CourseRateItem, CourseRateConstant.ChargesType),
            };
            
            return courseRate;
        }

        private CourseSchedule MapCourseScheduleContentToModel(DynamicContent CourseScheduleItem)
        {
            List<CourseSchedule> courseScheduleModelList = new List<CourseSchedule>();
            List<Taxon> applicableGenders = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSApplicableGenders);

            CourseSchedule courseSchedule = new CourseSchedule()
            {
                Title = DynamicContentUtility.ToString(CourseScheduleItem, CourseScheduleConstant.Title),
                Schedule_ID = DynamicContentUtility.ToGuid(CourseScheduleItem, CourseScheduleConstant.ScheduleId),
                ClassName = DynamicContentUtility.ToString(CourseScheduleItem, CourseScheduleConstant.ClassName),
                RecurringType = DynamicContentUtility.ToString(CourseScheduleItem, CourseScheduleConstant.RecurringType),
                StartDate = DynamicContentUtility.ToDateTime(CourseScheduleItem, CourseScheduleConstant.StartDate),
                Time = DynamicContentUtility.ToString(CourseScheduleItem, CourseScheduleConstant.Time),
                Recurrence = DynamicContentUtility.ToInteger(CourseScheduleItem, CourseScheduleConstant.Recurrence),
                WeeklyDay = DynamicContentUtility.ToString(CourseScheduleItem, CourseScheduleConstant.WeeklyDay),
                MonthlyRecurrence = DynamicContentUtility.ToString(CourseScheduleItem, CourseScheduleConstant.Time),
                MonthlyMonth = DynamicContentUtility.ToInteger(CourseScheduleItem, CourseScheduleConstant.MonthlyMonth),
                ApplicableGenders = ClassificationUtility.GetClassificationInfo(CourseScheduleItem, CourseScheduleConstant.DSSApplicableGenders, applicableGenders),
            };
            
            return courseSchedule;
        }
    }
}