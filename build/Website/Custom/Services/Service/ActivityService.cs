﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class ActivityService : IActivityService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String ActivityContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.Activity";
        private readonly String ActivityCategoryContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.ActivityCategory";
        private readonly String ActivityRateContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.ActivityRate";
        private readonly String ActivityAdminChargeContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.ActivityAdminCharge";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _ActivityContentType;
        private readonly Type _ActivityCategoryContentType;
        private readonly Type _ActivityRateContentType;
        private readonly Type _ActivityAdminChargeContentType;

        public ActivityService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _ActivityContentType = TypeResolutionService.ResolveType(ActivityContentTypeRef);
            _ActivityCategoryContentType = TypeResolutionService.ResolveType(ActivityCategoryContentTypeRef);
            _ActivityRateContentType = TypeResolutionService.ResolveType(ActivityRateContentTypeRef);
            _ActivityAdminChargeContentType = TypeResolutionService.ResolveType(ActivityAdminChargeContentTypeRef);
        }
        
        public List<Activity> GetAllActivities(String IsSimpleReturn)
        {
            Log.Write("Start of GetAllActivities()");
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean IsSimpleReturnInBoolean);
            Log.Write("isSimpleReturnValid?: "+ isSimpleReturnValid);

            var activityItems = _DynamicModuleManager.GetDataItems(_ActivityContentType).ToList()
                .FindAll(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true).ToList();
            Log.Write("activityItems.count(): " + activityItems.Count());

            if (activityItems != null && activityItems.Count() > 0)
            {
                List<Activity> activityModelList = new List<Activity>();

                foreach (var activityItem in activityItems)
                {
                    activityModelList.Add(MapActivityContentToModel(activityItem, IsSimpleReturnInBoolean));
                }

                return activityModelList;
            }

            Log.Write("End of GetAllActivities()");
            return new List<Activity>();
        }
        public Activity GetActivity(String ActivityId, String IsSimpleReturn)
        {
            Log.Write("Start of GetActivity()");
            Boolean isActivityIdValid = Guid.TryParse(ActivityId, out Guid activityIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);
            Log.Write("isActivityIdValid?: " + isActivityIdValid + " - " + activityIdInGuid);
            Log.Write("isSimpleReturnValid?: " + isSimpleReturnValid);

            if (isActivityIdValid)
            {
                var activityItem = _DynamicModuleManager.GetDataItems(_ActivityContentType)
                    .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                        && i.GetValue<Guid>(ActivityConstant.ActivityId) == activityIdInGuid || i.Id == activityIdInGuid).FirstOrDefault();

                if (activityItem != null)
                {
                    Log.Write("End of GetActivity()");
                    return MapActivityContentToModel(activityItem, isSimpleReturnInBoolean);
                }
            }

            Log.Write("End of GetActivity()");
            return new Activity();
        }

        public List<ActivityAdminCharge> GetActivityAdminCharge(String ActivityId)
        {
            Log.Write("Start of GetActivityAdminCharge()");
            Boolean isActivityIdValid = Guid.TryParse(ActivityId, out Guid activityIdInGuid);
            Log.Write("isActivityIdValid?: " + isActivityIdValid + " - " + activityIdInGuid);

            if (isActivityIdValid)
            {
                DynamicContent activityItem = _DynamicModuleManager.GetDataItems(_ActivityContentType)
                .FirstOrDefault(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                && i.GetValue<Guid>(ActivityConstant.ActivityId) == new Guid(ActivityId) || i.Id == new Guid(ActivityId));
                
                if (activityItem != null)
                {
                    var activityAdminChargeItems = activityItem.GetChildItems(_ActivityAdminChargeContentType).ToList();

                    if (activityAdminChargeItems != null && activityAdminChargeItems.Count() > 0)
                    {
                        List<ActivityAdminCharge> activityAdminChargeList = new List<ActivityAdminCharge>();

                        foreach (var activityAdminChargeItem in activityAdminChargeItems)
                        {
                            activityAdminChargeList.Add(MapActivityAdminChargeContentToModel(activityAdminChargeItem));
                        }

                        Log.Write("End of GetActivityAdminCharge()");
                        return activityAdminChargeList;
                    }
                }
            }

            Log.Write("End of GetActivityAdminCharge()");
            return new List<ActivityAdminCharge>();
        }

        public List<ActivityRate> GetActivityRate(String ActivityId)
        {
            Boolean isActivityIdGuid = Guid.TryParse(ActivityId, out Guid activityIdInGuid);

            if (isActivityIdGuid)
            {
                DynamicContent activityItem = _DynamicModuleManager.GetDataItems(_ActivityContentType)
                .FirstOrDefault(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                && i.GetValue<Guid>(ActivityConstant.ActivityId) == new Guid(ActivityId) || i.Id == new Guid(ActivityId));

                if (activityItem != null)
                {
                    var activityCategoryItems = activityItem.GetChildItems(_ActivityCategoryContentType).ToList();

                    if (activityCategoryItems != null && activityCategoryItems.Count() > 0)
                    {
                        foreach (var activityCategoryItem in activityCategoryItems)
                        {
                            var activityRateItems = activityCategoryItem.GetChildItems(_ActivityRateContentType);

                            if (activityRateItems != null && activityRateItems.Count() > 0)
                            {
                                List<ActivityRate> activiytRateModelList = new List<ActivityRate>();

                                foreach (var activityRateItem in activityRateItems)
                                {
                                    activiytRateModelList.Add(MapActivityRateContentToModel(activityRateItem));
                                }

                                return activiytRateModelList;
                            }
                        }
                    }
                }
            }

            return new List<ActivityRate>();
        }

        private Activity MapActivityContentToModel(DynamicContent ActivityItem, Boolean IsSimpleRequest)
        {
            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> applicablecustomertypes = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSApplicableCustomerTypes);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);

            Activity activity = null;

            if (IsSimpleRequest)
            {
                activity = new Activity()
                {
                    Activity_ID = DynamicContentUtility.ToGuid(ActivityItem, ActivityConstant.ActivityId),
                    Name = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.Name),
                };
            }
            else
            {
                activity = new Activity()
                {
                    Activity_ID = DynamicContentUtility.ToGuid(ActivityItem, ActivityConstant.ActivityId),
                    Title = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.Title),
                    Name = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.Name),
                    Code = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.Code),
                    Type = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.Type),
                    PublishDate = DynamicContentUtility.ToDateTime(ActivityItem, ActivityConstant.PublishDate),
                    StartDate = DynamicContentUtility.ToDateTime(ActivityItem, ActivityConstant.StartDate),
                    EndDate = DynamicContentUtility.ToDateTime(ActivityItem, ActivityConstant.EndDate),
                    RegStartDate = DynamicContentUtility.ToDateTime(ActivityItem, ActivityConstant.RegStartDate),
                    RegEndDate = DynamicContentUtility.ToDateTime(ActivityItem, ActivityConstant.RegEndDate),
                    MaxCapacity = DynamicContentUtility.ToInteger(ActivityItem, ActivityConstant.MaxCapacity),
                    Description = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.Description),//.Replace(LocalhostUrl, WebServerUrl)
                    SpecialRemarks = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.SpecialRemarks),//.Replace(LocalhostUrl, WebServerUrl)
                    ContactPersonInfo = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.ContactPersonInfo),
                    IndemnityWaiver = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.IndemnityWaiver),
                    TermsAndConditions = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.TermsAndConditions),
                    Url = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.Url),
                    BannerUrl = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.BannerUrl),
                    ImageUrl = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.ImageUrl),
                    UrlName = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.UrlName),
                    PostalCode = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.PostalCode),
                    RegistrationUrl = DynamicContentUtility.ToString(ActivityItem, ActivityConstant.RegistrationUrl),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(ActivityItem, ActivityConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl)
                    Locations = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.Location, locations),
                    Categories = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.DSSCategory, categories),
                    Department = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.DSSSector, sectors),
                    ApplicableCustomerTypes = ClassificationUtility.GetClassificationInfo(ActivityItem,
                        ActivityConstant.DSSApplicableCustomerTypes, applicablecustomertypes),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.DSSLifestyle, lifestyles),
                    Fabs = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(ActivityItem, ActivityConstant.DSSClubCategory, clubCategories)
                };
            }

            return activity;
        }

        private ActivityAdminCharge MapActivityAdminChargeContentToModel(DynamicContent ActivityAdminChargeItem)
        {
            ActivityAdminCharge activityAdminCharge = new ActivityAdminCharge()
            {
                Title = DynamicContentUtility.ToString(ActivityAdminChargeItem, ActivityAdminChargeConstant.Title),
                CustomerType = DynamicContentUtility.ToString(ActivityAdminChargeItem, ActivityAdminChargeConstant.CustomerType),
                ChargesFees = DynamicContentUtility.ToFloat(ActivityAdminChargeItem, ActivityAdminChargeConstant.ChargesFees),
                ChargesType = DynamicContentUtility.ToString(ActivityAdminChargeItem, ActivityAdminChargeConstant.ChargesType),
            };

            return activityAdminCharge;
        }

        private ActivityRate MapActivityRateContentToModel(DynamicContent ActivityRateContentItem)
        {
            ActivityRate activityRate = new ActivityRate()
            {
                Id = ActivityRateContentItem.Id,
                Title = DynamicContentUtility.ToString(ActivityRateContentItem, ActivityRateConstant.Title),
                CustomerType = DynamicContentUtility.ToString(ActivityRateContentItem, ActivityRateConstant.CustomerType),
                ChargesFees = DynamicContentUtility.ToFloat(ActivityRateContentItem, ActivityRateConstant.ChargesFees),
                ChargesType = DynamicContentUtility.ToString(ActivityRateContentItem, ActivityRateConstant.ChargesType),
            };

            return activityRate;
        }
    }
}