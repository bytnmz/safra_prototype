﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Map;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.Data.ContentLinks;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class DiscountGuideService : IDiscountGuideService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String DiscountGuideContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.Promotions.Promotion";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _DiscountGuideContentType;

        public DiscountGuideService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager();
            _DiscountGuideContentType = TypeResolutionService.ResolveType(DiscountGuideContentTypeRef);
        }

        public List<DiscountGuide> GetAllDiscountGuides(string IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var discountGuidesItems = _DynamicModuleManager.GetDataItems(_DiscountGuideContentType).ToList()
                .FindAll(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                    && (i.GetValue(DiscountGuideConstant.PublishStartDate) != null ? i.GetValue<DateTime>(DiscountGuideConstant.PublishStartDate) <= DateTime.UtcNow : false)
                    && (i.GetValue(DiscountGuideConstant.PublishEndDate) != null ? i.GetValue<DateTime>(DiscountGuideConstant.PublishEndDate) >= DateTime.UtcNow : false)).ToList();

            if (discountGuidesItems != null && discountGuidesItems.Count() > 0)
            {
                List<DiscountGuide> discountGuideModelList = new List<DiscountGuide>();

                foreach (var discountGuidesItem in discountGuidesItems)
                {
                    discountGuideModelList.Add(MapDiscountGuidesContentToModel(discountGuidesItem, isSimpleReturnInBoolean, 
                        _DynamicModuleManager, _DiscountGuideContentType));
                }

                return discountGuideModelList;
            }

            return new List<DiscountGuide>();
        }

        public DiscountGuide GetDiscountGuidebyId(string DiscountGuideId, string IsSimpleReturn)
        {
            Boolean isDiscountGuideIdValid = Guid.TryParse(DiscountGuideId, out Guid discountGuideIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isDiscountGuideIdValid)
            {
                var discountGuideItem = _DynamicModuleManager.GetDataItems(_DiscountGuideContentType)
                    .Where(b => b.Status == ContentLifecycleStatus.Live && b.Visible == true
                        && b.GetValue<Guid>(DiscountGuideConstant.DiscountGuideId) == discountGuideIdInGuid || b.Id == discountGuideIdInGuid).FirstOrDefault();

                if (discountGuideItem != null)
                {
                    return MapDiscountGuidesContentToModel(discountGuideItem, isSimpleReturnInBoolean, _DynamicModuleManager, _DiscountGuideContentType);
                }
            }

            return new DiscountGuide();
        }

        private DiscountGuide MapDiscountGuidesContentToModel(DynamicContent DiscountGuideItem, Boolean IsSimpleReturn,
            DynamicModuleManager dynamicModuleManager, Type DiscountGuideType)
        {
            IList<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems2(ClassificationConstant.Location);
            IList<Taxon> accord = ClassificationUtility.GetHierarchicalClassificationItems2(ClassificationConstant.Accord);
            IList<Taxon> categories = ClassificationUtility.GetFlatClassificationItems2(ClassificationConstant.DSSCategory);
            IList<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems2(ClassificationConstant.DSSLifestyle);
            IList<Taxon> departments = ClassificationUtility.GetFlatClassificationItems2(ClassificationConstant.DSSDepartment);
            IList<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems2(ClassificationConstant.DSSClub);
            IList<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems2(ClassificationConstant.DSSSector);
            IList<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems2(ClassificationConstant.DSSFab);
            IList<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems2(ClassificationConstant.DSSClubCategory);

            DiscountGuide discountGuide = null;

            if (IsSimpleReturn)
            {
                discountGuide = new DiscountGuide()
                {
                    Discount_ID = DynamicContentUtility.ToGuid(DiscountGuideItem, DiscountGuideConstant.DiscountGuideId),
                    Name = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.Name),
                };
            }
            else
            {
                discountGuide = new DiscountGuide()
                {
                    Title = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.Title),
                    Discount_ID = DynamicContentUtility.ToGuid(DiscountGuideItem, DiscountGuideConstant.DiscountGuideId),
                    Name = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.Name),
                    Description = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.Description),//.Replace(LocalhostUrl, WebServerUrl),
                    SpecialRemarks = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.SpecialRemarks),//.Replace(LocalhostUrl, WebServerUrl),
                    MerchantName = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.MerchantName),
                    MerchantAddress = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.MerchantAddress),
                    PostalCode = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.PostalCode),
                    PromotionDetails = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.PromotionDetails),//.Replace(LocalhostUrl, WebServerUrl),
                    ContactInfo = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.ContactInfo),
                    TermsAndConditions = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.TermsAndConditions),
                    Url = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.Url),
                    ImageUrl = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.ImageUrl),
                    UrlName = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.UrlName),
                    StartDate = DynamicContentUtility.ToDateTime(DiscountGuideItem, DiscountGuideConstant.StartDate),
                    EndDate = DynamicContentUtility.ToDateTime(DiscountGuideItem, DiscountGuideConstant.EndDate),
                    Latitude = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.Latitude),
                    Longitude = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.Longitude),
                    OverviewDescription = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.OverviewDescription),
                    Accord_indicator = DynamicContentUtility.ToBoolean(DiscountGuideItem, DiscountGuideConstant.AccordIndicator),
                    Accord_description = DynamicContentUtility.ToString(DiscountGuideItem, DiscountGuideConstant.AccordDescription),//.Replace(LocalhostUrl, WebServerUrl),
                    PublishStartDate = DynamicContentUtility.ToDateTime(DiscountGuideItem, DiscountGuideConstant.PublishStartDate),
                    PublishEndDate = DynamicContentUtility.ToDateTime(DiscountGuideItem, DiscountGuideConstant.PublishEndDate),
                    ImageData1 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData1, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData2 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData2, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData3 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData3, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData4 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData4, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData5 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData5, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData6 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData6, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData7 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData7, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData8 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData8, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData9 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData9, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData10 = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ImageData10, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    ThumbnailImageData = DynamicContentUtility.GetContentLinkImageDataUrl(DiscountGuideItem, DiscountGuideConstant.ThumbnailImageData, dynamicModuleManager),//.Replace(LocalhostUrl, WebServerUrl),
                    Locations = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.Location, locations),
                    Accord = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.Accord, accord),
                    Categories = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.DSSCategory, categories),
                    Lifestyles = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.DSSLifestyle, lifestyles),
                    Department = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.DSSSector, sectors),
                    Fabs = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo2(DiscountGuideItem, DiscountGuideConstant.DSSClubCategory, clubCategories),
                    PromoPublicationDate = DynamicContentUtility.ToDateTime(DiscountGuideItem, DiscountGuideConstant.PromoPublicationDate),
                };

                var parentMasterContent = dynamicModuleManager.Lifecycle.GetMaster(DiscountGuideItem);
                var selectedLinks = ContentLinksManager.GetManager().GetContentLinks(
                    parentMasterContent.Id, DiscountGuideType, DiscountGuideConstant.OutletRelatedData)
                    .Where(x => !x.IsChildDeleted && !x.IsParentDeleted && x.AvailableForLive);

                if (selectedLinks != null && selectedLinks.Any())
                {
                    var firstOne = selectedLinks.FirstOrDefault();
                    Type childType = TypeResolutionService.ResolveType(firstOne.ChildItemType);

                    foreach (var contentLink in selectedLinks)
                    {
                        var merchantOutlets = dynamicModuleManager.GetDataItems(childType)
                             .Where(x => x.Status == ContentLifecycleStatus.Master && contentLink.ChildItemId == x.Id);

                        // var merchantOutlets = discountguideContent.GetRelatedItems("OutletsRelatedData");
                        foreach (IDataItem merchantOutlet in merchantOutlets)
                        {
                            DynamicContent merchantOutletAsDynContent = merchantOutlet as DynamicContent;

                            discountGuide.MerchantOutlets.Add(MerchantOutletMap.MapMerchantOutletContentToModel2(merchantOutletAsDynContent,
                                    locations, categories, lifestyles, departments, clubs, sectors, clubCategories));
                        }
                    }
                }
            }

            return discountGuide;
        }
    }
}