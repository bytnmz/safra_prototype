﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class MerchantService : IMerchantService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String MerchantContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.Merchants.Merchant";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _MerchantContentType;
        
        public MerchantService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager();
            _MerchantContentType = TypeResolutionService.ResolveType(MerchantContentTypeRef);
        }

        public List<Merchants> GetAllMerchants(string IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var merchantItems = _DynamicModuleManager.GetDataItems(_MerchantContentType).ToList()
                .FindAll(b => b.Status == ContentLifecycleStatus.Live && b.Visible == true).ToList();

            if (merchantItems != null && merchantItems.Count() > 0)
            {
                List<Merchants> merchantModelList = new List<Merchants>();

                foreach (var merchantItem in merchantItems)
                {
                    merchantModelList.Add(MapMerchantContentToModel(merchantItem, isSimpleReturnInBoolean));
                }

                return merchantModelList;
            }

            return new List<Merchants>();
        }

        public Merchants GetMerchantByID(string MerchantId, string IsSimpleReturn)
        {
            Boolean isMerchantIdValid = Guid.TryParse(MerchantId, out Guid merchantIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isMerchantIdValid)
            {
                var merchantItem = _DynamicModuleManager.GetDataItems(_MerchantContentType)
                    .Where(b => b.Status == ContentLifecycleStatus.Live && b.Visible == true 
                        && b.GetValue<Guid>(MerchantConstant.MerchantId) == merchantIdInGuid || b.Id == merchantIdInGuid).FirstOrDefault();

                if (merchantItem != null)
                {
                    return MapMerchantContentToModel(merchantItem, isSimpleReturnInBoolean);
                }
            }

            return new Merchants();
        }

        private Merchants MapMerchantContentToModel(DynamicContent MerchantItem, Boolean IsSimpleReturn)
        {
            Merchants merchant = null;

            if (IsSimpleReturn)
            {
                merchant = new Merchants()
                {
                    Merchant_ID = DynamicContentUtility.ToGuid(MerchantItem, MerchantConstant.MerchantId),
                    Name = DynamicContentUtility.ToString(MerchantItem, MerchantConstant.Name),
                };
            }
            else
            {
                merchant = new Merchants()
                {
                    Merchant_ID = DynamicContentUtility.ToGuid(MerchantItem, MerchantConstant.MerchantId),
                    Name = DynamicContentUtility.ToString(MerchantItem, MerchantConstant.Name),
                    UrlName = DynamicContentUtility.ToString(MerchantItem, MerchantConstant.UrlName),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(MerchantItem, MerchantConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                };
            }

            return merchant;
        }
    }
}