﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class SafraClubhouseService : ISafraClubhouseService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String SafraClubhouseContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.SafraClubhouse";
        private readonly String CarparkRateContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.CarparkRate";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _SafraClubhouseContentType;
        private readonly Type _CarparkRateContentType;

        public SafraClubhouseService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _SafraClubhouseContentType = TypeResolutionService.ResolveType(SafraClubhouseContentTypeRef);
            _CarparkRateContentType = TypeResolutionService.ResolveType(CarparkRateContentTypeRef);
        }

        public List<SafraClubhouse> GetAllSafraClubhouse(string IsSimpleReturn)
        {
            Log.Write("Start of of GetAllSafraClubhouse()");
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var safraClubhouseItems = _DynamicModuleManager.GetDataItems(_SafraClubhouseContentType).ToList()
                .FindAll(b => b.Status == ContentLifecycleStatus.Live && b.Visible == true).ToList();

            Log.Write("safraClubhouseItems Count: " + safraClubhouseItems.Count());

            if (safraClubhouseItems != null && safraClubhouseItems.Count() > 0)
            {
                List<SafraClubhouse> safraClubhouseModelList = new List<SafraClubhouse>();

                foreach (var safraClubhouseItem in safraClubhouseItems)
                {
                    safraClubhouseModelList.Add(MapSafraClubhouseContentToModel(safraClubhouseItem, isSimpleReturnInBoolean));
                }

                Log.Write("End of GetAllSafraClubhouse()");
                return safraClubhouseModelList;
            }

            Log.Write("End of GetAllSafraClubhouse()");
            return new List<SafraClubhouse>();
        }

        public List<CarparkRate> GetCarParkRatebyClubId(string SafraClubhouseId)
        {
            Boolean isSafraClubhouseIdValid = Guid.TryParse(SafraClubhouseId, out Guid safraClubhouseIdInGuid);

            if (isSafraClubhouseIdValid)
            {
                var safraClubhouseItem = _DynamicModuleManager.GetDataItems(_SafraClubhouseContentType)
                    .FirstOrDefault(x => x.GetValue<Guid>(SafraClubhouseConstant.SafraClubhouseId) == safraClubhouseIdInGuid || x.Id == safraClubhouseIdInGuid
                        && x.Status == ContentLifecycleStatus.Live && x.Visible == true);

                if (safraClubhouseItem != null)
                {
                    List<CarparkRate> carparkRateModelList = new List<CarparkRate>();
                    var carparkRateItems = safraClubhouseItem.GetChildItems(_CarparkRateContentType);

                    if (carparkRateItems != null && carparkRateItems.Count() > 0) {
                        foreach (var carparkRateItem in carparkRateItems)
                        {
                            var carparkRateItemInDynContent = carparkRateItem as DynamicContent;
                            carparkRateModelList.Add(MapCarparkRateContentToModel(carparkRateItemInDynContent));
                        }
                    }

                    return carparkRateModelList;
                }
            }

            return new List<CarparkRate>();
        }

        public SafraClubhouse GetSafraClubhousebyId(string SafraClubhouseId, string IsSimpleReturn)
        {
            Log.Write("Start of of GetSafraClubhousebyId()");
            Boolean isSafraClubhouseIdValid = Guid.TryParse(SafraClubhouseId, out Guid safraClubhouseIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);
            Log.Write("isSafraClubhouseIdValid?: " + isSafraClubhouseIdValid);
            
            if (isSafraClubhouseIdValid)
            {
                var safraClubhouseItem = _DynamicModuleManager.GetDataItems(_SafraClubhouseContentType)
                    .Where(b => b.Status == ContentLifecycleStatus.Live 
                        && b.Visible == true && b.GetValue<Guid>(SafraClubhouseConstant.SafraClubhouseId) 
                            == safraClubhouseIdInGuid || b.Id == safraClubhouseIdInGuid).FirstOrDefault();

                if (safraClubhouseItem != null)
                {
                    Log.Write("End of GetSafraClubhousebyId()");
                    return MapSafraClubhouseContentToModel(safraClubhouseItem, isSimpleReturnInBoolean);
                }
            }

            Log.Write("End of GetSafraClubhousebyId()");
            return  new SafraClubhouse();
        }

        private SafraClubhouse MapSafraClubhouseContentToModel(DynamicContent safraClubhouseItem, Boolean IsSimpleReturn)
        {
            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);

            SafraClubhouse safraClubhouse = null;

            if (IsSimpleReturn)
            {
                safraClubhouse = new SafraClubhouse()
                {
                    Clubhouse_ID = DynamicContentUtility.ToGuid(safraClubhouseItem, SafraClubhouseConstant.SafraClubhouseId),
                    Name = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Name),
                };
            }
            else
            {
                safraClubhouse = new SafraClubhouse()
                {
                    Title = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Title),
                    Clubhouse_ID = DynamicContentUtility.ToGuid(safraClubhouseItem, SafraClubhouseConstant.SafraClubhouseId),
                    Name = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Name),
                    Description = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Description),//.Replace(LocalhostUrl, WebServerUrl),
                    Announcement = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Announcement),
                    ContactInfo = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.ContactInfo),
                    Url = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Url),
                    ImageUrl = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.ImageUrl),
                    VideoUrl = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.VideoUrl),
                    UrlName = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.UrlName),
                    PostalCode = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.PostalCode),
                    Longitude = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Longitude),
                    Latitude = DynamicContentUtility.ToString(safraClubhouseItem, SafraClubhouseConstant.Latitude),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(safraClubhouseItem, SafraClubhouseConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl)
                    Locations = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.Location, locations),
                    Categories = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.DSSCategory, categories),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.DSSLifestyle, lifestyles),
                    Department = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.DSSSector, sectors),
                    Fabs = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(safraClubhouseItem, SafraClubhouseConstant.DSSClubCategory, clubCategories)
                };
            }
            
            return safraClubhouse;
        }

        private CarparkRate MapCarparkRateContentToModel(DynamicContent carparkRateItem)
        {
            CarparkRate carparkRate = new CarparkRate()
            {
                Title = DynamicContentUtility.ToString(carparkRateItem, CarparkRateConstant.Title),
                VehicleType = DynamicContentUtility.ToString(carparkRateItem, CarparkRateConstant.VehicleType),
                CustomerType = DynamicContentUtility.ToString(carparkRateItem, CarparkRateConstant.CustomerType),
                ChargesFees = DynamicContentUtility.ToInteger(carparkRateItem, CarparkRateConstant.ChargesFees),
                ChargesType = DynamicContentUtility.ToString(carparkRateItem, CarparkRateConstant.ChargesType),
            };

            return carparkRate;
        }
    }
}