﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IAnnouncementService
    {
        List<Announcement> GetAllAnnouncement(String IsSimpleReturn);

        Announcement GetAnnouncementbyId(String AnnouncementId, String IsSimpleReturn);
    }
}
