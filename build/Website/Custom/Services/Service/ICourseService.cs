﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface ICourseService
    {
        List<Course> GetAllCourse(String IsSimpleReturn);

        Course GetCourse(String CourseId, String IsSimpleReturn);

        List<CourseSchedule> GetCourseSchedule(String CourseId);

        List<CourseRate> GetCourseRate(String CourseId);
    }
}
