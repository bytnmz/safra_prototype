﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IDiscountGuideService
    {
        List<DiscountGuide> GetAllDiscountGuides(String IsSimpleReturn);

        DiscountGuide GetDiscountGuidebyId(String DiscountGuideId, String IsSimpleReturn);
    }
}
