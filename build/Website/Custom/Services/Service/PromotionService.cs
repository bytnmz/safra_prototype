﻿using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Model.DynamicContent;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.OpenAccess;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class PromotionService : IPromotionService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String PromotionContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.Promotion";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly TaxonomyManager _TaxonomyManager;
        private readonly Type _PromotionContentType;

        public PromotionService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _TaxonomyManager = TaxonomyManager.GetManager();
            _PromotionContentType = TypeResolutionService.ResolveType(PromotionContentTypeRef);
        }

        public List<Promotions> GetAllPromotions(String IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var promotionItems = _DynamicModuleManager.GetDataItems(_PromotionContentType).ToList()
                .FindAll(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                    && (i.GetValue(PromotionConstant.PublishDate) != null ? 
                        i.GetValue<DateTime>(PromotionConstant.PublishDate) <= DateTime.UtcNow : false) 
                    && (i.GetValue(PromotionConstant.PublishEndDate) != null ? 
                        i.GetValue<DateTime>(PromotionConstant.PublishEndDate) >= DateTime.UtcNow : false)).ToList();

            if (promotionItems != null && promotionItems.Count() > 0)
            {
                List<Promotions> promotionModelList = new List<Promotions>();

                foreach (var promotionItem in promotionItems)
                {
                    promotionModelList.Add(MapPromotionContentToModel(promotionItem, isSimpleReturnInBoolean));
                }

                return promotionModelList;
            }
            
            return new List<Promotions>();
        }
        
        public Promotions GetPromotionsbyId(String PromotionId, String IsSimpleReturn)
        {
            Boolean isPromotionIdValid = Guid.TryParse(PromotionId, out Guid promotionIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isPromotionIdValid)
            {
                var promotionItem = _DynamicModuleManager.GetDataItems(_PromotionContentType)
                    .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                    && i.GetValue<Guid>(PromotionConstant.PromotionId) == promotionIdInGuid || i.Id == promotionIdInGuid).FirstOrDefault();

                if (promotionItem != null)
                {
                    return MapPromotionContentToModel(promotionItem, isSimpleReturnInBoolean);
                }
            }

            return new Promotions();
        }

        public Promotions GetPromotionsbyClubId(String ClubId)
        {
            Taxon taxon = _TaxonomyManager.GetTaxa<FlatTaxon>().Where(t => t.Description == ClubId).FirstOrDefault();

            Guid.TryParse(ClubId, out Guid clubIdInGuid);
            Guid taxonId = (taxon != null) ? taxon.Id : clubIdInGuid;

            var promotionItem = _DynamicModuleManager.GetDataItems(_PromotionContentType).ToList()
                .FindAll(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true
                  && i.GetValue<TrackedList<Guid>>(PromotionConstant.DSSClub).Contains(taxonId)
                  && (i.GetValue(PromotionConstant.PublishDate) != null ? i.GetValue<DateTime>(PromotionConstant.PublishDate) <= DateTime.UtcNow : false)
                  && (i.GetValue(PromotionConstant.PublishEndDate) != null ? i.GetValue<DateTime>(PromotionConstant.PublishEndDate) >= DateTime.UtcNow : false)).FirstOrDefault();

            if (promotionItem != null)
            {
                return MapPromotionContentToModel(promotionItem, false);
            }

            return new Promotions();
        }

        private Promotions MapPromotionContentToModel(DynamicContent PromotionItem, Boolean IsSimpleReturn)
        {   
            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> accord = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Accord);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);

            Promotions promotion = null;

            if (IsSimpleReturn)
            {
                promotion = new Promotions()
                {
                    Promotion_ID = DynamicContentUtility.ToGuid(PromotionItem, PromotionConstant.PromotionId),
                    Name = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Name),
                };
            }
            else
            {
                promotion = new Promotions()
                {
                    Title = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Title),
                    Promotion_ID = DynamicContentUtility.ToGuid(PromotionItem, PromotionConstant.PromotionId),
                    Name = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Name),
                    Type = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Type),
                    PublishDate = DynamicContentUtility.ToDateTime(PromotionItem, PromotionConstant.PublishDate),
                    PublishEndDate = DynamicContentUtility.ToDateTime(PromotionItem, PromotionConstant.PublishEndDate),
                    StartDate = DynamicContentUtility.ToDateTime(PromotionItem, PromotionConstant.StartDate),
                    EndDate = DynamicContentUtility.ToDateTime(PromotionItem, PromotionConstant.RegStartDate),
                    RegEndDate = DynamicContentUtility.ToDateTime(PromotionItem, PromotionConstant.RegEndDate),
                    Description = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Description),//.Replace(LocalhostUrl, WebServerUrl),
                    PromotionDetails = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.PromotionDetails),//.Replace(LocalhostUrl, WebServerUrl),
                    MerchantName = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.MerchantName),
                    MerchantAddress = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.MerchantAddress),
                    PostalCode = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.PostalCode),
                    ContactInfo = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.ContactInfo),
                    SpecialRemarks = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.SpecialRemarks),//.Replace(LocalhostUrl, WebServerUrl),
                    TermsAndConditions = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.TermsAndConditions),
                    Url = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Url),
                    BannerUrl = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.BannerUrl),
                    ImageUrl = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.ImageUrl),
                    UrlName = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.UrlName),
                    OverviewDescription = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.OverviewDescription),
                    Latitude = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Latitude),
                    Longitude = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.Longitude),
                    Accord_indicator = DynamicContentUtility.ToBoolean(PromotionItem, PromotionConstant.AccordIndicator),
                    Accord_description = DynamicContentUtility.ToString(PromotionItem, PromotionConstant.AccordDescription),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl),
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl),
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(PromotionItem, PromotionConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl),
                    Locations = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.Location, locations),
                    Accord = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.Accord, accord),
                    Categories = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.DSSCategory, categories),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.DSSLifestyle, lifestyles),
                    Department = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.DSSSector, sectors),
                    Fabs = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(PromotionItem, PromotionConstant.DSSClubCategory, clubCategories)
                };
            }

            return promotion;
        }
    }
}