﻿
using System;
using System.Collections.Generic;
using System.Linq;
using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Map;
using SAFRA.Custom.Services.Model.DynamicContent;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Custom.Services.Service
{
    public class BusinessUnitService : IBusinessUnitService
    {
        private readonly String ProviderName = "dynamicProvider2";
        private readonly String BusinessUnitContentTypeRef = "Telerik.Sitefinity.DynamicTypes.Model.DssApiEndpoint.BusinessUnit";

        private readonly DynamicModuleManager _DynamicModuleManager;
        private readonly Type _BusinessUnitContentType;

        public BusinessUnitService()
        {
            _DynamicModuleManager = DynamicModuleManager.GetManager(ProviderName);
            _BusinessUnitContentType = TypeResolutionService.ResolveType(BusinessUnitContentTypeRef);
        }

        public List<BusinessUnit> GetAllBusinessUnits(string IsSimpleReturn)
        {
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            var businessUnitItems = _DynamicModuleManager.GetDataItems(_BusinessUnitContentType)
                .Where(i => i.Status == ContentLifecycleStatus.Live && i.Visible == true).ToList();
            
            if (businessUnitItems != null && businessUnitItems.Count() > 0)
            {
                List<BusinessUnit> businessUnitModelList = new List<BusinessUnit>();

                foreach (var businessUnitItem in businessUnitItems)
                {
                    businessUnitModelList.Add(MapBusinessUnitsContentToModel(businessUnitItem, isSimpleReturnInBoolean));
                }

                return businessUnitModelList;
            }

            return new List<BusinessUnit>();
        }

        public BusinessUnit GetBusinessUnitbyId(string BusinessUnitsId, string IsSimpleReturn)
        {
            Boolean isBusinessUnitsIdValid = Guid.TryParse(BusinessUnitsId, out Guid businessUnitsIdInGuid);
            Boolean isSimpleReturnValid = Boolean.TryParse(IsSimpleReturn, out Boolean isSimpleReturnInBoolean);

            if (isBusinessUnitsIdValid)
            {
                var businessUnitItem = _DynamicModuleManager.GetDataItems(_BusinessUnitContentType)
                    .Where(b => b.Status == ContentLifecycleStatus.Live && b.Visible == true 
                        && b.GetValue<Guid>(BusinessUnitConstant.BusinessUnitId) == businessUnitsIdInGuid || b.Id == businessUnitsIdInGuid).FirstOrDefault();

                if (businessUnitItem != null)
                {
                    return MapBusinessUnitsContentToModel(businessUnitItem, isSimpleReturnInBoolean);
                }
            }

            return new BusinessUnit();
        }

        private BusinessUnit MapBusinessUnitsContentToModel(DynamicContent BusinessUnitItem, bool IsSimpleReturn)
        {
            List<Taxon> locations = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.Location);
            List<Taxon> categories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSCategory);
            List<Taxon> lifestyles = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSLifestyle);
            List<Taxon> departments = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSDepartment);
            List<Taxon> clubs = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClub);
            List<Taxon> sectors = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSSector);
            List<Taxon> fabs = ClassificationUtility.GetHierarchicalClassificationItems(ClassificationConstant.DSSFab);
            List<Taxon> clubCategories = ClassificationUtility.GetFlatClassificationItems(ClassificationConstant.DSSClubCategory);

            BusinessUnit businessUnit = null;

            if (IsSimpleReturn)
            {
                businessUnit = new BusinessUnit()
                {
                    BusinessUnit_ID = DynamicContentUtility.ToGuid(BusinessUnitItem, BusinessUnitConstant.BusinessUnitId),
                    Name = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.Name),
                };
            }
            else
            {
                businessUnit = new BusinessUnit()
                {
                    Title = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.Title),
                    BusinessUnit_ID = DynamicContentUtility.ToGuid(BusinessUnitItem, BusinessUnitConstant.BusinessUnitId),
                    Name = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.Name),
                    Description = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.Description),//.Replace(LocalhostUrl, WebServerUrl),
                    Content = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.Content),//.Replace(LocalhostUrl, WebServerUrl),
                    ContactInfo = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.ContactInfo),
                    Url = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.Url),
                    LogoUrl = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.LogoUrl),
                    ImageUrl = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.ImageUrl),
                    UrlName = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.UrlName),
                    PostalCode = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.PostalCode),
                    OverviewDescription = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.OverviewDescription),//.Replace(LocalhostUrl, WebServerUrl),
                    UnitNo = DynamicContentUtility.ToString(BusinessUnitItem, BusinessUnitConstant.UnitNo),
                    ImageData1 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData1),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData2 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData2),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData3 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData3),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData4 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData4),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData5 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData5),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData6 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData6),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData7 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData7),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData8 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData8),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData9 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData9),//.Replace(LocalhostUrl, WebServerUrl)
                    ImageData10 = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ImageData10),//.Replace(LocalhostUrl, WebServerUrl)
                    ThumbnailImageData = DynamicContentUtility.GetImageDataUrl(BusinessUnitItem, BusinessUnitConstant.ThumbnailImageData),//.Replace(LocalhostUrl, WebServerUrl)
                    Locations = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.Location, locations),
                    Categories = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.DSSCategory, categories),
                    Lifestyles = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.DSSLifestyle, lifestyles),
                    Department = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.DSSDepartment, departments),
                    Clubs = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.DSSClub, clubs),
                    Sectors = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.DSSSector, sectors),
                    Fabs = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.DSSFab, fabs),
                    ClubCategories = ClassificationUtility.GetClassificationInfo(BusinessUnitItem, BusinessUnitConstant.DSSClubCategory, clubCategories)
                };

                List<IDataItem> merchantOutletItems = BusinessUnitItem.GetRelatedItems(BusinessUnitConstant.OutletRelatedData).ToList();

                foreach (IDataItem merchantOutletItem in merchantOutletItems)
                {
                    DynamicContent merchantOutletItemAsDynContent = merchantOutletItem as DynamicContent;

                    businessUnit.MerchantOutlets.Add(MerchantOutletMap.MapMerchantOutletContentToModel2(merchantOutletItemAsDynContent,
                                    locations, categories, lifestyles, departments, clubs, sectors, clubCategories));
                }
            }

            return businessUnit;
        }
    }
}