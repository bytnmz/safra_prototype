﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IFacilityService
    {
        List<Facility> GetAllFacilities(String IsSimpleReturn);

        Facility GetFacilitybyId(String FacilityId, String IsSimpleReturn);
        
        List<FacilityRate> GetFacilityRate(String FacilityId);
    }
}
