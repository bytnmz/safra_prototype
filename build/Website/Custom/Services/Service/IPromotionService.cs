﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IPromotionService
    {
        List<Promotions> GetAllPromotions(String IsSimpleReturn);

        Promotions GetPromotionsbyId(String PromotionId, String IsSimpleReturn);

        Promotions GetPromotionsbyClubId(String ClubId);
    }
}
