﻿using SAFRA.Custom.Services.Model.DynamicContent;
using System;
using System.Collections.Generic;

namespace SAFRA.Custom.Services.Service
{
    public interface IMerchantService
    {
        List<Merchants> GetAllMerchants(String IsSimpleReturn);
        
        Merchants GetMerchantByID(String MerchantId, String IsSimpleReturn);
    }
}
