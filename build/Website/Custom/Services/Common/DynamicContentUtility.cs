﻿using System;
using System.ComponentModel;
using System.Linq;
using Telerik.Sitefinity.Data.ContentLinks;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.RelatedData;

namespace SAFRA.Custom.Services.Common
{
    public class DynamicContentUtility
    {
        public static int ToInteger(DynamicContent DynamicContentItem, String ColumnName)
        {
            Object obj = TypeDescriptor.GetProperties(DynamicContentItem)[ColumnName].GetValue(DynamicContentItem);

            if (obj != null)
            {
                var isInteger = int.TryParse(obj.ToString(), out int intValue);
                if (isInteger) return intValue;
            }

            return 0;
        }
        public static float ToFloat(DynamicContent DynamicContentItem, String ColumnName)
        {
            Object obj = TypeDescriptor.GetProperties(DynamicContentItem)[ColumnName].GetValue(DynamicContentItem);

            if (obj != null)
            {
                var isFloat = float.TryParse(obj.ToString(), out float floatValue);
                if (isFloat) return floatValue;
            }

            return 0;
        }

        public static String ToString(DynamicContent DynamicContentItem, String ColumnName)
        {
            Object obj = TypeDescriptor.GetProperties(DynamicContentItem)[ColumnName].GetValue(DynamicContentItem);

            if (obj != null)
                return obj.ToString();

            return String.Empty;
        }

        public static Boolean ToBoolean(DynamicContent DynamicContentItem, String ColumnName)
        {
            Object obj = TypeDescriptor.GetProperties(DynamicContentItem)[ColumnName].GetValue(DynamicContentItem);
            
            if (obj != null)
            {
                var isBoolean = Boolean.TryParse(obj.ToString(), out Boolean booleanValue);
                if (isBoolean) return booleanValue;
            }

            return false;
        }

        public static Guid ToGuid(DynamicContent DynamicContentItem, String ColumnName)
        {
            var obj = TypeDescriptor.GetProperties(DynamicContentItem)[ColumnName].GetValue(DynamicContentItem);

            if (obj != null)
            {
                var isGuid = Guid.TryParse(obj.ToString(), out Guid guidValue);
                if (isGuid && guidValue != Guid.Empty) return guidValue;
            }

            return DynamicContentItem.Id;
        }

        public static DateTime? ToDateTime(DynamicContent DynamicContentItem, String ColumnName)
        {
            Object obj = TypeDescriptor.GetProperties(DynamicContentItem)[ColumnName].GetValue(DynamicContentItem);

            if (obj != null)
                return Convert.ToDateTime(obj).ToLocalTime();

            return (DateTime?) null;
        }

        public static String GetImageDataUrl(DynamicContent DynamicContentItem, String ColumnName)
        {
            String imageUrl = String.Empty;
            Image image = (Image) DynamicContentItem.GetRelatedItems(ColumnName).FirstOrDefault();

            if (image != null)
                imageUrl = image.Url;
            
            return imageUrl;
        }

        public static string GetContentLinkImageDataUrl(DynamicContent content, string contentTypeItem, DynamicModuleManager dynamicModuleManager)
        {
            string imageUrl = string.Empty;
            var master = dynamicModuleManager.Lifecycle.GetMaster(content);

            if (master != null)
            {
                var contentLinks = ContentLinksManager.GetManager().GetContentLinks();
                var relatedImageLink = contentLinks.FirstOrDefault(cl => cl.ParentItemId == master.Id && !cl.IsChildDeleted && !cl.IsParentDeleted && cl.AvailableForLive && cl.ComponentPropertyName == contentTypeItem);
                string imageUpload = string.Empty;
                if (relatedImageLink != null)
                {
                    var librariesManager = LibrariesManager.GetManager();
                    var image = librariesManager.GetImage(relatedImageLink.ChildItemId);
                    if (image != null)
                    {
                        imageUrl = image.Url;
                    }
                }
            }

            return imageUrl;
        }
    }
}