﻿using SAFRA.Custom.Services.Model.Classification;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.OpenAccess;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SAFRA.Custom.Services.Common
{
    public class ClassificationUtility
    {

        public static List<Taxon> GetFlatClassificationItems(string classificationName)
        {
            List<Taxon> tags = new List<Taxon>();
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var FlatClassificationItems = taxonomyManager.GetTaxonomies<FlatTaxonomy>().FirstOrDefault(i => i.Name == classificationName);

            if (FlatClassificationItems != null)
            {
                tags = FlatClassificationItems.Taxa.OrderBy(i => i.Title).ToList();
            }

            return tags;
        }

        public static IList<Taxon> GetFlatClassificationItems2(string classificationName)
        {
            TaxonomyManager taxManager = TaxonomyManager.GetManager();
            var classification = taxManager.GetTaxonomies<FlatTaxonomy>().FirstOrDefault(i => i.Name == classificationName);

            if (classification != null)
            {
                return classification.Taxa;
            }

            return null;
        }

        public static List<Taxon> GetHierarchicalClassificationItems(string classificationName)
        {
            List<Taxon> categories = new List<Taxon>();
            TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
            var hierarchicalTaxonomyItems = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>().FirstOrDefault(i => i.Name == classificationName);

            if (hierarchicalTaxonomyItems != null)
            {
                categories = hierarchicalTaxonomyItems.Taxa.ToList();
            }

            return categories;
        }
        public static IList<Taxon> GetHierarchicalClassificationItems2(string classificationName)
        {
            TaxonomyManager taxManager = TaxonomyManager.GetManager();
            var classification = taxManager.GetTaxonomies<HierarchicalTaxonomy>().FirstOrDefault(x => x.Name == classificationName);
            List<Taxon> categories = new List<Taxon>();

            if (classification != null)
            {
                return classification.Taxa;
            }

            return null;
        }

        public static List<SF_Classification> GetClassificationInfo(DynamicContent content, string classificationName, List<Taxon> classificationList)
        {
            List<SF_Classification> classificationTitles = new List<SF_Classification>();

            TrackedList<Guid> classificationIds = content.GetValue<TrackedList<Guid>>(classificationName);
            if (classificationIds != null && classificationIds.Count() > 0)
            {
                foreach (Guid id in classificationIds)
                {
                    Taxon classificationItem = classificationList.Find(x => x.Id == id);
                    if (classificationItem != null)
                    {
                        classificationTitles.Add(new SF_Classification(GetClassificationDetails(classificationItem)));
                    }
                }
            }

            return classificationTitles;
        }

        public static List<SF_Classification> GetClassificationInfo2(DynamicContent content, string classificationName, IList<Taxon> classificationList)
        {
            List<SF_Classification> classificationTitles = new List<SF_Classification>();

            try
            {
                TrackedList<Guid> classificationIds = content.GetValue<TrackedList<Guid>>(classificationName);
                if (classificationIds != null && classificationIds.Any())
                {
                    foreach (Guid id in classificationIds)
                    {
                        if (classificationList != null)
                        {
                            Taxon classificationItem = classificationList.FirstOrDefault(x => x.Id == id);
                            if (classificationItem != null)
                            {
                                classificationTitles.Add(new SF_Classification(GetClassificationDetails(classificationItem)));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(content.UrlName.ToString() + " " + classificationName + " " + ex.Message + Environment.NewLine + ex.StackTrace);

            }

            return classificationTitles;
        }

        private static SF_Classification_Base GetClassificationDetails(Taxon taxon)
        {
            Guid taxonId = taxon.Id;

            if (taxon.Description != null && !string.IsNullOrEmpty(taxon.Description.ToString()))
            {
                if (!string.IsNullOrEmpty(taxon.Description.Value))
                {
                    Guid dGuid = Guid.Empty;

                    if (Guid.TryParse(taxon.Description.Value, out dGuid) && dGuid != Guid.Empty)
                    { 
                        taxonId = dGuid;
                    }
                }
            }

            return new SF_Classification_Base()
            {
                Id = taxonId,
                Title = taxon.Title
            };
        }
    }
}