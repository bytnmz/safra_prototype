﻿using SAFRA.Custom.Services.Common;
using SAFRA.Custom.Services.Constant;
using SAFRA.Custom.Services.Model.DynamicContent;
using System.Collections.Generic;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SAFRA.Custom.Services.Map
{
    public class MerchantOutletMap
    {
        public static MerchantOutlet MapMerchantOutletContentToModel(DynamicContent MerchantOutletItem,
            List<Taxon> LocationItems, List<Taxon> CategoryItems, List<Taxon> LifestyleItems, List<Taxon> DepartmentItems, List<Taxon> ClubItems, List<Taxon> SectorItems,
            List<Taxon> ClubCategoryItems)
        {
            MerchantOutlet merchantOutlet = new MerchantOutlet()
            {
                Title = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Title),
                Name = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Name),
                Latitude = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Latitude),
                Longitude = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Longitude),
                PostalCode = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.PostalCode),
                Address = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Address),
                OperatingHours = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.OperatingHours),
                ContactInfo = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.ContactInfo),
                Locations = ClassificationUtility.GetClassificationInfo(MerchantOutletItem, MerchantOutletConstant.Location, LocationItems),
                Categories = ClassificationUtility.GetClassificationInfo(MerchantOutletItem, MerchantOutletConstant.DSSCategory, CategoryItems),
                Lifestyles = ClassificationUtility.GetClassificationInfo(MerchantOutletItem, MerchantOutletConstant.DSSLifestyle, LifestyleItems),
                Department = ClassificationUtility.GetClassificationInfo(MerchantOutletItem, MerchantOutletConstant.DSSDepartment, DepartmentItems),
                Clubs = ClassificationUtility.GetClassificationInfo(MerchantOutletItem, MerchantOutletConstant.DSSClub, ClubItems),
                Sectors = ClassificationUtility.GetClassificationInfo(MerchantOutletItem, MerchantOutletConstant.DSSSector, SectorItems),
                ClubCategories = ClassificationUtility.GetClassificationInfo(MerchantOutletItem, MerchantOutletConstant.DSSClubCategory, ClubCategoryItems),
            };
            
            return merchantOutlet;
        }

        public static MerchantOutlet MapMerchantOutletContentToModel2(DynamicContent MerchantOutletItem,
            IList<Taxon> LocationItems, IList<Taxon> CategoryItems, IList<Taxon> LifestyleItems, IList<Taxon> DepartmentItems, 
            IList<Taxon> ClubItems, IList<Taxon> SectorItems, IList<Taxon> ClubCategoryItems)
        {
            MerchantOutlet merchantOutlet = new MerchantOutlet()
            {
                Title = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Title),
                Name = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Name),
                Latitude = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Latitude),
                Longitude = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Longitude),
                PostalCode = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.PostalCode),
                Address = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.Address),
                OperatingHours = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.OperatingHours),
                ContactInfo = DynamicContentUtility.ToString(MerchantOutletItem, MerchantOutletConstant.ContactInfo),
                Locations = ClassificationUtility.GetClassificationInfo2(MerchantOutletItem, ClassificationConstant.Location, LocationItems),
                Categories = ClassificationUtility.GetClassificationInfo2(MerchantOutletItem, ClassificationConstant.DSSCategory, CategoryItems),
                Lifestyles = ClassificationUtility.GetClassificationInfo2(MerchantOutletItem, ClassificationConstant.DSSLifestyle, LifestyleItems),
                Department = ClassificationUtility.GetClassificationInfo2(MerchantOutletItem, ClassificationConstant.DSSDepartment, DepartmentItems),
                Clubs = ClassificationUtility.GetClassificationInfo2(MerchantOutletItem, ClassificationConstant.DSSClub, ClubItems),
                Sectors = ClassificationUtility.GetClassificationInfo2(MerchantOutletItem, ClassificationConstant.DSSSector, SectorItems),
                ClubCategories = ClassificationUtility.GetClassificationInfo2(MerchantOutletItem, ClassificationConstant.DSSClubCategory, ClubCategoryItems),
            };

            return merchantOutlet;
        }
    }
}