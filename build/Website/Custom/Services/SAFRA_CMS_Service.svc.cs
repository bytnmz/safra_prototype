﻿using SAFRA.Custom.Services.Model;
using SAFRA.Custom.Services.Model.DynamicContent;
using SAFRA.Custom.Services.Service;
using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Security;

namespace SAFRA.Custom.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SAFRA_CMS_Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SAFRA_CMS_Service.svc or SAFRA_CMS_Service.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SAFRA_CMS_Service : ISAFRA_CMS_Service
    {
        private readonly IActivityService _ActivityService;
        private readonly ICourseService _CourseService;
        private readonly IPromotionService _PromotionService;
        private readonly IFacilityService _FacilityService;
        private readonly IAmenityService _AmenityService;
        private readonly IDiscountGuideService _DiscountGuideService;
        private readonly IBusinessUnitService _BusinessUnitService;
        private readonly IAnnouncementService _AnnouncementService;
        private readonly ISafraClubhouseService _SafraClubhouseService;
        private readonly IMerchantService _MerchantService;

        public SAFRA_CMS_Service(IActivityService ActivityService, ICourseService CourseService, IPromotionService PromotionService, 
            IFacilityService FacilityService, IAmenityService AmenityService, IDiscountGuideService DiscountGuideService, 
            IBusinessUnitService BusinessUnitService, IAnnouncementService AnnouncementService, ISafraClubhouseService SafraClubhouseService, 
            IMerchantService MerchantService)
        {
            _ActivityService = ActivityService;
            _CourseService = CourseService;
            _PromotionService = PromotionService;
            _FacilityService = FacilityService;
            _AmenityService = AmenityService;
            _DiscountGuideService = DiscountGuideService;
            _BusinessUnitService = BusinessUnitService;
            _AnnouncementService = AnnouncementService;
            _SafraClubhouseService = SafraClubhouseService;
            _MerchantService = MerchantService;
        }

        #region Activity Methods

        public GetResponseModel<List<Activity>> GetAllActivities(String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<List<Activity>> getResponse = new GetResponseModel<List<Activity>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;
            
            getResponse.Model = _ActivityService.GetAllActivities(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Activity> GetActivity(String ActivityId, String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<Activity> getResponse = new GetResponseModel<Activity>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;
            
            getResponse.Model = _ActivityService.GetActivity(ActivityId, IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<List<ActivityAdminCharge>> GetActivityAdminCharge(String ActivityId, String Username, String Pass)
        {
            GetResponseModel<List<ActivityAdminCharge>> getResponse = new GetResponseModel<List<ActivityAdminCharge>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _ActivityService.GetActivityAdminCharge(ActivityId);
            return getResponse;
        }

        public GetResponseModel<List<ActivityRate>> GetActivityRate(String ActivityId, String Username, String Pass)
        {
            GetResponseModel<List<ActivityRate>> getResponse = new GetResponseModel<List<ActivityRate>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _ActivityService.GetActivityRate(ActivityId);
            return getResponse;
        }

        #endregion

        #region Course Methods

        public GetResponseModel<List<Course>> GetAllCourse(String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<List<Course>> getResponse = new GetResponseModel<List<Course>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _CourseService.GetAllCourse(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Course> GetCourse(String CourseId, String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<Course> getResponse = new GetResponseModel<Course>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _CourseService.GetCourse(CourseId, IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<List<CourseSchedule>> GetCourseSchedule(String CourseId, String Username, String Pass)
        {
            GetResponseModel<List<CourseSchedule>> getResponse = new GetResponseModel<List<CourseSchedule>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _CourseService.GetCourseSchedule(CourseId);
            return getResponse;
        }

        public GetResponseModel<List<CourseRate>> GetCourseRate(String CourseId, String Username, String Pass)
        {
            GetResponseModel<List<CourseRate>> getResponse = new GetResponseModel<List<CourseRate>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _CourseService.GetCourseRate(CourseId);
            return getResponse;
        }

        #endregion

        #region Promotion Methods

        public GetResponseModel<List<Promotions>> GetAllPromotions(String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<List<Promotions>> getResponse = new GetResponseModel<List<Promotions>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _PromotionService.GetAllPromotions(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Promotions> GetPromotionsbyId(String PromotionId, String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<Promotions> getResponse = new GetResponseModel<Promotions>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _PromotionService.GetPromotionsbyId(PromotionId, IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Promotions> GetPromotionsbyClubId(String ClubId, String Username, String Pass)
        {
            GetResponseModel<Promotions> getResponse = new GetResponseModel<Promotions>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _PromotionService.GetPromotionsbyClubId(ClubId);
            return getResponse;
        }

        #endregion

        #region Facilities Methods

        public GetResponseModel<List<Facility>> GetAllFacilities(String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<List<Facility>> getResponse = new GetResponseModel<List<Facility>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _FacilityService.GetAllFacilities(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Facility> GetFacilitybyId(String FacilityId, String IsSimpleReturn, String Username, String Pass)
        {
            GetResponseModel<Facility> getResponse = new GetResponseModel<Facility>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _FacilityService.GetFacilitybyId(FacilityId, IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<List<FacilityRate>> GetFacilityRate(String FacilityId, String Username, String Pass)
        {
            GetResponseModel<List<FacilityRate>> getResponse = new GetResponseModel<List<FacilityRate>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _FacilityService.GetFacilityRate(FacilityId);
            return getResponse;
        }

        #endregion

        #region Amenity Methods

        public GetResponseModel<List<Amenity>> GetAllAmenity(string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<List<Amenity>> getResponse = new GetResponseModel<List<Amenity>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _AmenityService.GetAllAmenity(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Amenity> GetAmenitybyId(string AmenityId, string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<Amenity> getResponse = new GetResponseModel<Amenity>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _AmenityService.GetAmenitybyId(AmenityId, IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<List<AmenityRate>> GetAmenityRate(string AmenityId, string Username, string Pass)
        {
            GetResponseModel<List<AmenityRate>> getResponse = new GetResponseModel<List<AmenityRate>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _AmenityService.GetAmenityRate(AmenityId);
            return getResponse;
        }

        #endregion

        #region Discount Guide Methods

        public GetResponseModel<List<DiscountGuide>> GetAllDiscountGuides(string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<List<DiscountGuide>> getResponse = new GetResponseModel<List<DiscountGuide>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _DiscountGuideService.GetAllDiscountGuides(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<DiscountGuide> GetDiscountGuidebyId(string DiscountGuideId, string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<DiscountGuide> getResponse = new GetResponseModel<DiscountGuide>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _DiscountGuideService.GetDiscountGuidebyId(DiscountGuideId, IsSimpleReturn);
            return getResponse;
        }

        #endregion

        #region Business Unit Methods

        public GetResponseModel<List<BusinessUnit>> GetAllBusinessUnits(string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<List<BusinessUnit>> getResponse = new GetResponseModel<List<BusinessUnit>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _BusinessUnitService.GetAllBusinessUnits(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<BusinessUnit> GetBusinessUnitbyId(string BusinessUnitsId, string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<BusinessUnit> getResponse = new GetResponseModel<BusinessUnit>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _BusinessUnitService.GetBusinessUnitbyId(BusinessUnitsId, IsSimpleReturn);
            return getResponse;
        }

        #endregion

        #region Announcement Methods

        public GetResponseModel<List<Announcement>> GetAllAnnouncement(string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<List<Announcement>> getResponse = new GetResponseModel<List<Announcement>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _AnnouncementService.GetAllAnnouncement(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Announcement> GetAnnouncementbyId(string AnnouncementId, string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<Announcement> getResponse = new GetResponseModel<Announcement>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _AnnouncementService.GetAnnouncementbyId(AnnouncementId, IsSimpleReturn);
            return getResponse;
        }

        #endregion

        #region Safra Clubhouse Methods

        public GetResponseModel<List<SafraClubhouse>> GetAllSafraClubhouse(string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<List<SafraClubhouse>> getResponse = new GetResponseModel<List<SafraClubhouse>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _SafraClubhouseService.GetAllSafraClubhouse(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<SafraClubhouse> GetSafraClubhousebyId(string SafraClubhouseId, string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<SafraClubhouse> getResponse = new GetResponseModel<SafraClubhouse>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _SafraClubhouseService.GetSafraClubhousebyId(SafraClubhouseId, IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<List<CarparkRate>> GetCarParkRatebyClubId(string SafraClubhouseId, string Username, string Pass)
        {
            GetResponseModel<List<CarparkRate>> getResponse = new GetResponseModel<List<CarparkRate>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _SafraClubhouseService.GetCarParkRatebyClubId(SafraClubhouseId);
            return getResponse;
        }

        #endregion

        #region Merchant Methods

        public GetResponseModel<List<Merchants>> GetAllMerchants(string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<List<Merchants>> getResponse = new GetResponseModel<List<Merchants>>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _MerchantService.GetAllMerchants(IsSimpleReturn);
            return getResponse;
        }

        public GetResponseModel<Merchants> GetMerchantByID(string MerchantId, string IsSimpleReturn, string Username, string Pass)
        {
            GetResponseModel<Merchants> getResponse = new GetResponseModel<Merchants>();
            Authentication(Username, Pass, getResponse);

            if (!getResponse.IsSuccess)
                return getResponse;

            getResponse.Model = _MerchantService.GetMerchantByID(MerchantId, IsSimpleReturn);
            return getResponse;
        }

        #endregion

        private static void Authentication<T>(String Username, String Pass, GetResponseModel<T> responseJson)
        {
            Credentials sitefinityCredential = new Credentials();
            sitefinityCredential.UserName = Username;
            sitefinityCredential.Password = Pass;

            UserLoggingReason userLoggingResult = SecurityManager.AuthenticateUser(sitefinityCredential);

            responseJson.ErrorMessage = (userLoggingResult.ToString() == "Unknown") ? "UserNotAllowed"
                : Regex.Replace(userLoggingResult.ToString(), "(\\B[A-Z])", " $1");

            if (userLoggingResult == UserLoggingReason.UserAlreadyLoggedIn ||
                userLoggingResult == UserLoggingReason.UserLoggedFromDifferentIp ||
                userLoggingResult == UserLoggingReason.UserLoggedFromDifferentComputer ||
                userLoggingResult == UserLoggingReason.Success)
            {
                responseJson.IsSuccess = true;
            }
            else
            {
                responseJson.IsSuccess = false;
            }
        }
    }
}
