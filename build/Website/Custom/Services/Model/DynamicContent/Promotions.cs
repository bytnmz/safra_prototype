﻿using SAFRA.Custom.Services.Model.Classification;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAFRA.Custom.Services.Model.DynamicContent
{
    [DataContract]
    public class Promotions
    {
        public Promotions()
        {
            Locations = new List<SF_Classification>();
            Accord = new List<SF_Classification>();
            Categories = new List<SF_Classification>();
            Fabs = new List<SF_Classification>();
            Lifestyles = new List<SF_Classification>();
            Department = new List<SF_Classification>();
            Sectors = new List<SF_Classification>();
            Clubs = new List<SF_Classification>();
			ClubCategories = new List<SF_Classification>();
            LocationId = "";
            Longitude = "";
            Latitude = "";
            AccordId = "";
            CategoryId = "";
            FabId = "";
            LifestyleId = "";
            DepartmentId = "";
            SectorId = "";
            ClubId = "";
			ClubCategoriesId = "";

            MerchantOutlets = new List<MerchantOutlet>();
            MerchantOutletList = new List<string>();
        }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public Guid Promotion_ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public DateTime? PublishDate { get; set; }

        [DataMember]
        public DateTime? PublishEndDate { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? RegStartDate { get; set; }

        [DataMember]
        public DateTime? RegEndDate { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string PromotionDetails { get; set; }

        [DataMember]
        public string MerchantName { get; set; }

        [DataMember]
        public string MerchantAddress { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string ContactInfo { get; set; }

        [DataMember]
        public string SpecialRemarks { get; set; }

        [DataMember]
        public string TermsAndConditions { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string BannerUrl { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public string UrlName { get; set; }

        [DataMember]
        public string OverviewDescription { get; set; }

        [DataMember]
        public string ThumbnailImageData { get; set; }
        [DataMember]
        public string ImageData1 { get; set; }
        [DataMember]
        public string ImageData2 { get; set; }
        [DataMember]
        public string ImageData3 { get; set; }
        [DataMember]
        public string ImageData4 { get; set; }
        [DataMember]
        public string ImageData5 { get; set; }
        [DataMember]
        public string ImageData6 { get; set; }
        [DataMember]
        public string ImageData7 { get; set; }
        [DataMember]
        public string ImageData8 { get; set; }
        [DataMember]
        public string ImageData9 { get; set; }
        [DataMember]
        public string ImageData10 { get; set; }
        
        [DataMember]
        public string LocationId { get; set; }
        public List<SF_Classification> Locations { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string CategoryId { get; set; }
        public List<SF_Classification> Categories { get; set; }

        [DataMember]
        public string FabId { get; set; }

        [DataMember]
        public List<SF_Classification> Fabs { get; set; }

        [DataMember]
        public string LifestyleId { get; set; }

        [DataMember]
        public List<SF_Classification> Lifestyles { get; set; }

        [DataMember]
        public string DepartmentId { get; set; }

        [DataMember]
        public List<SF_Classification> Department { get; set; }

        [DataMember]
        public string SectorId { get; set; }

        [DataMember]
        public List<SF_Classification> Sectors { get; set; }

        [DataMember]
        public string ClubId { get; set; }

        [DataMember]
        public List<SF_Classification> Clubs { get; set; }

		[DataMember]
		public string ClubCategoriesId { get; set; }
		[DataMember]
		public List<SF_Classification> ClubCategories { get; set; }

        [DataMember]
        public List<MerchantOutlet> MerchantOutlets { get; set; }

        [DataMember]
        public List<string> MerchantOutletList { get; set; }

        [DataMember]
        public string AccordId { get; set; }

        [DataMember]
        public List<SF_Classification> Accord { get; set; }

        [DataMember]
        public Boolean Accord_indicator { get; set; }

        [DataMember]
        public string Accord_description { get; set; }
    }
}