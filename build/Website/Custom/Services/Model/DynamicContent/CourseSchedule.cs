﻿using SAFRA.Custom.Services.Model.Classification;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAFRA.Custom.Services.Model.DynamicContent
{
    public class CourseSchedule
    {
        public CourseSchedule()
        {
            ApplicableGenders = new List<SF_Classification>();
            ApplicableCustomerTypes = new List<SF_Classification>();

            ApplicableCustomerTypeId = "";
            ApplicableGendersId = "";
        }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public Guid Schedule_ID { get; set; }

        [DataMember]
        public string ClassName { get; set; }

        [DataMember]
        public List<SF_Classification> ApplicableGenders { get; set; }

        [DataMember]
        public List<SF_Classification> ApplicableCustomerTypes { get; set; }

        [DataMember]
        public string ApplicableGendersId { get; set; }

        [DataMember]
        public string ApplicableCustomerTypeId { get; set; }

        [DataMember]
        public string RecurringType { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public string Time { get; set; }

        [DataMember]
        public int Recurrence { get; set; }

        [DataMember]
        public string WeeklyDay { get; set; }

        [DataMember]
        public string MonthlyRecurrence { get; set; }

        [DataMember]
        public int MonthlyMonth { get; set; }

    }
}