﻿using System.Runtime.Serialization;

namespace SAFRA.Custom.Services.Model.DynamicContent
{
    [DataContract]
    public class CarparkRate
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string VehicleType { get; set; }
        [DataMember]
        public string CustomerType { get; set; }
        [DataMember]
        public float ChargesFees { get; set; }
        [DataMember]
        public string ChargesType { get; set; }
    }
}