﻿using SAFRA.Custom.Services.Model.Classification;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAFRA.Custom.Services.Model.DynamicContent
{
    public class Course
    {
        public Course()
        {
            Locations = new List<SF_Classification>();
            Categories = new List<SF_Classification>();
            Fabs = new List<SF_Classification>();
            Lifestyles = new List<SF_Classification>();
            Department = new List<SF_Classification>();
            Sectors = new List<SF_Classification>();
            Clubs = new List<SF_Classification>();
            ClubCategories = new List<SF_Classification>();
            ApplicableCustomerTypes = new List<SF_Classification>();

            LocationId = "";
            CategoryId = "";
            FabId = "";
            LifestyleId = "";
            DepartmentId = "";
            SectorId = "";
            ClubId = "";
            ClubCategoriesId = "";
            ApplicableCustomerTypeId = "";

            MerchantOutlets = new List<MerchantOutlet>();
            MerchantOutletList = new List<string>();
        }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public Guid Course_ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int ApplicableAgeFrom { get; set; }

        [DataMember]
        public int ApplicableAgeTo { get; set; }

        [DataMember]
        public int NoOfLesson { get; set; }

        [DataMember]
        public string CourseOrganizer { get; set; }

        [DataMember]
        public DateTime? PublishDate { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public string CourseTerm { get; set; }

        [DataMember]
        public DateTime? RegStartDate { get; set; }

        [DataMember]
        public DateTime? RegEndDate { get; set; }

        [DataMember]
        public int MaxCapacity { get; set; }

        [DataMember]
        public string InstructorName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string SpecialRemarks { get; set; }

        [DataMember]
        public string ContactPersonInfo { get; set; }

        [DataMember]
        public string IndemnityWaiver { get; set; }

        [DataMember]
        public string TermsAndConditions { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string BannerUrl { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public string UrlName { get; set; }

        [DataMember]
        public string RegistrationUrl { get; set; }

        [DataMember]
        public string ThumbnailImageData { get; set; }
        [DataMember]
        public string ImageData1 { get; set; }
        [DataMember]
        public string ImageData2 { get; set; }
        [DataMember]
        public string ImageData3 { get; set; }
        [DataMember]
        public string ImageData4 { get; set; }
        [DataMember]
        public string ImageData5 { get; set; }
        [DataMember]
        public string ImageData6 { get; set; }
        [DataMember]
        public string ImageData7 { get; set; }
        [DataMember]
        public string ImageData8 { get; set; }
        [DataMember]
        public string ImageData9 { get; set; }
        [DataMember]
        public string ImageData10 { get; set; }

        public string PostalCode { get; set; }

        [DataMember]
        public List<SF_Classification> ApplicableCustomerTypes { get; set; }

        [DataMember]
        public string ApplicableCustomerTypeId { get; set; }

        [DataMember]
        public string LocationId { get; set; }
        public List<SF_Classification> Locations { get; set; }

        [DataMember]
        public string CategoryId { get; set; }
        public List<SF_Classification> Categories { get; set; }

        [DataMember]
        public string FabId { get; set; }

        [DataMember]
        public List<SF_Classification> Fabs { get; set; }

        [DataMember]
        public string LifestyleId { get; set; }

        [DataMember]
        public List<SF_Classification> Lifestyles { get; set; }

        [DataMember]
        public string DepartmentId { get; set; }

        [DataMember]
        public List<SF_Classification> Department { get; set; }

        [DataMember]
        public string SectorId { get; set; }

        [DataMember]
        public List<SF_Classification> Sectors { get; set; }

        [DataMember]
        public string ClubId { get; set; }

        [DataMember]
        public List<SF_Classification> Clubs { get; set; }

        [DataMember]
        public string ClubCategoriesId { get; set; }
        [DataMember]
        public List<SF_Classification> ClubCategories { get; set; }

        [DataMember]
        public List<MerchantOutlet> MerchantOutlets { get; set; }

        [DataMember]
        public List<string> MerchantOutletList { get; set; }
    }
}