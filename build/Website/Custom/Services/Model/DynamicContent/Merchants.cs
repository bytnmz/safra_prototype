﻿using System;
using System.Runtime.Serialization;

namespace SAFRA.Custom.Services.Model.DynamicContent
{
    [DataContract]
    public class Merchants
    {

        [DataMember]
        public Guid Merchant_ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UrlName { get; set; }


        [DataMember]
        public string ImageData1 { get; set; }
        [DataMember]
        public string ImageData2 { get; set; }
        [DataMember]
        public string ImageData3 { get; set; }
        [DataMember]
        public string ImageData4 { get; set; }
        [DataMember]
        public string ImageData5 { get; set; }
        [DataMember]
        public string ImageData6 { get; set; }
        [DataMember]
        public string ImageData7 { get; set; }
        [DataMember]
        public string ImageData8 { get; set; }
        [DataMember]
        public string ImageData9 { get; set; }
        [DataMember]
        public string ImageData10 { get; set; }
    }
}