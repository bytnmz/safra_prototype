﻿using SAFRA.Custom.Services.Model.Classification;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SAFRA.Custom.Services.Model.DynamicContent
{
    [DataContract]
    public class MerchantOutlet
    {
        public MerchantOutlet()
        {
            Locations = new List<SF_Classification>();
            Categories = new List<SF_Classification>();
            Lifestyles = new List<SF_Classification>();
            Department = new List<SF_Classification>();
            Sectors = new List<SF_Classification>();
            Clubs = new List<SF_Classification>();
			ClubCategories = new List<SF_Classification>();

            LocationId = "";
            CategoryId = "";
            LifestyleId = "";
            DepartmentId = "";
            SectorId = "";
            ClubId = "";
			ClubCategoriesId = "";
        }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string OperatingHours { get; set; }
        [DataMember]
        public string ContactInfo { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        //public List<string> Locations { get; set; }
        //public List<string> Categories { get; set; }
        //public List<string> Lifestyles { get; set; }
        //public List<string> Department { get; set; }
        //public List<string> Sectors { get; set; }
        //public List<string> Clubs { get; set; }



        [DataMember]
        public string LocationId { get; set; }

        [DataMember]
        public List<SF_Classification> Locations { get; set; }

        [DataMember]
        public string CategoryId { get; set; }

        [DataMember]
        public List<SF_Classification> Categories { get; set; }

        [DataMember]
        public string LifestyleId { get; set; }

        [DataMember]
        public List<SF_Classification> Lifestyles { get; set; }

        [DataMember]
        public string DepartmentId { get; set; }

        [DataMember]
        public List<SF_Classification> Department { get; set; }

        [DataMember]
        public string SectorId { get; set; }

        [DataMember]
        public List<SF_Classification> Sectors { get; set; }

		[DataMember]
		public List<SF_Classification> ClubCategories { get; set; }

		[DataMember]
		public string ClubCategoriesId { get; set; }

        [DataMember]
        public string ClubId { get; set; }

        [DataMember]
        public List<SF_Classification> Clubs { get; set; }
    }
}