﻿
namespace SAFRA.Custom.Services.Model
{
    public class GetResponseModel<T>
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public int TotalCount { get; set; }

        public T Model { get; set; }
    }
}