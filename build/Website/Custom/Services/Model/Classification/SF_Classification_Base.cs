﻿using System;

namespace SAFRA.Custom.Services.Model.Classification
{
    public class SF_Classification_Base
    {
        public SF_Classification_Base()
        {
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}