﻿
namespace SAFRA.Custom.Services.Model.Classification
{
    public class SF_Classification: SF_Classification_Base
    {
        public SF_Classification()
        {}

        public SF_Classification(SF_Classification_Base sf_base)
        {
            Id = sf_base.Id;
            Title = sf_base.Title;
        }
    }
}