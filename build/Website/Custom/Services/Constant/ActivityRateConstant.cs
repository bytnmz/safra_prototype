﻿
namespace SAFRA.Custom.Services.Constant
{
    public class AmenityRateConstant
    {
        public const string Title = "Title";
        public const string CustomerType = "CustomerType";
        public const string ChargesFees = "ChargesFees";
        public const string ChargesType = "ChargesType";
    }
}