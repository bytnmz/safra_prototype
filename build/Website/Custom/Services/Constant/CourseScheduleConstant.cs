﻿
namespace SAFRA.Custom.Services.Constant
{
    public class CourseScheduleConstant
    {
        public const string Title = "Title";
        public const string ScheduleId = "Schedule_ID";
        public const string ClassName = "ClassName";
        public const string RecurringType = "RecurringType";
        public const string StartDate = "StartDate";
        public const string Time = "Time";
        public const string Recurrence = "Recurrence";
        public const string WeeklyDay = "WeeklyDay";
        public const string MonthlyRecurrence = "MonthlyRecurrence";
        public const string MonthlyMonth = "MonthlyMonth";
        public const string DSSApplicableGenders = "dssapplicablegenders";
    }
}