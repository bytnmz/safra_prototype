﻿
namespace SAFRA.Custom.Services.Constant
{
    public static class ClassificationConstant
    {   
        public const string Location = "locations";
        public const string Accord = "accord";
        public const string DSSCategory = "dss-categories";
        public const string DSSLifestyle = "dss-lifestyles";
        public const string DSSDepartment = "dss-departments";
        public const string DSSClub = "dss-clubs";
        public const string DSSSector = "dss-sectors";
        public const string DSSFab = "dss-fabs";
        public const string DSSClubCategory = "dss-club-categories";
        public const string DSSApplicableCustomerTypes = "dss-application-customer-types";
        public const string DSSApplicableGenders = "dss-applicable-genders";
    }
}