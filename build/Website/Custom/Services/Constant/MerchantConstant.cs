﻿
namespace SAFRA.Custom.Services.Constant
{
    public class MerchantConstant
    {
        public const string Title = "Title";
        public const string MerchantId = "MerchantId";
        public const string Name = "Name";
        public const string UrlName = "UrlName";
        public const string ImageData1 = "Image1";
        public const string ImageData2 = "Image2";
        public const string ImageData3 = "Image3";
        public const string ImageData4 = "Image4";
        public const string ImageData5 = "Image5";
        public const string ImageData6 = "Image6";
        public const string ImageData7 = "Image7";
        public const string ImageData8 = "Image8";
        public const string ImageData9 = "Image9";
        public const string ImageData10 = "Image10";
    }
}