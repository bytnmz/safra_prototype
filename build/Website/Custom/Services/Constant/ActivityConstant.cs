﻿
namespace SAFRA.Custom.Services.Constant
{
    public static class ActivityConstant
    {
        public const string Title = "Title";
        public const string ActivityId = "ActivityId";
        public const string Name = "Name";
        public const string Code = "Code";
        public const string Type = "Type";
        public const string PublishDate = "PublishDate";
        public const string StartDate = "StartDate";
        public const string EndDate = "EndDate";
        public const string RegStartDate = "RegStartDate";
        public const string RegEndDate = "RegEndtDate";
        public const string MaxCapacity = "MaxCapacity";
        public const string Description = "Description";
        public const string ContactPersonInfo = "ContactPersonInfo";
        public const string IndemnityWaiver = "IndemnityWaiver";
        public const string PostalCode = "PostalCode";
        public const string SpecialRemarks = "SpecialRemarks";
        public const string TermsAndConditions = "TermsAndConditions";
        public const string Url = "Url";
        public const string BannerUrl = "BannerUrl";
        public const string ImageUrl = "ImageUrl";
        public const string UrlName = "UrlName";
        public const string RegistrationUrl = "RegistrationUrl";
        public const string ImageData1 = "Image1";
        public const string ImageData2 = "Image2";
        public const string ImageData3 = "Image3";
        public const string ImageData4 = "Image4";
        public const string ImageData5 = "Image5";
        public const string ImageData6 = "Image6";
        public const string ImageData7 = "Image7";
        public const string ImageData8 = "Image8";
        public const string ImageData9 = "Image9";
        public const string ImageData10 = "Image10";
        public const string ThumbnailImageData = "ThumbnailImageData";
        public const string Location = "locations";
        public const string DSSCategory = "dsscategories";
        public const string DSSDepartment = "dssdepartments";
        public const string DSSClub = "dssclubs";
        public const string DSSSector = "dsssectors";
        public const string DSSApplicableCustomerTypes = "dssapplicationcustomertypes";
        public const string DSSLifestyle = "dsslifestyles";
        public const string DSSFab = "dssfabs";
        public const string DSSClubCategory = "dssclubcategories";
    }
}