﻿
namespace SAFRA.Custom.Services.Constant
{
    public class MerchantOutletConstant
    {
        public const string Title = "Title";
        public const string Name = "Name";
        public const string Latitude = "Latitude";
        public const string Longitude = "Longitude";
        public const string PostalCode = "PostalCodeString";
        public const string Address = "Address";
        public const string OperatingHours = "OperatingHours";
        public const string ContactInfo = "ContactInfo";
        public const string Location = "locations";
        public const string DSSCategory = "dsscategories";
        public const string DSSLifestyle = "dsslifestyles";
        public const string DSSDepartment = "dssdepartments";
        public const string DSSClub = "dssclubs";
        public const string DSSSector = "dsssectors";
        public const string DSSClubCategory = "dssclubcategories";
    }
}