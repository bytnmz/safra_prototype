﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Data.Metadata;
using Telerik.Sitefinity.Metadata.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.ModuleEditor.Web.Services.Model;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Web.UI.ContentUI.Config;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Detail;
using Telerik.Sitefinity.Web.UI.ContentUI.Views.Backend.Master.Config;
using Telerik.Sitefinity.Web.UI.Fields.Config;
using Telerik.Sitefinity.Web.UI.Fields.Definitions;
using Telerik.Sitefinity.Web.UI.Fields.Enums;

namespace SAFRA.CustomLogin
{
    public class CustomUserProfile
    {
        public static readonly string OtpSecretField = "OtpSecret";
        public static readonly string IsAuthRegisteredField = "IsAuthRegistered";
        public static readonly string IsOtpSetupRequiredField = "IsOtpSetupRequired";

        //making changes to db
        public static void CheckIfOtpFieldsExist<TProfileType>() where TProfileType : UserProfile
        {
            var profileType = typeof(TProfileType);

            var sitefinityProfileDefinition = string.Format("ProfileType_{0}", profileType.Name);

            var metadataManager = MetadataManager.GetManager();

            using (new ElevatedModeRegion(metadataManager))
            {

                if (metadataManager.GetMetaType(profileType) == null)
                {
                    metadataManager.CreateMetaType(profileType);

                    metadataManager.SaveChanges();
                }

                var dynamicType = metadataManager.GetMetaType(profileType);
                var fieldList = dynamicType.Fields;

                //if otp secret field does not exist, add field
                if (fieldList.All(x => x.FieldName != OtpSecretField))
                {
                    Log.Write("---Adding field: " + OtpSecretField + "---");
                    var metaField = metadataManager.CreateMetafield(OtpSecretField);
                    metaField.DBSqlType = "NVARCHAR(255)";
                    metaField.DBType = "LONGVARCHAR";
                    metaField.ClrType = typeof(string).FullName;
                    metaField.Hidden = true;

                    dynamicType.Fields.Add(metaField);
                    metadataManager.SaveChanges();
                    Log.Write("---Adding field: " + OtpSecretField + " complete---");
                }

                //if isAuthRegistered field does not exist, add field
                if (fieldList.All(x => x.FieldName != IsAuthRegisteredField))
                {
                    Log.Write("---Adding field: " + IsAuthRegisteredField + "---");
                    var metaField = metadataManager.CreateMetafield(IsAuthRegisteredField);
                    metaField.DBType = "BIT";
                    metaField.ClrType = typeof(bool).FullName;
                    metaField.MetaAttributes.Add(new MetaFieldAttribute
                    {
                        Name = "UserFriendlyDataType",
                        Value = UserFriendlyDataType.YesNo.ToString()
                    });
                    metaField.MetaAttributes.Add(new MetaFieldAttribute { Name = "IsCommonProperty", Value = "true" });
                    dynamicType.Fields.Add(metaField);
                    metadataManager.SaveChanges();
                    Log.Write("---Adding field: " + IsAuthRegisteredField + " complete---");

                    var configManager = ConfigManager.GetManager();
                    using (new ElevatedModeRegion(configManager))
                    {
                        Log.Write("---Making changes to backend cms view for sitefinity user profile---");
                        var section = Telerik.Sitefinity.Configuration.Config.Get<ContentViewConfig>();
                        var backendSection = section.ContentViewControls[sitefinityProfileDefinition];
                        var views = backendSection.ViewsConfig.Values.Where(v => v.ViewType == typeof(DetailFormView));
                        foreach (var view in views)
                        {
                            //if (view.ViewName == "ViewFrontendCreate" || view.ViewName == "ViewBackendCreate")
                            //{
                            //    continue;
                            //}
                            Log.Write("---Making changes to view: " + view.ViewName + "---");
                            var element = (DetailFormViewElement)view;
                            var sectionToInsert = CustomFieldsContext.GetSection(element, CustomFieldsContext.customFieldsSectionName, profileType.FullName);

                            if (sectionToInsert.Fields.Elements.All(x => x.FieldName != IsAuthRegisteredField))
                            {
                                //the checkbox that checks if user is registered for Authenticator
                                var checkbox = new ChoiceFieldElement(sectionToInsert.Fields)
                                {
                                    Title = "This user is currently registered for 2FA (Do not check/uncheck this checkbox)",
                                    FieldName = IsAuthRegisteredField,
                                    ID = IsAuthRegisteredField,
                                    RenderChoiceAs = RenderChoicesAs.SingleCheckBox,
                                    DataFieldName = IsAuthRegisteredField,
                                    CssClass = "sfIsPublicProfile  sfprofileField"
                                };

                                checkbox.Choices.Add(new ChoiceDefinition());
                                checkbox.ChoicesConfig.Add(new ChoiceElement(checkbox.ChoicesConfig));
                                sectionToInsert.Fields.Add(checkbox);
                                Log.Write("---Added checkbox: " + checkbox.Title + "---");

                            }
                            Log.Write("---Completed changes to view: " + view.ViewName + "---");
                        }
                        
                        configManager.SaveSection(section);
                        Log.Write("---Completed changes to all views---");
                    }
                }

                //if isOtpSetupRequired field does not exist, add field
                if (fieldList.All(x => x.FieldName != IsOtpSetupRequiredField))
                {
                    Log.Write("---Adding field: " + IsOtpSetupRequiredField + "---");
                    var metaField = metadataManager.CreateMetafield(IsOtpSetupRequiredField);
                    metaField.DBType = "BIT";
                    metaField.ClrType = typeof(bool).FullName;
                    metaField.MetaAttributes.Add(new MetaFieldAttribute
                    {
                        Name = "UserFriendlyDataType",
                        Value = UserFriendlyDataType.YesNo.ToString()
                    });
                    metaField.MetaAttributes.Add(new MetaFieldAttribute { Name = "IsCommonProperty", Value = "true" });
                    dynamicType.Fields.Add(metaField);
                    metadataManager.SaveChanges();
                    Log.Write("---Adding field: " + IsOtpSetupRequiredField + " complete---");

                    var configManager = ConfigManager.GetManager();
                    using (new ElevatedModeRegion(configManager))
                    {
                        Log.Write("---Making changes to backend cms view for sitefinity user profile---");
                        var section = Telerik.Sitefinity.Configuration.Config.Get<ContentViewConfig>();
                        var backendSection = section.ContentViewControls[sitefinityProfileDefinition];
                        var views = backendSection.ViewsConfig.Values.Where(v => v.ViewType == typeof(DetailFormView));
                        foreach (var view in views)
                        {
                            //if (view.ViewName == "ViewFrontendCreate" || view.ViewName == "ViewBackendCreate")
                            //{
                            //    continue;
                            //}
                            Log.Write("---Making changes to view: " + view.ViewName + "---");
                            var element = (DetailFormViewElement)view;
                            var sectionToInsert = CustomFieldsContext.GetSection(element, CustomFieldsContext.customFieldsSectionName, profileType.FullName);

                            if (sectionToInsert.Fields.Elements.All(x => x.FieldName != IsOtpSetupRequiredField))
                            {
                                //this checkbox allows admin to force user to setup 2FA
                                var checkbox = new ChoiceFieldElement(sectionToInsert.Fields)
                                {
                                    Title = " 2FA setup is required",
                                    FieldName = IsOtpSetupRequiredField,
                                    RenderChoiceAs = RenderChoicesAs.SingleCheckBox,
                                    DataFieldName = IsOtpSetupRequiredField,
                                    CssClass = "sfIsPublicProfile  sfprofileField"
                                };

                                checkbox.Choices.Add(new ChoiceDefinition());
                                checkbox.ChoicesConfig.Add(new ChoiceElement(checkbox.ChoicesConfig));
                                sectionToInsert.Fields.Add(checkbox);
                                Log.Write("---Added checkbox: " + checkbox.Title + "---");

                            }
                            Log.Write("---Completed changes to view: " + view.ViewName + "---");
                        }

                        configManager.SaveSection(section);
                        Log.Write("---Completed changes to all views---");
                    }
                }
            }

            Log.Write("---Completed check for otp fields---");
        }
    }
}