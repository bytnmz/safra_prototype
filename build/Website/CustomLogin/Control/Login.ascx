﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="SAFRA.CustomLogin.Control.Login" %>
<%@ Register TagPrefix="sf" Assembly="Telerik.Sitefinity" Namespace="Telerik.Sitefinity.Web.UI" %>

<sf:ResourceLinks runat="server">
    <sf:ResourceFile Name="Styles/Login.css" />
</sf:ResourceLinks>

<script type="text/javascript">
    $(document).ready(function () {
        var listControlId = [
            "#<%= errorLabel.ClientID %>",
            "#<%= OtpAuthError.ClientID %>",
            "#<%= OtpAuthSuccess.ClientID %>",
        ];
        for (i = 0; i < listControlId.length; i++) {
            if ($(listControlId[i]).html()) {
                if ($(listControlId[i]).html().trim()) {
                    $(listControlId[i]).css("display", "block");
                }
            }
        }
    });
</script>

<h1 class="sfBreadCrumb" id="sfToMainContent">Sitefinity<span class="sfBreadCrumbBack"></span>
</h1>
<div class="sfMain sfClearfix">
    <div class="sfContent">
        <fieldset class="sfLoginForm">
            <div class="sfForm">
                <asp:MultiView ID="MViewLogin" runat="server">

                    <asp:View ID="ViewLogin" runat="server">
                        <asp:Panel runat="server" DefaultButton="LnkBtnSubmit">
                            <div class="sfFormIn" id="LoginFormControl">
                                <div class="sfLoginShadowTopRight"></div>
                                <div class="sfLoginShadowBottomLeft"></div>
                                <h2>
                                    <asp:Literal ID="LoginTitle" Text="<%$ Resources:Labels, LoginToManage %>" runat="server" />
                                </h2>

                                <sf:SitefinityLabel runat="server" ID="errorLabel" WrapperTagName="div" HideIfNoText="True" CssClass="sfFailure" />
                                <ol>
                                    <li>
                                        <label for="wrap_name" class="sfTxtLbl">
                                            <asp:Literal ID="Literal1" Text="<%$ Resources:Labels, Username %>" runat="server" />
                                        </label>
                                        <asp:TextBox ID="TxtUserName" runat="server" autocomplete="off" name="wrap_name" class="sfTxt" />
                                    </li>
                                    <li>
                                        <label for="wrap_password" class="sfTxtLbl">
                                            <asp:Literal ID="Literal2" Text="<%$ Resources:Labels, Password %>" runat="server" />
                                        </label>
                                        <asp:TextBox ID="TxtPassword" autocomplete="off" TextMode="Password" name="wrap_password" class="sfTxt" runat="server" />
                                    </li>
                                </ol>

                                <div class="sfSubmitBtn sfMainFormBtns">
                                    <asp:LinkButton ID="LnkBtnSubmit" runat="server" OnClick="LnkBtnSubmit_Click" CssClass="sfLinkBtn sfSave">
                                        <strong class="sfLinkBtnIn">
                                            <asp:Literal ID="LoginButtonLiteral" runat="server" Text="<%$ Resources:Labels, LoginCaps %>" />
                                        </strong>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:View>

                    <asp:View ID="ViewOtpAuth" runat="server">
                        <asp:Panel runat="server" DefaultButton="LnkBtnOtpAuthSubmit">
                            <div class="sfFormIn">
                                <div class="sfLoginShadowTopRight"></div>
                                <div class="sfLoginShadowBottomLeft"></div>
                                <h2>Authenticator OTP</h2>

                                <sf:SitefinityLabel runat="server" ID="OtpAuthError" WrapperTagName="div" HideIfNoText="True" CssClass="sfFailure" />
                                <sf:SitefinityLabel runat="server" ID="OtpAuthSuccess" WrapperTagName="div" HideIfNoText="True" CssClass="sfSuccess" />

                                <ol>
                                    <li style="text-align: center;"></li>
                                    <li>
                                        <asp:TextBox ID="TxtOtpAuth" runat="server" autocomplete="off" name="wrap_name" class="sfTxt" TextMode="Password" />
                                        <asp:RequiredFieldValidator ID="RequiredOtpAuth" ForeColor="Red" ErrorMessage="Please enter the OTP code" ControlToValidate="TxtOtpAuth"
                                            runat="server" Display="Dynamic" ValidationGroup="OtpAuth" />
                                    </li>
                                </ol>
                                <div class="sfSubmitBtn sfMainFormBtns">
                                    <asp:LinkButton ID="LnkBtnOtpAuthSubmit" runat="server" CssClass="sfLinkBtn sfSave" ValidationGroup="OtpAuth" OnClick="LnkBtnOtpAuthSubmit_OnClick">
                                        <strong class="sfLinkBtnIn">
                                            <asp:Literal ID="LitOtpAuthSubmit" runat="server" Text="<%$ Resources:Labels, LoginCaps %>" />
                                        </strong>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:View>

                    <asp:View ID="ViewRegisterAuth" runat="server">
                        <div class="sfFormIn">
                            <div class="sfLoginShadowTopRight"></div>
                            <div class="sfLoginShadowBottomLeft"></div>
                            <h2>Register Authenticator OTP</h2>
                            <p class="sfNeutral">Scan the following QR code using Authenticator App</p>
                            <ol>
                                <li style="text-align: center;">
                                    <telerik:RadBarcode ID="RbcAuth" runat="server" Width="150" Height="150" Type="QRCode" OutputType="EmbeddedPNG">
                                        <QRCodeSettings Version="0"></QRCodeSettings>
                                    </telerik:RadBarcode>
                                </li>
                            </ol>
                            <asp:Literal ID="LitSkipOtp" runat="server" Text="Do not scan QR code if you prefer to setup 2FA next time" />
                            <p class="sfSubmitBtn sfMainFormBtns">
                                <asp:LinkButton ID="LnkBtnProceedOtp" runat="server" CssClass="sfLinkBtn sfSave" OnClick="LnkBtnProceedOtp_OnClick">
                                    <strong class="sfLinkBtnIn">
                                        Enter OTP on next page
                                    </strong>
                                </asp:LinkButton>
                            </p>
                            <p class="sfSubmitBtn sfMainFormBtns">
                                <asp:LinkButton ID="LnkBtnSkipOtp" runat="server" CssClass="sfLinkBtn" OnClick="LnkBtnSkipOtp_OnClick">
                                    <strong class="sfLinkBtnIn">
                                        Setup 2FA next time
                                    </strong>
                                </asp:LinkButton>
                            </p>
                        </div>
                    </asp:View>   
                </asp:MultiView>
            </div>
        </fieldset>
    </div>
</div>
