﻿using System;
using System.Web;
using System.Web.UI;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Security.Claims;
using Telerik.Sitefinity.Security.Configuration;
using SAFRA.Utilities;
using Telerik.Sitefinity.Abstractions;
using SAFRA.CustomLogin;

namespace SAFRA.CustomLogin.Control
{
    public partial class Login : UserControl
    {
        //will not work with sts if false
        private const bool IsPersistent = true;
        //sets tolerance of authenticator (each period of tolerance is 30 seconds to the past or future)
        private const int OtpTolerance = 6;
        private const int OtpMaxFailure = 3;
        private readonly UserManager UserManager;
        private readonly UserProfileManager UserProfileManager;

        private string Username
        {
            get { return (string)(Session["username"] ?? string.Empty); }
            set { Session["username"] = value; }
        }
        private string Password
        {
            get { return (string)(Session["password"] ?? string.Empty); }
            set { Session["password"] = value; }
        }

        private static readonly string SUCCESS_REDIRECT_URL = "/Sitefinity/Public/Admin/Login.aspx";
        private static readonly string ERROR_REDIRECT_URL = "/Sitefinity/Public/Admin/Login.aspx";

        public Login()
        {
            UserManager = UserManager.GetManager();
            UserProfileManager = UserProfileManager.GetManager();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MViewLogin.SetActiveView(ViewLogin);
                var node = FindPagebyUrl("loginform");
                if (node.NavigationNode.RequireSsl && !Request.IsSecureConnection)
                {
                    var redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                    Response.Redirect(redirectUrl);
                }

                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var identity = ClaimsManager.GetCurrentIdentity();
                    var loginUser = UserManager.GetManager().GetUser(identity.UserId);

                    if (loginUser != null && loginUser.IsBackendUser)
                    {
                        Response.Redirect(@"/Sitefinity");
                    }
                }
                else
                {
                    SecurityManager.Logout();
                }
            }
            else
            {
                errorLabel.Text = "";
                OtpAuthError.Text = "";
                OtpAuthSuccess.Text = "";
            }
        }

        //on login button click
        protected void LnkBtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                using (new ElevatedModeRegion(UserManager))
                {
                    if (UserManager.ValidateUser(TxtUserName.Text, TxtPassword.Text))
                    {
                        var currentUser = UserManager.GetUser(TxtUserName.Text);
                        var profile = UserProfileManager.GetUserProfile<SitefinityProfile>(currentUser);
                        var isAuthRegistered = profile.GetValue<bool>(CustomUserProfile.IsAuthRegisteredField);
                        var isOtpSetupRequired = profile.GetValue<bool>(CustomUserProfile.IsOtpSetupRequiredField);
                        Username = currentUser.UserName;
                        Password = TxtPassword.Text;

                        ShowAuthView(currentUser, !isAuthRegistered, isOtpSetupRequired);
                    }
                    else
                    {
                        errorLabel.Text = "Unsuccessful login. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                if (!(ex is ThreadAbortException))
                {
                    Log.Write("User login error." + ex.Message);
                }
            }
        }

        protected void LnkBtnOtpAuthSubmit_OnClick(object sender, EventArgs e)
        {
            ValidateOtp();
        }

        protected void LnkBtnProceedOtp_OnClick(object sender, EventArgs e)
        {
            var user = UserManager.GetUser(Username);
            ShowAuthView(user);
        }

        protected void LnkBtnSkipOtp_OnClick(object sender, EventArgs e)
        {
            AuthenticateUser(Username, Password);
        }

        protected void LnkBtnCancel_Click(object sender, EventArgs e)
        {
            SecurityManager.Logout();
            MViewLogin.SetActiveView(ViewLogin);
        }

        public PageData FindPagebyUrl(string urlName)
        {
            var pageManager = PageManager.GetManager();
            var pageNode = pageManager.GetPageNodes().FirstOrDefault(p => p.UrlName == urlName);

            if (pageNode == null)
            {
                return null;
            }

            return pageNode.GetPageData();
        }

        private void AuthenticateUser(string username, string password)
        {
            //for claims authentication
            if (Telerik.Sitefinity.Configuration.Config.Get<SecurityConfig>().AuthenticationMode == AuthenticationMode.Claims)
            {
                var owinContext = SystemManager.CurrentHttpContext.Request.GetOwinContext();
                var challengeProperties = ChallengeProperties.ForLocalUser(username, password, "Default", IsPersistent, ERROR_REDIRECT_URL);
                challengeProperties.RedirectUri = SUCCESS_REDIRECT_URL;

                //redirect to identity provider
                owinContext.Authentication.Challenge(challengeProperties, ClaimsManager.CurrentAuthenticationModule.STSAuthenticationType);
            }
            //sitefinity's implementation?
            else
            {
                User user;
                UserLoggingReason result = SecurityManager.AuthenticateUser("Default", username, password, IsPersistent, out user);

                if (result != UserLoggingReason.Success)
                {
                    SystemManager.CurrentHttpContext.Response.Redirect(ERROR_REDIRECT_URL, false);
                }
                else
                {
                    SystemManager.CurrentHttpContext.Response.Redirect(SUCCESS_REDIRECT_URL, false);
                }
            }
        }

        private void ValidateOtp()
        {
            var password = TxtOtpAuth.Text;

            using (new ElevatedModeRegion(UserManager))
            {
                var loginUser = UserManager.GetUser(Username);

                int failedCount;
                if (!int.TryParse(loginUser.Comment, out failedCount))
                {
                    failedCount = 0;
                }

                if (GoogleAuthenticatorUtils.IsValidAuth(GetSecret(loginUser), password, OtpTolerance))
                {
                    loginUser.Comment = "";
                    UserManager.SaveChanges();

                    AuthenticateUser(Username, Password);
                    TrySetUserProperty(loginUser, CustomUserProfile.IsAuthRegisteredField, true);
                }
                else
                {
                    failedCount++;

                    if (failedCount >= OtpMaxFailure)
                    {
                        loginUser.Comment = "";
                        loginUser.FailedPasswordAttemptCount++;
                        UserManager.SaveChanges();

                        MViewLogin.SetActiveView(ViewLogin);
                        errorLabel.Text = "OTP tries exceeded. Please enter your username and password again.";
                    }
                    else
                    {
                        loginUser.Comment = failedCount.ToString();
                        UserManager.SaveChanges();
                        OtpAuthError.Text = "Incorrect OTP. Please try again.";
                    }
                }
            }
        }

        private bool TrySetUserProperty(User user, string fieldName, object value)
        {
            using (new ElevatedModeRegion(UserProfileManager))
            {
                var profile = UserProfileManager.GetUserProfile<SitefinityProfile>(user);
                if (profile == null)
                {
                    return false;
                }

                try
                {
                    profile.SetValue(fieldName, value);
                    UserProfileManager.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Log.Write("Failed to set user property with property name of: " + fieldName + "---" + ex.Message);
                    return false;
                }
            }
        }

        private void ShowAuthView(User user, bool notRegisteredForAuthYet = false, bool isOtpSetupRequired = false)
        {
            var profile = UserProfileManager.GetUserProfile<SitefinityProfile>(user);
            if (profile != null)
            {
                //if the user has not yet registered for authenticator
                if (notRegisteredForAuthYet)
                {
                    var baseWebUrl = HttpContext.Current.Request.Url.Authority;
                    var label = baseWebUrl + ":" + user.UserName;

                    //true - sets otp secret in db for the first time
                    var secret = GetSecret(user, true);

                    RbcAuth.Text = GoogleAuthenticatorUtils.GetKeyUri(secret, label, baseWebUrl);
                    
                    //if admin forces user to setup 2fa under user settings
                    //user will not see the "setup 2fa next time" button 
                    LnkBtnSkipOtp.Enabled = !isOtpSetupRequired;
                    LnkBtnSkipOtp.Visible = !isOtpSetupRequired;
                    LitSkipOtp.Visible = !isOtpSetupRequired;
                    
                    MViewLogin.SetActiveView(ViewRegisterAuth);
                }
                else
                {
                    MViewLogin.SetActiveView(ViewOtpAuth);
                }
            }
        }

        private string GetSecret(User user, bool isReset = false)
        {
            using (new ElevatedModeRegion(UserProfileManager))
            {
                var profile = UserProfileManager.GetUserProfile<SitefinityProfile>(user);

                var secret = profile.GetValue(CustomUserProfile.OtpSecretField) as string;
                if (string.IsNullOrWhiteSpace(secret) || isReset)
                {
                    profile.SetValue(CustomUserProfile.OtpSecretField, GoogleAuthenticatorUtils.GenerateSecret(16));
                    UserProfileManager.SaveChanges();
                }

                return profile.GetValue(CustomUserProfile.OtpSecretField).ToString();
            }
        }
    }
}