﻿using System.Web.Http;
using System.Web.Routing;

namespace SAFRA
{
    public static class WebApiConfig
    {
        private static RouteBase attributeRoutes;

        public static void Register(HttpConfiguration config)
        {
            if (attributeRoutes == null)
            {
                int routesNum = RouteTable.Routes.Count;

                config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                config.MapHttpAttributeRoutes();
                config.EnsureInitialized();

                if (routesNum < RouteTable.Routes.Count)
                {
                    attributeRoutes = RouteTable.Routes[routesNum];
                }
            }
            else
            {
                RouteTable.Routes.Add(attributeRoutes);
            }

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}