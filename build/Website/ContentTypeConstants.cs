﻿namespace SAFRA
{
    public static class ContentTypeConstants
    {
        public const string WhatsOn = "Telerik.Sitefinity.DynamicTypes.Model.SAFRAevents.Event";
        public const string EventSubpage = "Telerik.Sitefinity.DynamicTypes.Model.SAFRAevents.EventSubpage";
        public const string Promotion = "Telerik.Sitefinity.DynamicTypes.Model.Promotions.Promotion";
        public const string Facility = "Telerik.Sitefinity.DynamicTypes.Model.Facilities.Facility";
        public const string Service = "Telerik.Sitefinity.DynamicTypes.Model.Services.Service";
        public const string Article = "Telerik.Sitefinity.DynamicTypes.Model.Articles.Article";
        public const string MediaRelease = "Telerik.Sitefinity.DynamicTypes.Model.Mediareleases.MediaRelease";
        public const string Club = "Telerik.Sitefinity.DynamicTypes.Model.Clubs.Club";
        public const string Announcement = "Telerik.Sitefinity.DynamicTypes.Model.Announcements.Announcement";
        public const string Recommendation = "Telerik.Sitefinity.DynamicTypes.Model.Recommendations.Recommendation";
    }
}