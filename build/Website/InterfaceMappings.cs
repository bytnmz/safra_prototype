﻿using Ninject.Modules;
using SAFRA.Domain;
using SAFRA.Domain.Impl;

namespace SAFRA
{
    public class InterfaceMappings : NinjectModule
    {
        public override void Load()
        {
            Bind<ICategoriesService>().To<CategoriesService>();
            Bind<IPromotionsService>().To<PromotionsService>();
            Bind<IFilterContentService>().To<FilterContentService>();
        }
    }
}