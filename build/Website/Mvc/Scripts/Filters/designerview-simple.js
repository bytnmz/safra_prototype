﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.Filters = [];

        $scope.$watch('properties.FiltersJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Filters))
                    $scope.Filters = JSON.parse(newValue);
            }
        });
        $scope.$watch('Filters', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.FiltersJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.FiltersJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
