﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.Options = [];

        $scope.add = function () {
            this.Options.push({
                "Title": "", "ImageId": ""
            });
        };

        $scope.remove = function (current) {
            var index = this.Options.indexOf(current);
            this.Options.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.Options.indexOf(current);
            if (index != 0) {
                var temp = this.Options[index - 1];
                this.Options[index - 1] = this.Options[index];
                this.Options[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.Options.indexOf(current);
            var itemMaxCount = this.Options.length - 1;

            if (itemMaxCount != index) {
                var temp = this.Options[index + 1];
                this.Options[index + 1] = this.Options[index];
                this.Options[index] = temp;
            }
        };

        $scope.$watch('properties.OptionsJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Options))
                    $scope.Options = JSON.parse(newValue);
            }
        });
        $scope.$watch('Options', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.OptionsJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.OptionsJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
