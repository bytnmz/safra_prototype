﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.Items1 = [];
        $scope.Items2 = [];
        $scope.Items3 = [];
        $scope.Items4 = [];

        $scope.addItem = function (collection) {
            collection.push({ "PageId": "" });
        };

        $scope.remove = function (collection, page) {
            var index = collection.indexOf(page);
            collection.splice(index, 1);
        };

        $scope.moveUp = function (collection, page) {
            var index = collection.indexOf(page);
            if (index != 0) {
                var temp = collection[index - 1];
                collection[index - 1] = collection[index];
                collection[index] = temp;
            }
        };

        $scope.moveDown = function (collection, page) {
            var index = collection.indexOf(page);
            var itemMaxCount = collection.length - 1;

            if (itemMaxCount != index) {
                var temp = collection[index + 1];
                collection[index + 1] = collection[index];
                collection[index] = temp;
            }
        };

        $scope.$watch('properties.SelectedPages1.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Items1))
                    $scope.Items1 = JSON.parse(newValue);
            }
        });
        $scope.$watch('Items1', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.SelectedPages1.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.SelectedPages1.PropertyValue = angular.toJson(newValue);
            }

        }, true);

        $scope.$watch('properties.SelectedPages2.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Items2))
                    $scope.Items2 = JSON.parse(newValue);
            }
        });
        $scope.$watch('Items2', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.SelectedPages2.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.SelectedPages2.PropertyValue = angular.toJson(newValue);
            }

        }, true);

        $scope.$watch('properties.SelectedPages3.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Items3))
                    $scope.Items3 = JSON.parse(newValue);
            }
        });
        $scope.$watch('Items3', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.SelectedPages3.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.SelectedPages3.PropertyValue = angular.toJson(newValue);
            }

        }, true);

        $scope.$watch('properties.SelectedPages4.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Items4))
                    $scope.Items4 = JSON.parse(newValue);
            }
        });
        $scope.$watch('Items4', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.SelectedPages4.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.SelectedPages4.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
