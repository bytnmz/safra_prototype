﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (!data) {
                    return;
                }

                $scope.properties = propertyService.toAssociativeArray(data.Items);

                var isPageSelectMode1 = $scope.properties.IsPageSelectMode1.PropertyValue;
                $scope.properties.IsPageSelectMode1.PropertyValue = isPageSelectMode1.toLowerCase() === "true";

                var isPageSelectMode2 = $scope.properties.IsPageSelectMode2.PropertyValue;
                $scope.properties.IsPageSelectMode2.PropertyValue = isPageSelectMode2.toLowerCase() === "true";

                var isPageSelectMode3 = $scope.properties.IsPageSelectMode3.PropertyValue;
                $scope.properties.IsPageSelectMode3.PropertyValue = isPageSelectMode3.toLowerCase() === "true";

                var isPageSelectMode4 = $scope.properties.IsPageSelectMode4.PropertyValue;
                $scope.properties.IsPageSelectMode4.PropertyValue = isPageSelectMode4.toLowerCase() === "true";
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });
    }]);
})(jQuery);
