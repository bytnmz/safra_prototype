﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.Items = [];

        $scope.addItem = function () {
            this.Items.push({
                "Title": "", "StartDate": "", "EndDate" : "", "HasLink": false,
                "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": ""
            });
        };

        $scope.remove = function (current) {
            var index = this.Items.indexOf(current);
            this.Items.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.Items.indexOf(current);
            if (index != 0) {
                var temp = this.Items[index - 1];
                this.Items[index - 1] = this.Items[index];
                this.Items[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.Items.indexOf(current);
            var itemMaxCount = this.Items.length - 1;

            if (itemMaxCount != index) {
                var temp = this.Items[index + 1];
                this.Items[index + 1] = this.Items[index];
                this.Items[index] = temp;
            }
        };

        $scope.$watch('properties.AnnouncementsJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Items)) {
                    var newValueArr = JSON.parse(newValue);

                    for (let i = 0; i < newValueArr.length; i++) {
                        if (newValueArr[i].StartDate) {
                            newValueArr[i].StartDate = new Date(newValueArr[i].StartDate);
                        }

                        if (newValueArr[i].EndDate) {
                            newValueArr[i].EndDate = new Date(newValueArr[i].EndDate);
                        }
                    }

                    $scope.Items = newValueArr;
                }
            }
        });
        $scope.$watch('Items', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.AnnouncementsJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson) {
                    var newValueArr = angular.toJson(newValue);

                    for (let i = 0; i < newValueArr.length; i++) {
                        if (newValueArr[i].StartDate) {
                            newValueArr[i].StartDate = newValueArr[i].StartDate.toString();;
                        }

                        if (newValueArr[i].EndDate) {
                            newValueArr[i].EndDate = newValueArr[i].EndDate.toString();
                        }
                    }
                    $scope.properties.AnnouncementsJson.PropertyValue = angular.toJson(newValue);
                }
            }

        }, true);
    }]);
})(jQuery);
