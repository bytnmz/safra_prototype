﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.PricingCards = [];

        $scope.addCard = function () {
            this.PricingCards.push({
                "Title": "", "Content": "",
                "Price": 0.00,
                "Features": []
            });
        };

        $scope.remove = function (current) {
            var index = this.PricingCards.indexOf(current);
            this.PricingCards.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.PricingCards.indexOf(current);
            if (index != 0) {
                var temp = this.PricingCards[index - 1];
                this.PricingCards[index - 1] = this.PricingCards[index];
                this.PricingCards[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.PricingCards.indexOf(current);
            var itemMaxCount = this.PricingCards.length - 1;

            if (itemMaxCount != index) {
                var temp = this.PricingCards[index + 1];
                this.PricingCards[index + 1] = this.PricingCards[index];
                this.PricingCards[index] = temp;
            }
        };

        $scope.addFeature = function (current) {
            var index = this.PricingCards.indexOf(current);
            this.PricingCards[index].Features.push({ "IsExclusion": false, "Text": "" });
        };

        $scope.removeFeature = function (current, currentFeature) {
            var index = this.PricingCards.indexOf(current);
            var featureIndex = this.PricingCards[index].Features.indexOf(currentFeature);

            this.PricingCards[index].Features.splice(featureIndex, 1);
        };

        $scope.moveUpFeature = function (current, currentFeature) {
            var index = this.PricingCards.indexOf(current);
            var featureIndex = this.PricingCards[index].Features.indexOf(currentFeature);

            if (featureIndex != 0) {
                var temp = this.PricingCards[index].Features[featureIndex - 1];
                this.PricingCards[index].Features[featureIndex - 1] = this.PricingCards[index].Features[featureIndex];
                this.PricingCards[index].Features[featureIndex] = temp;
            }
        };

        $scope.moveDownFeature = function (current, currentFeature) {
            var index = this.PricingCards.indexOf(current);
            var featureIndex = this.PricingCards[index].Features.indexOf(currentFeature);
            var itemMaxCount = this.PricingCards[index].Features.length - 1;

            if (itemMaxCount != featureIndex) {
                var temp = this.PricingCards[index].Features[featureIndex + 1];
                this.PricingCards[index].Features[featureIndex + 1] = this.PricingCards[index].Features[featureIndex];
                this.PricingCards[index].Features[featureIndex] = temp;
            }
        };

        $scope.$watch('properties.PricingCardsJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.PricingCards))
                    $scope.PricingCards = JSON.parse(newValue);
            }
        });
        $scope.$watch('PricingCards', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.PricingCardsJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.PricingCardsJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
