﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
                function (data) {
                    $scope.feedback.showError = true;
                    if (data)
                        $scope.feedback.errorMessage = data.Detail;
                })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        //Banner Carousel Items
        $scope.BannerItems = [];

        $scope.addBanner = function () {
            this.BannerItems.push({ "ImageId": "", "Title": "", "Subtitle": "" });
        };

        $scope.remove = function (current) {
            var index = this.BannerItems.indexOf(current);
            this.BannerItems.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.BannerItems.indexOf(current);
            if (index != 0) {
                var temp = this.BannerItems[index - 1];
                this.BannerItems[index - 1] = this.BannerItems[index];
                this.BannerItems[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.BannerItems.indexOf(current);
            var itemMaxCount = this.BannerItems.length - 1;

            if (itemMaxCount != index) {
                var temp = this.BannerItems[index + 1];
                this.BannerItems[index + 1] = this.BannerItems[index];
                this.BannerItems[index] = temp;
            }
        };

        $scope.$watch('properties.BannersJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.BannerItems))
                    $scope.BannerItems = JSON.parse(newValue);
            }
        });
        $scope.$watch('BannerItems', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.BannersJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.BannersJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
