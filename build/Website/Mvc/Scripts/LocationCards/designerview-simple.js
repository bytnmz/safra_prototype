﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.LocationCards = [];

        $scope.addCard = function () {
            this.LocationCards.push({
                "Title": "", "ImageId": "",
                "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false,
                "OpeningHours": []
            });
        };

        $scope.remove = function (current) {
            var index = this.LocationCards.indexOf(current);
            this.LocationCards.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.LocationCards.indexOf(current);
            if (index != 0) {
                var temp = this.LocationCards[index - 1];
                this.LocationCards[index - 1] = this.LocationCards[index];
                this.LocationCards[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.LocationCards.indexOf(current);
            var itemMaxCount = this.LocationCards.length - 1;

            if (itemMaxCount != index) {
                var temp = this.LocationCards[index + 1];
                this.LocationCards[index + 1] = this.LocationCards[index];
                this.LocationCards[index] = temp;
            }
        };

        $scope.addHours = function (current) {
            var index = this.LocationCards.indexOf(current);
            this.LocationCards[index].OpeningHours.push({
                "Day": "", "Time": ""
            });
        };

        $scope.removeHours = function (current, currentHours) {
            var index = this.LocationCards.indexOf(current);
            var hoursIndex = this.LocationCards[index].OpeningHours.indexOf(currentHours);

            this.LocationCards[index].OpeningHours.splice(hoursIndex, 1);
        };

        $scope.moveUpHours = function (current, currentHours) {
            var index = this.LocationCards.indexOf(current);
            var hoursIndex = this.LocationCards[index].OpeningHours.indexOf(currentHours);

            if (hoursIndex != 0) {
                var temp = this.LocationCards[index].OpeningHours[hoursIndex - 1];
                this.LocationCards[index].OpeningHours[hoursIndex - 1] = this.LocationCards[index].OpeningHours[hoursIndex];
                this.LocationCards[index].OpeningHours[hoursIndex] = temp;
            }
        };

        $scope.moveDownHours = function (current, currentHours) {
            var index = this.LocationCards.indexOf(current);
            var hoursIndex = this.LocationCards[index].OpeningHours.indexOf(currentHours);
            var itemMaxCount = this.LocationCards[index].OpeningHours.length - 1;

            if (itemMaxCount != hoursIndex) {
                var temp = this.LocationCards[index].OpeningHours[hoursIndex + 1];
                this.LocationCards[index].OpeningHours[hoursIndex + 1] = this.LocationCards[index].OpeningHours[hoursIndex];
                this.LocationCards[index].OpeningHours[hoursIndex] = temp;
            }
        };

        $scope.$watch('properties.LocationCardsJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.LocationCards))
                    $scope.LocationCards = JSON.parse(newValue);
            }
        });
        $scope.$watch('LocationCards', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.LocationCardsJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.LocationCardsJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
