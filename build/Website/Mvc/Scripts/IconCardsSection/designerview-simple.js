﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);

                    var isPageSelectMode = $scope.properties.IsPageSelectMode.PropertyValue;
                    $scope.properties.IsPageSelectMode.PropertyValue = isPageSelectMode.toLowerCase() === "true";
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.IconCards = [];

        $scope.addCard = function () {
            this.IconCards.push({
                "ImageId": "", "Title": "", "Subtitle": "",
                "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false
            });
        };

        $scope.remove = function (current) {
            var index = this.IconCards.indexOf(current);
            this.IconCards.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.IconCards.indexOf(current);
            if (index != 0) {
                var temp = this.IconCards[index - 1];
                this.IconCards[index - 1] = this.IconCards[index];
                this.IconCards[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.IconCards.indexOf(current);
            var itemMaxCount = this.IconCards.length - 1;

            if (itemMaxCount != index) {
                var temp = this.IconCards[index + 1];
                this.IconCards[index + 1] = this.IconCards[index];
                this.IconCards[index] = temp;
            }
        };

        $scope.$watch('properties.IconCardsJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.IconCards))
                    $scope.IconCards = JSON.parse(newValue);
            }
        });
        $scope.$watch('IconCards', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.IconCardsJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.IconCardsJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
