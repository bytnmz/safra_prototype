﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        $scope.itemsSelector = {
            selectedItemsIds: [],
            selectedItems: []
        };

        $scope.$watch(
            'itemsSelector.selectedItemsIds',
            function (newSelectedItemsIds, oldSelectedItemsIds) {
                if (newSelectedItemsIds !== oldSelectedItemsIds) {
                    if (newSelectedItemsIds) {
                        $scope.properties.SerializedSelectedItemsIds.PropertyValue = JSON.stringify(newSelectedItemsIds);
                    }
                }
            },
            true
        );

        $scope.$watch(
            'itemsSelector.selectedItems',
            function (newSelectedItems, oldSelectedItems) {
                if (newSelectedItems !== oldSelectedItems) {
                    if (newSelectedItems) {
                        $scope.properties.SerializedSelectedItems.PropertyValue = JSON.stringify(newSelectedItems);
                    }
                }
            },
            true
        );

        propertyService.get()
            .then(function (data) {
                if (!data) {
                    return;
                }

                $scope.properties = propertyService.toAssociativeArray(data.Items);

                var isPageSelectMode = $scope.properties.IsPageSelectMode.PropertyValue;
                $scope.properties.IsPageSelectMode.PropertyValue = isPageSelectMode.toLowerCase() === "true";

                var selectedItemsIds = $.parseJSON($scope.properties.SerializedSelectedItemsIds.PropertyValue || null);

                if (selectedItemsIds) {
                    $scope.itemsSelector.selectedItemsIds = selectedItemsIds;
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });
    }]);
})(jQuery);
