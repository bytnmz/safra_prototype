﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
            function (data) {
                $scope.feedback.showError = true;
                if (data)
                    $scope.feedback.errorMessage = data.Detail;
            })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.Items = [];

        $scope.addItem = function () {
            this.Items.push({
                "Type": "Listing", "PageId": "", "ListId": "", "ListingPageId": "",
                "TopLink1": { "Label": "", "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false },
                "LinksGroup1": [],
                "TopLink2": { "Label": "", "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false },
                "LinksGroup2": [],
                "HasFeatured": false,
                "Featured": { "ImageId": "", "Title": "", "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false }
            });
        };

        $scope.remove = function (current) {
            var index = this.Items.indexOf(current);
            this.Items.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.Items.indexOf(current);
            if (index != 0) {
                var temp = this.Items[index - 1];
                this.Items[index - 1] = this.Items[index];
                this.Items[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.Items.indexOf(current);
            var itemMaxCount = this.Items.length - 1;

            if (itemMaxCount != index) {
                var temp = this.Items[index + 1];
                this.Items[index + 1] = this.Items[index];
                this.Items[index] = temp;
            }
        };

        $scope.addLink = function (current, group) {
            var index = this.Items.indexOf(current);
            if (group == 1) {
                this.Items[index].LinksGroup1.push({ "Label": "", "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false });
            }
            else if (group == 2) {
                this.Items[index].LinksGroup2.push({ "Label": "", "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false });
            }
        };

        $scope.removeLink = function (current, group, link) {
            var index = this.Items.indexOf(current);
            if (group == 1) {
                var linkIndex = this.Items[index].LinksGroup1.indexOf(link);
                this.Items[index].LinksGroup1.splice(linkIndex, 1);
            }
            else if (group == 2) {
                var linkIndex = this.Items[index].LinksGroup2.indexOf(link);
                this.Items[index].LinksGroup2.splice(linkIndex, 1);
            }
        };

        $scope.moveUpLink = function (current, group, link) {
            var index = this.Items.indexOf(current);
            if (group == 1) {
                var linkIndex = this.Items[index].LinksGroup1.indexOf(link);
                if (linkIndex != 0) {
                    var temp = this.Items[index].LinksGroup1[linkIndex - 1];
                    this.Items[index].LinksGroup1[linkIndex - 1] = this.Items[index].LinksGroup1[linkIndex];
                    this.Items[index].LinksGroup1[linkIndex] = temp;
                }
            }
            else if (group == 2) {
                var linkIndex = this.Items[index].LinksGroup2.indexOf(link);
                if (linkIndex != 0) {
                    var temp = this.Items[index].LinksGroup2[linkIndex - 1];
                    this.Items[index].LinksGroup2[linkIndex - 1] = this.Items[index].LinksGroup2[linkIndex];
                    this.Items[index].LinksGroup2[linkIndex] = temp;
                }
            }
        };

        $scope.moveDownLink = function (current, group, link) {
            var index = this.Items.indexOf(current);
            if (group == 1) {
                var linkIndex = this.Items[index].LinksGroup1.indexOf(currentFeature);
                var itemMaxCount = this.Items[index].LinksGroup1.length - 1;

                if (itemMaxCount != linkIndex) {
                    var temp = this.Items[index].LinksGroup1[linkIndex + 1];
                    this.Items[index].LinksGroup1[linkIndex + 1] = this.Items[index].LinksGroup1[linkIndex];
                    this.Items[index].LinksGroup1[linkIndex] = temp;
                }
            }
            else if (group == 2) {
                var linkIndex = this.Items[index].LinksGroup2.indexOf(currentFeature);
                var itemMaxCount = this.Items[index].LinksGroup2.length - 1;

                if (itemMaxCount != linkIndex) {
                    var temp = this.Items[index].LinksGroup2[linkIndex + 1];
                    this.Items[index].LinksGroup2[linkIndex + 1] = this.Items[index].LinksGroup2[linkIndex];
                    this.Items[index].LinksGroup2[linkIndex] = temp;
                }
            }
        };

        $scope.$watch('properties.MenuItemsJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Items))
                    $scope.Items = JSON.parse(newValue);
            }
        });
        $scope.$watch('Items', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.MenuItemsJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.MenuItemsJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
