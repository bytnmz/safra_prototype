﻿(function ($) {

    var simpleViewModule = angular.module('simpleViewModule', ['expander', 'designer', 'ngSanitize']);
    angular.module('designer').requires.push('simpleViewModule');
    angular.module('designer').requires.push('sfFields');
    angular.module('designer').requires.push('sfSelectors');

    simpleViewModule.controller('SimpleCtrl', ['$scope', 'propertyService', function ($scope, propertyService) {
        $scope.feedback.showLoadingIndicator = true;

        propertyService.get()
            .then(function (data) {
                if (data) {
                    $scope.properties = propertyService.toAssociativeArray(data.Items);
                }
            },
                function (data) {
                    $scope.feedback.showError = true;
                    if (data)
                        $scope.feedback.errorMessage = data.Detail;
                })
            .finally(function () {
                $scope.feedback.showLoadingIndicator = false;
            });

        $scope.Items = [];

        $scope.addItem = function () {
            this.Items.push({ "Label": "", "IsPageSelectMode": true, "LinkedPageId": "", "LinkedUrl": "", "OpenInNewTab": false });
        };

        $scope.remove = function (current) {
            var index = this.Items.indexOf(current);
            this.Items.splice(index, 1);
        };

        $scope.moveUp = function (current) {
            var index = this.Items.indexOf(current);
            if (index != 0) {
                var temp = this.Items[index - 1];
                this.Items[index - 1] = this.Items[index];
                this.Items[index] = temp;
            }
        };

        $scope.moveDown = function (current) {
            var index = this.Items.indexOf(current);
            var itemMaxCount = this.Items.length - 1;

            if (itemMaxCount != index) {
                var temp = this.Items[index + 1];
                this.Items[index + 1] = this.Items[index];
                this.Items[index] = temp;
            }
        };

        $scope.$watch('properties.LinksJson.PropertyValue', function (newValue, oldValue) {
            if (newValue) {
                if (newValue != angular.toJson($scope.Items))
                    $scope.Items = JSON.parse(newValue);
            }
        });
        $scope.$watch('Items', function (newValue, oldValue) {
            var memberjson;
            try {
                memberjson = angular.fromJson($scope.properties.LinksJson.PropertyValue);
            }
            catch (err) {

            }
            if (newValue) {
                if (newValue != memberjson)
                    $scope.properties.LinksJson.PropertyValue = angular.toJson(newValue);
            }

        }, true);
    }]);
})(jQuery);
