﻿using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "FeedCustomizer", Title = "Feed Customizer", SectionName = "Homepage")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class FeedCustomiserController : Controller
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string OptionsJson { get; set; }
        public string CtaText { get; set; }
        public string CustomiseCtaText { get; set; }

        public ActionResult Index()
        {
            var model = new FeedCustomiserModel
            {
                Title = Title,
                Subtitle = Subtitle,
                CtaText = CtaText,
                CustomiseCtaText = CustomiseCtaText
            };

            if (!OptionsJson.IsNullOrWhitespace())
            {
                var items = JsonConvert.DeserializeObject<IEnumerable<ImageTextItem>>(OptionsJson);

                var librariesManager = LibrariesManager.GetManager();

                foreach (var item in items)
                {
                    model.Options.Add(new FeedCustomiserOptionModel
                    {
                        Title = item.Title,
                        Image = new ImageModel(new Guid(item.ImageId), librariesManager)
                    });
                }
            }

            return View("Index", model);
        }
    }
}