﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using SAFRA.Domain;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using Telerik.Sitefinity.Frontend.Mvc.Helpers;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HeaderNavigation", Title = "Navigation", SectionName = "Header")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class HeaderNavigationController : Controller
    {
        public bool Mobile { get; set; }

        public string MenuItemsJson { get; set; }

        private ICategoriesService categoriesService;

        public HeaderNavigationController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        public ActionResult Index()
        {
            var model = new HeaderNavigationModel
            {
                Mobile = Mobile
            };

            var menuItems = JsonConvert.DeserializeObject<IEnumerable<MenuItem>>(MenuItemsJson);

            var pageManager = PageManager.GetManager();
            var librariesManager = LibrariesManager.GetManager();

            foreach (var item in menuItems)
            {
                var menuItem = new NavMenuItem();

                if (item.Type == "Listing" && !string.IsNullOrWhiteSpace(item.ListId) && !string.IsNullOrWhiteSpace(item.ListingPageId))
                {
                    var categoriesList = categoriesService.GetCategories(new Guid(item.ListId));

                    var node = !item.PageId.IsNullOrWhitespace() ? pageManager.GetPageNode(new Guid(item.PageId))
                        : pageManager.GetPageNode(new Guid(item.ListingPageId));

                    menuItem.Item = new PageLinkModel(node.Title, node.Id);

                    menuItem.ChildItems = GetChildItems(categoriesList, HyperLinkHelpers.GetFullPageUrl(new Guid(item.ListingPageId)));

                    if (!item.TopLink1.Label.IsNullOrWhitespace())
                    {
                        menuItem.ChildItems.Add(new PageLinkModel(item.TopLink1));
                    }
                }
                else if (item.Type == "Page" && !string.IsNullOrWhiteSpace(item.PageId))
                {
                    var node = pageManager.GetPageNode(new Guid(item.PageId));
                    
                    if (node != null)
                    {
                        menuItem.Item = new PageLinkModel(node.Title, node.Id);

                        var childNodes = pageManager.GetPageNodes().Where(p => p.Parent == node && p.ShowInNavigation).OrderBy(p => p.Title);

                        foreach (var child in childNodes)
                        {
                            var pageData = child.GetPageData();
                            if (pageData == null || (pageData.Status == ContentLifecycleStatus.Live && pageData.Visible))
                            {
                                menuItem.ChildItems.Add(new PageLinkModel(child.Title, child.Id));
                            }
                        }
                    }
                }
                else if (item.Type == "Links")
                {
                    menuItem.IsGroupedLinks = true;
                    
                    if (!item.PageId.IsNullOrWhitespace())
                    {
                        var node = pageManager.GetPageNode(new Guid(item.PageId));

                        menuItem.Item = new PageLinkModel(node.Title, node.Id);
                    }

                    var group1 = new NavMenuItemGroupedLinks();
                    group1.TopLink = new PageLinkModel(item.TopLink1);
                    group1.Links = item.LinksGroup1.Select(l => new PageLinkModel(l)).ToList();
                    menuItem.GroupedLinks.Add(group1);

                    var group2 = new NavMenuItemGroupedLinks();
                    group2.TopLink = new PageLinkModel(item.TopLink2);
                    group2.Links = item.LinksGroup2.Select(l => new PageLinkModel(l)).ToList();
                    menuItem.GroupedLinks.Add(group2);
                }

                if (item.HasFeatured)
                {
                    menuItem.HasFeatured = true;

                    if (librariesManager != null)
                    {
                        if (!item.Featured.ImageId.IsNullOrWhitespace())
                        {
                            menuItem.FeaturedImage = new ImageModel(new Guid(item.Featured.ImageId), librariesManager);
                        }
                    }
                    menuItem.FeaturedLink = new PageLinkModel(new LinkItem(item.Featured.Title, item.Featured.IsPageSelectMode, item.Featured.LinkedPageId, item.Featured.LinkedUrl, item.Featured.OpenInNewTab));
                }

                model.MenuItems.Add(menuItem);
            }

            return View("Index", model);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }

        private List<PageLinkModel> GetChildItems(List<Category> categories, string url)
        {
            var childItems = new List<PageLinkModel>();

            foreach (var cat in categories)
            {
                childItems.Add(new PageLinkModel(cat.DisplayName, url + "?interest=" + cat.CategoryId));
            }

            return childItems;
        }
    }
}