﻿using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "FooterCopyright", Title = "Copyright Statement", SectionName = "Footer")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class FooterCopyrightController : Controller
    {
        public string CopyrightStatement { get; set; }

        public ActionResult Index()
        {
            return View("Index", (object)CopyrightStatement);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}