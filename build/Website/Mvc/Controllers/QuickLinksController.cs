﻿using SAFRA.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "QuickLinks", Title = "Quick Links", SectionName = "ContentToolboxSection")]
    public class QuickLinksController : Controller
    {
        public Guid? ImageId1 { get; set; }
        public string Text1 { get; set; }
        public bool IsPageSelectMode1 { get; set; }
        public Guid? LinkedPageId1 { get; set; }
        public string LinkedUrl1 { get; set; }
        public bool OpenInNewTab1 { get; set; }

        public Guid? ImageId2 { get; set; }
        public string Text2 { get; set; }
        public bool IsPageSelectMode2 { get; set; }
        public Guid? LinkedPageId2 { get; set; }
        public string LinkedUrl2 { get; set; }
        public bool OpenInNewTab2 { get; set; }

        public Guid? ImageId3 { get; set; }
        public string Text3 { get; set; }
        public bool IsPageSelectMode3 { get; set; }
        public Guid? LinkedPageId3 { get; set; }
        public string LinkedUrl3 { get; set; }
        public bool OpenInNewTab3 { get; set; }

        public Guid? ImageId4 { get; set; }
        public string Text4 { get; set; }
        public bool IsPageSelectMode4 { get; set; }
        public Guid? LinkedPageId4 { get; set; }
        public string LinkedUrl4 { get; set; }
        public bool OpenInNewTab4 { get; set; }


        public ActionResult Index()
        {
            var model = new List<ImageCardModel>();

            var librariesManager = LibrariesManager.GetManager();

            var link1 = new ImageCardModel();

            if (ImageId1.HasValue && ImageId1.Value != Guid.Empty)
            {
                link1.Image = new ImageModel(ImageId1.Value, librariesManager);
            }

            link1.Link = new PageLinkModel(Text1, IsPageSelectMode1, LinkedPageId1, LinkedUrl1, OpenInNewTab1);

            model.Add(link1);

            var link2 = new ImageCardModel();

            if (ImageId2.HasValue && ImageId2.Value != Guid.Empty)
            {
                link2.Image = new ImageModel(ImageId2.Value, librariesManager);
            }

            link2.Link = new PageLinkModel(Text2, IsPageSelectMode2, LinkedPageId2, LinkedUrl2, OpenInNewTab2);

            model.Add(link2);

            var link3 = new ImageCardModel();

            if (ImageId3.HasValue && ImageId3.Value != Guid.Empty)
            {
                link3.Image = new ImageModel(ImageId3.Value, librariesManager);
            }

            link3.Link = new PageLinkModel(Text3, IsPageSelectMode3, LinkedPageId3, LinkedUrl3, OpenInNewTab3);

            model.Add(link3);

            var link4 = new ImageCardModel();

            if (ImageId4.HasValue && ImageId4.Value != Guid.Empty)
            {
                link4.Image = new ImageModel(ImageId4.Value, librariesManager);
            }

            link4.Link = new PageLinkModel(Text4, IsPageSelectMode4, LinkedPageId4, LinkedUrl4, OpenInNewTab4);

            model.Add(link4);

            return View("Index", model);
        }
    }
}