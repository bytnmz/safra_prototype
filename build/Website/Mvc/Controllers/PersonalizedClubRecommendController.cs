﻿using SAFRA.Domain;
using SAFRA.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;
using Telerik.Sitefinity.DynamicModules.Builder;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PersonalizedClubRecommendations", Title = "Club Recommendations", SectionName = "Personalized")]
    public class PersonalizedClubRecommendController : Controller
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? PersonaListId { get; set; }
        public Guid? InterestListId { get; set; }

        public string LocationsTaxonId
        {
            get
            {
                return "1047af5d-9044-4a5e-8c3e-4b55bfc9a060";
            }
        }

        private readonly ICategoriesService categoriesService;

        public PersonalizedClubRecommendController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        public ActionResult Index()
        {
            var model = new PersonalizedClubRecommendModel
            {
                Title = Title,
                Subtitle = Subtitle
            };

            if (PersonaListId.HasValue && PersonaListId.Value != Guid.Empty)
            {
                var filters = categoriesService.GetCategories(PersonaListId.Value, "personas");

                foreach (var filter in filters)
                {
                    model.PersonaFilters.Add(filter.CategoryId, filter.DisplayName);
                }
            }

            model.DurationFilters = GetDurationFilters();

            if (InterestListId.HasValue && InterestListId.Value != Guid.Empty)
            {
                var filters = categoriesService.GetCategories(InterestListId.Value, "Categories");

                foreach (var filter in filters)
                {
                    model.InterestFilters.Add(filter.CategoryId, filter.DisplayName);
                }
            }

            model.Location = GetLocationName();

            return View("Index", model);
        }

        private Dictionary<string, string> GetDurationFilters()
        {
            var durationFilters = new Dictionary<string, string>();
            var moduleBuilderManager = ModuleBuilderManager.GetManager();
            var field = moduleBuilderManager.Provider.GetDynamicModuleFields()
                .Where(f => f.Name == "Duration" && f.FieldNamespace == ContentTypeConstants.WhatsOn)
                .SingleOrDefault();

            if (field != null)
            {
                var choices = XElement.Parse(field.Choices);

                foreach(var choice in choices.Elements("choice"))
                {
                    durationFilters.Add(choice.Attribute("value").Value, choice.Attribute("text").Value);
                }
            }

            return durationFilters;
        }

        private string GetLocationName()
        {
            var locationName = string.Empty;

            if (LocationId.HasValue && LocationId.Value != Guid.Empty)
            {
                var categoriesTaxon = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                    .SingleOrDefault(t => t.Name == "locations");

                if (categoriesTaxon != null)
                {
                    locationName = categoriesTaxon.Taxa.FirstOrDefault(t => t.Id == LocationId)?.Name;
                }
            }

            return locationName;
        }
    }
}