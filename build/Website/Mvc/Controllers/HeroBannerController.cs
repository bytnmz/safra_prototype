﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Personalization;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HeroBanner", Title = "Hero Banner", SectionName = "Landing")]
    public class HeroBannerController : Controller, IPersonalizable
    {
        public string ImageProviderName { get; set; }
        public Guid? ImageId { get; set; }

        public string Title { get; set; }
        public string Subtitle { get; set; }

        public bool HasCta { get; set; }
        public string CtaLabel { get; set; }
        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }
        public bool HasBannerHyperlink { get; set; }

        public ActionResult Index()
        {
            var model = new HeroBannerModel
            {
                Title = Title.UpdateLinks(),
                Subtitle = Subtitle,
                HasCta = HasCta,
                HasBannerHyperlink = HasBannerHyperlink
            };

            var librariesManager = LibrariesManager.GetManager(ImageProviderName);

            if (librariesManager != null)
            {
                if (ImageId.HasValue && ImageId.Value != Guid.Empty)
                {
                    model.BannerImage = new ImageModel(ImageId.Value, librariesManager);
                }
            }

            if (HasCta)
            {
                model.CtaLink = new PageLinkModel(CtaLabel, IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);
            }

            if (HasBannerHyperlink)
            {
                model.BannerHyperlink = new PageLinkModel(IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);
            }

            return View("Index", model);
        }
    }
}