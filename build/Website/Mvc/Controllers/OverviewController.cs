﻿using SAFRA.Mvc.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Overview", Title = "Overview", SectionName = "ContentToolboxSection")]
    public class OverviewController : Controller
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public ActionResult Index()
        {
            var model = new List<string>
            {
                Title, Description.UpdateLinks()
            };

            return View(model);
        }
    }
}