﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "ImageCard", Title = "Image Card", SectionName = "ContentToolboxSection")]
    public class ImageCardController : Controller
    {
        public Guid? ImageId { get; set; }

        public string Text { get; set; }

        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public string TemplateName { get; set; }

        public ActionResult Index()
        {
            var model = new ImageCardModel();

            var librariesManager = LibrariesManager.GetManager();

            if (ImageId.HasValue && ImageId.Value != Guid.Empty)
            {
                model.Image = new ImageModel(ImageId.Value, librariesManager);
            }

            model.Link = new PageLinkModel(Text, IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);

            if (TemplateName.IsNullOrWhitespace())
            {
                TemplateName = "ImageCard";
            }

            return View(TemplateName, model);
        }
    }
}