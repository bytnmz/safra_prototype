﻿using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "ExploreSafra", Title = "Explore SAFRA", SectionName = "Landing")]
    public class ExploreSafraController : Controller
    {
        public string TemplateName { get; set; }
        public string Heading { get; set; }

        public ActionResult Index()
        {
            if (TemplateName.IsNullOrWhitespace())
            {
                TemplateName = "WhatsOn";
            }

            return View(TemplateName, (object)Heading);
        }

    }
}