﻿using SAFRA.Mvc.Models;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Libraries.Model;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PageTitleAndLogo", Title = "Page Title and Logo", SectionName = "Site-wide")]
    public class PageTitleAndLogoController : Controller
    {
        public ActionResult Index()
        {
            var model = new ContentHeaderModel();
            var currentNode = SiteMapBase.GetActualCurrentNode();

            if (currentNode != null)
            {
                model.Title = currentNode.Title;
                var logo = (Image)currentNode.GetCustomFieldValue("Logo");

                if (logo != null)
                {
                    model.Image = new ImageModel
                    {
                        Url = logo.MediaUrl,
                        AltText = logo.AlternativeText
                    };
                }
            }

            return View("Index", model);
        }
    }
}