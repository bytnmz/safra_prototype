﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PersonalizedWhatsOn", Title = "What's On", SectionName = "Personalized")]
    public class PersonalizedWhatsOnController : Controller
    {
        public string Heading { get; set; }
        public string Description { get; set; }
        public string CtaLabel { get; set; }
        public Guid? LinkedPageId { get; set; }
        public Guid? BackgroundImageId { get; set; }

        public ActionResult Index()
        {
            var model = new PersonalizedWhatsOnModel
            {
                Heading = Heading,
                Description = Description,
                CtaLink = new PageLinkModel(CtaLabel, LinkedPageId)
            };

            var librariesManager = LibrariesManager.GetManager();

            if (BackgroundImageId.HasValue && BackgroundImageId.Value != Guid.Empty)
            {
                model.BackgroundImage = new ImageModel(BackgroundImageId.Value, librariesManager);
            }

            return View("Index", model);
        }
    }
}