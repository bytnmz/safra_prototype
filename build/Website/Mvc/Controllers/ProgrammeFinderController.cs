﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "ProgrammeFinder", Title = "Programme Finder", SectionName = "Landing")]
    public class ProgrammeFinderController : Controller
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public string Proficiency1 { get; set; }
        public string Proficiency2 { get; set; }
        public string Proficiency3 { get; set; }
        public string Proficiency4 { get; set; }

        public string SelectedPages1 { get; set; }
        public string SelectedPages2 { get; set; }
        public string SelectedPages3 { get; set; }
        public string SelectedPages4 { get; set; }

        public ProgrammeFinderController()
        {
            if (SelectedPages1.IsNullOrEmpty() && SelectedPages2.IsNullOrEmpty() 
                && SelectedPages3.IsNullOrEmpty() && SelectedPages4.IsNullOrEmpty())
            {
                var pages = new List<Dictionary<string, string>> { new Dictionary<string, string> { { "PageId", "" } },
                                                                new Dictionary<string, string> { { "PageId", "" } },
                                                                new Dictionary<string, string> { { "PageId", "" } } };

                var initPages = JsonConvert.SerializeObject(pages);

                SelectedPages1 = initPages;
                SelectedPages2 = initPages;
                SelectedPages3 = initPages;
                SelectedPages4 = initPages;
            }
        }

        public ActionResult Index()
        {
            var model = new ProgrammeFinderModel
            {
                Title = Title,
                Description = Description
            };

            var pageManager = PageManager.GetManager();

            if (!Proficiency1.IsNullOrWhitespace())
            {
                model.Programmes.Add(Proficiency1, GetImageCards(SelectedPages1, pageManager));
            }

            if (!Proficiency2.IsNullOrWhitespace())
            {
                model.Programmes.Add(Proficiency2, GetImageCards(SelectedPages2, pageManager));
            }

            if (!Proficiency3.IsNullOrWhitespace())
            {
                model.Programmes.Add(Proficiency3, GetImageCards(SelectedPages3, pageManager));
            }

            if (!Proficiency4.IsNullOrWhitespace())
            {
                model.Programmes.Add(Proficiency4, GetImageCards(SelectedPages4, pageManager));
            }

            return View("Index", model);
        }

        private List<ImageCardModel> GetImageCards(string pages, PageManager pageManager)
        {
            var model = new List<ImageCardModel>();
            var pageIds = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string, string>>>(pages);

            foreach (var page in pageIds)
            {
                if (page.TryGetValue("PageId", out string pageId) && !pageId.IsNullOrWhitespace())
                {
                    var node = pageManager.GetPageNode(new Guid(pageId));
                    if (node != null)
                    {
                        var pageData = node.GetPageData();
                        if (pageData.Status == ContentLifecycleStatus.Live && pageData.Visible)
                        {
                            var pageLink = new PageLinkModel(node.Title, node.Id);
                            var overviewImage = node.GetValue<Image>("OverviewImage");
                            var image = new ImageModel();

                            if (overviewImage != null)
                            {
                                image.Url = overviewImage.ResolveMediaUrl();
                                image.AltText = overviewImage.AlternativeText;
                            }

                            model.Add(new ImageCardModel(image, pageLink));
                        }
                    }
                }
            }

            return model;
        }
    }
}