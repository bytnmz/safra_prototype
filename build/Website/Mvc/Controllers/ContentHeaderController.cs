﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;


namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "ContentHeader", Title = "Content Header", SectionName = "ContentToolboxSection")]
    public class ContentHeaderController : Controller
    {
        public string Title { get; set; }
        public string ImageProviderName { get; set; }
        public Guid? ImageId { get; set; }

        public ActionResult Index()
        {
            var model = new ContentHeaderModel
            {
                Title = Title
            };

            if (ImageId.HasValue && ImageId.Value != Guid.Empty)
            {
                var librariesManager = LibrariesManager.GetManager(ImageProviderName);

                if (librariesManager != null)
                {
                    model.Image = new ImageModel(ImageId.Value, librariesManager);
                }
            }

            return View("Index", model);
        }
    }
}