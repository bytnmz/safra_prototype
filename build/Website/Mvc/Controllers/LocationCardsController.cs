﻿using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "LocationCards", Title = "Location Cards", SectionName = "Landing")]
    public class LocationCardsController : Controller
    {
        public string Heading { get; set; }
        public string TemplateName { get; set; }
        public string LocationCardsJson { get; set; }

        public LocationCardsController()
        {
            if (string.IsNullOrEmpty(LocationCardsJson))
            {
                var locationCards = new List<LocationItem> { new LocationItem() };

                LocationCardsJson = JsonConvert.SerializeObject(locationCards);
            }
        }

        public ActionResult Index()
        {
            var model = new LocationCardsModel
            {
                Heading = Heading
            };

            var items = JsonConvert.DeserializeObject<IEnumerable<LocationItem>>(LocationCardsJson);

            var librariesManager = LibrariesManager.GetManager();

            foreach (var item in items)
            {
                if (!item.Title.IsNullOrWhitespace())
                {
                    var location = new LocationCardModel
                    {
                        Title = item.Title,
                        Link = new PageLinkModel(new LinkItem(item.Title, item.IsPageSelectMode, item.LinkedPageId, item.LinkedUrl, item.OpenInNewTab))
                    };

                    if (!string.IsNullOrWhiteSpace(item.ImageId))
                    {
                        location.Image = new ImageModel(new Guid(item.ImageId), librariesManager);
                    }

                    foreach (var hours in item.OpeningHours)
                    {
                        if (!hours.Day.IsNullOrWhitespace())
                        {
                            location.OpeningHours.Add(hours.Day, hours.Time);
                        }
                    }

                    model.Locations.Add(location);
                }
            }

            if (TemplateName.IsNullOrWhitespace())
            {
                TemplateName = "Carousel";
            }

            return View(TemplateName, model);
        }
    }
}