﻿using System;
using System.Web.Mvc;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "ImageLink", Title = "Image with External Link", SectionName = "ContentToolboxSection")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class ImageLinkController : Controller
    {
        public string ImageProviderName { get; set; }
        public Guid? ImageId { get; set; }
        public string LinkedUrl { get; set; }

        public ActionResult Index()
        {
            var model = new ImageLinkModel
            {
                Link = ControlUtilities.SanitizeUrl(LinkedUrl)
            };

            var librariesManager = LibrariesManager.GetManager(ImageProviderName);

            if (librariesManager != null)
            {
                if (ImageId.HasValue && ImageId.Value != Guid.Empty)
                {
                    model.Image = new ImageModel(ImageId.Value, librariesManager);
                }
            }

            return View("Index", model);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}