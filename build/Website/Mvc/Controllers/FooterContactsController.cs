﻿using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using SAFRA.Mvc.Models.Serialization;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "FooterContacts", Title = "Contacts", SectionName = "Footer")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class FooterContactsController : Controller
    {
        public string ContactsJson { get; set; }

        public ActionResult Index()
        {
            var model = new Dictionary<string, string>();

            var items = JsonConvert.DeserializeObject<IEnumerable<ContactItem>>(ContactsJson);

            foreach (var item in items)
            {
                model.Add(item.Name, item.Contact);
            }

            return View("Index", model);

        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}