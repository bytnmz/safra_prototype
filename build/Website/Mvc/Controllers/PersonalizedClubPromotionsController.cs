﻿using SAFRA.Domain;
using SAFRA.Mvc.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PersonalizedClubPromotions", Title = "Club Promotions", SectionName = "Personalized")]
    public class PersonalizedClubPromotionsController : Controller
    {
        public string Heading { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? InterestListId { get; set; }

        public string CtaLabel1 { get; set; }
        public string LinkedUrl { get; set; }

        public string CtaLabel2 { get; set; }
        public Guid? LinkedPageId { get; set; }

        public string LocationsTaxonId
        {
            get
            {
                return "1047af5d-9044-4a5e-8c3e-4b55bfc9a060";
            }
        }

        private readonly ICategoriesService categoriesService;

        public PersonalizedClubPromotionsController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        public ActionResult Index()
        {
            var model = new PersonalizedClubPromotionsModel
            {
                Heading = Heading,
                CtaLink1 = new PageLinkModel(CtaLabel1, LinkedUrl),
                CtaLink2 = new PageLinkModel(CtaLabel2, LinkedPageId)
            };

            if (InterestListId.HasValue && InterestListId.Value != Guid.Empty)
            {
                var filters = categoriesService.GetCategories(InterestListId.Value, "Categories");

                foreach (var filter in filters)
                {
                    model.InterestFilters.Add(filter.CategoryId, filter.DisplayName);
                }
            }

            if (LocationId.HasValue && LocationId.Value != Guid.Empty)
            {
                var categoriesTaxon = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                    .SingleOrDefault(t => t.Name == "locations");

                if (categoriesTaxon != null)
                {
                    model.Location = categoriesTaxon.Taxa.FirstOrDefault(t => t.Id == LocationId)?.Name;
                }
            }

            return View("Index", model);
        }
    }
}