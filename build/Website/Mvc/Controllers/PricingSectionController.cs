﻿using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PricingSection", Title = "Pricing Section", SectionName = "ContentToolboxSection")]
    public class PricingSectionController : Controller
    {
        public string Heading { get; set; }
        public string PricingCardsJson { get; set; }

        public PricingSectionController()
        {
            if (string.IsNullOrEmpty(PricingCardsJson))
            {
                var pricingCards = new List<PricingCardModel> { new PricingCardModel(), new PricingCardModel(), new PricingCardModel() };

                PricingCardsJson = JsonConvert.SerializeObject(pricingCards);
            }
        }

        public ActionResult Index()
        {
            var model = new PricingSectionModel
            {
                Heading = Heading
            };

            model.PricingCards = JsonConvert.DeserializeObject<IEnumerable<PricingCardModel>>(PricingCardsJson)
                .OrderBy(p => p.Price).ToList();

            return View("Index", model);
        }
    }
}