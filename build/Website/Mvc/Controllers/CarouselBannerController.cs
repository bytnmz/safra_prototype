﻿using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Personalization;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "CarouselBanner", Title = "Carousel Banner", SectionName = "ContentToolboxSection")]
    public class CarouselBannerController : Controller, IPersonalizable
    {
        public string BannersJson { get; set; }
        public string TemplateName { get; set; }


        public CarouselBannerController()
        {
            if (string.IsNullOrEmpty(BannersJson))
            {
                var banners = new BannerItem[3] { new BannerItem(), new BannerItem(), new BannerItem() };

                BannersJson = JsonConvert.SerializeObject(banners);
            }
        }

        public ActionResult Index()
        {
            var model = new List<BannerModel>();

            var items = JsonConvert.DeserializeObject<IEnumerable<BannerItem>>(BannersJson);

            var librariesManager = LibrariesManager.GetManager();

            foreach (var item in items)
            {
                if (!string.IsNullOrWhiteSpace(item.ImageId))
                {
                    model.Add(new BannerModel
                    {
                        Title = item.Title,
                        Subtitle = item.Subtitle,
                        BannerImage = new ImageModel(new Guid(item.ImageId), librariesManager),
                        HasBannerHyperlink = item.HasBannerHyperlink,
                        BannerHyperlink = item.HasBannerHyperlink ? new PageLinkModel(new LinkItem(item.IsPageSelectMode, item.LinkedPageId, item.LinkedUrl, item.OpenInNewTab)) : null
                    });
                }
            }

            if (TemplateName.IsNullOrWhitespace())
            {
                TemplateName = "Homepage";
            }

            return View(TemplateName, model);
        }
    }
}