﻿using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HeaderDropdown", Title = "Dropdown", SectionName = "Header")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class HeaderDropdownController : Controller
    {
        public string Label { get; set; }
        public string LinksJson { get; set; }

        public ActionResult Index()
        {
            var model = new HeaderDropdownModel
            {
                Label = Label
            };

            var items = JsonConvert.DeserializeObject<IEnumerable<LinkItem>>(LinksJson);

            foreach (var item in items)
            {
                model.Links.Add(new PageLinkModel(item));
            }

            return View("Index", model);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}