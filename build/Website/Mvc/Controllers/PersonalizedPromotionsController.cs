﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PersonalizedPromotions", Title = "Promotions", SectionName = "Personalized")]
    public class PersonalizedPromotionsController : Controller
    {
        public string Heading { get; set; }
        public string CtaLabel { get; set; }
        public Guid? LinkedPageId { get; set; }

        public ActionResult Index()
        {
            var model = new SectionHeaderModel
            {
                Heading = Heading,
                CtaLink = new PageLinkModel(CtaLabel, LinkedPageId)
            };

            return View("Index", model);
        }
    }
}