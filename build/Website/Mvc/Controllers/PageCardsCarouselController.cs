﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PageCardsCarousel", Title = "Page Cards Carousel", SectionName = "Homepage")]
    public class PageCardsCarouselController : Controller
    {
        public string Heading { get; set; }

        public bool HasCta { get; set; }
        public string CtaLabel { get; set; }
        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public string SelectedPages { get; set; }

        public ActionResult Index()
        {
            var model = new PageCardsCarouselModel
            {
                Heading = Heading,
                HasCta = HasCta
            };

            if (HasCta)
            {
                model.CtaLink = new PageLinkModel(CtaLabel, IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);
            }

            var pageIds = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string, string>>>(SelectedPages);
            var pageManager = PageManager.GetManager();

            foreach (var page in pageIds)
            {
                var pageId = page["PageId"];

                if (!pageId.IsNullOrWhitespace())
                {
                    var node = pageManager.GetPageNode(new Guid(pageId));
                    if (node != null)
                    {
                        var pageData = node.GetPageData();
                        if (pageData.Status == ContentLifecycleStatus.Live && pageData.Visible)
                        {
                            var pageLink = new PageLinkModel(node.Title, node.Id);
                            var overviewImage = node.GetValue<Image>("OverviewImage");
                            var image = new ImageModel();

                            if (overviewImage != null)
                            {
                                image.Url = overviewImage.ResolveMediaUrl();
                                image.AltText = overviewImage.AlternativeText;
                            }

                            model.Pages.Add(new ImageCardModel(image, pageLink));
                        }
                    }
                }
            }

            return View("Index", model);
        }
    }
}