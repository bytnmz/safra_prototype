﻿using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "FacebookPagePosts", Title = "Facebook Page Posts", SectionName = "Landing")]
    public class FacebookPagePostsController : Controller
    {
        public string PageId { get; set; }
        public string Token { get; set; }

        public ActionResult Index()
        {
            var model = new List<FacebookPagePostModel>();

            if (!PageId.IsNullOrWhitespace() && !Token.IsNullOrWhitespace())
            {
                var postsJson = GetPagePosts();

                if (!postsJson.IsNullOrWhitespace())
                {
                    var posts = JsonConvert.DeserializeObject<FacebookPostsModel>(postsJson);

                    foreach (var post in posts.data)
                    {
                        if (post.attachments != null && post.attachments.data != null && post.attachments.data.Any())
                        {
                            foreach (var attachment in post.attachments.data)
                            {
                                if (attachment.media != null && attachment.media.image != null)
                                {
                                    model.Add(new FacebookPagePostModel
                                    {
                                        PostUrl = attachment.url,
                                        ImageSrc = attachment.media.image.src
                                    });
                                }

                                if (model.Count == 5)
                                {
                                    break;
                                }
                            }
                        }

                        if (model.Count == 5)
                        {
                            break;
                        }
                    }
                }
            }

            return View("Index", model);
        }

        private string GetPagePosts()
        {
            var responseString = string.Empty;

            using (var httpClient = new HttpClient { BaseAddress = new Uri("https://graph.facebook.com/v5.0/") })
            {
                httpClient.DefaultRequestHeaders
                    .Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.GetAsync(PageId + "/posts?fields=attachments{url,media}&access_token=" + Token).Result;

                if (response.IsSuccessStatusCode)
                {
                    // by calling .Result you are performing a synchronous call
                    var responseContent = response.Content;

                    // by calling .Result you are synchronously reading the result
                    responseString = responseContent.ReadAsStringAsync().Result;
                }
            }

            return responseString;
        }

        private class FacebookPostsModel
        {
            public List<FacebookPostModel> data { get; set; }
            public Dictionary<string, object> paging { get; set; }
        }

        private class FacebookPostModel
        {
            public string id { get; set; }
            public FacebookAttachmentsModel attachments { get; set; }
        }

        private class FacebookAttachmentsModel
        {
            public List<FacebookAttachmentDataModel> data { get; set; }
        }

        private class FacebookAttachmentDataModel
        {
            public string url { get; set; }
            public FacebookMediaModel media { get; set; }
        }

        private class FacebookMediaModel
        {
            public FacebookImageModel image { get; set; }
        }

        private class FacebookImageModel
        {
            public int height { get; set; }
            public string src { get; set; }
            public int width { get; set; }
        }
    }
}