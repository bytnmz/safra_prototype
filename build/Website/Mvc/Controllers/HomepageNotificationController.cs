﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HomepageNotification", Title = "Notification", SectionName = "Homepage")]
    public class HomepageNotificationController : Controller
    {

        public string Title { get; set; }

        public Guid? ImageId { get; set; }

        public string CtaLabel { get; set; }
        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public ActionResult Index()
        {
            var model = new HomepageNotificationModel
            {
                Title = Title
            };

            var librariesManager = LibrariesManager.GetManager();

            if (ImageId.HasValue && ImageId.Value != Guid.Empty)
            {
                model.Image = new ImageModel(ImageId.Value, librariesManager);
            }

            model.Link = new PageLinkModel(CtaLabel, IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);

            return View("Index", model);
        }
    }
}