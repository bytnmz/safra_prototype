﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using SAFRA.Domain;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Taxonomies;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Filters", Title = "Filters", SectionName = "Listing")]
    public class FiltersController : Controller
    {
        public string FiltersJson { get; set; }

        public bool IsPromotions { get; set; }
        public Guid? PromotionCategoryId { get; set; }

        public string PromotionsTaxonId
        {
            get
            {
                return "9be433f6-df8c-4865-aa34-29885d50d1c0";
            }
        }

        private ICategoriesService categoriesService;

        public FiltersController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;

            if (string.IsNullOrEmpty(FiltersJson))
            {
                var filters = new FilterItem[3] { new FilterItem("location"), new FilterItem("persona"), new FilterItem("interest") };
                FiltersJson = JsonConvert.SerializeObject(filters);
            }
        }

        public ActionResult Index()
        {
            var model = new Dictionary<string, FilterModel>();

            var filters = JsonConvert.DeserializeObject<IEnumerable<FilterItem>>(FiltersJson);

            foreach (var filter in filters)
            {
                var filterModel = new FilterModel
                {
                    IsCheckbox = filter.IsCheckbox,
                    Header = filter.HeaderText,
                    AllText = filter.AllText
                };

                if (!filter.ListId.IsNullOrWhitespace())
                {
                    var categoryName = filter.Name == "interest" ? "Categories" : filter.Name + "s";
                    filterModel.KeyValuePairs = categoriesService.GetCategories(new Guid(filter.ListId), categoryName)
                        .Select(x => new KeyValuePair<string, string>(x.CategoryId, x.DisplayName)).ToList();
                }

                model.Add(filter.Name, filterModel);
            }

            if (IsPromotions)
            {
                var filterModel = new FilterModel
                {
                    IsCheckbox = true,
                    IsHidden = true
                };

                var catName = "all";

                if (PromotionCategoryId.HasValue)
                {
                    var promoCatName = GetPromotionCategoryName();

                    if (!promoCatName.IsNullOrEmpty())
                    {
                        catName = promoCatName;
                    }
                }

                filterModel.KeyValuePairs.Add(new KeyValuePair<string, string>(catName, catName));

                model.Add("category", filterModel);
            }

            return View("Index", model); 
        }

        private string GetPromotionCategoryName()
        {
            var catName = string.Empty;

            var categoriesTaxon = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                .SingleOrDefault(t => t.Name == "promotion-category");

            if (categoriesTaxon != null)
            {
                catName = categoriesTaxon.Taxa.FirstOrDefault(t => t.Id == PromotionCategoryId)?.Name;
            }

            return catName;
        }
    }
}