﻿using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "AccordionTitle", Title = "Accordion Title", SectionName = "ContentToolboxSection")]
    public class AccordionTitleController : Controller
    {
        public string Title { get; set; }

        public ActionResult Index()
        {
            return View("Index", (object)Title);
        }
    }
}