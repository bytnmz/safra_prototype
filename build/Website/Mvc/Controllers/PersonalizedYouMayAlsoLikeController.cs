﻿using SAFRA.Domain;
using System;
using System.Net;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PersonalizedYouMayAlsoLike", Title = "You May Also Like", SectionName = "Personalized")]
    public class PersonalizedYouMayAlsoLikeController : Controller
    {
        public string TemplateName { get; set; }
        public string Heading { get; set; }

        private IFilterContentService contentService;

        public PersonalizedYouMayAlsoLikeController(IFilterContentService contentService)
        {
            this.contentService = contentService;
        }

        [RelativeRoute("{urlName?}")]
        public ActionResult Index(string urlName = null)
        {
            if (TemplateName.IsNullOrWhitespace())
            {
                TemplateName = "WhatsOn";
            }

            string qStringVal = Request.QueryString["sf-content-action"];

            if (urlName.IsNullOrWhitespace() || qStringVal == "preview")
            {
                TemplateName = "Blank";
            }
            else
            {
                var contentType = ContentTypeConstants.WhatsOn;

                switch(TemplateName)
                {
                    case "WhatsOn":
                        contentType = ContentTypeConstants.WhatsOn;
                        break;
                    case "Facilities":
                        contentType = ContentTypeConstants.Facility;
                        break;
                    case "Services":
                        contentType = ContentTypeConstants.Service;
                        break;
                    case "Promotions":
                        contentType = ContentTypeConstants.Promotion;
                        break;
                    default:
                        break;
                }

                if (!contentService.IsContentLive(urlName, contentType))
                {
                    return HttpNotFound();
                }
            }

            return View(TemplateName, (object)Heading);
        }
    }
}