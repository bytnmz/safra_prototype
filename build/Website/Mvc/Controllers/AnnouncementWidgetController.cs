﻿using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "AnnouncementWidget", Title = "Announcement", SectionName = "ContentToolboxSection")]
    public class AnnouncementWidgetController : Controller
    {
        public string AnnouncementsJson { get; set; }

        public ActionResult Index()
        {
            var model = new List<AnnouncementWidgetModel>();

            if (!AnnouncementsJson.IsNullOrWhitespace())
            {
                var items = JsonConvert.DeserializeObject<IEnumerable<AnnouncementItem>>(AnnouncementsJson);

                foreach (var item in items)
                {
                    var hasStartDate = DateTime.TryParse(item.StartDate, out DateTime startDate);
                    var hasEndDate = DateTime.TryParse(item.EndDate, out DateTime endDate);

                    if ((hasStartDate && startDate > DateTime.Now) ||
                        (hasEndDate && endDate < DateTime.Now))
                    {
                        continue;
                    }

                    var announcement = new AnnouncementWidgetModel
                    {
                        Title = item.Title,
                        HasLink = item.HasLink
                    };

                    if (item.HasLink)
                    {
                        announcement.Link = new PageLinkModel(new LinkItem(item.Title, item.IsPageSelectMode, item.LinkedPageId, item.LinkedUrl, false));
                    }

                    model.Add(announcement);
                }
            }

            return View("Index", model);
        }
    }
}