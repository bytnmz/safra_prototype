﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using SAFRA.Domain;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.ContentLocations;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Builder.Model;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Frontend.Mvc.Helpers;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI.ContentUI.Contracts;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "FacilitiesCard", Title = "Facilities Card", SectionName = "Telerik.Sitefinity.DynamicTypes.Model.Facilities")]
    public class FacilitiesCardController : Controller, IContentLocatableView, IHasCacheDependency
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }

        public string SerializedSelectedItemsIds { get; set; }
        public string SerializedSelectedItems { get; set; }

        public string ItemType
        {
            get
            {
                return ContentTypeConstants.Facility;
            }
        }

        private IFilterContentService contentService;
        private IContentLocationService clService;

        public bool? DisableCanonicalUrlMetaTag
        {
            get
            {
                return true;
            }

            set { }
        }

        public IEnumerable<IContentLocationInfo> GetLocations()
        {
            var location = new ContentLocationInfo();

            var type = TypeResolutionService.ResolveType(ContentTypeConstants.Facility);
            location.ContentType = type;

            var manager = DynamicModuleManager.GetManager();
            var providerName = manager.Provider.Name;
            location.ProviderName = providerName;

            yield return location;
        }

        public IList<CacheDependencyKey> GetCacheDependencyObjects()
        {
            var contentType = TypeResolutionService.ResolveType(ContentTypeConstants.Facility);
            var cacheDependencyNotifiedObjects = new List<CacheDependencyKey>();
            var manager = DynamicModuleManager.GetManager();

            string applicationName = manager != null && manager.Provider != null ? manager.Provider.ApplicationName : string.Empty;
            cacheDependencyNotifiedObjects.Add(new CacheDependencyKey { Key = string.Concat(ContentLifecycleStatus.Live.ToString(), applicationName, contentType.FullName), Type = typeof(DynamicContent) });
            cacheDependencyNotifiedObjects.Add(new CacheDependencyKey() { Key = contentType.FullName, Type = typeof(DynamicModule) });

            return cacheDependencyNotifiedObjects;
        }

        public FacilitiesCardController(IFilterContentService contentService)
        {
            this.contentService = contentService;
            clService = SystemManager.GetContentLocationService();
        }

        public ActionResult Index()
        {
            SubscribeCacheDependency();

            var model = new FacilitiesServicesCardModel
            {
                Title = Title,
                Description = Description.UpdateLinks(),
                ViewAllLink = new PageLinkModel("View all", IsPageSelectMode, LinkedPageId, LinkedUrl)
            };

            if (!SerializedSelectedItemsIds.IsNullOrWhitespace())
            {
                var itemsIds = JsonConvert.DeserializeObject<IEnumerable<Guid>>(SerializedSelectedItemsIds);

                foreach (var id in itemsIds)
                {
                    var contentItem = contentService.GetLiveContent(id, ItemType);

                    if (contentItem != null)
                    {
                        var link = string.Empty;

                        var page = contentItem.GetRelatedItems("Page");

                        if (page != null && page.Any())
                        {
                            var pageNode = (PageNode)page.First();
                            link = HyperLinkHelpers.GetFullPageUrl(pageNode.Id);
                        }
                        else
                        {
                            var location = clService.GetItemDefaultLocation(contentItem);
                            link = location.ItemAbsoluteUrl;
                        }

                        var pageLink = new PageLinkModel
                        {
                            Url = link,
                            Label = contentItem.GetString("Title")
                        };

                        var overviewImage = contentItem.GetValue<Image>("OverviewImage");

                        var image = new ImageModel();

                        if (overviewImage != null)
                        {
                            image.Url = overviewImage.ResolveMediaUrl();
                            image.AltText = overviewImage.AlternativeText;
                        }

                        model.FacilitiesServices.Add(new ImageCardModel(image, pageLink));
                    }
                }
            }

            return View("Index", model);
        }

        private void SubscribeCacheDependency()
        {
            var contextItems = SystemManager.CurrentHttpContext.Items;
            var isBackendRequest = contextItems.Contains(SystemManager.IsBackendRequestKey) && ((bool)contextItems[SystemManager.IsBackendRequestKey]);

            if (isBackendRequest)
            {
                return;
            }

            if (!contextItems.Contains(PageCacheDependencyKeys.PageData))
            {
                contextItems.Add(PageCacheDependencyKeys.PageData, new List<CacheDependencyKey>());
            }

            var existingKeys = (List<CacheDependencyKey>)contextItems[PageCacheDependencyKeys.PageData];
            var keysToAdd = GetCacheDependencyObjects();

            existingKeys.AddRange(keysToAdd);
        }
    }
}