﻿using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "NSManFeed", Title = "NSMan Feed", SectionName = "Homepage")]
    public class NSManFeedController : Controller
    {
        public string CtaLabel { get; set; }
        public string CtaLink { get; set; }
        public string RssFeedUrl { get; set; }

        public ActionResult Index()
        {
            var model = new List<string>
            {
                CtaLabel,
                CtaLink,
                RssFeedUrl
            };

            return View("Index", model);
        }
    }
}