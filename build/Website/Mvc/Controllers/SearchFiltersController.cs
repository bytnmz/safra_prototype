﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using SAFRA.Domain;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "SearchFilters", Title = "Search Filters", SectionName = "Search")]
    public class SearchFiltersController : Controller
    {
        public string AllText { get; set; }
        public Guid? LocationsId { get; set; }
        private ICategoriesService categoriesService;

        public SearchFiltersController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        public ActionResult Index()
        {
            var filterModel = new FilterModel
            {
                AllText = AllText
            };

            if (LocationsId.HasValue)
            {
                filterModel.KeyValuePairs = categoriesService.GetCategories(LocationsId.Value, "locations")
                    .Select(x => new KeyValuePair<string, string>(x.CategoryId, x.DisplayName)).ToList();
            }

            return View("Index", filterModel);
        }
    }
}