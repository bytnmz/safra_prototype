﻿using SAFRA.Domain;
using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PersonalizedExploreSafra", Title = "Explore SAFRA", SectionName = "Personalized")]
    public class PersonalizedExploreSafraController : Controller
    {
        public string Heading { get; set; }
        public string CtaLabel { get; set; }
        public Guid? LinkedPageId { get; set; }
        public Guid? PersonaListId { get; set; }
        public Guid? InterestListId { get; set; }
        public Guid? LocationListId { get; set; }

        private ICategoriesService categoriesService;

        public PersonalizedExploreSafraController(ICategoriesService categoriesService)
        {
            this.categoriesService = categoriesService;
        }

        public ActionResult Index()
        {
            var model = new PersonalizedExploreSafraModel
            {
                Heading = Heading,
                CtaLink = new PageLinkModel(CtaLabel, LinkedPageId)
            };

            if (PersonaListId.HasValue && PersonaListId.Value != Guid.Empty)
            {
                var filters = categoriesService.GetCategories(PersonaListId.Value, "personas");

                foreach (var filter in filters)
                {
                    model.PersonaFilters.Add(filter.CategoryId, filter.DisplayName);
                }
            }

            if (InterestListId.HasValue && InterestListId.Value != Guid.Empty)
            {
                var filters = categoriesService.GetParentCategories(InterestListId.Value, "Categories");

                foreach (var filter in filters)
                {
                    model.InterestFilters.Add(filter.CategoryId, filter.DisplayName);
                }
            }

            if (LocationListId.HasValue && LocationListId.Value != Guid.Empty)
            {
                var filters = categoriesService.GetCategories(LocationListId.Value, "locations");

                foreach (var filter in filters)
                {
                    model.LocationFilters.Add(filter.CategoryId, filter.DisplayName);
                }
            }

            return View("Index", model);
        }

    }
}