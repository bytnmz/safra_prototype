﻿using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "LocationNotification", Title = "Location Notification", SectionName = "Header")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class HeaderLocationNotificationController : Controller
    {
        public string NotificationText { get; set; }

        public HeaderLocationNotificationController()
        {
            if (string.IsNullOrEmpty(NotificationText))
            {
                NotificationText = "We have noticed that you are near [clubName] club, would you like to visit the club page instead?";
            }
        }

        public ActionResult Index()
        {
            return View("Index", (object)NotificationText);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}