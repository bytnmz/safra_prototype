﻿using System;
using System.Web.Mvc;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "SectionHeader", Title = "Section Header", SectionName = "Landing")]
    public class SectionHeaderWidgetController : Controller
    {
        public string Heading { get; set; }

        public bool HasCta { get; set; }
        public string CtaLabel { get; set; }
        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public ActionResult Index()
        {
            var model = new SectionHeaderModel
            {
                Heading = Heading,
                HasCta = HasCta
            };

            if (HasCta)
            {
                model.CtaLink = new PageLinkModel(CtaLabel, IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);
            }

            return View("Index", model);
        }
    }
}