﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Button", Title = "Button", SectionName = "ContentToolboxSection")]
    public class ButtonController : Controller
    {
        public string TemplateName { get; set; }
        public string CtaLabel { get; set; }
        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public ActionResult Index()
        {
            var model = new PageLinkModel(CtaLabel, IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);

            if (TemplateName.IsNullOrWhitespace())
            {
                TemplateName = "Index";
            }

            return View(TemplateName, model);
        }
    }
}