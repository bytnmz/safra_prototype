﻿using System;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "PersonalizedClubWhatsOn", Title = "Club What's On", SectionName = "Personalized")]
    public class PersonalizedClubWhatsOnController : Controller
    {
        public Guid? LocationId { get; set; }

        public string LocationsTaxonId
        {
            get
            {
                return "1047af5d-9044-4a5e-8c3e-4b55bfc9a060";
            }
        }

        public ActionResult Index()
        {
            var locationName = string.Empty;

            if (LocationId.HasValue && LocationId.Value != Guid.Empty)
            {
                var categoriesTaxon = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                    .SingleOrDefault(t => t.Name == "locations");

                if (categoriesTaxon != null)
                {
                    locationName = categoriesTaxon.Taxa.FirstOrDefault(t => t.Id == LocationId)?.Name;
                }
            }

            return View("Index", (object)locationName);
        }
    }
}