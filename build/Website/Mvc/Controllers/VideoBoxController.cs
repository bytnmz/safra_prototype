﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "VideoBox", Title = "Video Box", SectionName = "ContentToolboxSection")]
    public class VideoBoxController : Controller
    {
        public string VideoLink { get; set; }
        public Guid? ImageId { get; set; }

        public ActionResult Index()
        {
            var model = new VideoBoxModel
            {
                VideoLink = VideoLink
            };

            if (ImageId.HasValue && ImageId.Value != Guid.Empty)
            {
                var librariesManager = LibrariesManager.GetManager();

                model.PreviewImage = new ImageModel(ImageId.Value, librariesManager);
                model.HasPreviewImage = true;
            }
            else
            {
                if (!VideoLink.IsNullOrWhitespace() && VideoLink.Contains("youtube"))
                {
                    var link = VideoLink.TrimEnd('/');
                    var id = link.Substring(link.LastIndexOfAny(new char[] { '/', '=' }) + 1);

                    if (!id.IsNullOrWhitespace())
                    {
                        model.YoutubeMaxResUrl = string.Format("https://i3.ytimg.com/vi/{0}/maxresdefault.jpg", id);
                        model.YoutubeHqUrl = string.Format("https://i3.ytimg.com/vi/{0}/hqdefault.jpg", id);
                    }
                }
            }

            return View("Index", model);
        }
    }
}