﻿using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Careers", Title = "Careers", SectionName = "ContentToolboxSection")]
    public class CareersController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }
    }
}