﻿using Newtonsoft.Json;
using SAFRA.Mvc.Models;
using SAFRA.Mvc.Models.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "IconCardsSection", Title = "Icon Cards Section", SectionName = "ContentToolboxSection")]
    public class IconCardsSectionController : Controller
    {
        public string Heading { get; set; }

        public string IconCardsJson { get; set; }

        public bool HasCta { get; set; }
        public string CtaLabel { get; set; }
        public bool IsPageSelectMode { get; set; }
        public Guid? LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public string TemplateName { get; set; }

        public IconCardsSectionController()
        {
            if (string.IsNullOrEmpty(IconCardsJson))
            {
                var iconCards = new ImageTextLinkItem[4] { new ImageTextLinkItem(),
                    new ImageTextLinkItem(), new ImageTextLinkItem(), new ImageTextLinkItem() };

                IconCardsJson = JsonConvert.SerializeObject(iconCards);
            }
        }

        public ActionResult Index()
        {
            var model = new IconCardsSectionModel
            {
                Heading = Heading,
                HasCta = HasCta
            };

            if (HasCta)
            {
                model.CtaLink = new PageLinkModel(CtaLabel, IsPageSelectMode, LinkedPageId, LinkedUrl, OpenInNewTab);
            }

            var items = JsonConvert.DeserializeObject<IEnumerable<ImageTextLinkItem>>(IconCardsJson);

            var librariesManager = LibrariesManager.GetManager();

            foreach (var item in items)
            {
                if (!string.IsNullOrWhiteSpace(item.ImageId))
                {
                    model.IconCards.Add(new IconCardModel
                    {
                        Subtitle = item.Subtitle,
                        Icon = new ImageModel(new Guid(item.ImageId), librariesManager),
                        Link = new PageLinkModel(new LinkItem(item.Title, item.IsPageSelectMode, item.LinkedPageId, item.LinkedUrl, item.OpenInNewTab))
                    });
                }
            }

            if (TemplateName.IsNullOrWhitespace())
            {
                TemplateName = "Home";
            }

            return View(TemplateName, model);
        }
    }
}