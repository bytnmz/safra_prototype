﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SAFRA.Domain;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.ContentLocations;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Builder.Model;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web;
using Telerik.Sitefinity.Web.UI;
using Telerik.Sitefinity.Web.UI.ContentUI.Contracts;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "FeaturedDealsOfTheMonth", Title = "Featured Deals of the Month", SectionName = "Telerik.Sitefinity.DynamicTypes.Model.Promotions")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class PromotionsFeatOfTheMonthController : Controller, IContentLocatableView, IHasCacheDependency
    {
        public string Heading { get; set; }
        public int Count { get; set; }
        public string Cta { get; set; }
        public Guid PageId { get; set; }
        private IPromotionsService promotionsService;

        public bool? DisableCanonicalUrlMetaTag
        {
            get
            {
                return true;
            }

            set { }
        }

        public IEnumerable<IContentLocationInfo> GetLocations()
        {
            var location = new ContentLocationInfo();

            var type = TypeResolutionService.ResolveType(ContentTypeConstants.Promotion);
            location.ContentType = type;

            var manager = DynamicModuleManager.GetManager();
            var providerName = manager.Provider.Name;
            location.ProviderName = providerName;

            yield return location;
        }

        public IList<CacheDependencyKey> GetCacheDependencyObjects()
        {
            var contentType = TypeResolutionService.ResolveType(ContentTypeConstants.Promotion);
            var cacheDependencyNotifiedObjects = new List<CacheDependencyKey>();
            var manager = DynamicModuleManager.GetManager();

            string applicationName = manager != null && manager.Provider != null ? manager.Provider.ApplicationName : string.Empty;
            cacheDependencyNotifiedObjects.Add(new CacheDependencyKey { Key = string.Concat(ContentLifecycleStatus.Live.ToString(), applicationName, contentType.FullName), Type = typeof(DynamicContent) });
            cacheDependencyNotifiedObjects.Add(new CacheDependencyKey() { Key = contentType.FullName, Type = typeof(DynamicModule) });

            return cacheDependencyNotifiedObjects;
        }

        public PromotionsFeatOfTheMonthController(IPromotionsService promotionsService)
        {
            this.promotionsService = promotionsService;
        }

        public ActionResult Index()
        {
            SubscribeCacheDependency();

            var model = new PromotionsFeatOfTheMonthModel
            {
                Heading = Heading ?? "Featured Promotions",
            };

            var promotions = promotionsService.GetFeaturedOfTheMonth(Count);

            foreach (var item in promotions)
            {
                model.Promotions.Add(GetModel(item));
            }

            if (!string.IsNullOrWhiteSpace(Cta) && PageId != Guid.Empty)
            {
                model.Link = new PageLinkModel(Cta, PageId);
            }

            return View("Index", model);
        }

        private PromotionModel GetModel(DynamicContent item)
        {
            var model = new PromotionModel
            {
                MerchantName = item.GetString("MerchantName"),
                OverviewDescription = item.GetString("OverviewDescription")
            };

            var image = item.GetValue<Image>("OverviewImage");

            if (image != null)
            {
                model.Image.Url = image.ResolveMediaUrl();
                model.Image.AltText = image.AlternativeText;
            }

            var tagChoice = item.GetValue<ChoiceOption>("TagLabel");

            if (tagChoice.PersistedValue == "1")
            {
                model.TagClass = "card__type--red";
            }
            else if (tagChoice.PersistedValue == "2")
            {
                model.TagClass = "card__type--blue";
            }
            else if (tagChoice.PersistedValue == "3")
            {
                model.TagClass = "card__type--orange";
            }

            model.TagText = tagChoice.Text;

            var startDate = item.GetValue<DateTime?>("StartDate");

            if (startDate.HasValue)
            {
                model.StartDate = startDate.Value.ToLocalTime().ToString("dd MMM yyyy");
            }

            var endDate = item.GetValue<DateTime?>("EndDate");

            if (endDate.HasValue)
            {
                model.EndDate = endDate.Value.ToLocalTime().ToString("dd MMM yyyy");
            }

            model.Url = "/promotions/" + item.UrlName; 

            return model;
        }

        private void SubscribeCacheDependency()
        {
            var contextItems = SystemManager.CurrentHttpContext.Items;
            var isBackendRequest = contextItems.Contains(SystemManager.IsBackendRequestKey) && ((bool)contextItems[SystemManager.IsBackendRequestKey]);

            if (isBackendRequest)
            {
                return;
            }

            if (!contextItems.Contains(PageCacheDependencyKeys.PageData))
            {
                contextItems.Add(PageCacheDependencyKeys.PageData, new List<CacheDependencyKey>());
            }

            var existingKeys = (List<CacheDependencyKey>)contextItems[PageCacheDependencyKeys.PageData];
            var keysToAdd = GetCacheDependencyObjects();

            existingKeys.AddRange(keysToAdd);
        }
    }
}