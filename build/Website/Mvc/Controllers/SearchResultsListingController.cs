﻿using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "SearchResultsListing", Title = "Results Listing", SectionName = "Search")]
    public class SearchResultsListingController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }
    }
}