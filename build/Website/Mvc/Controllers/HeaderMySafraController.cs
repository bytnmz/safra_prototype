﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using SAFRA.Api.Models;
using SAFRA.Mvc.Models;
using Telerik.DigitalExperienceCloud.Client;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.DataIntelligenceConnector.Configuration;
using Telerik.Sitefinity.DataIntelligenceConnector.EventHandlers;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Web.UI;
using static SAFRA.Api.Models.MySafraMember;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HeaderMySafra", Title = "mySAFRA", SectionName = "Header")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class HeaderMySafraController : Controller
    {
        public string SignUpLabel { get; set; }
        public string SignUpUrl { get; set; }
        public bool SignUpOpenInNewTab { get; set; }
        public string LoginLabel { get; set; }
        public string LoginUrl { get; set; }
        public bool LoginOpenInNewTab { get; set; }

        private string trackId => Request.Cookies["sf-data-intell-subject"] != null ? Request.Cookies["sf-data-intell-subject"].Value.ToString() : string.Empty;

        public ActionResult Index()
        {
            var model = new HeaderMySafraModel
            {
                SignUp = new PageLinkModel(SignUpLabel ?? "Sign up for mySAFRA", SignUpUrl ?? "https://mysafra.safra.sg/web/register.aspx", SignUpOpenInNewTab),
                Login = new PageLinkModel(LoginLabel ?? "Login to mySAFRA", LoginUrl ?? "https://mysafra.safra.sg/web/login.aspx", LoginOpenInNewTab)
             };

            CheckMySafraDecContactsInMemoryCache();
            return View("Index", model);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }

        private void CheckMySafraDecContactsInMemoryCache()
        {
            var cache = MemoryCache.Default;
            List<MemberDetails> decContacts = (List<MemberDetails>)cache.Get("mysafra-dec-contacts");

            if (decContacts != null)
                CompareTrackIds(decContacts);       
        }

        private void CompareTrackIds(List<MemberDetails> decContacts)
        {
            foreach (var contact in decContacts)
            {
                if (contact.TrackId == trackId)
                {
                    AddToCookie(contact);
                    RemoveMySafraDecContactFromMemoryCache(contact, decContacts);
                }
            }
        }

        private void RemoveMySafraDecContactFromMemoryCache(MemberDetails contact, List<MemberDetails> decContacts)
        {
            decContacts.Remove(contact);
            MemoryCache.Default.AddOrGetExisting("mysafra-dec-contacts", decContacts, DateTime.Now.AddMinutes(10));
        }

        private void AddToCookie(MemberDetails member)
        {
            HttpCookie cookie = new HttpCookie("memberId", member.Email);
            // update the expiration timestamp
            cookie.Expires = DateTime.UtcNow.AddDays(30);

            // overwrite the cookie
            Response.Cookies.Add(cookie);
        }

    }
}