﻿using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Error404", Title = "404 Error", SectionName = "CustomError")]
    public class Error404Controller : Controller
    {
        public ActionResult Index()
        {
            Response.Status = "404 Not Found";
            Response.StatusCode = 404;
            Response.StatusDescription = "Not Found";

            return View("Index");
        }
    }
}