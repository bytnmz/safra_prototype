﻿using SAFRA.Mvc.Models;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "CookieMessage", Title = "Cookie Message", SectionName = "Site-wide")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class CookieMessageController : Controller
    {
        public string Message { get; set; }

        public ActionResult Index()
        {
            var message = Message.UpdateLinks() ?? string.Empty;

            return View("Index", (object)message);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}