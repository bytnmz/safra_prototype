﻿using SAFRA.Mvc.Models;
using System;
using System.Web.Mvc;
using Telerik.Sitefinity.Frontend.Mvc.Helpers;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HeaderSiteLogo", Title = "Site Logo", SectionName = "Header")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class HeaderSiteLogoController : Controller
    {
        public string ImageProviderName { get; set; }
        public Guid? WhiteLogoImageId { get; set; }
        public Guid? DarkLogoImageId { get; set; }
        public Guid? LinkedPageId { get; set; }

        public ActionResult Index()
        {
            var model = new HeaderSiteLogoModel();

            var librariesManager = LibrariesManager.GetManager(ImageProviderName);

            if (librariesManager != null)
            {
                if (WhiteLogoImageId.HasValue && WhiteLogoImageId.Value != Guid.Empty)
                {
                    model.WhiteLogo = new ImageModel(WhiteLogoImageId.Value, librariesManager);
                }
                if (DarkLogoImageId.HasValue && DarkLogoImageId.Value != Guid.Empty)
                {
                    model.DarkLogo = new ImageModel(DarkLogoImageId.Value, librariesManager);
                }
            }

            if (LinkedPageId.HasValue && LinkedPageId.Value != Guid.Empty)
            {
                model.Url = HyperLinkHelpers.GetFullPageUrl(LinkedPageId.Value);
            }

            return View("Index", model);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}