﻿using System.Web.Mvc;
using SAFRA.Mvc.Models;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "HeaderSocialLinks", Title = "Social Links", SectionName = "Header")]
    [IndexRenderMode(IndexRenderModes.NoOutput)]
    public class HeaderSocialLinksController : Controller
    {
        public string ConnectWithUsLabel { get; set; }
        public string InstagramUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }

        public ActionResult Index()
        {
            var model = new HeaderSocialLinksModel
            {
                Label = ConnectWithUsLabel ?? "Connect with us",
                InstagramUrl = InstagramUrl,
                FacebookUrl = FacebookUrl,
                TwitterUrl = TwitterUrl
            };

            return View("Index", model);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            ActionInvoker.InvokeAction(ControllerContext, "Index");
        }
    }
}