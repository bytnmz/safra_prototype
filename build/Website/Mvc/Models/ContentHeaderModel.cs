﻿namespace SAFRA.Mvc.Models
{
    public class ContentHeaderModel
    {
        public string Title { get; set; }
        public ImageModel Image { get; set; }
    }
}