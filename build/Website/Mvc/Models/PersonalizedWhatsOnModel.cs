﻿namespace SAFRA.Mvc.Models
{
    public class PersonalizedWhatsOnModel
    {
        public string Heading { get; set; }
        public string Description { get; set; }
        public PageLinkModel CtaLink { get; set; }
        public ImageModel BackgroundImage { get; set; }
    }
}