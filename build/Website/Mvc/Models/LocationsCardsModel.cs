﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class LocationCardsModel
    {
        public string Heading { get; set; }
        public IList<LocationCardModel> Locations { get; set; }

        public LocationCardsModel()
        {
            Heading = string.Empty;
            Locations = new List<LocationCardModel>();
        }
    }

    public class LocationCardModel
    {
        public string Title { get; set; }
        public ImageModel Image { get; set; }
        public PageLinkModel Link { get; set; }
        public Dictionary<string, string> OpeningHours { get; set; }

        public LocationCardModel()
        {
            Title = string.Empty;
            Image = new ImageModel();
            Link = new PageLinkModel();
            OpeningHours = new Dictionary<string, string>();
        }
    }
}