﻿namespace SAFRA.Mvc.Models
{
    public class VideoBoxModel
    {
        public string VideoLink { get; set; }
        public bool HasPreviewImage { get; set; }
        public ImageModel PreviewImage { get; set; }
        public string YoutubeMaxResUrl { get; set; }
        public string YoutubeHqUrl { get; set; }
    }
}