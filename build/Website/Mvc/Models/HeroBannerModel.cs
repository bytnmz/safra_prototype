﻿namespace SAFRA.Mvc.Models
{
    public class HeroBannerModel
    {
        public ImageModel BannerImage { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public bool HasCta { get; set; }
        public PageLinkModel CtaLink { get; set; }     
        public bool HasBannerHyperlink { get; set; }
        public PageLinkModel BannerHyperlink { get; set; }

        public HeroBannerModel()
        {
            BannerImage = new ImageModel();
            CtaLink = new PageLinkModel();
            BannerHyperlink = new PageLinkModel();
        }
    }
}