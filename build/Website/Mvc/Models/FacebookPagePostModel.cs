﻿namespace SAFRA.Mvc.Models
{
    public class FacebookPagePostModel
    {
        public string PostUrl { get; set; }
        public string ImageSrc { get; set; }
    }
}