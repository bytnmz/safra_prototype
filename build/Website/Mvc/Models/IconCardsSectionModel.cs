﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class IconCardsSectionModel
    {
        public string Heading { get; set; }
        public IList<IconCardModel> IconCards { get; set; }
        public bool HasCta { get; set; }
        public PageLinkModel CtaLink { get; set; }

        public IconCardsSectionModel()
        {
            IconCards = new List<IconCardModel>();
        }
    }

    public class IconCardModel
    {
        public ImageModel Icon { get; set; }
        public PageLinkModel Link { get; set; }
        public string Subtitle { get; set; }

        public IconCardModel()
        {
            Icon = new ImageModel();
            Link = new PageLinkModel();
        }
    }
}