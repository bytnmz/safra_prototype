﻿namespace SAFRA.Mvc.Models
{
    public class HomepageNotificationModel
    {
        public string Title { get; set; }
        public ImageModel Image { get; set; }
        public PageLinkModel Link { get; set; }

        public HomepageNotificationModel()
        {
            Image = new ImageModel();
            Link = new PageLinkModel();
        }
    }
}