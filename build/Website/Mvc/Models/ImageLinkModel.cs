﻿namespace SAFRA.Mvc.Models
{
    public class ImageLinkModel
    {
        public ImageModel Image { get; set; }
        public string Link { get; set; }

        public ImageLinkModel()
        {
            Image = new ImageModel();
        }
    }
}