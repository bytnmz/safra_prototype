﻿namespace SAFRA.Mvc.Models.Serialization
{
    public class LinkItem
    {
        public string Label { get; set; }
        public bool IsPageSelectMode { get; set; }
        public string LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public LinkItem()
        {
            Label = string.Empty;
            IsPageSelectMode = true;
            LinkedPageId = string.Empty;
            LinkedUrl = string.Empty;
        }

        public LinkItem(string label, bool isPageSelectMode, string linkedPageId, string linkedUrl, bool openInNewTab)
        {
            Label = label;
            IsPageSelectMode = isPageSelectMode;
            LinkedPageId = linkedPageId;
            LinkedUrl = linkedUrl;
            OpenInNewTab = openInNewTab;
        }

        public LinkItem(bool isPageSelectMode, string linkedPageId, string linkedUrl, bool openInNewTab)
        {
            IsPageSelectMode = isPageSelectMode;
            LinkedPageId = linkedPageId;
            LinkedUrl = linkedUrl;
            OpenInNewTab = openInNewTab;
        }
    }
}