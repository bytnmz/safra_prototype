﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models.Serialization
{
    public class MenuItem
    {
        public string Type { get; set; } // Listing, Page or Links

        // Page and its children; or links
        public string PageId { get; set; }

        // Listing Page
        public string ListId { get; set; }
        public string ListingPageId { get; set; }

        // Links
        public LinkItem TopLink1 { get; set; }
        public List<LinkItem> LinksGroup1 { get; set; }
        public LinkItem TopLink2 { get; set; }
        public List<LinkItem> LinksGroup2 { get; set; }

        public bool HasFeatured { get; set; }
        public ImageTextLinkItem Featured { get; set; }

        public MenuItem()
        {
            Type = "Listing"; 
            PageId = string.Empty;
            ListId = string.Empty;
            ListingPageId = string.Empty;
            TopLink1 = new LinkItem();
            LinksGroup1 = new List<LinkItem>();
            TopLink2 = new LinkItem();
            LinksGroup2 = new List<LinkItem>();
            HasFeatured = false;
            Featured = new ImageTextLinkItem();
        }
    }
}