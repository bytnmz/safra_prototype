﻿namespace SAFRA.Mvc.Models.Serialization
{
    public class ContactItem
    {
        public string Name { get; set; }
        public string Contact { get; set; }
    }
}