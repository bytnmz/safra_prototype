﻿namespace SAFRA.Mvc.Models.Serialization
{
    public class ImageTextItem
    {
        public string Title { get; set; }
        public string ImageId { get; set; }

        public ImageTextItem()
        {
            Title = string.Empty;
            ImageId = string.Empty;
        }
    }
}