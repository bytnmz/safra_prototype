﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models.Serialization
{
    public class LocationItem
    {
        public string Title { get; set; }
        public string ImageId { get; set; }

        public bool IsPageSelectMode { get; set; }
        public string LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }
        
        public List<OpeningHoursItem> OpeningHours { get; set; }

        public LocationItem()
        {
            Title = string.Empty;
            ImageId = string.Empty;

            IsPageSelectMode = true;
            LinkedPageId = string.Empty;
            LinkedUrl = string.Empty;

            OpeningHours = new List<OpeningHoursItem> { new OpeningHoursItem() };
        }
    }

    public class OpeningHoursItem
    {
        public string Day { get; set; }
        public string Time { get; set; }

        public OpeningHoursItem()
        {
            Day = string.Empty;
            Time = string.Empty;
        }
    }
}