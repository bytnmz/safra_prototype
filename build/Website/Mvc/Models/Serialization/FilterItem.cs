﻿namespace SAFRA.Mvc.Models.Serialization
{
    public class FilterItem
    {
        public bool IsCheckbox { get; set; }
        public string ListId { get; set; }
        public string Name { get; set; }
        public string HeaderText { get; set; }
        public string AllText { get; set; }

        public FilterItem(string name)
        {
            Name = name;
        }
    }
}