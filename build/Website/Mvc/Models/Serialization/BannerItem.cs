﻿using System;

namespace SAFRA.Mvc.Models.Serialization
{
    public class BannerItem
    {
        public string ImageId { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public bool HasBannerHyperlink { get; set; }
        public bool IsPageSelectMode { get; set; }
        public string LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public BannerItem()
        {
            ImageId = string.Empty;
            Title = string.Empty;
            Subtitle = string.Empty;
            IsPageSelectMode = true;
            LinkedPageId = string.Empty;
            LinkedUrl = string.Empty;
            OpenInNewTab = false;
        }
    }
}