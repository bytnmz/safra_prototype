﻿namespace SAFRA.Mvc.Models.Serialization
{
    public class AnnouncementItem
    {
        public string Title { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool HasLink { get; set; }
        public bool IsPageSelectMode { get; set; }
        public string LinkedPageId { get; set; }
        public string LinkedUrl { get; set; }
    }
}