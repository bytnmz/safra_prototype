﻿namespace SAFRA.Mvc.Models
{
    public class PromotionModel
    {
        public string MerchantName { get; set; }
        public string OverviewDescription { get; set; }
        public ImageModel Image { get; set; }
        public string TagClass { get; set; }
        public string TagText { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Url { get; set; }

        public PromotionModel()
        {
            MerchantName = string.Empty;
            OverviewDescription = string.Empty;
            Image = new ImageModel();
            TagClass = string.Empty;
            TagText = string.Empty;
            StartDate = string.Empty;
            EndDate = string.Empty;
            Url = string.Empty;
        }
    }
}