﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class HeaderDropdownModel
    {
        public string Label { get; set; }
        public List<PageLinkModel> Links { get; set; }

        public HeaderDropdownModel()
        {
            Links = new List<PageLinkModel>();
        }
    }
}