﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class PersonalizedExploreSafraModel
    {
        public string Heading { get; set; }
        public PageLinkModel CtaLink { get; set; }

        public Dictionary<string, string> PersonaFilters { get; set; }
        public Dictionary<string, string> InterestFilters { get; set; }
        public Dictionary<string, string> LocationFilters { get; set; }

        public PersonalizedExploreSafraModel()
        {
            PersonaFilters = new Dictionary<string, string>
            {
                { "all", "All" }
            };

            InterestFilters = new Dictionary<string, string>
            {
                { "all", "All" }
            };

            LocationFilters = new Dictionary<string, string>
            {
                { "all", "All" }
            };
        }
    }
}