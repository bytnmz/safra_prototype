﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class PromotionsFeatOfTheMonthModel
    {
        public string Heading { get; set; }
        public PageLinkModel Link { get; set; }
        public List<PromotionModel> Promotions { get; set; }

        public PromotionsFeatOfTheMonthModel()
        {
            Heading = string.Empty;
            Promotions = new List<PromotionModel>();
        }
    }
}