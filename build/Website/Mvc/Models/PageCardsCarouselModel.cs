﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class PageCardsCarouselModel
    {
        public string Heading { get; set; }
        public bool HasCta { get; set; }
        public PageLinkModel CtaLink { get; set; }
        public List<ImageCardModel> Pages { get; set; }

        public PageCardsCarouselModel()
        {
            Pages = new List<ImageCardModel>();
        }
    }
}