﻿using System;
using System.Linq;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules;
using Telerik.Sitefinity.Modules.Libraries;

namespace SAFRA.Mvc.Models
{
    public class ImageModel
    {
        public string Url { get; set; }
        public string AltText { get; set; }

        public ImageModel()
        {
            Url = string.Empty;
            AltText = string.Empty;
        }

        public ImageModel(Guid id, LibrariesManager librariesManager)
        {
            var image = librariesManager.GetImages()
                .Where(i => i.Id == id).FirstOrDefault(PredefinedFilters.PublishedItemsFilter<Image>());

            if (image != null)
            {
                Url = image.ResolveMediaUrl();
                AltText = image.AlternativeText;
            }
            else
            {
                Url = string.Empty;
                AltText = string.Empty;
            }
        }
    }
}