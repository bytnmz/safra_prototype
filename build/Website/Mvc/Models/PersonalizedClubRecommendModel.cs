﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class PersonalizedClubRecommendModel
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Location { get; set; }

        public Dictionary<string, string> PersonaFilters { get; set; }
        public Dictionary<string, string> DurationFilters { get; set; }
        public Dictionary<string, string> InterestFilters { get; set; }

        public PersonalizedClubRecommendModel()
        {
            PersonaFilters = new Dictionary<string, string>();
            DurationFilters = new Dictionary<string, string>();
            InterestFilters = new Dictionary<string, string>();
        }
    }
}