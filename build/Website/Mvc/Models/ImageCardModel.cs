﻿namespace SAFRA.Mvc.Models
{
    public class ImageCardModel
    {
        public ImageModel Image { get; set; }
        public PageLinkModel Link { get; set; }

        public ImageCardModel()
        {
            Image = new ImageModel();
            Link = new PageLinkModel();
        }

        public ImageCardModel(ImageModel image, PageLinkModel pageLink)
        {
            Image = image;
            Link = pageLink;
        }
    }
}