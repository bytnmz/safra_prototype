﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Telerik.Sitefinity.Modules.GenericContent;
using Telerik.Sitefinity.Web.Utilities;

namespace SAFRA.Mvc.Models
{
    public static class StringExtensions
    {
        public static string UpdateLinks(this string content)
        {
            var fixedHtml = LinkParser.UnresolveLinks(content);
            return LinkParser.ResolveLinks(fixedHtml, DynamicLinksParser.GetContentUrl, null, false);
        }

        public static string GenerateSlug(this string phrase)
        {
            string str = phrase.RemoveDiacritics().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        private static string RemoveDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}