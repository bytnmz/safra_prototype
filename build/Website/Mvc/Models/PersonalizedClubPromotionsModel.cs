﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class PersonalizedClubPromotionsModel
    {
        public string Heading { get; set; }
        public string Location { get; set; }
        public Dictionary<string, string> InterestFilters { get; set; }
        public PageLinkModel CtaLink1 { get; set; }
        public PageLinkModel CtaLink2 { get; set; }

        public PersonalizedClubPromotionsModel()
        {
            InterestFilters = new Dictionary<string, string>();
            CtaLink1 = new PageLinkModel();
            CtaLink2 = new PageLinkModel();
        }
    }
}