﻿namespace SAFRA.Mvc.Models
{
    public class AnnouncementWidgetModel
    {
        public string Title { get; set; }
        public bool HasLink { get; set; }
        public PageLinkModel Link { get; set; }

        public AnnouncementWidgetModel()
        {
            Link = new PageLinkModel();
        }
    }
}