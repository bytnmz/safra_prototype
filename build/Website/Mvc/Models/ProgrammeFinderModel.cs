﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class ProgrammeFinderModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public IDictionary<string, List<ImageCardModel>> Programmes { get; set; }

        public ProgrammeFinderModel()
        {
            Programmes = new Dictionary<string, List<ImageCardModel>>();
        }
    }
}