﻿namespace SAFRA.Mvc.Models
{
    public class SectionHeaderModel
    {
        public string Heading { get; set; }
        public bool HasCta { get; set; }
        public PageLinkModel CtaLink { get; set; }
    }
}