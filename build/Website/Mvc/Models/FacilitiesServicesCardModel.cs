﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class FacilitiesServicesCardModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public PageLinkModel ViewAllLink { get; set; }
        public List<ImageCardModel> FacilitiesServices { get; set; }

        public FacilitiesServicesCardModel()
        {
            Title = string.Empty;
            Description = string.Empty;
            ViewAllLink = new PageLinkModel();
            FacilitiesServices = new List<ImageCardModel>();
        }
    }
}