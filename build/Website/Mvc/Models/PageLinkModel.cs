﻿using SAFRA.Mvc.Models.Serialization;
using System;
using Telerik.Sitefinity.Frontend.Mvc.Helpers;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Mvc.Models
{
    public class PageLinkModel
    {
        public string Label { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }

        public PageLinkModel()
        {
            Label = string.Empty;
            Url = string.Empty;
            Target = string.Empty;
        }

        public PageLinkModel(string label, string url, bool openInNewTab = false)
        {
            Label = label;
            Url = ControlUtilities.SanitizeUrl(url);
            Target = openInNewTab ? "target=_blank" : string.Empty;
        }

        public PageLinkModel(string label, Guid? pageId, bool openInNewTab = false)
        {
            Label = label;

            if (pageId.HasValue && pageId.Value != Guid.Empty)
            {
                Url = HyperLinkHelpers.GetFullPageUrl(pageId.Value);
            }
            else
            {
                Url = string.Empty;
            }

            Target = openInNewTab ? "target=_blank" : string.Empty;
        }

        public PageLinkModel(string label, bool isPageSelectMode, Guid? linkedPageId, string linkedUrl, bool openInNewTab = false)
        {
            Label = label;

            if (isPageSelectMode && linkedPageId.HasValue && linkedPageId.Value != Guid.Empty)
            {
                Url = HyperLinkHelpers.GetFullPageUrl(linkedPageId.Value);
            }
            else if (!isPageSelectMode)
            {
                Url = ControlUtilities.SanitizeUrl(linkedUrl);
            }
            else
            {
                Url = string.Empty;
            }

            Target = openInNewTab ? "target=_blank" : string.Empty;
        }

        //for banner hyperlink
        public PageLinkModel(bool isPageSelectMode, Guid? linkedPageId, string linkedUrl, bool openInNewTab = false)
        {
            if (isPageSelectMode && linkedPageId.HasValue && linkedPageId.Value != Guid.Empty)
            {
                Url = HyperLinkHelpers.GetFullPageUrl(linkedPageId.Value);
            }
            else if (!isPageSelectMode)
            {
                Url = ControlUtilities.SanitizeUrl(linkedUrl);
            }
            else
            {
                Url = string.Empty;
            }

            Target = openInNewTab ? "target=_blank" : string.Empty;
        }

        public PageLinkModel(LinkItem item)
        {
            Label = item.Label;

            if (item.IsPageSelectMode && !string.IsNullOrWhiteSpace(item.LinkedPageId))
            {
                Url = HyperLinkHelpers.GetFullPageUrl(new Guid(item.LinkedPageId));
            }
            else if (!item.IsPageSelectMode)
            {
                Url = ControlUtilities.SanitizeUrl(item.LinkedUrl);
            }
            else
            {
                Url = string.Empty;
            }

            Target = item.OpenInNewTab ? "target=_blank" : string.Empty;
        }
    }
}