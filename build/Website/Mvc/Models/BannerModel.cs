﻿namespace SAFRA.Mvc.Models
{
    public class BannerModel
    {
        public ImageModel BannerImage { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public bool HasBannerHyperlink { get; set; }
        public PageLinkModel BannerHyperlink { get; set; }
        
        public BannerModel()
        {
            BannerImage = new ImageModel();
            Title = string.Empty;
            Subtitle = string.Empty;
            BannerHyperlink = new PageLinkModel();
        }
    }
}