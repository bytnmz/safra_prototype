﻿namespace SAFRA.Mvc.Models
{
    public class HeaderSocialLinksModel
    {
        public string Label { get; set; }
        public string InstagramUrl { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
    }
}