﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class FeedCustomiserModel
    {
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string CtaText { get; set; }
        public string CustomiseCtaText { get; set; }
        public List<FeedCustomiserOptionModel> Options { get; set; }

        public FeedCustomiserModel()
        {
            Options = new List<FeedCustomiserOptionModel>();
        }
    }

    public class FeedCustomiserOptionModel
    {
        public string Title { get; set; }
        public ImageModel Image { get; set; }

        public FeedCustomiserOptionModel()
        {
            Image = new ImageModel();
        }
    }
}