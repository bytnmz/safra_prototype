﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class PricingSectionModel
    {
        public string Heading { get; set; }
        public IList<PricingCardModel> PricingCards { get; set; }

        public PricingSectionModel()
        {
            Heading = string.Empty;
            PricingCards = new List<PricingCardModel>();
        }
    }

    public class PricingCardModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public decimal Price { get; set; }
        public List<PricingFeatures> Features { get; set; }

        public PricingCardModel()
        {
            Title = string.Empty;
            Content = string.Empty;
            Features = new List<PricingFeatures>();
        }
    }

    public class PricingFeatures
    {
        public bool IsExclusion { get; set; }
        public string Text { get; set; }
    }
}