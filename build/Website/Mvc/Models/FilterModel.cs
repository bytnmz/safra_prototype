﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class FilterModel
    {
        public bool IsCheckbox { get; set; }
        public string Header { get; set; }
        public string AllText { get; set; }
        public IList<KeyValuePair<string, string>> KeyValuePairs { get; set; }
        public bool IsHidden { get; set; }

        public FilterModel()
        {
            KeyValuePairs = new List<KeyValuePair<string, string>>();
        }
    }
}