﻿namespace SAFRA.Mvc.Models
{
    public class HeaderSiteLogoModel
    {
        public string Url { get; set; }
        public ImageModel WhiteLogo { get; set; }
        public ImageModel DarkLogo { get; set; }
    }
}