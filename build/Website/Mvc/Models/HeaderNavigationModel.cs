﻿using System.Collections.Generic;

namespace SAFRA.Mvc.Models
{
    public class HeaderNavigationModel
    {
        public bool Mobile { get; set; }
        public List<NavMenuItem> MenuItems { get; set; }

        public HeaderNavigationModel()
        {
            MenuItems = new List<NavMenuItem>();
        }
    }

    public class NavMenuItem
    {
        public PageLinkModel Item { get; set; }
        public List<PageLinkModel> ChildItems { get; set; }

        public bool IsGroupedLinks { get; set; }
        public List<NavMenuItemGroupedLinks> GroupedLinks { get; set; }

        public bool HasFeatured { get; set; }
        public ImageModel FeaturedImage { get; set; }
        public PageLinkModel FeaturedLink { get; set; }

        public NavMenuItem()
        {
            Item = new PageLinkModel();
            ChildItems = new List<PageLinkModel>();
            GroupedLinks = new List<NavMenuItemGroupedLinks>();
            FeaturedImage = new ImageModel();
            FeaturedLink = new PageLinkModel();
        }
    }

    public class NavMenuItemGroupedLinks
    {
        public PageLinkModel TopLink { get; set; }
        public List<PageLinkModel> Links { get; set; }
    }
}