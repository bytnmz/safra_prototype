﻿namespace SAFRA.Mvc.Models
{
    public class HeaderMySafraModel
    {
        public PageLinkModel SignUp { get; set; }
        public PageLinkModel Login { get; set; }

        public HeaderMySafraModel()
        {
            SignUp = new PageLinkModel();
            Login = new PageLinkModel();
        }
    }
}