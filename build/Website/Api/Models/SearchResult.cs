﻿using System;
using System.Globalization;
using System.Linq;
using Telerik.Sitefinity.Services.Search.Data;

namespace SAFRA.Api.Models
{
    public class SearchResult
    {
        public string ContentType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string HighlightedResult { get; set; }
        public string LastModified { get; set; }

        public SearchResult(IDocument resultDoc)
        {
            ContentType = resultDoc.GetValue("ContentType").ToString()
            .Split('.').LastOrDefault();

            if (ContentType == "PageNode")
            {
                ContentType = "Page";
            }

            Title = resultDoc.GetValue("Title").ToString();

            Description = ContentType.Contains("Page") ? resultDoc.GetValue("Description").ToString()
                : resultDoc.GetValue("OverviewDescription")?.ToString() ?? string.Empty;
                   
            Link = resultDoc.GetValue("Link")?.ToString();

            if (!Link.IsNullOrWhitespace())
            {
                Link = new Uri(Link).PathAndQuery;

                if (ContentType == "Promotion")
                {
                    var urlName = Link.Split('/').LastOrDefault();

                    if (urlName != null)
                    {
                        Link = "/promotions/" + urlName;
                    }
                }
            }

            HighlightedResult = resultDoc.GetValue("HighLighterResult")?.ToString();

            var lastModifiedStr = resultDoc.GetValue("LastModified").ToString().Substring(0, 14);

            DateTime lastModifiedDate;

            if (DateTime.TryParseExact(lastModifiedStr, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastModifiedDate))
            {
                LastModified = lastModifiedDate.ToLocalTime().ToString("dd MMM yyyy");
            }
        }
    }
}