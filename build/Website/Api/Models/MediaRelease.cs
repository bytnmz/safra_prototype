﻿using System;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace SAFRA.Api.Models
{
    public class MediaRelease
    {
        public string Title { get; set; }
        public string Date { get; set; }
        public string Link { get; set; }

        private const string dateFormat = "dd MMM yyyy";

        public MediaRelease(DynamicContent item)
        {
            Title = item.GetString("Title");

            var date = item.GetValue<DateTime?>("Date");

            if (date != null && date.HasValue)
            {
                this.Date = date.Value.ToLocalTime().ToString(dateFormat);
            }

            var pdf = item.GetValue<Document>("Pdf");

            if (pdf != null)
            {
                Link = pdf.ResolveMediaUrl();
            }
        }
    }
}