﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models
{
    public class MySafraMember
    {
        public class MemberDetails : RequestBody
        {
        }

        public class RequestBody
        {
            public string MemberId { get; set; }

            public string TrackId { get; set; }

            public string Email { get; set; }
        }       
    }
}