﻿using System;
using System.Collections.Generic;

namespace SAFRA.Api.Models
{
    public class CalendarDayEvent
    {
        public DateTime DateTime { get; set; }
        public bool IsCurrent { get; set; }
        public int Day { get; set; }
        public string DayOfTheWeek { get; set; }
        public List<EventLink> Events { get; set; }

        public CalendarDayEvent(DateTime dateTime, bool isCurrent = false)
        {
            DateTime = dateTime;
            IsCurrent = isCurrent;
            Day = dateTime.Day;
            DayOfTheWeek = dateTime.ToString("ddd").ToUpperInvariant();
            Events = new List<EventLink>();
        }
    }

    public class EventLink
    {
        public string Name { get; set; }
        public string Url { get; set; }

        public EventLink(string name, string url)
        {
            Name = name;
            Url = url;
        }
    }
}