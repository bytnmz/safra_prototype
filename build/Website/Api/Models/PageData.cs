﻿using System.Collections.Generic;

namespace SAFRA.Api.Models
{
    public class PageData
    {
        public int Page { get; set; }
        public int TotalCount { get; set; }
        public object Data { get; set; }

        public PageData(int page)
        {
            Page = page;
        }
    }
}