﻿using System;
using Telerik.Sitefinity.ContentLocations;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;

namespace SAFRA.Api.Models
{
    public class Article
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImgSrc { get; set; }
        public string ImgAltText { get; set; }
        public string Date { get; set; }
        public string Link { get; set; }

        private const string dateFormat = "dd MMM yyyy";

        public Article(DynamicContent item, IContentLocationService clService)
        {
            Title = item.GetString("Title");
            Description = item.GetString("OverviewDescription");

            var image = item.GetValue<Image>("OverviewImage");

            if (image != null)
            {
                ImgSrc = image.ResolveMediaUrl();
                ImgAltText = image.AlternativeText;
            }

            var date = item.GetValue<DateTime?>("ArticleDate");

            if (date != null && date.HasValue)
            {
                this.Date = date.Value.ToLocalTime().ToString(dateFormat);
            }

            var location = clService.GetItemDefaultLocation(item);
            Link = location.ItemAbsoluteUrl;
            if (!Link.IsNullOrWhitespace())
            {
                Link = new Uri(Link).PathAndQuery;
            }
        }
    }
}