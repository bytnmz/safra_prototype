﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models.DEC
{
    public class ExcelContact
    {
        public string email { get; set; }
        public string tag_code { get; set; }
        public string tag_value { get; set; }
    }
}