﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models.DEC
{
    public class DECPersona
    {
        public List<PersonaItem> items { get; set; }
    }

    public class Fields
    {
        public string Gender { get; set; }
        public string Age { get; set; }
        public string Income { get; set; }
    }

    public class PersonaItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Fields Fields { get; set; }
        public string DistinctiveCharacteristic { get; set; }
        public int State { get; set; }
        public int Threshold { get; set; }
        public string PhotoUrl { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedByUserId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedByUserId { get; set; }
        public int CalculationState { get; set; }
    }    
}