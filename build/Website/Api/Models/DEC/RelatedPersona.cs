﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models.DEC
{
    public class RelatedPersona
    {
        public List<Item> items { get; set; }
    }

    public class Item
    {
        public DateTime Timestamp { get; set; }
        public int PersonaId { get; set; }
    }    
}