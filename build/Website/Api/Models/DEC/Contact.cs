﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models.DEC
{
    public class Contact
    {
        public List<ContactItem> items { get; set; }
    }

    public class ContactItem
    {
        public string ContactIdentifier { get; set; }
        public int ContactId { get; set; }
        public Properties Properties { get; set; }
    }

    public class Properties
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public object Country { get; set; }
        public object Company { get; set; }
        public object JobTitle { get; set; }
        public object Gender { get; set; }
        public object Address { get; set; }
        public object Phone { get; set; }
        public DateTime FirstVisitOn { get; set; }
        public DateTime? LastVisitOn { get; set; }
        public DateTime BecameContactOn { get; set; }
    }    
}