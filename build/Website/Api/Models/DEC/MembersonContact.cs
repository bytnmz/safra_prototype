﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models.DEC
{
    public class MembersonContact
    {
        public int ContactId { get; set; }
        public string Email { get; set; }
        public List<RelatedPersonaDetails> Personas { get; set; }
    }

    public class RelatedPersonaDetails
    {
        public int PersonaId { get; set; }
        public string PersonaName { get; set; }
        public int? Score { get; set; }
    }
}