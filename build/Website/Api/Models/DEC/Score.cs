﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models.DEC
{
    public class Score
    {
        public int ContactId { get; set; }
        public List<PersonaScoring> Personas { get; set; }
    }

    public class PersonaScoring
    {
        public int ScoringId { get; set; }
        public int Score { get; set; }
        public object PassedOn { get; set; }
        public int Threshold { get; set; }
    }
}