﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAFRA.Api.Models.DEC
{
    public class DECAccessToken
    {
        public string Access_Token { get; set; }
        public string Token_Type { get; set; }
        public int Expires_In { get; set; }
        public AccessKeyInfo Access_Key_Info { get; set; }
    }

    public class AccessKeyInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }    
}