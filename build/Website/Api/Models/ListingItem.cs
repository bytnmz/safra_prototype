﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.ContentLocations;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;

namespace SAFRA.Api.Models
{
    public class ListingItem
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string TagClass { get; set; }
        public string Description { get; set; }
        public string ImgSrc { get; set; }
        public string ImgAltText { get; set; }
        public string Location { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Link { get; set; }

        private DynamicContent item;
        private const string dateFormat = "dd MMM yyyy";

        public ListingItem(DynamicContent item, IContentLocationService clService)
        {
            this.item = item;

            Title = item.GetString("Title");

            //specifially for promotion content images on personalised explore safra widget
            var isContentTypePromotion = item.GetType().FullName.ToLower().Equals(ContentTypeConstants.Promotion.ToLower());
            //NewOverviewImage is required for promotions
            var image = isContentTypePromotion ? item.GetValue<Image>("NewOverviewImage") : item.GetValue<Image>("OverviewImage");

            if (image != null)
            {
                ImgSrc = image.ResolveMediaUrl();
                ImgAltText = image.AlternativeText;
            }

            var location = clService.GetItemDefaultLocation(item);
            Link = location?.ItemAbsoluteUrl;
            if (!Link.IsNullOrWhitespace())
            {
                Link = new Uri(Link).PathAndQuery;
            }
        }

        public void SetDescription()
        {
            Description = item.GetString("OverviewDescription");
        }

        public void SetStartEndDates()
        {
            if (item.GetType().FullName.ToLower().Equals(ContentTypeConstants.Facility.ToLower()) || item.GetType().FullName.ToLower().Equals(ContentTypeConstants.Service.ToLower()))
                return;

            var startDate = item.GetValue<DateTime?>("StartDate");

            if (startDate != null && startDate.HasValue)
            {
                StartDate = startDate.Value.ToLocalTime().ToString(dateFormat);
            }

            var endDate = item.GetValue<DateTime?>("EndDate");

            if (endDate != null && endDate.HasValue)
            {
                EndDate = endDate.Value.ToLocalTime().ToString(dateFormat);
            }
        }

        public void SetClubLocation()
        {
            var locationsTaxon = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                    .SingleOrDefault(t => t.Name == "Locations");

            var locationIds = item.GetValue<IList<Guid>>("locations");

            if (locationsTaxon != null && locationIds != null && locationIds.Any())
            {
                var locations = new List<string>();

                foreach (var locationId in locationIds)
                {
                    var taxon = locationsTaxon.Taxa.FirstOrDefault(t => t.Id == locationId);

                    if (taxon != null)
                    {
                        var clubStr = taxon.Title;

                        if (!clubStr.StartsWith("SAFRA"))
                        {
                            clubStr = "SAFRA " + taxon.Title;
                        }

                        locations.Add(clubStr);
                    }
                }

                Location = string.Join(", ", locations);
            }
        }
    }
}