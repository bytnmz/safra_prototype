﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Api
{
    public class ClubsController : ApiController
    {
        public IHttpActionResult Get()
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                var dynamicModuleManager = DynamicModuleManager.GetManager();
                var type = TypeResolutionService.ResolveType(ContentTypeConstants.Club);

                var queryData = dynamicModuleManager.GetDataItems(type)
                        .Where(c => c.Status == ContentLifecycleStatus.Live && c.Visible);

                var result = queryData.ToList().Select(c => new
                {
                    Name = c.GetValue("Title").ToString(),
                    Location = c.GetValue("Address").ToString(),
                    Phone = c.GetValue("PhoneNumber").ToString(),
                    Email = c.GetValue("EmailAddress").ToString(),
                    Coordinates = new Dictionary<string, string>()
                {
                    { "lat", c.GetValue("Latitude").ToString() },
                    { "long", c.GetValue("Longitude").ToString() }
                },
                    Link = "/clubs/" + c.UrlName
                });

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("Clubs API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }
    }
}