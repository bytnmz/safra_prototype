﻿using SAFRA.Api.Models;
using SAFRA.Domain.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.ContentLocations;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Api
{
    public class WhatsOnController : ApiController
    {   
        [Route("api/whatson")]
        [HttpGet]
        public IHttpActionResult GetEvents(string keyword = "", string location = "", string persona = "", string interest = "", int page = 1, int pageSize = 12)
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                keyword = keyword == null ? string.Empty : ControlUtilities.Sanitize(keyword);
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                persona = persona == null ? string.Empty : ControlUtilities.Sanitize(persona);
                interest = interest == null ? string.Empty : ControlUtilities.Sanitize(interest);

                if (keyword.Length > 70 || location.Length > 170 || persona.Length > 170 ||
                    interest.Length > 270 || pageSize > 24 || page > 10000)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new PageData(page);

                var query = new FilterContentService().FilterContent(ContentTypeConstants.WhatsOn, interest, location, persona, keyword)
                    .Where(e => e.GetValue<DateTime?>("RegistrationStartDate") == null || e.GetValue<DateTime?>("RegistrationStartDate") <= DateTime.UtcNow
                    && e.GetValue<DateTime?>("RegistrationEndDate") != null ? (e.GetValue<DateTime?>("RegistrationEndDate") < e.GetValue<DateTime?>("EndDate") ? 
                    e.GetValue<DateTime?>("RegistrationEndDate") > DateTime.UtcNow : e.GetValue<DateTime?>("EndDate") > DateTime.UtcNow) : e.GetValue<DateTime?>("EndDate") > DateTime.UtcNow);

                result.TotalCount = query.Count();

                var items = query.OrderBy(e => e.GetValue<DateTime?>("StartDate"))
                    .Skip((page - 1) * pageSize).Take(pageSize)
                    .ToList();

                result.Data = GetData(items);

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("What's On API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        [Route("api/whatson/open")]
        [HttpGet]
        public IHttpActionResult GetOpenEvents(string interest = "", string location = "", string persona = "", string from = "", string duration = "", int page = 1, int pageSize = 12)
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                persona = persona == null ? string.Empty : ControlUtilities.Sanitize(persona);
                interest = interest == null ? string.Empty : ControlUtilities.Sanitize(interest);
                from = from == null ? string.Empty : ControlUtilities.Sanitize(from);
                duration = duration == null ? string.Empty : ControlUtilities.Sanitize(duration);

                if (location.Length > 170 || persona.Length > 170 || interest.Length > 270 ||
                    from.Length > 15 || duration.Length > 10 || pageSize > 24 || page > 10000)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new PageData(page);

                var query = new FilterContentService().FilterContent(ContentTypeConstants.WhatsOn, interest, location, persona)
                    .Where(e => e.GetValue<DateTime?>("RegistrationStartDate") == null || e.GetValue<DateTime?>("RegistrationStartDate") <= DateTime.UtcNow
                    && e.GetValue<DateTime?>("RegistrationEndDate") != null ? (e.GetValue<DateTime?>("RegistrationEndDate") < e.GetValue<DateTime?>("EndDate") ? 
                    e.GetValue<DateTime?>("RegistrationEndDate") > DateTime.UtcNow : e.GetValue<DateTime?>("EndDate") > DateTime.UtcNow) : e.GetValue<DateTime?>("EndDate") > DateTime.UtcNow);

                if (!from.IsNullOrWhitespace())
                {
                    var dateTime = DateTime.Now;

                    if (from == "currentWeek")
                    {
                        var baseDate = dateTime.Date;
                        var weekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                        var weekEnd = weekStart.AddDays(7).AddSeconds(-1);

                        query = query.Where(e => e.GetValue<DateTime?>("EndDate") > weekStart.ToUniversalTime() &&
                                                e.GetValue<DateTime?>("StartDate") < weekEnd.ToUniversalTime());
                    }
                    else if (from == "currentMonth")
                    {
                        var monthStart = new DateTime(dateTime.Year, dateTime.Month, 1);
                        var monthEnd = monthStart.AddMonths(1).AddSeconds(-1);

                        query = query.Where(e => e.GetValue<DateTime?>("EndDate") > monthStart.ToUniversalTime() &&
                                                e.GetValue<DateTime?>("StartDate") < monthEnd.ToUniversalTime());
                    }
                }

                if (!duration.IsNullOrWhitespace() && duration != "all")
                {
                    query = query.Where(e => e.GetValue<string[]>("Duration").Any(d => duration.Contains(d)));
                }
                 
                result.TotalCount = query.Count();

                var items = query.OrderByDescending(e => e.GetValue<DateTime?>("StartDate") > DateTime.UtcNow)
                    .ThenByDescending(e => e.GetValue<DateTime?>("StartDate"))
                    .Skip((page - 1) * pageSize).Take(pageSize)
                    .ToList();

                result.Data = GetData(items.OrderByDescending(e => e.GetValue<DateTime?>("StartDate") > DateTime.UtcNow)
                    .ThenBy(e => Math.Abs((e.GetValue<DateTime?>("StartDate").GetValueOrDefault() - DateTime.UtcNow).TotalSeconds)).ToList());

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("What's On API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        [Route("api/whatson/explore")]
        [HttpGet]
        public IHttpActionResult GetOpenExplore(string interest = "", string location = "", string persona = "", string from = "", string duration = "", int page = 1, int pageSize = 12)
        {
            //var acceptType = Request.Headers.Accept;
            //if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            //{
            //    HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
            //    notAcceptableResult.Content = new StringContent("Not acceptable");
            //    return ResponseMessage(notAcceptableResult);
            //}

            try
            {
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                persona = persona == null ? string.Empty : ControlUtilities.Sanitize(persona);
                interest = interest == null ? string.Empty : ControlUtilities.Sanitize(interest);
                from = from == null ? string.Empty : ControlUtilities.Sanitize(from);
                duration = duration == null ? string.Empty : ControlUtilities.Sanitize(duration);

                if (location.Length > 170 || persona.Length > 170 || interest.Length > 270 ||
                    from.Length > 15 || duration.Length > 10 || pageSize > 24 || page > 10000)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new PageData(page);

                var whatsOnQuery = new FilterContentService().FilterContent(ContentTypeConstants.WhatsOn, interest, location, persona)
                    .Where(e => (e.GetValue<DateTime?>("RegistrationStartDate") == null || e.GetValue<DateTime?>("RegistrationStartDate") <= DateTime.UtcNow)
                    && (e.GetValue<DateTime?>("RegistrationEndDate") == null || DateTime.UtcNow < e.GetValue<DateTime?>("RegistrationEndDate")));

                var promotionQuery = new FilterContentService().FilterContent(ContentTypeConstants.Promotion, interest, location, persona)
                    .Where(e => (e.GetValue<DateTime?>("StartDate") == null || e.GetValue<DateTime?>("StartDate") <= DateTime.UtcNow)
                    && (e.GetValue<DateTime?>("EndDate") == null || DateTime.UtcNow < e.GetValue<DateTime?>("EndDate")));

                var facilityQuery = new FilterContentService().FilterContent(ContentTypeConstants.Facility, interest, location, persona);
                var serviceQuery = new FilterContentService().FilterContent(ContentTypeConstants.Service, interest, location, persona);

                if (!from.IsNullOrWhitespace())
                {
                    var dateTime = DateTime.Now;

                    if (from == "currentWeek")
                    {
                        var baseDate = dateTime.Date;
                        var weekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                        var weekEnd = weekStart.AddDays(7).AddSeconds(-1);

                        whatsOnQuery = whatsOnQuery.Where(e => e.GetValue<DateTime?>("EndDate") > weekStart.ToUniversalTime() &&
                                                e.GetValue<DateTime?>("StartDate") < weekEnd.ToUniversalTime());

                        promotionQuery = promotionQuery.Where(e => e.GetValue<DateTime?>("EndDate") > weekStart.ToUniversalTime() &&
                                                e.GetValue<DateTime?>("StartDate") < weekEnd.ToUniversalTime());
                    }
                    else if (from == "currentMonth")
                    {
                        var monthStart = new DateTime(dateTime.Year, dateTime.Month, 1);
                        var monthEnd = monthStart.AddMonths(1).AddSeconds(-1);

                        whatsOnQuery = whatsOnQuery.Where(e => e.GetValue<DateTime?>("EndDate") > monthStart.ToUniversalTime() &&
                                                e.GetValue<DateTime?>("StartDate") < monthEnd.ToUniversalTime());

                        promotionQuery = promotionQuery.Where(e => e.GetValue<DateTime?>("EndDate") > monthStart.ToUniversalTime() &&
                                                e.GetValue<DateTime?>("StartDate") < monthEnd.ToUniversalTime());
                    }
                }

                if (!duration.IsNullOrWhitespace() && duration != "all")
                {
                    whatsOnQuery = whatsOnQuery.Where(e => e.GetValue<string[]>("Duration").Any(d => duration.Contains(d)));
                }

                var whatsOnItems = whatsOnQuery.OrderByDescending(e => e.GetValue<DateTime?>("StartDate") > DateTime.UtcNow)
                    .ThenByDescending(e => e.GetValue<DateTime?>("StartDate"))
                    //.Skip((page - 1) * pageSize).Take(pageSize)
                    .ToList();

                var whatsOnData = GetData(whatsOnItems.OrderByDescending(e => e.GetValue<DateTime?>("StartDate") > DateTime.UtcNow)
                    .ThenBy(e => Math.Abs((e.GetValue<DateTime?>("StartDate").GetValueOrDefault() - DateTime.UtcNow).TotalSeconds)).ToList());


                var promotionItems = promotionQuery.OrderByDescending(e => e.GetValue<DateTime?>("StartDate") > DateTime.UtcNow)
                    .ThenByDescending(e => e.GetValue<DateTime?>("StartDate"))
                    .ToList();

                var promotionData = GetData(promotionItems.OrderByDescending(e => e.GetValue<DateTime?>("StartDate") > DateTime.UtcNow)
                    .ThenBy(e => Math.Abs((e.GetValue<DateTime?>("StartDate").GetValueOrDefault() - DateTime.UtcNow).TotalSeconds)).ToList());

                 

                var facilityItems = facilityQuery.OrderByDescending(e => e.PublicationDate > DateTime.UtcNow)
                    .ThenByDescending(e => e.PublicationDate)
                    .ToList();

                var facilityData = GetData(facilityItems.OrderByDescending(e => e.PublicationDate > DateTime.UtcNow)
                    .ThenBy(e => Math.Abs((e.PublicationDate - DateTime.UtcNow).TotalSeconds)).ToList());

                var serciceItems = serviceQuery.OrderByDescending(e => e.PublicationDate > DateTime.UtcNow)
                    .ThenByDescending(e => e.PublicationDate)
                    .ToList();

                var serviceData = GetData(serciceItems.OrderByDescending(e => e.PublicationDate > DateTime.UtcNow)
                    .ThenBy(e => Math.Abs((e.PublicationDate - DateTime.UtcNow).TotalSeconds)).ToList());


                var consolidatedData = whatsOnData.Union(promotionData).Union(facilityData).Union(serviceData).ToList();
                result.Data = consolidatedData.OrderByDescending(x => x.StartDate);
                result.TotalCount = consolidatedData.Count;

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("What's On API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        [Route("api/pastevents")]
        [HttpGet]
        public IHttpActionResult GetPastEvents(string keyword = "", string location = "", string persona = "", string interest = "", int page = 1, int pageSize = 12)
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                keyword = keyword == null ? string.Empty : ControlUtilities.Sanitize(keyword);
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                persona = persona == null ? string.Empty : ControlUtilities.Sanitize(persona);
                interest = interest == null ? string.Empty : ControlUtilities.Sanitize(interest);

                if (keyword.Length > 70 || location.Length > 170 || persona.Length > 170 ||
                    interest.Length > 270 || pageSize > 24 || page > 10000)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new PageData(page);

                var events = new FilterContentService().FilterContent(ContentTypeConstants.WhatsOn, interest, location, persona, keyword)
                    .Where(e => e.GetValue<string>("Type") == "1" && e.GetValue<DateTime?>("EndDate") <= DateTime.UtcNow);

                result.TotalCount = events.Count();

                var items = events.OrderByDescending(e => e.GetValue<DateTime?>("EndDate"))
                    .Skip((page - 1) * pageSize).Take(pageSize)
                    .ToList();

                result.Data = GetData(items);

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("What's On API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        [Route("api/eventscalendar")]
        [HttpGet]
        public IHttpActionResult GetEventsCalendar(string date = "")
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                date = date == null ? string.Empty : ControlUtilities.Sanitize(date);

                if (date.Length > 30)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var contentService = new FilterContentService();
                var clService = SystemManager.GetContentLocationService();

                DateTime dateTime;
                if (!DateTime.TryParse(date, out dateTime))
                {
                    dateTime = DateTime.Now;
                }

                var baseDate = dateTime.Date;
                var weekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                var weekEnd = weekStart.AddDays(7).AddSeconds(-1);

                var events = contentService.FilterContent(ContentTypeConstants.WhatsOn)
                    .Where(e => e.GetValue<bool>("ShowInCalendar") &&
                            e.GetValue<DateTime?>("EndDate") > weekStart.ToUniversalTime() &&
                            e.GetValue<DateTime?>("StartDate") < weekEnd.ToUniversalTime());

                var result = new List<CalendarDayEvent>();

                for (int i = 0; i < 7; i++)
                {
                    var calendarDay = weekStart.AddDays(i);
                    result.Add(new CalendarDayEvent(calendarDay, calendarDay == baseDate));
                }

                foreach (var e in events)
                {
                    AddToCalendar(e, result, clService);
                }

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("What's On API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        private void AddToCalendar(DynamicContent e, List<CalendarDayEvent> result, IContentLocationService clService)
        {
            var title = e.GetString("Title");
            var startDate = e.GetValue<DateTime?>("StartDate")?.ToLocalTime();
            var endDate = e.GetValue<DateTime?>("EndDate")?.ToLocalTime();
            var location = clService.GetItemDefaultLocation(e);
            var link = location.ItemAbsoluteUrl;
            if (!link.IsNullOrWhitespace())
            {
                link = new Uri(link).PathAndQuery;
            }

            if (startDate.HasValue && endDate.HasValue)
            {
                foreach (var calendarDay in result)
                {
                    if (startDate.Value.Date <= calendarDay.DateTime && calendarDay.DateTime <= endDate.Value.Date)
                    {
                        calendarDay.Events.Add(new EventLink(title, link));
                    }
                }
            }
        }

        private List<ListingItem> GetData(List<DynamicContent> items)
        {
            var data = new List<ListingItem>();

            var clService = SystemManager.GetContentLocationService();

            foreach (var item in items)
            {
                var dataItem = new ListingItem(item, clService);
                dataItem.SetDescription();
                dataItem.SetStartEndDates();
                dataItem.SetClubLocation();

                if (item.GetType().FullName.ToLower().Equals(ContentTypeConstants.WhatsOn.ToLower()))
                {
                    var location = item.GetString("Location")?.ToString().Trim();
                    if (!location.IsNullOrWhitespace())
                    {
                        dataItem.Location = location;
                    }

                    dataItem.Type = item.GetValue<ChoiceOption>("Type")?.Text;
                }

                if (item.GetType().FullName.ToLower().Equals(ContentTypeConstants.Facility.ToLower()))
                {
                    dataItem.Type = "Facility";
                }

                if (item.GetType().FullName.ToLower().Equals(ContentTypeConstants.Promotion.ToLower()))
                {
                    dataItem.Type = "Promotion";
                }

                if (item.GetType().FullName.ToLower().Equals(ContentTypeConstants.Service.ToLower()))
                {
                    dataItem.Type = "Service";
                }

                data.Add(dataItem);
            }

            return data;
        }
    }
}