﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using SAFRA.Api.Models;
using Telerik.DigitalExperienceCloud.Client;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.DataIntelligenceConnector.Configuration;
using Telerik.Sitefinity.DataIntelligenceConnector.EventHandlers;
using Telerik.Sitefinity.Security.Events;
using Telerik.Sitefinity.Web.Events;
using Telerik.Sitefinity.Web.UI;
using static SAFRA.Api.Models.MySafraMember;

namespace SAFRA.Api.MySafra
{
    public class MySafraController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage SetLoginUserId(RequestBody requestBody)
        {
            try
            {
                if (requestBody != null)
                {
                    StoreMemberDataInMemCache(requestBody);
                   
                    HttpResponseMessage respMessage = new HttpResponseMessage();
                    respMessage.StatusCode = HttpStatusCode.OK;          

                    Log.Write("mySAFRA Login - Set Login User Id Successfully.");
                    Log.Write("TrackId :"+ requestBody.TrackId);
                    Log.Write("Email :"+ requestBody.Email);
                    Log.Write("MemberId :"+ requestBody.MemberId);
                    return respMessage;
                }
                else
                {
                    Log.Write("mySAFRA Login - Request Body Not Found.");
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Request Body Not Found.");
                }

            }
            catch (Exception ex)
            {
                Log.Write("mySAFRA Login - Internal Server Error", ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private void StoreMemberDataInMemCache(RequestBody requestBody)
        {
            var cache = MemoryCache.Default;
            if (cache.Contains("mysafra-dec-contacts"))
            {
                List<MemberDetails> decContacts = (List<MemberDetails>)cache.Get("mysafra-dec-contacts");
                decContacts.Add(new MemberDetails()
                {
                    TrackId = ControlUtilities.Sanitize(requestBody.TrackId),
                    Email = ControlUtilities.Sanitize(requestBody.Email),
                    MemberId = ControlUtilities.Sanitize(requestBody.MemberId)
                });
                MemoryCache.Default.AddOrGetExisting("mysafra-dec-contacts", decContacts, DateTime.Now.AddMinutes(10));
            }
            else
            {
                List<MemberDetails> decContacts = new List<MemberDetails>();
                decContacts.Add(new MemberDetails()
                {
                    TrackId = ControlUtilities.Sanitize(requestBody.TrackId),
                    Email = ControlUtilities.Sanitize(requestBody.Email),
                    MemberId = ControlUtilities.Sanitize(requestBody.MemberId)
                });
                MemoryCache.Default.AddOrGetExisting("mysafra-dec-contacts", decContacts, DateTime.Now.AddMinutes(10));
            }
        }
    }
}