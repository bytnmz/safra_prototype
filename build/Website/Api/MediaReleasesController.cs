﻿using SAFRA.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.Api
{
    public class MediaReleasesController : ApiController
    {
        public IHttpActionResult Get(int page = 1, int pageSize = 12)
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                if (pageSize > 24 || page > 10000)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new PageData(page);

                var dynamicModuleManager = DynamicModuleManager.GetManager();
                var type = TypeResolutionService.ResolveType(ContentTypeConstants.MediaRelease);

                var query = dynamicModuleManager.GetDataItems(type)
                        .Where(c => c.Status == ContentLifecycleStatus.Live && c.Visible);

                result.TotalCount = query.Count();

                var items = query
                        .OrderByDescending(c => c.GetValue<DateTime?>("Date"))
                        .Skip((page - 1) * pageSize).Take(pageSize)
                        .ToList();

                var data = new List<MediaRelease>();

                foreach (var item in items)
                {
                    data.Add(new MediaRelease(item));
                }

                result.Data = data;

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("Media Releases API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }
    }
}