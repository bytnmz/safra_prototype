﻿using SAFRA.Api.Models;
using SAFRA.Domain.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Frontend.Mvc.Helpers;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Api
{
    public class FacilitiesController : ApiController
    {
        public IHttpActionResult Get(string keyword = "", string location = "", string persona = "", string interest = "", int page = 1, int pageSize = 12)
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                keyword = keyword == null ? string.Empty : ControlUtilities.Sanitize(keyword);
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                persona = persona == null ? string.Empty : ControlUtilities.Sanitize(persona);
                interest = interest == null ? string.Empty : ControlUtilities.Sanitize(interest);

                if (keyword.Length > 70 || location.Length > 170 || persona.Length > 170 ||
                    interest.Length > 270 || pageSize > 24 || page > 100)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new Models.PageData(page);

                var facilities = new FilterContentService().FilterContent(ContentTypeConstants.Facility, interest, location, persona, keyword);

                result.TotalCount = facilities.Count();

                var items = facilities
                    .OrderBy(f => f.GetValue<string>("Title"))
                    .Skip((page - 1) * pageSize).Take(pageSize)
                    .ToList();

                result.Data = GetData(items);

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("Facilities API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        private List<ListingItem> GetData(List<DynamicContent> items)
        {
            var data = new List<ListingItem>();

            var clService = SystemManager.GetContentLocationService();

            foreach (var item in items)
            {
                var dataItem = new ListingItem(item, clService);
                dataItem.SetDescription();
                dataItem.SetClubLocation();

                var page = item.GetRelatedItems("Page");

                if (page != null && page.Any())
                {
                    var pageNode = (PageNode) page.First();
                    dataItem.Link = HyperLinkHelpers.GetFullPageUrl(pageNode.Id);
                    if (!dataItem.Link.IsNullOrWhitespace())
                    {
                        dataItem.Link = new Uri(dataItem.Link).PathAndQuery;
                    }
                }

                data.Add(dataItem);
            }

            return data;
        }
    }
}