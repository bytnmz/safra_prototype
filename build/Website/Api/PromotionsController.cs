﻿using SAFRA.Api.Models;
using SAFRA.Domain.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Web.UI;

namespace SAFRA.Api
{
    public class PromotionsController : ApiController
    {
        [Route("api/promotions")]
        [HttpGet]
        public IHttpActionResult Get(string keyword = "", string location = "", string persona = "", string interest = "", string category = "", int page = 1, int pageSize = 12)
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                keyword = keyword == null ? string.Empty : ControlUtilities.Sanitize(keyword);
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                persona = persona == null ? string.Empty : ControlUtilities.Sanitize(persona);
                interest = interest == null ? string.Empty : ControlUtilities.Sanitize(interest);
                category = category == null ? string.Empty : ControlUtilities.Sanitize(category);

                if (keyword.Length > 70 || location.Length > 170 || persona.Length > 170 ||
                    interest.Length > 270 || category.Length > 70 || pageSize > 24 || page > 10000)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new PageData(page);

                var filterContentService = new FilterContentService();

                var promotions = filterContentService.FilterContent(ContentTypeConstants.Promotion, interest, location, persona, keyword)
                    .Where(e => e.GetValue<DateTime?>("EndDate").HasValue ? e.GetValue<DateTime?>("EndDate") > DateTime.UtcNow : true);

                filterContentService.ApplyFilter(ref promotions, category, "promotion-category", "promotioncategories");

                result.TotalCount = promotions.Count();

                var items = promotions.OrderByDescending(e => e.GetValue<DateTime?>("PromoPublicationDate").HasValue ? e.GetValue<DateTime?>("PromoPublicationDate") : e.LastModified)
                    .Skip((page - 1) * pageSize).Take(pageSize)
                    .ToList();

                result.Data = GetData(items);

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("Promotions API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        [Route("api/promotions/clubdeals")]
        [HttpGet]
        public IHttpActionResult GetClubDeals(string location = "", string persona = "", string interest = "")
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                persona = persona == null ? string.Empty : ControlUtilities.Sanitize(persona);
                interest = interest == null ? string.Empty : ControlUtilities.Sanitize(interest);

                if (location.Length > 170 || persona.Length > 170 || interest.Length > 270)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var result = new PageData(1);

                var filterContentService = new FilterContentService();

                var promotions = filterContentService.FilterContent(ContentTypeConstants.Promotion, interest, location, persona)
                    .Where(e => e.GetValue<DateTime?>("EndDate").HasValue ? e.GetValue<DateTime?>("EndDate") > DateTime.UtcNow : true);

                result.TotalCount = promotions.Count();

                var items = promotions.OrderByDescending(e => e.GetValue<DateTime?>("PromoPublicationDate").HasValue ? e.GetValue<DateTime?>("PromoPublicationDate") : e.LastModified);

                var clubDeals = new List<ListingItem>();
                var nearClubDeals = new List<ListingItem>();
                var clService = SystemManager.GetContentLocationService();

                var taxonomyManager = TaxonomyManager.GetManager();
                var categoriesTaxon = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>()
                    .SingleOrDefault(t => t.Name == "Categories");
                var locationsTaxon = taxonomyManager.GetTaxonomies<HierarchicalTaxonomy>()
                    .SingleOrDefault(t => t.Name == "Locations");

                string locationName = string.Empty;
                var hasLocation = locationsTaxon.Taxa.FirstOrDefault(t => t.Name == location)?.Title.TryGetValue(out locationName);

                foreach (var item in items)
                {
                    var dataItem = new ListingItem(item, clService);
                    dataItem.Title = item.GetString("MerchantName") + " - " + item.GetString("OverviewDescription");
                    dataItem.Description = GetCategory(categoriesTaxon, item, interest);

                    dataItem.SetStartEndDates();

                    dataItem.Link = "/promotions/" + item.UrlName;

                    var image = item.GetValue<Image>("NewOverviewImage") ?? item.GetValue<Image>("Image");

                    if (image != null)
                    {
                        dataItem.ImgSrc = image.ResolveMediaUrl();
                        dataItem.ImgAltText = image.AlternativeText;
                    }

                    if (item.GetValue<bool>("InClubDeal"))
                    {
                        dataItem.Type = locationName + " Deal";
                        if (clubDeals.Count < 4)
                        {
                            clubDeals.Add(dataItem);
                        }
                    }
                    else
                    {
                        dataItem.Type = "Near-Club Deal";
                        if (nearClubDeals.Count < 4)
                        {
                            nearClubDeals.Add(dataItem);
                        }
                    }

                    if (clubDeals.Count == 4 && nearClubDeals.Count == 4)
                    {
                        break;
                    }
                }

                clubDeals.AddRange(nearClubDeals);
                result.Data = clubDeals;

                return Json(result);
            }
            catch (Exception e)
            {
                Log.Write("Promotions API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        private string GetCategory(HierarchicalTaxonomy categoriesTaxon, DynamicContent item, string interestFilter)
        {
            var itemCategories = item.GetValue<IList<Guid>>("Category");
            var category = string.Empty;

            foreach (var itemCat in itemCategories)
            {
                var taxon = categoriesTaxon.Taxa.FirstOrDefault(t => t.Id == itemCat);
                category = taxon.Title;

                if (interestFilter.Contains(taxon.Name))
                {
                    break;
                }
            }

            return category;
        }

        private List<ListingItem> GetData(List<DynamicContent> items)
        {
            var data = new List<ListingItem>();

            var clService = SystemManager.GetContentLocationService();

            foreach (var item in items)
            {
                var dataItem = new ListingItem(item, clService);
                dataItem.Title = item.GetString("MerchantName");
                dataItem.SetDescription();
                dataItem.SetStartEndDates();

                var image = item.GetValue<Image>("NewOverviewImage") ?? item.GetValue<Image>("Image");

                if (image != null)
                {
                    dataItem.ImgSrc = image.ResolveMediaUrl();
                    dataItem.ImgAltText = image.AlternativeText;
                }

                var tagChoice = item.GetValue<ChoiceOption>("TagLabel");

                if (tagChoice.PersistedValue == "1")
                {
                    dataItem.TagClass = "card__type--red";
                }
                else if (tagChoice.PersistedValue == "2")
                {
                    dataItem.TagClass = "card__type--blue";
                }
                else if (tagChoice.PersistedValue == "3")
                {
                    dataItem.TagClass = "card__type--orange";
                }

                dataItem.Type = tagChoice.Text;

                dataItem.Link = "/promotions/" + item.UrlName;

                data.Add(dataItem);
            }

            return data;
        }
    }
}