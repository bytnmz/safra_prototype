﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using CsvHelper;
using SAFRA.Api.Models.DEC;
using SAFRA.Config;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;

namespace SAFRA.Api.Services.DEC
{
    public class GenerateExcelService
    {
        internal static string GenerateExcel(List<ExcelContact> excelContacts)
        {
            try
            {
                using (var mem = new MemoryStream())
                using (var writer = new StreamWriter(mem))
                using (var csvWriter = new CsvWriter(writer, System.Globalization.CultureInfo.CurrentCulture))
                {
                    csvWriter.WriteField("email");
                    csvWriter.WriteField("tag_code");
                    csvWriter.WriteField("tag_value");
                    csvWriter.NextRecord();

                    foreach (var contact in excelContacts)
                    {
                        csvWriter.WriteField(contact.email);
                        csvWriter.WriteField(contact.tag_code);
                        csvWriter.WriteField(contact.tag_value);
                        csvWriter.NextRecord();
                    }

                    writer.Flush();
                    var result = Encoding.UTF8.GetString(mem.ToArray());

                    Log.Write("[GenerateExcelService]--- Generate Excel ---" + result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Write("[GenerateExcelService]--- Generate Excel ---" + ex.Message + ex.InnerException);
                return string.Empty;
            }
        }
    }
}