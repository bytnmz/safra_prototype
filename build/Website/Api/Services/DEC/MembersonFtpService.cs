﻿using System;
using Telerik.Sitefinity.Configuration;
using SAFRA.Config;
using Telerik.Sitefinity.Abstractions;
using System.Net;
using System.IO;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace SAFRA.Api.Services.DEC
{
    public class MembersonFtpService
    {
        private static ConfigManager _configManager => ConfigManager.GetManager();
        private static MembersonFtpConfig _membersonFtpConfig => _configManager.GetSection<MembersonFtpConfig>();
        private static string _username => _membersonFtpConfig.Username;
        private static string _password => _membersonFtpConfig.Password;
        private static string _containerName => _membersonFtpConfig.ContainerName;
        private static string _storageConn => _membersonFtpConfig.StorageConn;
        private static string _requestUri => _membersonFtpConfig.RequestUri.Replace("##TodayDate##", DateTime.Now.ToString("yyyyMMdd"));
        private static string _fileName => _membersonFtpConfig.Filename;

        internal static void UploadToBlobStorage(string result)
        {
            try
            {
                Log.Write("Uploading Blob Storage");

                // Create Reference to Azure Storage Account
                String strorageconn = _storageConn;
                Log.Write(strorageconn);
                CloudStorageAccount storageacc = CloudStorageAccount.Parse(strorageconn);

                //Create Reference to Azure Blob
                CloudBlobClient blobClient = storageacc.CreateCloudBlobClient();

                //The next 2 lines create if not exists a container named "democontainer"
                Log.Write(_containerName);
                CloudBlobContainer container = blobClient.GetContainerReference(_containerName);
                container.CreateIfNotExists();

                // convert string to stream
                Log.Write(result);
                byte[] byteArray = Encoding.ASCII.GetBytes(result);
                MemoryStream stream = new MemoryStream(byteArray);

                //The next 7 lines upload the file test.txt with the name DemoBlob on the container "democontainer"
                string filePath = string.Format("{0}/{1}", _requestUri,_fileName);
                Log.Write(filePath);
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(filePath);

                blockBlob.UploadFromStream(stream);

                Log.Write("Uploaded Blob Storage");
            }
            catch (Exception ex)
            {
                Log.Write(ex.StackTrace);
            }
        }

        internal static void UploadtoFTP(string result)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

                Log.Write("[GenerateExcelService]--- Upload to FTP --- Start");

                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(string.Format("{0}/{1}", _requestUri, _fileName));
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.Credentials = new NetworkCredential(_username, _password);
                ftpRequest.UsePassive = true;
                ftpRequest.UseBinary = true;
                ftpRequest.KeepAlive = true;
                ftpRequest.EnableSsl = true;

                //overwrite the certificate checks
                ServicePointManager.ServerCertificateValidationCallback =
                                     (s, certificate, chain, sslPolicyErrors) => true;

                Stream requestStream = ftpRequest.GetRequestStream();
                byte[] bytes = Encoding.UTF8.GetBytes(result);
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                Log.Write("[GenerateExcelService]--- Upload to FTP --- End");
            }
            catch (Exception ex)
            {
                Log.Write("[GenerateExcelService]--- Upload to FTP ---" + ex.Message + ex.InnerException);
            }
        }
                
        internal static void CreateFolder()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

                FtpWebRequest ftpCreate = (FtpWebRequest)WebRequest.Create(_requestUri);
                ftpCreate.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpCreate.Credentials = new NetworkCredential(_username, _password);
                ftpCreate.EnableSsl = true;

                //overwrite the certificate checks
                ServicePointManager.ServerCertificateValidationCallback =
                                     (s, certificate, chain, sslPolicyErrors) => true;

                var resp = (FtpWebResponse)ftpCreate.GetResponse();
                Log.Write("[GenerateExcelService]--- Create Folder --- " + _requestUri + resp.StatusCode);
                resp.Close();
            }
            catch (WebException ex)
            {
                Log.Write("[GenerateExcelService]--- Create Folder ---" + ex.Message + ex.InnerException);
            }
        }

        internal static bool FolderExist()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls| SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(string.Format("{0}/{1}", _requestUri, _fileName));
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.Credentials = new NetworkCredential(_username, _password);
                ftpRequest.EnableSsl = true;

                //overwrite the certificate checks
                ServicePointManager.ServerCertificateValidationCallback =
                                     (s, certificate, chain, sslPolicyErrors) => true;

                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();

                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
                else
                    return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
                else
                    return true;
            }
        }
    }
}