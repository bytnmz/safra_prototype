﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Telerik.Sitefinity.Abstractions;
using Telerik.DigitalExperienceCloud.Client;
using Telerik.Sitefinity.Configuration;
using SAFRA.Config;
using Telerik.Sitefinity.DataIntelligenceConnector.Configuration;

namespace SAFRA.Api.Services.DEC
{
    public class DECApiService
    {
        private static ConfigManager _configManager => ConfigManager.GetManager();
        private static SafraDecConfig _safraDecConfig => _configManager.GetSection<SafraDecConfig>();
        private static string _accessKey => _safraDecConfig.AccessKey;
        private static string _siteName => _safraDecConfig.SiteName;
        private static string _accessToken => new AccessToken("AccessKey", _accessKey).ToString();
        private static DigitalExperienceCloudConnectorConfig _decConfigManager => _configManager.GetSection<DigitalExperienceCloudConnectorConfig>();
        private static string _dataCenterKey = _decConfigManager.SiteToApiKeyMappings[_siteName]?.DataCenterApiKey;

        internal static string CallDECApi(string apiUrl, string method, string body, string token, string contactId, string contacts)
        {
            try
            {
                Log.Write(_accessKey + " "+ _siteName + " "+ _accessToken + " "+ _dataCenterKey);

                string result = string.Empty;

                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(apiUrl);
                Log.Write("[DEC Api Service]--- httpWebRequest ok ---");

                if (!string.IsNullOrEmpty(token))
                {
                    httpWebRequest.Headers.Add(HeaderOptions.DataCenterKeyHeader, _dataCenterKey);
                    httpWebRequest.Headers.Add(HeaderOptions.Authorization, "bearer " + token);
                    httpWebRequest.Headers.Add(HeaderOptions.DataSourceHeader, _siteName);
                }

                if(!string.IsNullOrEmpty(contactId))
                {
                    httpWebRequest.Headers.Add(HeaderOptions.LeadInContactsHeader, contactId);
                }

                if(!string.IsNullOrEmpty(contacts))
                {
                    httpWebRequest.Headers.Add(HeaderOptions.TakeHeader, "500");
                }

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = method;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                if (!string.IsNullOrEmpty(body))
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(body);
                    httpWebRequest.ContentLength = (long)bytes.Length;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                }

                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                var responseStr = new StreamReader(response.GetResponseStream()).ReadToEnd();
                response.Close();

                return responseStr;
            }
            catch (Exception ex)
            {
                Log.Write("[DEC Api Service]--- Call DEC Api ---" + ex.Message + ex.InnerException);
                return ex.Message;
            }
        }
    }
}