﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Services.Search;
using Telerik.Sitefinity.Services.Search.Data;
using Telerik.Sitefinity.Services;
using SAFRA.Api.Models;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Web.UI;
using System.Net.Http;
using System.Net;

namespace SAFRA.Api
{
    public class SearchController : ApiController
    {
        public IHttpActionResult Get(string keyword, string location = "", string type = "", int page = 1, int pageSize = 10)
        {
            var acceptType = Request.Headers.Accept;
            if (acceptType == null || !acceptType.Any(a => a.MediaType == "application/json"))
            {
                HttpResponseMessage notAcceptableResult = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                notAcceptableResult.Content = new StringContent("Not acceptable");
                return ResponseMessage(notAcceptableResult);
            }

            try
            {
                keyword = keyword == null ? string.Empty : ControlUtilities.Sanitize(keyword);
                location = location == null ? string.Empty : ControlUtilities.Sanitize(location);
                type = type == null ? string.Empty : ControlUtilities.Sanitize(type);

                if (keyword.Length == 0)
                {
                    return Json(new PageData(page));
                }

                if (keyword.Length > 70 || location.Length > 170 || type.Length > 70
                    || pageSize > 24 || page > 10000)
                {
                    var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Status = "Error",
                        Error = "Input exceeds expected length"
                    });

                    return ResponseMessage(errorResult);
                }

                var contentTypes = new List<string>();

                if (!type.IsNullOrEmpty() && type != "all")
                {
                    foreach (var token in type.Split('|'))
                    {
                        var contentType = token.Trim();

                        if (!contentType.IsNullOrWhitespace())
                        {
                            contentTypes.Add(contentType);
                        }
                    }
                }

                var resultData = Search(keyword, contentTypes, location, page, pageSize);

                return Json(resultData);
            }
            catch (Exception e)
            {
                Log.Write("Search API ERROR: " + e.Message + " ---- " + e.StackTrace);
                var errorResult = Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Status = "Error",
                });

                return ResponseMessage(errorResult);
            }
        }

        private PageData Search(string keyword, List<string> contentTypes, string location = "", int page = 1, int pageSize = 10)
        {
            var service = ServiceBus.ResolveService<ISearchService>();
            var queryBuilder = ObjectFactory.Resolve<IQueryBuilder>();
            var resultSet = new PageData(page);

            string[] searchFields = new string[] { "Title", "Content", "OverviewDescription", "Description" };

            var searchQuery = queryBuilder.BuildQuery(keyword, searchFields);
            searchQuery.IndexName = "site-search";
            searchQuery.HighlightedFields = new string[] { };

            bool hasLocationFilter = !location.IsNullOrWhitespace() && location != "all";
            bool hasContentTypeFilter = contentTypes != null && contentTypes.Any();

            var dynamicModuleManager = DynamicModuleManager.GetManager();

            searchQuery.Take = 1000;
            IResultSet result = service.Search(searchQuery);

            if (!hasLocationFilter && !hasContentTypeFilter)
            {
                var list = new List<IDocument>();

                foreach (var item in result)
                {
                    var contentType = item.GetValue("ContentType").ToString();
                    var type = TypeResolutionService.ResolveType(contentType);

                    SearchFilterContent(item, list, contentType, type, dynamicModuleManager);                
                }

                resultSet.TotalCount = list.Count();
                var pageResult = list.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                resultSet.Data = pageResult.SetContentLinks().Select(r => new SearchResult(r));
            }
            else if (hasContentTypeFilter /*&& !hasLocationFilter*/)
            {
                var fullNameContentTypes = string.Join(",", contentTypes.Select(c => GetFullName(c)));
                var filteredResult = result.Where(x => fullNameContentTypes.Contains(x.GetValue("ContentType").ToString())).ToList();
                var list = new List<IDocument>();

                if(hasLocationFilter)
                {
                    var locationId = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                           .FirstOrDefault(t => t.Name == "locations")?
                           .Taxa.FirstOrDefault(t => t.Name == location)?.Id;

                    if (locationId.HasValue)
                    {
                        foreach (var item in filteredResult)
                        {
                            var contentType = item.GetValue("ContentType").ToString();
                            var type = TypeResolutionService.ResolveType(contentType);

                            SearchFilterLocation(item, list, contentType, type, dynamicModuleManager, locationId);
                        }
                    }
                }
                else
                {
                    foreach (var item in filteredResult)
                    {
                        var contentType = item.GetValue("ContentType").ToString();
                        var type = TypeResolutionService.ResolveType(contentType);

                        SearchFilterContent(item, list, contentType, type, dynamicModuleManager);
                    }
                }      

                resultSet.TotalCount = list.Count();
                var pageResult = list.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                resultSet.Data = pageResult.SetContentLinks().Select(r => new SearchResult(r));
            }
            else
            {
                var list = new List<IDocument>();

                var locationId = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                            .FirstOrDefault(t => t.Name == "locations")?
                            .Taxa.FirstOrDefault(t => t.Name == location)?.Id;

                if (locationId.HasValue)
                {
                    foreach (var item in result)
                    {
                        var contentType = item.GetValue("ContentType").ToString();
                        var type = TypeResolutionService.ResolveType(contentType);

                        SearchFilterLocation(item, list, contentType, type, dynamicModuleManager, locationId);
                    }        
                }

                resultSet.TotalCount = list.Count();
                var pageResult = list.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                resultSet.Data = pageResult.SetContentLinks().Select(r => new SearchResult(r));
            }

            return resultSet;
        }

        private void SearchFilterContent(IDocument item, List<IDocument> list, string contentType, Type type, DynamicModuleManager dynamicModuleManager)
        {
            if (contentType == ContentTypeConstants.WhatsOn)
            {
                var dataItem = dynamicModuleManager.GetDataItems(type)
                        .FirstOrDefault(c => c.Id == new Guid(item.GetValue("Id").ToString())
                        && (c.GetValue<DateTime?>("RegistrationEndDate") != null ? (c.GetValue<DateTime?>("RegistrationEndDate") < c.GetValue<DateTime?>("EndDate") ?
            c.GetValue<DateTime?>("RegistrationEndDate") > DateTime.UtcNow : c.GetValue<DateTime?>("EndDate") > DateTime.UtcNow) : c.GetValue<DateTime?>("EndDate") > DateTime.UtcNow)
                        && c.Status == ContentLifecycleStatus.Live && c.Visible);

                if (dataItem != null)
                {
                    list.Add(item);
                }
            }
            else if (contentType == ContentTypeConstants.Promotion)
            {
                var dataItem = dynamicModuleManager.GetDataItems(type)
                        .FirstOrDefault(c => c.Id == new Guid(item.GetValue("Id").ToString())
                        && (c.GetValue<DateTime?>("EndDate") != null ? c.GetValue<DateTime?>("EndDate") > DateTime.UtcNow : true) 
                        && c.Status == ContentLifecycleStatus.Live && c.Visible);

                if (dataItem != null)
                {
                    list.Add(item);
                }
            }
            else if (contentType == ContentTypeConstants.Service || contentType == ContentTypeConstants.Facility)
            {
                var dataItem = dynamicModuleManager.GetDataItems(type)
                        .FirstOrDefault(c => c.Id == new Guid(item.GetValue("Id").ToString())
                        && c.Status == ContentLifecycleStatus.Live && c.Visible);

                if (dataItem != null)
                {
                    list.Add(item);
                }
            }
            else
            {
                list.Add(item);
            }
        }

        private void SearchFilterLocation(IDocument item, List<IDocument> list, string contentType, Type type, DynamicModuleManager dynamicModuleManager, Guid? locationId)
        {
            if (contentType == ContentTypeConstants.WhatsOn)
            {
                var dataItem = dynamicModuleManager.GetDataItems(type)
                        .FirstOrDefault(c => c.Id == new Guid(item.GetValue("Id").ToString())
                        && (c.GetValue<DateTime?>("RegistrationEndDate") != null ? (c.GetValue<DateTime?>("RegistrationEndDate") < c.GetValue<DateTime?>("EndDate") ?
            c.GetValue<DateTime?>("RegistrationEndDate") > DateTime.UtcNow : c.GetValue<DateTime?>("EndDate") > DateTime.UtcNow) : c.GetValue<DateTime?>("EndDate") > DateTime.UtcNow)
                        && c.Status == ContentLifecycleStatus.Live && c.Visible);

                if (dataItem != null && dataItem.GetValue<IList<Guid>>("locations").Contains(locationId.Value))
                {
                    list.Add(item);
                }
            }
            else if (contentType == ContentTypeConstants.Promotion)
            {
                var dataItem = dynamicModuleManager.GetDataItems(type)
                        .FirstOrDefault(c => c.Id == new Guid(item.GetValue("Id").ToString())
                        && (c.GetValue<DateTime?>("EndDate") != null ? c.GetValue<DateTime?>("EndDate") > DateTime.UtcNow : true)
                        && c.Status == ContentLifecycleStatus.Live && c.Visible);

                if (dataItem != null && dataItem.GetValue<IList<Guid>>("locations").Contains(locationId.Value))
                {
                    list.Add(item);
                }
            }
            else if (contentType == ContentTypeConstants.Service || contentType == ContentTypeConstants.Facility)
            {
                var dataItem = dynamicModuleManager.GetDataItems(type)
                        .FirstOrDefault(c => c.Id == new Guid(item.GetValue("Id").ToString())
                        && c.Status == ContentLifecycleStatus.Live && c.Visible);

                if (dataItem != null && dataItem.GetValue<IList<Guid>>("locations").Contains(locationId.Value))
                {
                    list.Add(item);
                }
            }
            else
            {
                list.Add(item);
            }
        }

        private string GetFullName(string contentType)
        {
            var fullName = string.Empty;

            if (contentType == "Events")
            {
                fullName = ContentTypeConstants.WhatsOn;
            }
            else if (contentType == "Facilities")
            {
                fullName = ContentTypeConstants.Facility;
            }
            else if (contentType == "Services")
            {
                fullName = ContentTypeConstants.Service;
            }
            else if (contentType == "Promotions")
            {
                fullName = ContentTypeConstants.Promotion;
            }

            return fullName;
        }
    }
}