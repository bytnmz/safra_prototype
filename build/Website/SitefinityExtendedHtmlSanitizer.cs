﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Dom;
using Ganss.XSS;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Security.Configuration;

namespace SAFRA
{
    public class SitefinityExtendedHtmlSanitizer : Telerik.Sitefinity.Security.Sanitizers.IHtmlSanitizer
    {
        #region Properties

        /// <summary>
        /// Gets the internal sanitizer object being used.
        /// </summary>
        /// <value>
        /// The sanitizer object.
        /// </value>
        protected object SanitizerObject
        {
            get
            {
                return this.sanitizer;
            }
        }

        /// <summary>
        /// Gets the allowed HTTP schemes such as "http" and "https".
        /// </summary>
        /// <value>
        /// The allowed HTTP schemes.
        /// </value>
        protected ISet<string> AllowedSchemes
        {
            get
            {
                return this.sanitizer.AllowedSchemes;
            }
        }

        /// <summary>
        /// Gets the allowed HTML tag names such as "a" and "div".
        /// </summary>
        /// <value>
        /// The allowed tag names.
        /// </value>
        protected ISet<string> AllowedTags
        {
            get
            {
                return this.sanitizer.AllowedTags;
            }
        }

        /// <summary>
        /// Gets the HTML attributes that can contain a URI such as "href".
        /// </summary>
        /// <value>
        /// The URI attributes.
        /// </value>
        protected ISet<string> UriAttributes
        {
            get
            {
                return this.sanitizer.UriAttributes;
            }
        }

        /// <summary>
        /// Gets the allowed CSS properties such as "font" and "margin".
        /// </summary>
        /// <value>
        /// The allowed CSS properties.
        /// </value>
        protected ISet<string> AllowedCssProperties
        {
            get
            {
                return this.sanitizer.AllowedCssProperties;
            }
        }

        /// <summary>
        /// Gets the allowed HTML attributes such as "href" and "alt".
        /// </summary>
        /// <value>
        /// The allowed HTML attributes.
        /// </value>
        protected ISet<string> AllowedAttributes
        {
            get
            {
                return this.sanitizer.AllowedAttributes;
            }
        }

        #endregion

        #region Construction

        /// <inheritdoc/>
        public SitefinityExtendedHtmlSanitizer()
        {
            this.sanitizer.AllowDataAttributes = true;

            this.AllowedAttributes.Add("controls"); // for video tag
            this.AllowedAttributes.Add("class");
            this.AllowedAttributes.Add("id"); // for mvc content block insert anchor link
            this.AllowedAttributes.Add("sfref"); // for inline editing content blocks

            this.AllowedTags.Add("video");
            this.AllowedTags.Add("source");

            this.AllowedSchemes.Add("mailto");

            //CUSTOMIZATION
            //this.AllowedAttributes.Add("sandbox");

            //whitelist attributes to allow them to work in content block widget
            this.AllowedAttributes.Add("allowfullscreen");
            this.AllowedAttributes.Add("allow");
        }

        #endregion

        #region Public methods

        /// <inheritdoc/>
        public string Sanitize(string html)
        {
            if (html != null)
            {
                return this.sanitizer.Sanitize(html);
            }

            return html;
        }

        /// <inheritdoc/>
        public string SanitizeUrl(string url)
        {
            if (url != null)
            {
                return this.sanitizer.SanitizeUrl(url);
            }

            return url;
        }

        #endregion

        #region Private members

        private GanssHtmlSanitizer sanitizer = new GanssHtmlSanitizer();

        private class GanssHtmlSanitizer : Ganss.XSS.HtmlSanitizer
        {
            public GanssHtmlSanitizer()
            {
                this.AllowedTags.Add(IframeNodeName);
                this.PostProcessNode += this.GanssHtmlSanitizer_PostProcessNode;
                this.RemovingAttribute += this.GanssHtmlSanitizer_RemovingAttribute;
            }

            private void GanssHtmlSanitizer_PostProcessNode(object sender, PostProcessNodeEventArgs e)
            {
                if (string.Compare(e.Node.NodeName, IframeNodeName, true) == 0)
                {   
                    //CUSTOMIZATION
                    //if (!(e.Node as IElement).OuterHtml.Contains("allow-top-navigation"))
                    //{
                    //    (e.Node as IElement).SetAttribute("sandbox", "allow-scripts allow-same-origin");
                    //}
                    
                    var src = (e.Node as IElement).GetAttribute("src");
                    (e.Node as IElement).SetAttribute("src", this.SanitizeUrl(src));
                }
            }

            private void GanssHtmlSanitizer_RemovingAttribute(object sender, RemovingAttributeEventArgs e)
            {
                // DEC HTML5 attribute instrumentation
                if (e.Attribute.Name.StartsWith("sfdi-", StringComparison.OrdinalIgnoreCase))
                {
                    e.Cancel = true;
                }
            }

            /// <inheritdoc/>
            public string SanitizeUrl(string url)
            {
                return base.SanitizeUrl(url, null);
            }

            private const string IframeNodeName = "iframe";
        }

        #endregion

    }
}