﻿<%@ Page MasterPageFile="~/CustomLogin/Template/SafraLogin.master" Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="SAFRA.Sitefinity.Public.Admin.Login" %>

<%@ Register Src="~/CustomLogin/Control/Login.ascx" TagName="BackendLogin" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Body" runat="server">
    <uc1:BackendLogin runat="server" ID="backendLogin" />
</asp:Content>