﻿using Newtonsoft.Json;
using SAFRA.Api.Models.DEC;
using SAFRA.Api.Services.DEC;
using SAFRA.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using Telerik.DigitalExperienceCloud.Client;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Scheduling;
using System.IO;
using CsvHelper;
using System.Globalization;
using Telerik.Sitefinity.DataIntelligenceConnector.Configuration;

namespace SAFRA.Scheduler
{
    public class DecMembersonScheduledTask : ScheduledTask
    {
        private ConfigManager _configManager => ConfigManager.GetManager();
        private SafraDecConfig _safraDecConfig => _configManager.GetSection<SafraDecConfig>();
        private string _siteName => _safraDecConfig.SiteName;
        private string _accessKey => _safraDecConfig.AccessKey;
        private DigitalExperienceCloudConnectorConfig _decConfigManager => _configManager.GetSection<DigitalExperienceCloudConnectorConfig>();
        private string _dataCenterApiKey => _decConfigManager.SiteToApiKeyMappings[_siteName]?.DataCenterApiKey;
        private DECPersona persona => GetAllPersona();
        private string _token => GetAuthorizationToken();

        private MemoryCache cache = MemoryCache.Default;

        public DecMembersonScheduledTask()
        {
        }

        public override void ExecuteTask()
        {
            try
            {
                var webResponse = DECApiService.CallDECApi(string.Format("{0}{1}", _safraDecConfig.DecUrl, _safraDecConfig.ContactsAPIUrl), "GET", string.Empty, _token, string.Empty, "contacts");

                var Contacts = JsonConvert.DeserializeObject<Contact>(webResponse);

                if (Contacts != null && Contacts.items.Count > 0)
                {
                    Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- Count > 0");
                    if (Contacts.items.Where(x => x.Properties.LastVisitOn?.Date == DateTime.Now.Date).Count() > 0)
                    {
                        Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- LastVisitOn > 0");
                        var membersonContacts = MapDECDataToMemberson(Contacts);

                        Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- MapMembersonContactsToExcelModel");
                        var excelModel = MapMembersonContactsToExcelModel(membersonContacts);

                        if (excelModel.Count > 0)
                        {
                            Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- MapMembersonContacts count" + excelModel.Count);
                            Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- excelModel > 0");
                            var result = GenerateExcelService.GenerateExcel(excelModel);
                            Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- GenerateExcel done");
                            //MembersonFtpService.UploadToBlobStorage(result);
                            //Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- Upload To Blob Storage done");
                            if (!string.IsNullOrEmpty(result))
                            {
                                if (!MembersonFtpService.FolderExist())
                                {
                                    MembersonFtpService.CreateFolder();
                                }
                                MembersonFtpService.UploadtoFTP(result);
                            }
                            Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- Upload To FTP Storage done");
                        }
                        else
                        {
                            Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- excelModel.Count < 0");
                        }
                    }
                    else
                    {
                        Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- No LastVisitOn");
                    }
                }
                else
                {
                    Log.Write("[DecMembersonScheduledTask]--- ExecuteTask --- No new contact is found.");
                }
            }
            catch (Exception ex)
            {
                Log.Write("[DecMembersonScheduledTask]--- ExecuteTask ---" + ex.Message + ex.InnerException);
            }

            // Do your scheduled logic here
            Log.Write("DecMembersonScheduledTask is run.");
        }

        public override string TaskName
        {
            get
            {
                return "SAFRA.Scheduler.DecMembersonScheduledTask";
            }
        }

        private List<ExcelContact> MapMembersonContactsToExcelModel(List<MembersonContact> membersonContacts)
        {
            Log.Write("[DEC Api Controller]--- Map Memberson Contacts To Excel Model ---");
            var excelContacts = new List<ExcelContact>();
            try
            {
                foreach (var contact in membersonContacts)
                {
                    Log.Write(contact.Email + " " + contact.Personas?.Count);
                    if (contact.Personas?.Count > 0)
                    {
                        foreach (var persona in contact.Personas)
                        {
                            excelContacts.Add(new ExcelContact() { email = contact.Email, tag_code = persona.PersonaName, tag_value = persona.Score > 100 ? "100" : persona.Score.ToString() });
                        }
                    }
                    else
                    {
                        Log.Write("[DEC Api Controller]--- Map Memberson Contacts Count < 0 ---");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write("[DEC Api Controller]--- Map Memberson Contacts To Excel Model ---" + ex.Message + ex.InnerException);
            }
            return excelContacts;
        }

        private List<MembersonContact> MapDECDataToMemberson(Contact contacts)
        {
            return contacts?.items?.Where(x => x.Properties.LastVisitOn?.Date == DateTime.Now.Date)?.Select(c => new MembersonContact()
            {
                ContactId = c.ContactId,
                Email = c.Properties.Email,
                Personas = GetRelatedPersons(c.ContactId)
            }).ToList();
        }

        private List<RelatedPersonaDetails> GetRelatedPersons(int _contactId)
        {
            var relatedPersonaDetails = new List<RelatedPersonaDetails>();
            try
            {
                var webResponse = DECApiService.CallDECApi(string.Format("{0}{1}", _safraDecConfig.DecUrl, _safraDecConfig.ScoringContactIdAPIUrl), "GET", string.Empty, _token, _contactId.ToString(), string.Empty);

                var Score = JsonConvert.DeserializeObject<List<Score>>(webResponse);
               
                if (Score?.FirstOrDefault()?.Personas.Count > 0)
                {
                    Log.Write("[DEC Api Controller]--- Score?.FirstOrDefault()?.Personas.Count > 0 ---");

                    foreach (var item in Score?.FirstOrDefault()?.Personas)
                    {
                        relatedPersonaDetails.Add(new RelatedPersonaDetails() { PersonaId = item.ScoringId, PersonaName = GetPersonaName(item.ScoringId), Score = item.Score > 100 ? 100 : item.Score });
                    }
                }
                else
                {
                    Log.Write("[DEC Api Controller]--- No related personas ---");
                }
            }
            catch (Exception ex)
            {
                Log.Write("[DEC Api Controller]--- Get Related Persons ---" + ex.Message + ex.InnerException);
            }
            return relatedPersonaDetails;
        }

        private int? GetScoring(int _contactId, int _personaId)
        {
            int? score = 0;
            var Score = new List<Score>();
            try
            {
                var webResponse = DECApiService.CallDECApi(string.Format("{0}{1}", _safraDecConfig.DecUrl, _safraDecConfig.ScoringContactIdAPIUrl), "GET", string.Empty, _token, _contactId.ToString(), string.Empty);

                Score = JsonConvert.DeserializeObject<List<Score>>(webResponse);

                score = Score?.FirstOrDefault()?.Personas?.Where(p => p.ScoringId == _personaId).FirstOrDefault().Score;
            }
            catch (Exception ex)
            {
                Log.Write("[DEC Api Controller]--- Get GetScore ---" + ex.Message + ex.InnerException);
            }

            return score;
        }

        private string GetPersonaName(int _personaId)
        {
            string personaName = string.Empty;
            try
            {
                personaName = persona?.items?.FirstOrDefault(p => p.Id == _personaId).Name;
            }
            catch (Exception ex)
            {
                Log.Write("[DEC Api Controller]--- Get Persona Name ---" + ex.Message + ex.InnerException);
            }
            return personaName;
        }

        private string GetAuthorizationToken()
        {
            string _authToken = string.Empty;
            if (cache.Contains("dec-authtoken"))
            {
                _authToken = (string)cache.Get("dec-authtoken");
            }
            else
            {
                var body = "{\"AccessKey\":\"" + _accessKey + "\"}";

                var webResponse = DECApiService.CallDECApi(string.Format("{0}{1}", _safraDecConfig.DecUrl, _safraDecConfig.AuthorizationAPIUrl), "POST", body, string.Empty, string.Empty, string.Empty);

                var responseObj = JsonConvert.DeserializeObject<DECAccessToken>(webResponse);

                _authToken = responseObj?.Access_Token.ToString();

                MemoryCache.Default.AddOrGetExisting("dec-authtoken", responseObj?.Access_Token.ToString(), DateTime.Now.AddMinutes(10));
            }
            return _authToken;
        }

        private DECPersona GetAllPersona()
        {
            var Persona = new DECPersona();
            try
            {
                if (cache.Contains("dec-personas"))
                {
                    Persona = (DECPersona)cache.Get("dec-personas");
                }
                else
                {
                    var webResponse = DECApiService.CallDECApi(string.Format("{0}{1}", _safraDecConfig.DecUrl, _safraDecConfig.ScoringAPIUrl), "GET", string.Empty, _token, string.Empty, string.Empty);

                    Persona = JsonConvert.DeserializeObject<DECPersona>(webResponse);

                    MemoryCache.Default.AddOrGetExisting("dec-personas", Persona, DateTime.Now.AddMinutes(10));
                }
            }
            catch (Exception ex)
            {
                Log.Write("[DEC Api Controller]--- Get All Persona ---" + ex.Message + ex.InnerException);
            }
            return Persona;
        }
    }
}