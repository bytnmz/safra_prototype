﻿using SAFRA.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Scheduling;
using Telerik.Sitefinity.Scheduling.Model;

namespace SAFRA.Scheduler
{
    public class CustomScheduledTaskBase
    {
        private static ConfigManager _configManager => ConfigManager.GetManager();
        private static MembersonFtpConfig _membersonFtpConfig => _configManager.GetSection<MembersonFtpConfig>();
        private static bool _runExportDECScheduler => _membersonFtpConfig.RunExportDECScheduler;

        public static void RegisterMembersonScheduledTasks()
        {
            if (_runExportDECScheduler)
            {
                Log.Write("-- Schedule Dec Api Call --");
                SchedulingManager manager = SchedulingManager.GetManager();
                DecMembersonScheduledTask newTask = new DecMembersonScheduledTask()
                {
                    ExecuteTime = DateTime.UtcNow.AddSeconds(10),
                    //CustomData = new DecMembersonScheduledTask.MyCustomData() { MyIntData = 3, MyStringData = "My string data" }
                };

                // With cron syntax
                DecMembersonScheduledTask newCronTask = new DecMembersonScheduledTask()
                {
                    ExecuteTime = DateTime.UtcNow,
                    ScheduleSpec = "0 0 * * *", // The task will execute every day at midnight
                    ScheduleSpecType = "crontab",
                    //CustomData = new DecMembersonScheduledTask.MyCustomData() { MyIntData = 3, MyStringData = "My string data" }
                };

                //Log.Write("-- Schedule Dec Api Call -- Add new Task Start");
                //manager.AddTask(newTask);
                Log.Write("-- Schedule Dec Api Call -- Add new Cron Task Start");
                manager.AddTask(newCronTask);
                manager.SaveChanges();
            }
        }
    }
}