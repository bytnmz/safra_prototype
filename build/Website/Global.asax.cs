﻿using SAFRA.Config;
using SAFRA.CustomLogin;
using SAFRA.CustomWorkflow;
using SAFRA.Notifications;
using SAFRA.Personalization;
using SAFRA.Scheduler;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Telerik.Microsoft.Practices.Unity;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Data.Events;
using Telerik.Sitefinity.DynamicModules.Events;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Publishing;
using Telerik.Sitefinity.Publishing.Pipes;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Security.Sanitizers;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Web.Events;
using Telerik.Sitefinity.Workflow;
using Telerik.Sitefinity.Workflow.Activities;

namespace SAFRA
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Bootstrapped += Bootstrapper_Bootstrapped;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = (HttpException)Server.GetLastError();
            int httpCode = ex.GetHttpCode();

            if (httpCode == 404)
            {
                Server.ClearError();
                Response.Clear();
                Response.Redirect("~/404");
            }

            //Case Comment 00821145 - The website showed "no page" display out of sudden and unable to login to backend
            var err = ((HttpApplication)sender).Server.GetLastError();
            if (err.Message.Contains("Invalid root node configured for pages. No root node with the name of") || err.InnerException.Message.Contains("Invalid root node configured for pages. No root node with the name of"))
            {
                SystemManager.RestartApplication("Cause: invalid root node");
            }
        }

        private void Bootstrapper_Bootstrapped(object sender, EventArgs e)
        {
            ObjectFactory.Container.RegisterType<IWorkflowDefinitionResolver, CustomWorkflowDefinitionResolver>(new ContainerControlledLifetimeManager());
            ObjectFactory.Container.RegisterType<IWorkflowNotifier, CustomWorkflowNotifier>();

            EventHub.Subscribe<IDataEvent>(Content_Action);
            EventHub.Subscribe<IDynamicContentUpdatedEvent>(evt => DynamicContentUpdatedEventHandler(evt));

            GlobalConfiguration.Configure(WebApiConfig.Register);

            ObjectFactory.Container.RegisterType<ISitefinityControllerFactory, NinjectControllerFactory>(new ContainerControlledLifetimeManager());
            ObjectFactory.Container.RegisterType<IHtmlSanitizer, SitefinityExtendedHtmlSanitizer>(new ContainerControlledLifetimeManager());

            var factory = ObjectFactory.Resolve<ISitefinityControllerFactory>();
            ControllerBuilder.Current.SetControllerFactory(factory);

            Telerik.Sitefinity.Configuration.Config.RegisterSection<FuzzySearchConfig>();
            Telerik.Sitefinity.Configuration.Config.RegisterSection<SearchResultsConfig>();
            Telerik.Sitefinity.Configuration.Config.RegisterSection<SafraDecConfig>();
            Telerik.Sitefinity.Configuration.Config.RegisterSection<MembersonFtpConfig>();
            Telerik.Sitefinity.Configuration.Config.RegisterSection<PublishNotificationConfig>();
            Telerik.Sitefinity.Configuration.Config.RegisterSection<CustomWorkflowNotifierConfig>();

            FeatherActionInvokerCustom.Register();

            ApplyPageHeader();

            PublishingSystemFactory.UnregisterPipe(PageInboundPipe.PipeName);
            PublishingSystemFactory.RegisterPipe(PageInboundPipe.PipeName, typeof(CustomPageInboundPipe));

            CustomUserProfile.CheckIfOtpFieldsExist<SitefinityProfile>();
            CustomScheduledTaskBase.RegisterMembersonScheduledTasks();

            DecPersonas.Init();
        }

        private void Content_Action(IDataEvent dataEvent)
        {
            try
            {
                if (dataEvent.ItemType == typeof(PageNode) && dataEvent.Action == "Updated" &&
                    dataEvent.GetPropertyValue<string>("ApprovalWorkflowState") == "Published")
                {
                    new NotificationService().ProcessPagePublished(dataEvent);
                }
            }
            catch (Exception e)
            {
                Log.Write("ERROR in DataEvent : " + e.Message + " --- " + e.StackTrace);
            }
        }

        private void DynamicContentUpdatedEventHandler(IDynamicContentUpdatedEvent eventInfo)
        {
            try
            {
                var dataType = ((IDataEvent)eventInfo).ItemType;
                if (CustomWorkflowHelper.NotifyPublish(dataType) && 
                    eventInfo.Item.ApprovalWorkflowState == "Published" &&
                    eventInfo.Item.Status == ContentLifecycleStatus.Master)
                {
                    new NotificationService().ProcessContentPublished(eventInfo.Item, dataType);
                }
            }
            catch (Exception e)
            {
                Log.Write("ERROR in IDynamicContentUpdatedEvent : " + e.Message + " --- " + e.StackTrace);
            }
        }

        private void ApplyPageHeader()
        {
            EventHub.Subscribe<IPagePreRenderCompleteEvent>((x) =>
            {
                if (!x.PageSiteNode.IsBackend)
                {
                    var page = x.Page;
                    var pageTitle = page.Header.Title;
                    if (pageTitle == "Home")
                    {
                        page.Header.Title = "Singapore Armed Forces Reservist Association (SAFRA)";
                    }
                    else if (!(pageTitle.Contains("| SAFRA") || pageTitle.Contains("SAFRA |")))
                    {
                        page.Header.Title = string.Format("{0} | SAFRA", pageTitle);
                    }

                    var controls = x.Page.Header.Controls;
                    System.Web.UI.Control generatorControl = null;
                    for (int i = 0; i < x.Page.Header.Controls.Count; i++)
                    {
                        var control = x.Page.Header.Controls[i];
                        if ((control is HtmlMeta) && (control as HtmlMeta).Name == "Generator")
                        {
                            generatorControl = control;
                        }
                    }

                    x.Page.Header.Controls.Remove(generatorControl);
                }
            });
        }
    }
}