﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SAFRA.Utilities
{
	//credits to https://www.codeproject.com/Articles/403355/Implementing-Two-Factor-Authentication-in-ASP-NET
	public static class GoogleAuthenticatorUtils
    {
		private static readonly DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		private static long GetCounter()
		{
			return (long)(DateTime.UtcNow - UNIX_EPOCH).TotalSeconds / 30;
		}

		//generates string using RNGCryptoServiceProvider
		public static string GenerateSecret(int length)
		{
			var charArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_".ToCharArray();
			var byteArray = new byte[length];
			
			using (var rNGCryptoServiceProvider = new RNGCryptoServiceProvider())
			{
				rNGCryptoServiceProvider.GetBytes(byteArray);
			}
			
			var stringBuilder = new StringBuilder(length);
			var array = byteArray;
			
			for (int i = 0; i < array.Length; i++)
			{
				var singleByte = array[i];
				stringBuilder.Append(charArray[singleByte % charArray.Length]);
			}

			return stringBuilder.ToString();
		}

		//validates generated password in authenticator against generated password in server
		public static bool IsValidAuth(string secret, string password, int checkAdjacentIntervals = 1)
		{
			if (password == GetPassword(secret))
			{
				return true;
			}

			for (int i = 1; i <= checkAdjacentIntervals; i++)
			{
				if (password == GetPassword(secret, GetCounter() + i))
				{
					return true;
				}

				if (password == GetPassword(secret, GetCounter() - i))
				{
					return true;
				}
			}

			return false;
		}

		//gets key URI for QR code which will be used in authenticator
		public static string GetKeyUri(string secret, string label, string issuer)
		{
			var encoded = Base32EncodingUtils.Encode(Encoding.ASCII.GetBytes(secret), false);

			return string.Format("otpauth://totp/{2}:{1}?secret={0}&issuer={2}", encoded, label, issuer);
		}
	
		//gets generated token
		public static string GetPassword(string secret)
		{
			return GeneratePassword(secret, GetCounter(), 6);
		}

		//overload of above method
		private static string GetPassword(string secret, long counter)
		{
			return GeneratePassword(secret, counter, 6);
		}

		private static string GeneratePassword(string secret, long iterationNumber, int digits = 6)
		{
			var counter = BitConverter.GetBytes(iterationNumber);
			
			if (BitConverter.IsLittleEndian)
			{
				Array.Reverse(counter);
			}

			var key = Encoding.ASCII.GetBytes(secret);		
			var hmac = new HMACSHA1(key, true);		
			var hash = hmac.ComputeHash(counter);
			var offset = hash[hash.Length - 1] & 0xf;
			var binary = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16) | ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

			//change to 6 digits
			return (binary % (int)Math.Pow(10, digits)).ToString(new string('0', digits));
		}
	}
}