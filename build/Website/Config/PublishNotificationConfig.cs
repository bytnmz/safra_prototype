﻿using System.Configuration;
using Telerik.Sitefinity.Configuration;

namespace SAFRA.Config
{
    public class PublishNotificationConfig : ConfigSection
    {
        [ConfigurationProperty("isEnabled", DefaultValue = true, IsRequired = true)]
        public bool EnablePublishNotification
        {
            get
            {
                return (bool)this["isEnabled"];
            }
            set
            {
                this["isEnabled"] = value;
            }
        }

        [ConfigurationProperty("profile", DefaultValue = "SendGrid", IsRequired = true)]
        public string Profile
        {
            get
            {
                return (string)this["profile"];
            }
            set
            {
                this["profile"] = value;
            }
        }

        [ConfigurationProperty("subject", DefaultValue = "Publish notification", IsRequired = true)]
        public string Subject
        {
            get
            {
                return (string)this["subject"];
            }
            set
            {
                this["subject"] = value;
            }
        }

        [ConfigurationProperty("messageTemplateName", DefaultValue = "Publish notification", IsRequired = true)]
        public string MessageTemplateName
        {
            get
            {
                return (string)this["messageTemplateName"];
            }
            set
            {
                this["messageTemplateName"] = value;
            }
        }
    }
}