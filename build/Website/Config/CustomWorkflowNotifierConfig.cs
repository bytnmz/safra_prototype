﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Telerik.Sitefinity.Configuration;

namespace SAFRA.Config
{
    public class CustomWorkflowNotifierConfig : ConfigSection
    {
        [ConfigurationProperty("customForApproval", DefaultValue = false, IsRequired = true)]
        public bool CustomForApproval
        {
            get
            {
                return (bool)this["customForApproval"];
            }
            set
            {
                this["customForApproval"] = value;
            }
        }

        [ConfigurationProperty("forApprovalSubject", DefaultValue = "Request for approval", IsRequired = true)]
        public string ForApprovalSubject
        {
            get
            {
                return (string)this["forApprovalSubject"];
            }
            set
            {
                this["forApprovalSubject"] = value;
            }
        }

        [ConfigurationProperty("forApprovalMessageTemplateName", DefaultValue = "Request for approval", IsRequired = true)]
        public string ForApprovalMessageTemplateName
        {
            get
            {
                return (string)this["forApprovalMessageTemplateName"];
            }
            set
            {
                this["forApprovalMessageTemplateName"] = value;
            }
        }

        [ConfigurationProperty("customForPublishing", DefaultValue = false, IsRequired = true)]
        public bool CustomForPublishing
        {
            get
            {
                return (bool)this["customForPublishing"];
            }
            set
            {
                this["customForPublishing"] = value;
            }
        }

        [ConfigurationProperty("forPublishingSubject", DefaultValue = "Request for publishing", IsRequired = true)]
        public string ForPublishingSubject
        {
            get
            {
                return (string)this["forPublishingSubject"];
            }
            set
            {
                this["forPublishingSubject"] = value;
            }
        }

        [ConfigurationProperty("forPublishingMessageTemplateName", DefaultValue = "Request for publishing", IsRequired = true)]
        public string ForPublishingMessageTemplateName
        {
            get
            {
                return (string)this["forPublishingMessageTemplateName"];
            }
            set
            {
                this["forPublishingMessageTemplateName"] = value;
            }
        }

        [ConfigurationProperty("customRejection", DefaultValue = false, IsRequired = true)]
        public bool CustomRejection
        {
            get
            {
                return (bool)this["customRejection"];
            }
            set
            {
                this["customRejection"] = value;
            }
        }

        [ConfigurationProperty("rejectionSubject", DefaultValue = "Rejected #itemType", IsRequired = true)]
        public string RejectionSubject
        {
            get
            {
                return (string)this["rejectionSubject"];
            }
            set
            {
                this["rejectionSubject"] = value;
            }
        }

        [ConfigurationProperty("rejectionMessageTemplateName", DefaultValue = "Rejected content", IsRequired = true)]
        public string RejectionMessageTemplateName
        {
            get
            {
                return (string)this["rejectionMessageTemplateName"];
            }
            set
            {
                this["rejectionMessageTemplateName"] = value;
            }
        }

        [ConfigurationProperty("customPublishRejection", DefaultValue = false, IsRequired = true)]
        public bool CustomPublishRejection
        {
            get
            {
                return (bool)this["customPublishRejection"];
            }
            set
            {
                this["customPublishRejection"] = value;
            }
        }

        [ConfigurationProperty("publishRejectionSubject", DefaultValue = "Rejected #itemType", IsRequired = true)]
        public string PublishRejectionSubject
        {
            get
            {
                return (string)this["publishRejectionSubject"];
            }
            set
            {
                this["publishRejectionSubject"] = value;
            }
        }

        [ConfigurationProperty("publishRejectionMessageTemplateName", DefaultValue = "Rejected content", IsRequired = true)]
        public string PublishRejectionMessageTemplateName
        {
            get
            {
                return (string)this["publishRejectionMessageTemplateName"];
            }
            set
            {
                this["publishRejectionMessageTemplateName"] = value;
            }
        }
    }
}