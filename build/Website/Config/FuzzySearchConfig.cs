﻿using System.Configuration;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Localization;

namespace SAFRA.Config
{
    public class FuzzySearchConfig : ConfigSection
    {
        [ConfigurationProperty("isEnabled", DefaultValue = true, IsRequired = true)]
        public bool EnableFuzzySearch
        {
            get
            {
                return (bool)this["isEnabled"];
            }
            set
            {
                this["isEnabled"] = value;
            }
        }

        [ConfigurationProperty("similarity", DefaultValue = 0.7, IsRequired = false)]
        [ObjectInfo(Title = "Similarity", Description = "The value is between 0 and 1, with a value closer to 1 only terms with a higher similarity will be matched. Invalid value will default to 0.7")]
        public double Similarity
        {
            get
            {
                return (double)this["similarity"];
            }
            set
            {
                this["similarity"] = value;
            }
        }
    }
}