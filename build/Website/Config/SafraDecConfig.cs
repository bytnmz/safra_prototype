﻿using System.Configuration;
using Telerik.Sitefinity.Configuration;

namespace SAFRA.Config
{
    public class SafraDecConfig : ConfigSection
    {
        [ConfigurationProperty("authToken", DefaultValue = "", IsRequired = false)]
        public string AuthToken
        {
            get
            {
                return (string)this["authToken"];
            }
            set
            {
                this["authToken"] = value;
            }
        }

        [ConfigurationProperty("accessKey", DefaultValue = "", IsRequired = true)]
        public string AccessKey
        {
            get
            {
                return (string)this["accessKey"];
            }
            set
            {
                this["accessKey"] = value;
            }
        }

        [ConfigurationProperty("siteName", DefaultValue = "Safra", IsRequired = false)]
        public string SiteName
        {
            get
            {
                return (string)this["siteName"];
            }
            set
            {
                this["siteName"] = value;
            }
        }

        [ConfigurationProperty("decUrl", DefaultValue = "https://api.dec.sitefinity.com", IsRequired = true)]
        public string DecUrl
        {
            get
            {
                return (string)this["decUrl"];
            }
            set
            {
                this["decUrl"] = value;
            }
        }

        [ConfigurationProperty("contactsApiUrl", DefaultValue = "/analytics/v1/contacts", IsRequired = true)]
        public string ContactsAPIUrl
        {
            get
            {
                return (string)this["contactsApiUrl"];
            }
            set
            {
                this["contactsApiUrl"] = value;
            }
        }

        [ConfigurationProperty("authorizationApiUrl", DefaultValue = "/admin/v1/access-keys/issue-access-token", IsRequired = true)]
        public string AuthorizationAPIUrl
        {
            get
            {
                return (string)this["authorizationApiUrl"];
            }
            set
            {
                this["authorizationApiUrl"] = value;
            }
        }

        [ConfigurationProperty("relatedPersonasApiUrl", DefaultValue = "/analytics/v1/contacts/{contact-id}/personas", IsRequired = true)]
        public string RelatedPersonasAPIUrl
        {
            get
            {
                return (string)this["relatedPersonasApiUrl"];
            }
            set
            {
                this["relatedPersonasApiUrl"] = value;
            }
        }

        [ConfigurationProperty("scoringByContactIdApiUrl", DefaultValue = "/analytics/v1/scorings/personas/by-contact-ids", IsRequired = true)]
        public string ScoringContactIdAPIUrl
        {
            get
            {
                return (string)this["scoringByContactIdApiUrl"];
            }
            set
            {
                this["scoringByContactIdApiUrl"] = value;
            }
        }

        [ConfigurationProperty("scoringApiUrl", DefaultValue = "/analytics/v1/scorings/personas", IsRequired = true)]
        public string ScoringAPIUrl
        {
            get
            {
                return (string)this["scoringApiUrl"];
            }
            set
            {
                this["scoringApiUrl"] = value;
            }
        }
    }
}