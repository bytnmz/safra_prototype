﻿using System.Configuration;
using Telerik.Sitefinity.Configuration;

namespace SAFRA.Config
{
    public class MembersonFtpConfig : ConfigSection
    {
        [ConfigurationProperty("runExportDECScheduler", DefaultValue = "false", IsRequired = true)]
        public bool RunExportDECScheduler
        {
            get
            {
                return (bool)this["runExportDECScheduler"];
            }
            set
            {
                this["runExportDECScheduler"] = value;
            }
        }

        [ConfigurationProperty("containerName", DefaultValue = "", IsRequired = true)]
        public string ContainerName
        {
            get
            {
                return (string)this["containerName"];
            }
            set
            {
                this["containerName"] = value;
            }
        }

        [ConfigurationProperty("storageConn", DefaultValue = "", IsRequired = true)]
        public string StorageConn
        {
            get
            {
                return (string)this["storageConn"];
            }
            set
            {
                this["storageConn"] = value;
            }
        }

        [ConfigurationProperty("requestUri", DefaultValue = "##TodayDate##.ImportPersona", IsRequired = true)]
        public string RequestUri
        {
            get
            {
                return (string)this["requestUri"];
            }
            set
            {
                this["requestUri"] = value;
            }
        }

        [ConfigurationProperty("fileName", DefaultValue = "EmailTags.csv", IsRequired = true)]
        public string Filename
        {
            get
            {
                return (string)this["fileName"];
            }
            set
            {
                this["fileName"] = value;
            }
        }

        [ConfigurationProperty("userName", DefaultValue = "", IsRequired = true)]
        public string Username
        {
            get
            {
                return (string)this["userName"];
            }
            set
            {
                this["userName"] = value;
            }
        }

        [ConfigurationProperty("password", DefaultValue = "", IsRequired = true)]
        public string Password
        {
            get
            {
                return (string)this["password"];
            }
            set
            {
                this["password"] = value;
            }
        }
    }
}