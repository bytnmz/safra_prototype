﻿using System.Configuration;
using Telerik.Sitefinity.Configuration;

namespace SAFRA.Config
{
    public class SearchResultsConfig : ConfigSection
    {
        [ConfigurationProperty("noResultMessage", DefaultValue = "Sorry no items found", IsRequired = true)]
        public string NoResultMessage
        {
            get
            {
                return (string)this["noResultMessage"];
            }
            set
            {
                this["noResultMessage"] = value;
            }
        }
    }
}