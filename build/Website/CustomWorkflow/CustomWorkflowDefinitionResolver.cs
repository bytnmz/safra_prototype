﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Claims;
using Telerik.Sitefinity.Versioning;
using Telerik.Sitefinity.Workflow;
using Telerik.Sitefinity.Workflow.Model;

namespace SAFRA.CustomWorkflow
{
    public class CustomWorkflowDefinitionResolver : WorkflowDefinitionResolver
    {
        private string[] DepartmentRoles =
        {
            "BU/BD", "CE&I", "IAWR", "EnergyOne", "Kidz Amaze", "NurtureStars", "FINS",
            "Corporate Services", "Special Projects", "Cohesion Services",
            "CCK", "Mount Faber", "Jurong", "Punggol", "Tampines", "Toa Payoh", "Yishun", 
            "SM&C", "Membership Services MA", "Membership Services MB", "Membership Services MM", 
            "VMU"
        };

        public override IWorkflowExecutionDefinition ResolveWorkflowExecutionDefinition(IWorkflowResolutionContext context)
        {
            var currentIdentity = ClaimsManager.GetCurrentIdentity();

            if (currentIdentity.Roles.Any(r => r.Name == "Editors" || r.Name == "Administrators") &&
                CustomWorkflowHelper.ApplyWorkflow(context.ContentType))
            {
                bool wasPublished = false;
                Guid ownerId = Guid.Empty;
                WorkflowDefinition workflow = null;

                if (context.ContentType == typeof(PageNode))
                {
                    var node = PageManager.GetManager().GetPageNodes().FirstOrDefault(n => n.Id == context.ContentId);

                    if (node != null)
                    {
                        ownerId = node.Owner;

                        if (node.WasPublished)
                        {
                            wasPublished = true;

                            if (node.ApprovalWorkflowState == "AwaitingApproval")
                            {
                                workflow = GetEditWorkflowBasedOnLastModifier(node.PageId);
                            }
                            else
                            {
                                workflow = GetEditWorkflowBasedOnCurrentIdentity(currentIdentity);
                            }
                        }
                    }
                }
                else
                {
                    var item = new DynamicModuleManager().GetDataItems(context.ContentType)
                        .FirstOrDefault(c => c.Id == context.ContentId);

                    if (item != null)
                    {

                        var masterItem = item.Status == ContentLifecycleStatus.Master ? item :
                            new DynamicModuleManager().GetDataItems(context.ContentType)
                                        .FirstOrDefault(c => c.Id == item.OriginalContentId);

                        ownerId = masterItem.Owner;

                        wasPublished = new DynamicModuleManager().GetDataItems(context.ContentType)
                                        .Any(c => c.OriginalContentId == masterItem.Id &&
                                                    c.Status == ContentLifecycleStatus.Live && c.Visible);

                        if (wasPublished)
                        {
                            if (item != null && item.ApprovalWorkflowState == "AwaitingApproval")
                            {
                                workflow = GetEditWorkflowBasedOnLastModifier(masterItem.Id);
                            }
                            else
                            {
                                workflow = GetEditWorkflowBasedOnCurrentIdentity(currentIdentity);
                            }
                        }
                    }
                }

                if (!wasPublished)
                {
                    var roleName = string.Empty;

                    if (ownerId != Guid.Empty)
                    {
                        RoleManager roleManager = RoleManager.GetManager();
                        var roles = roleManager.GetRolesForUser(ownerId).ToList();
                        roleName = GetRole(roles.Select(r => r.Name));
                    }
                    else
                    {
                        roleName = GetRole(currentIdentity.Roles.Select(r => r.Name));
                    }

                    workflow = WorkflowManager.GetManager().GetWorkflowDefinitions()
                        .Where(d => d.Title == roleName + " Create").FirstOrDefault();
                }

                if (workflow != null && workflow.IsActive)
                {
                    return new WorkflowDefinitionProxy(workflow);
                }
            }

            return base.ResolveWorkflowExecutionDefinition(context);
        }

        private string GetRole(IEnumerable<string> roles)
        {
            foreach (var role in roles)
            {
                if (DepartmentRoles.Contains(role))
                {
                    return role;
                }
            }

            return string.Empty;
        }

        private WorkflowDefinition GetEditWorkflowBasedOnLastModifier(Guid id)
        {
            var vManager = VersionManager.GetManager();
            var history = vManager.GetItemVersionHistory(id);

            if (history.Any())
            {
                var lastModifiedOwner = history.First().Owner;

                RoleManager roleManager = RoleManager.GetManager();
                var roles = roleManager.GetRolesForUser(lastModifiedOwner).ToList();

                var roleName = GetRole(roles.Select(r => r.Name));

                return WorkflowManager.GetManager().GetWorkflowDefinitions()
                    .Where(d => d.Title == roleName + " Edit").FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        private WorkflowDefinition GetEditWorkflowBasedOnCurrentIdentity(SitefinityIdentity currentIdentity)
        {
            var roleName = GetRole(currentIdentity.Roles.Select(r => r.Name));

            return WorkflowManager.GetManager().GetWorkflowDefinitions()
                .Where(d => d.Title == roleName + " Edit").FirstOrDefault();
        }
    }
}