﻿using SAFRA.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Modules.Newsletters;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Services.Notifications;
using Telerik.Sitefinity.Workflow;
using Telerik.Sitefinity.Workflow.Activities;

namespace SAFRA.CustomWorkflow
{
    public class CustomWorkflowNotifier : WorkflowNotifier
    {
        private CustomWorkflowNotifierConfig config = ConfigManager.GetManager().GetSection<CustomWorkflowNotifierConfig>();

        protected override void BeforeEmailSend(
                                WorkflowNotificationContext context,
                                ref string subjectTemplate,
                                ref string bodyTemplate,
                                ref IDictionary<string, string> tokens,
                                ref IEnumerable<ISubscriberRequest> subscribers)
        {
            // If we want to override the default email that is sent when an item is for approval.
            if (context.ApprovalStatus == ApprovalStatusConstants.AwaitingApproval && config.CustomForApproval)
            {
                this.ComposeCustomEmail(context, ref subjectTemplate, ref bodyTemplate, ref tokens, 
                    config.ForApprovalSubject, config.ForApprovalMessageTemplateName, "Note for approvers:");
            }

            // If we want to override the default email that is sent when an item is for publishing.
            if (context.ApprovalStatus == ApprovalStatusConstants.AwaitingPublishing && config.CustomForPublishing)
            {
                this.ComposeCustomEmail(context, ref subjectTemplate, ref bodyTemplate, ref tokens,
                    config.ForPublishingSubject, config.ForPublishingMessageTemplateName, "Note for approvers:");
            }

            // If we want to override the default email that is sent when an item is rejected.
            if (context.ApprovalStatus == ApprovalStatusConstants.Rejected && config.CustomRejection)
            {
                this.ComposeCustomEmail(context, ref subjectTemplate, ref bodyTemplate, ref tokens,
                    config.RejectionSubject, config.RejectionMessageTemplateName, "Reason for rejection:");
            }

            // If we want to override the default email that is sent when an item is rejected.
            if (context.ApprovalStatus == ApprovalStatusConstants.RejectedForPublishing && config.CustomPublishRejection)
            {
                this.ComposeCustomEmail(context, ref subjectTemplate, ref bodyTemplate, ref tokens,
                    config.PublishRejectionSubject, config.PublishRejectionMessageTemplateName, "Reason for rejection:");
            }
        }

        private void ComposeCustomEmail(
                        WorkflowNotificationContext context,
                        ref string subjectTemplate,
                        ref string bodyTemplate,
                        ref IDictionary<string, string> tokens,
                        string subject,
                        string bodyTemplateName,
                        string noteForApprovers)
        {
            subjectTemplate = subject;

            var messageTemplate = NewslettersManager.GetManager().GetMessageBodies()
                    .FirstOrDefault(x => x.Name == bodyTemplateName);

            if (messageTemplate != null)
            {
                bodyTemplate = messageTemplate.BodyText;
            }

            User user = UserManager.FindUser(context.LastStateChangerId);

            tokens = new Dictionary<string, string>
            {
                { "#subject", subjectTemplate },
                { "#itemType", context.ItemTypeLabel },
                { "#itemTitle", context.ItemTitle },
                { "#itemUrl", context.ItemPreviewUrl },
                { "#user", user == null ? "N/A" : user.UserName },
                { "#note", context.ApprovalNote },
                { "#labelForNote", !context.ApprovalNote.IsNullOrWhitespace() ? noteForApprovers : string.Empty }
            };
        }
    }
}