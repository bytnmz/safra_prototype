﻿using System;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SAFRA.CustomWorkflow
{
    public static class CustomWorkflowHelper
    {
        public static bool ApplyWorkflow(Type contentType)
        {
            return contentType == typeof(PageNode) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.WhatsOn) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Facility) || 
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Service) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Promotion) || 
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Club) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.EventSubpage) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Announcement) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Recommendation) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.MediaRelease);
        }

        public static bool NotifyPublish(Type contentType)
        {
            return contentType == typeof(PageNode) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.WhatsOn) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Facility) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Service) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.Promotion) ||
                contentType == TypeResolutionService.ResolveType(ContentTypeConstants.EventSubpage);
        }
    }
}