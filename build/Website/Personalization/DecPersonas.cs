﻿using Newtonsoft.Json;
using SAFRA.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Telerik.DigitalExperienceCloud.Client;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DataIntelligenceConnector.Configuration;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Workflow;

namespace SAFRA.Personalization
{
    public class DecPersonas
    {
        private const string LibraryTitle = "Personalization";
        private const string DocumentTitle = "DecPersonas";
        private const string Extension = ".json";

        private const string InterestsTaxonName = "Categories";
        private const string LocationsTaxonName = "locations";
        private const string PersonasTaxonName = "personas";

        private const string InterestsFilterName = "interest";
        private const string LocationsFilterName = "location";
        private const string PersonasFilterName = "persona";

        public static void Init()
        {
            try
            {
                var decPersonas = GetDecPersonas();

                if (decPersonas.Any())
                {
                    GetCmsMapping(ref decPersonas);
                    var personasString = JsonConvert.SerializeObject(decPersonas);
                    WriteToFile(LibraryTitle, DocumentTitle, Extension, personasString);
                }
            }
            catch (Exception e)
            {
                Log.Write("ERROR in initialising DEC Personas : " + e.Message + " --- " + e.StackTrace);
            }
        }

        private static List<Dictionary<string, object>> GetDecPersonas()
        {
            var personaDict = new List<Dictionary<string, object>>();

            var configManager = ConfigManager.GetManager();
            var safraDecConfig = configManager.GetSection<SafraDecConfig>();
            var accessKey = safraDecConfig.AccessKey;
            var siteName = safraDecConfig.SiteName;

            if (!accessKey.IsNullOrWhitespace())
            {
                var accessToken = new AccessToken("AccessKey", accessKey);

                var config = configManager.GetSection<DigitalExperienceCloudConnectorConfig>();
                var dataCenterKey = config.SiteToApiKeyMappings[siteName]?.DataCenterApiKey;

                var scoringClient = new ScoringClient(accessToken, dataCenterKey);

                var personas = scoringClient.GetAllPersonas(new LoadOptions()).Result;

                foreach (var persona in personas)
                {
                    personaDict.Add(new Dictionary<string, object>
                {
                    { "Id", persona.Id },
                    { "Name", persona.Name }
                });
                }
            }

            return personaDict;
        }
        
        private static void GetCmsMapping(ref List<Dictionary<string, object>> decPersonas)
        {
            var interests = GetMapping(InterestsTaxonName);
            var locations = GetMapping(LocationsTaxonName);
            var personas = GetMapping(PersonasTaxonName);

            foreach (var decPersona in decPersonas)
            {
                object decName;

                if (decPersona.TryGetValue("Name", out decName))
                {
                    var name = decName.ToString();
                    var cmsName = string.Empty;

                    if (interests.TryGetValue(name, out cmsName))
                    {
                        decPersona.Add("CmsName", cmsName);
                        decPersona.Add("Type", InterestsFilterName);
                    }
                    else if (locations.TryGetValue(name, out cmsName))
                    {
                        decPersona.Add("CmsName", cmsName);
                        decPersona.Add("Type", LocationsFilterName);
                    }
                    else if (personas.TryGetValue(name, out cmsName))
                    {
                        decPersona.Add("CmsName", cmsName);
                        decPersona.Add("Type", PersonasFilterName);
                    }
                    else
                    {
                        Log.Write("ERROR : persona " + name + " is not recognised in the CMS");
                    }
                }
            }
        }

        private static Dictionary<string, string> GetMapping(string taxonName)
        {
            var mapping = new Dictionary<string, string>();

            var taxon = TaxonomyManager.GetManager().GetTaxonomies<HierarchicalTaxonomy>()
                .SingleOrDefault(t => t.Name == taxonName);

            if (taxon != null)
            {
                foreach (var t in taxon.Taxa)
                {
                    mapping.Add(t.Title, t.Name);
                }
            }
            else
            {
                Log.Write("ERROR in retrieving taxonomy with name " + taxonName);
            }

            return mapping;
        }

        private static void WriteToFile(string library, string docTitle, string extension, string content)
        {
            var librariesManager = LibrariesManager.GetManager();

            var documentLibrary = librariesManager.GetDocumentLibraries().Where(d => d.Title == library).SingleOrDefault();

            if (documentLibrary != null)
            {
                var document = librariesManager.GetDocuments().Where(d => d.Title == docTitle
                                && d.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Master).FirstOrDefault();

                byte[] byteArray = Encoding.ASCII.GetBytes(content);
                MemoryStream documentStream = new MemoryStream(byteArray);

                using (new ElevatedModeRegion(librariesManager))
                {
                    if (document != null && document.Parent == documentLibrary)
                    {
                        Document temp = librariesManager.Lifecycle.CheckOut(document) as Document;
                        temp.LastModified = DateTime.UtcNow;
                        librariesManager.Upload(temp, documentStream, extension);

                        //Recompiles and validates the url of the document.
                        librariesManager.RecompileAndValidateUrls(temp);

                        //Checkin the temp and get the updated master version. 
                        //After the check in the temp version is deleted.
                        document = librariesManager.Lifecycle.CheckIn(temp) as Document;
                    }
                    else
                    {
                        //The document is created as master. 
                        document = librariesManager.CreateDocument();

                        //Set the parent document library.
                        document.Parent = documentLibrary;

                        //Set the properties of the document.
                        document.Title = docTitle;
                        document.DateCreated = DateTime.UtcNow;
                        document.PublicationDate = DateTime.UtcNow;
                        document.LastModified = DateTime.UtcNow;
                        document.UrlName = Regex.Replace(docTitle.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");
                        document.MediaFileUrlName = Regex.Replace(docTitle.ToLower(), @"[^\w\-\!\$\'\(\)\=\@\d_]+", "-");

                        //Upload the document file.
                        librariesManager.Upload(document, documentStream, extension);

                        //Recompiles and validates the url of the document.
                        librariesManager.RecompileAndValidateUrls(document);
                    }

                    librariesManager.SaveChanges();
                }

                //Publish the document.
                var bag = new Dictionary<string, string>();
                bag.Add("ContentType", typeof(Document).FullName);
                SystemManager.RunWithElevatedPrivilege(d => WorkflowManager.MessageWorkflow(document.Id, typeof(Document), null, "Publish", false, bag));
            }
            else
            {
                Log.Write(string.Format("ERROR in writing {0} : {1} is missing.", docTitle, library));
            }
        }
    }
}