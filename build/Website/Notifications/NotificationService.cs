﻿using SAFRA.Config;
using SAFRA.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.Data.Events;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Newsletters;
using Telerik.Sitefinity.Modules.Pages;
using Telerik.Sitefinity.Pages.Model;
using Telerik.Sitefinity.Publishing;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.Services.Notifications;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Web;

namespace SAFRA.Notifications
{
    public class NotificationService
    {
        private PublishNotificationConfig publishConfig;

        public NotificationService()
        {
            publishConfig = ConfigManager.GetManager().GetSection<PublishNotificationConfig>();
        }

        public void ProcessPagePublished(IDataEvent dataEvent)
        {
            if (publishConfig != null && publishConfig.EnablePublishNotification)
            {
                var manager = ManagerBase.GetMappedManager(dataEvent.ItemType, dataEvent.ProviderName);

                if (dataEvent.GetPropertyValue<bool>("HasPageDataChanged"))
                {
                    var item = (PageNode)manager.GetItemOrDefault(dataEvent.ItemType, dataEvent.ItemId);

                    if (item != null)
                    {
                        var approvalRecords = item.GetApprovalRecords();

                        if (approvalRecords != null && approvalRecords.Any())
                        {
                            var userManager = UserManager.GetManager();
                            var authorsApprovers = approvalRecords.Select(x => userManager.GetUser(x.UserId));
                            var emails = authorsApprovers.Select(x => x.Email).Distinct();
                            var title = item.Title;
                            var url = UrlPath.ResolveUrl(item.GetUrl(), true, true);
                            SendPublishedNotification(title, url, emails.ToList());
                        }
                    }
                }
            }
        }

        public void ProcessContentPublished(DynamicContent item, Type itemType)
        {
            if (publishConfig != null && publishConfig.EnablePublishNotification)
            {
                var approvalRecords = item.GetApprovalRecords();

                if (approvalRecords != null && approvalRecords.Any())
                {
                    var userManager = UserManager.GetManager();
                    var authorsApprovers = approvalRecords.Select(x => userManager.GetUser(x.UserId));
                    var emails = authorsApprovers.Select(x => x.Email).Distinct();

                    var title = item.GetString("Title");
                    var clService = SystemManager.GetContentLocationService();
                    var url = clService.GetItemDefaultLocation(item)?.ItemAbsoluteUrl;

                    if (itemType == TypeResolutionService.ResolveType(ContentTypeConstants.Promotion)) 
                    {
                        url = "https://wwww.safra.sg/promotions/" + item.UrlName;
                    }

                    SendPublishedNotification(title, url, emails.ToList());
                }
            }
        }

        private void SendPublishedNotification(string title, string url, List<string> userEmails)
        {
            var ns = SystemManager.GetNotificationService();
            var titleSlug = title.GenerateSlug();
            var context = new ServiceContext(titleSlug, "SendPublishedNotification");
            var contextDictionary = new Dictionary<string, string>();
            contextDictionary.Add("MergeData.Time", DateTime.UtcNow.ToString());

            List<ISubscriberRequest> subscribers = new List<ISubscriberRequest>();
            var count = 0;

            foreach (var email in userEmails)
            {
                subscribers.Add(new SubscriberRequestProxy
                {
                    Email = email,
                    ResolveKey = titleSlug + "-" + ++count
                });
            }

            var profileName = publishConfig.Profile;
            var subjectTemplate = publishConfig.Subject;

            var bodyTemplate = "Item has been published " + title + " " + url;

            var messageTemplate = NewslettersManager.GetManager().GetMessageBodies()
                .FirstOrDefault(x => x.Name == publishConfig.MessageTemplateName);

            if (messageTemplate != null && !messageTemplate.BodyText.IsNullOrWhitespace())
            {
                bodyTemplate = messageTemplate.BodyText.Replace("{|Title|}", title).Replace("{|URL|}", url);
            }

            var tmpl = new MessageTemplateRequestProxy() { Subject = subjectTemplate, BodyHtml = bodyTemplate };

            IMessageJobRequest job = new MessageJobRequestProxy()
            {
                MessageTemplate = tmpl,
                Subscribers = subscribers,
                SenderProfileName = profileName
            };

            ns.SendMessage(context, job, contextDictionary);
        }
    }
}