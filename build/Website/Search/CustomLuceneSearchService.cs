﻿using SAFRA.Config;
using System.Collections.Generic;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Services.Search;
using Telerik.Sitefinity.Services.Search.Data;
using Telerik.Sitefinity.Utilities;

namespace SAFRA
{
    public class CustomLuceneSearchService : LuceneSearchService
    {
        public override void UpdateIndex(string indexName, IEnumerable<IDocument> documents)
        {
            var batches = documents.OnBatchesOf(200);

            foreach (var batch in batches)
            {
                base.UpdateIndex(indexName, batch);
            }
        }

        protected override string BuildQuery(ISearchQuery input)
        {

            var manager = ConfigManager.GetManager();
            var config = manager.GetSection<FuzzySearchConfig>();

            if (config.EnableFuzzySearch)
            {
                var similarity = config.Similarity;

                if (!(0 < similarity && similarity < 1))
                {
                    similarity = 0.7;
                    config.Similarity = 0.7;
                }

                // the default query
                var oldQuery = base.BuildQuery(input);

                // adding ~ to all terms,
                // so that when more than one word is searched,
                // all of them (not just the last) can use the fuzzy functionality
                foreach (var term in input.SearchGroup.Terms)
                {
                    term.Value = string.Join("~ ", term.Value.ToString().Split(' '));
                }

                // fuzzy search - replacing the dafult *s with ~s
                var fuzzyQuery = base.BuildQuery(input).Replace("*", "~" + similarity);

                // to preserve the default functionality of the search service, keep the old query
                var final = string.Format("{0} OR {1}", oldQuery, fuzzyQuery);

                return final;
            }
            else
            {
                return base.BuildQuery(input);
            }
        }
    }
}