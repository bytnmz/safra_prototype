﻿using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Services.Search.Configuration;
using Telerik.Sitefinity.Services.Search;
using Telerik.Sitefinity.Services.Search.Data;
using Telerik.Sitefinity.Utilities;

namespace SAFRA
{
    public class CustomAzureSearchService : AzureSearchService
    {
        private readonly ISearchServiceClient azureSearchClient;

        public CustomAzureSearchService()
        {
            var searchConfig = Telerik.Sitefinity.Configuration.Config.Get<SearchConfig>();
            var parameters = searchConfig.SearchServices[AzureSearchService.ServiceName].Parameters;
            string azureSearchServiceName = parameters[AzureSearchService.AzureSearchServiceName];
            string azureAdminKey = parameters[AzureSearchService.AzureServiceAdminKey];
            if (!string.IsNullOrEmpty(azureSearchServiceName) && !string.IsNullOrEmpty(azureAdminKey))
            {
                try
                {
                    this.azureSearchClient = new SearchServiceClient(azureSearchServiceName, new SearchCredentials(azureAdminKey));
                }
                catch (Exception ex)
                {
                    if (Exceptions.HandleException(ex, ExceptionPolicyName.IgnoreExceptions))
                        throw;
                }
            }
        }

        public override void UpdateIndex(string indexName, IEnumerable<IDocument> documents)
        {
            var batches = documents.OnBatchesOf(200);

            foreach (var batch in batches)
            {
                base.UpdateIndex(indexName, batch);
            }
        }

        public override void RemoveDocuments(string indexName, IEnumerable<IDocument> documents)
        {
            this.RemoveDocumentsInternal(indexName, documents.Select(d => d.IdentityField));
        }

        public override void RemoveDocument(string indexName, IField identityField)
        {
            this.RemoveDocumentsInternal(indexName, new List<IField>() { identityField });
        }

        private void RemoveDocumentsInternal(string indexName, IEnumerable<IField> identityFields)
        {
            if (string.IsNullOrEmpty(indexName))
                throw new ArgumentNullException("indexName");
            if (identityFields == null)
                throw new ArgumentNullException("identityFields");

            if (identityFields.Any())
            {
                using (var indexClient = this.azureSearchClient.Indexes.GetClient(indexName))
                {
                    List<string> docsToDelete = new List<string>();

                    foreach (var identityField in identityFields)
                    {
                        docsToDelete.Add(identityField.Value.ToString());
                    }

                    var batch = IndexBatch.Delete("IdentityField", docsToDelete);
                    var result = indexClient.Documents.Index(batch);
                }
            }
        }
    }
}
