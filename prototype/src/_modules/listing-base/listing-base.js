"use strict";

import $ from "jquery";
import dot from "dot";
import imagesLoaded from "imagesloaded";
//import CustomSelect from '../custom-select/custom-select';
//import TabsFilter from '../molecules/tabs-filter/tabs-filter';

imagesLoaded.makeJQueryPlugin($);

export default class ListingBase {
  constructor() {
    this.windowHash = window.location.hash;

    this.template = `
    {{?it.length}}
      {{~it :item:index}}
        <li>
          <a href="{{=item.url}}">{{=item.name}}</a>
        </li>
      {{~}}
    {{??}}
      <li>No data.</li>
    {{?}}
    `;

    this.endpoint = "/apis/endpoint";

    this.parameters = {};

    this.queries = this.getUrlQueries();

    this.beforeGetData = () => {
      console.log("Before calling AJAX");
    };

    this.getDataCallback = res => {
      console.log("Response from AJAX", res);
    };
  }

  getData(before = this.beforeGetData, cb = this.getDataCallback) {
    before();

    $.ajax({
      url: this.endpoint,
      method: "GET",
      data: this.parameters,
      dataType: "json",
      success: res => {
        cb(res);
      },
      error: err => {
        console.log(err);
      }
    });
  }

  renderTemplate(data, $container, type) {
    let errorMessage = $(".listing-template").data("error-message");
    /*
      data = [
        ..., { item object }
      ]
     */

    if (data.length) {
      let dotTemplate = dot.template(this.template)(data);

      if (type === "loadmore") {
        let $lastItemBeforeLoad = $(".listing-item", $container).last(),
          headerHeight = $(".site-header").outerHeight(),
          scrollPosition = $lastItemBeforeLoad.offset().top - headerHeight - 20;

        $container.append(dotTemplate);
        this.loadmore = false;

        //Scroll to last item before loaded more
        $("html, body").animate({ scrollTop: scrollPosition }, 600);
      } else {
        $container.html(dotTemplate);
      }
    } else {
      $container.html(`<h2>${errorMessage}</h2>`);
    }

    $container.imagesLoaded().done(function(instance) {
      $(".match-height-elm").matchHeight();
      $(".match-height-section").matchHeight();
    });
  }

  getUrlQueries() {
    let queryString = {};
    let query = window.location.search.substring(1);
    if (!query.length) {
      return queryString;
    }
    let vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
      let pair = vars[i].split("=");

      /* If first entry with this name */
      if (typeof queryString[pair[0]] === "undefined") {
        queryString[pair[0]] = decodeURIComponent(pair[1]);

        /* If second entry with this name */
      } else if (typeof queryString[pair[0]] === "string") {
        let arr = [queryString[pair[0]], decodeURIComponent(pair[1])];
        queryString[pair[0]] = arr;

        /* If third or later entry with this name */
      } else {
        queryString[pair[0]].push(decodeURIComponent(pair[1]));
      }
    }
    return queryString;
  }

  updateURL(replaceState = false) {
    let queryString = "?";

    let i = 0;
    for (let param in this.parameters) {
      if (i > 0) {
        queryString += "&";
      }

      queryString += param + "=" + encodeURIComponent(this.parameters[param]);
      i++;
    }

    for (let query in this.queries) {
      if (typeof this.parameters[query] == "undefined") {
        if (i > 0) {
          queryString += "&";
        }

        queryString += query + "=" + encodeURIComponent(this.queries[query]);
        i++;
      }
    }

    queryString += this.windowHash;

    if (replaceState) {
      window.history.replaceState(this.parameters, "", queryString);
    } else {
      window.history.pushState(this.parameters, "", queryString);
    }
  }

  setupCheckboxes(fieldName) {
    /*
      - used by listing pages which has checkboxes of name="{fieldName}" with "All" options
      - to be called upon page load
      - will set {this.parameters[fieldName]} with the value of {this.queries[fieldName]}
    */

    this.parameters[fieldName] = this.getCheckboxesValues(fieldName);

    if (this.queries[fieldName]) {
      this.parameters[fieldName] = this.queries[fieldName]
        ? this.queries[fieldName]
        : "all";
    }
    this.updateCheckboxesView(fieldName);

    this._addCheckboxesListener(fieldName);
  }

  updateCheckboxesView(fieldName) {
    /*
    - to be called whenever the {fieldName} checkboxes needs to react based on {this.parameters[fieldName]}
    - will set the 'checked' prop for the selected {fieldName} checkboxes based on the {this.parameters[fieldName]}
     */

    if (this.parameters[fieldName] == "all") {
      $(`input[name="${fieldName}"]`).map((i, ele) => {
        let $this = $(ele);

        if ($this.val() == "all") {
          $this.prop("checked", true);
        } else {
          $this.prop("checked", false);
        }
      });
    } else {
      let values = this.parameters[fieldName].split("|");
      $(`input[name="${fieldName}"][value="all"]`).prop("checked", false);
      $(`input[name="${fieldName}"]`).map((i, ele) => {
        let $this = $(ele);

        if (values.indexOf($this.val()) !== -1) {
          $this.prop("checked", true);
        } else {
          $this.prop("checked", false);
        }
      });
    }
  }

  getCheckboxesValues(fieldName) {
    /*       console.log(fieldName);
      - will return the piped-value string based on selected checkboxes under {$fields}
    */
    let values = [];
    $(`input[name="${fieldName}"]`)
      .toArray()
      .map(ele => {
        let $this = $(ele);

        if ($this.is(":checked")) {
          values.push($this.val());

          window.decClient.sentenceClient.writeSentence({
            predicate: `filter${fieldName}`,
            object: $this.val()
          });
        }

        //personalization triggers
      });

    return values.join("|");
  }

  _addCheckboxesListener(fieldName) {
    /*
      - for checkboxes with "ALL" options only
      - to be called to add event listener to the checkbox of {fieldName}
      - on every change of value (delay of 400ms), this will:
        - update this.parameters[fieldName]
        - uncheck all the other checkboxes when "ALL" is being checked
        - uncheck "ALL" when any of other checkbox is being checked
        - check "ALL" when all other checkboxes are being unchecked
        - always reset this.parameters.page = 1
        - call {this.getData()}
        - call {this.updateURL()}
    */

    let checkingTimeout;
    $(`input[name="${fieldName}"]`).map((i, ele) => {
      let $this = $(ele);
      $this.on("change", e => {
        if ($this.prop("checked") == true) {
          if ($this.val() == "all") {
            /* uncheck all other checkbox when ALL is checked, and hide all subgroups */
            $(`input[name="${fieldName}"]`).prop("checked", false);
            $this.prop("checked", true);
            // this.parameters[fieldName] = this.getCheckboxValues(fieldName);
            // this.parameters.page = 1;
            // this.getData();
            // this.updateURL();
          } else {
            $(`input[name="${fieldName}"][value="all"]`).prop("checked", false);
            // clearTimeout(checkingTimeout);
            // checkingTimeout = setTimeout(() => {
            //   this.parameters[fieldName] = this.getCheckboxValues(fieldName);
            //   this.parameters.page = 1;
            //   this.getData();
            //   this.updateURL();
            // }, 300);
          }
        } else {
          if ($this.val() == "all") {
            /* ALL checkbox cannot be unchecked */
            $this.prop("checked", true);
          } else {
            if ($(`input[name="${fieldName}"]:checked`).length == 0) {
              /* when no checkbox is checked, ALL will be checked automatically */
              $(`input[name="${fieldName}"][value="all"]`).prop(
                "checked",
                true
              );
            }
            // clearTimeout(checkingTimeout);
            // checkingTimeout = setTimeout(() => {
            //   this.parameters[fieldName] = this.getCheckboxValues(fieldName);
            //   this.parameters.page = 1;
            //   this.getData();
            //   this.updateURL();
            // }, 300);
          }
        }
      });
    });
  }

  setupCheckbox(fieldName) {
    /*
      - used for single checkbox that normally indicate as true/false value
      - to be called upon page load
      - will set {this.parameters[fieldName]} with the value of {this.queries[fieldName]}
    */
    this.parameters[fieldName] = this.queries[fieldName]
      ? this.queries[fieldName]
      : "";

    this.updateCheckboxView(fieldName);

    // this._addCheckboxListener(fieldName);
  }

  updateCheckboxView(fieldName) {
    /*
      - to be called whenever the {fieldName} checkbox needs to react based on {this.parameters[fieldName]}`
      - will set the 'checked' prop for the selected {fieldName} checkboxes based on the {this.parameters[f`ieldName]}
    */

    if (this.parameters[fieldName] == $(`input[name="${fieldName}"]`).val()) {
      $(`input[name="${fieldName}"]`).prop("checked", true);
    } else {
      $(`input[name="${fieldName}"]`).prop("checked", false);
    }
  }

  getCheckboxValue(fieldName) {
    let $field = $(`input[name="${fieldName}"]`);

    return $field.prop("checked") == true ? $field.val() : "";
  }

  _addCheckboxListener(fieldName) {
    /*
      - for single checkbox only
      - to be called to add event listener to the checkbox of {fieldName}
    */

    let $checkbox = $(`input[name="${fieldName}"]`);

    $checkbox.on("change", e => {
      if ($checkbox.prop("checked") == true) {
        this.parameters[fieldName] = $checkbox.val();
      } else {
        this.parameters[fieldName] = "";
      }
      this.parameters.page = 1;
      this.getData();
      this.updateURL();
    });
  }

  setupSearchbar(fieldName) {
    this.parameters[fieldName] = this.queries[fieldName]
      ? this.queries[fieldName]
      : "";

    this.updateSearchbarView(fieldName);

    // this._addSearchbarListener(fieldName);
  }

  updateSearchbarView(fieldName) {
    $(`input[name="${fieldName}"]`).val(this.parameters[fieldName]);
  }

  // _addSearchbarListener(fieldName) {
  //   let inputTimeout;
  //   $(`input[name="${fieldName}"]`).on('input', e => {
  //     let $this = $(e.target);
  //     clearTimeout(inputTimeout);

  //     inputTimeout = setTimeout(() => {
  //       this.parameters[fieldName] = $this.val();
  //       this.parameters.page = 1;
  //       this.getData();
  //       this.updateURL();
  //     }, 400);
  //   });
  // }

  // setupRadioButton(fieldName) {
  //   this.parameters[fieldName] = (this.queries[fieldName]) ? this.queries[fieldName] : '';

  //   this.updateRadioButtonView(fieldName);

  //   this._addRadioButtonListener(fieldName);
  // }

  // updateRadioButtonView(fieldName) {
  //   $(`input[name="${fieldName}"]`).map((i, ele) => {
  //     let $this = $(ele);
  //     if ($this.val() == this.parameters[fieldName]) {
  //       $this.prop('checked', true);
  //     } else {
  //       $this.prop('checked', false);
  //     }
  //   });
  // }

  // getRadioButtonValue(fieldName) {
  //   let value;
  //   $(`input[name="${fieldName}"]`).map((i, ele) => {
  //     let $this = $(ele);
  //     if ($this.prop('checked') == true) {
  //       value = $this.val();
  //       return false; // break the loop once value is found
  //     }
  //   });

  //   return value;
  // }

  // _addRadioButtonListener(fieldName) {
  //   $(`input[name="${fieldName}"]`).map((i, ele) => {
  //     let $this = $(ele);
  //     $this.on('change', e => {
  //       if ($this.prop('checked') == true) {
  //         this.parameters[fieldName] = $this.val();
  //       }
  //       this.getData();
  //       this.updateURL();
  //     });
  //   });
  // }

  setupSelect(fieldName) {
    let $select = $(`select[name="${fieldName}"]`);

    this.parameters[fieldName] = this.queries[fieldName]
      ? this.queries[fieldName]
      : $select.find("option:eq(0)").val();

    let $parent = $select.parent();

    this.updateSelectView(fieldName);
  }

  updateSelectView(fieldName) {
    let $select = $(`select[name="${fieldName}"]`);
    $select.val(this.parameters[fieldName]);
    $select.niceSelect("update");
  }

  setupPagination(paramName) {
    this.parameters[paramName] = this.queries[paramName]
      ? parseInt(this.queries[paramName])
      : 1;
  }

  setupTabsFilter(fieldName) {
    this.parameters[fieldName] = this.queries[fieldName]
      ? this.queries[fieldName]
      : "all";
  }

  updateTabsFilterView(fieldName, $tabsFilter, tabsFilter) {
    let $selectedOption = $(
      `li[data-value="${this.parameters[fieldName]}"]`,
      $tabsFilter
    );

    tabsFilter.setActive($selectedOption);
  }
}
