"use strict";

export default class SiteNav {
  constructor() {
    $(".lvl1item").on("mouseenter", function() {
      let $activeIndicator = $(".active-menu-indicator");
      let activeItemwidth = $(this).outerWidth();
      let position = $(this).position().left;

      $activeIndicator.css("width", activeItemwidth);
      $activeIndicator.css("left", position);
    });

    $(".lvl1item").on("mouseleave", function() {
      let $activeIndicator = $(".active-menu-indicator");

      $activeIndicator.css("width", 0);
      $activeIndicator.css("left", "100%");
    });
  }
}
