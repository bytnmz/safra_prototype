"use strict";

import slick from "slick-carousel";

export default class PhotoGallery {
  constructor() {
    let $parent = $(".photo-gallery");

    $(".gallery-slider__main", $parent).on("init", function() {
      $(".loader").hide();
    });

    $(".gallery-slider__main", $parent).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: ".gallery-slider__nav-wrapper"
    });
    $(".gallery-slider__nav-wrapper", $parent).slick({
      slidesToShow: 8,
      slidesToScroll: 1,
      nextArrow: $(".next-image", ".gallery-slider__nav"),
      prevArrow: $(".prev-image", ".gallery-slider__nav"),
      asNavFor: ".gallery-slider__main",
      dots: false,
      centerMode: false,
      focusOnSelect: true,
      infinite: true,
      responsive: [
        {
          breakpoint: 1023,
          settings: {
            slidesToShow: 6
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3
          }
        }
      ]
    });
  }
}
