"use strict";

import { getDistance } from "geolib";
import dot from "dot";
import Cookies from "js-cookie";

export default class LocationNotification {
  constructor() {
    let $parent = $(".location-notification");
    let templateId = $(".message-template", $parent).data("template-id");
    this.endpoint = $(".message-template", $parent).data("api");
    this.template = $(`#${templateId}`)
      .html()
      .trim();
    let locationConfiguration = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    let cookieDisabled = Cookies.get("notification-disabled");

    if (cookieDisabled !== undefined) {
      if (!cookieDisabled) {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
            this.handleLocationData.bind(this),
            this.error,
            locationConfiguration
          );
        }
      }
    } else {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          this.handleLocationData.bind(this),
          this.error,
          locationConfiguration
        );
      }
    }
  }

  error(error) {
    console.log(error);
  }

  handleLocationData(pos) {
    if (pos) {
      this.currentLocation = {
        latitude: pos.coords.latitude,
        longitude: pos.coords.longitude
      };
      $.ajax({
        url: this.endpoint,
        method: "GET",
        dataType: "json",
        success: res => {
          this.showLocationMessage(res);
        },
        error: err => {
          console.log(err);
        }
      });
    }
  }

  showLocationMessage(locations) {
    let nearestClub = {
      name: "",
      distance: 0,
      item: null
    };
    locations.forEach(location => {
      let latitiude = parseFloat(location.Coordinates.lat);
      let longitude = parseFloat(location.Coordinates.long);
      let distance = getDistance(
        {
          latitude: this.currentLocation.latitude,
          longitude: this.currentLocation.longitude
        },
        {
          latitude: latitiude,
          longitude: longitude
        }
      );

      if (nearestClub.distance !== 0) {
        if (distance <= nearestClub.distance) {
          nearestClub.item = location;
          nearestClub.name = location.Name;
          nearestClub.distance = distance;
        }
      } else {
        nearestClub.item = location;
        nearestClub.name = location.Name;
        nearestClub.distance = distance;
      }
    });

    let dotTemplate = dot.template(this.template)(nearestClub);
    $(".notification-message", ".location-notification").html(dotTemplate);

    $(".location-notification").slideDown();
    $(".notification-yes").click(e => {
      e.preventDefault();
      $(".location-notification").hide();
      Cookies.set("notification-disabled", true);
      let url = $(e.currentTarget).attr("href");
      window.location.href = url;
    });

    $(".notification-no").click(e => {
      e.preventDefault();
      $(".location-notification").fadeOut();
      Cookies.set("notification-disabled", true);
    });
  }
}
