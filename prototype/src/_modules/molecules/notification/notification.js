"use strict";

import Cookies from "js-cookie";

export default class Notification {
  constructor() {
    let $announcementBar = $(".announcement"),
      $closeIcon = $(".announcement__close", $announcementBar);

    const slickAnnouncementSettings = {
      infinite: true,
      speed: 500,
      autoplaySpeed: 4000,
      autoplay: true,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      dotsClass: "announcement__paging",
      customPaging: function(slider, i) {
        //Counter for paging
        return i + 1 + "/" + slider.slideCount;
      },
      //Arrows will be auto created in the same level as the text
      nextArrow:
        '<button class="announcement__next announcement__arrow"><i class="icomoon-chevron-right"></i><span class="vh">Button to scroll to the next announcement</span></button>',
      prevArrow:
        '<button class="announcement__prev announcement__arrow"><i class="icomoon-chevron-left"></i><span class="vh">Button to scroll to the previous announcement</span></button>'
    };

    let viewedAnnouncements = Cookies.get("viewed_announcements");

    viewedAnnouncements === undefined
      ? (viewedAnnouncements = { ids: [] })
      : (viewedAnnouncements = JSON.parse(viewedAnnouncements));
    let haveNewAnnouncement = false;

    if (viewedAnnouncements.ids.length) {
      $(".announcement__text").each(function(index, item) {
        let announcementId = $(item).data("id");
        if (viewedAnnouncements.ids.indexOf(announcementId) === -1) {
          haveNewAnnouncement = true;
        }
      });
    } else {
      haveNewAnnouncement = true;
    }

    if (haveNewAnnouncement) {
      $(".announcement")
        .fadeIn()
        .addClass("active");
    }

    $announcementBar.on(
      "click",
      ".announcement__arrow, .announcement__close",
      function(e) {
        e.preventDefault();
      }
    );

    $(".announcement__close", ".announcement").on("click", function() {
      $(".announcement__text").each(function(index, item) {
        let announcementId = $(item).data("id");
        if (viewedAnnouncements.ids.indexOf(announcementId) === -1) {
          viewedAnnouncements.ids.push(announcementId);
        }
      });

      Cookies.set("viewed_announcements", viewedAnnouncements);
      $(".announcement").fadeOut();
    });

    $(".announcement__content").slick(slickAnnouncementSettings);

    $(".notification-wrapper").on("click", function() {
      $(this).fadeOut();
    });

    $(".notification-close-btn").on("click", function() {
      $(".notification-wrapper").fadeOut();
    });

    $(".notification-content").on("click", function(e) {
      e.stopPropagation();
    });

    setTimeout(() => {
      this.calculatePosition();
    }, 350);

    let timer;

    $(window).on("resize", () => {
      clearTimeout(timer);

      timer = setTimeout(() => {
        this.calculatePosition();
      }, 300);
    });
  }
  calculatePosition() {
    const cookieHeight = !Cookies.get("cookieMsg")
      ? $(".cookie-message").outerHeight()
      : 0;
    const bottom =
      $(window).width() > 767
        ? `${cookieHeight + $(".announcement").outerHeight() + 20}`
        : `${cookieHeight + $(".announcement").outerHeight()}`;

    $(".announcement").css({
      top: `calc(100% - ${bottom}px)`
    });
  }
}
