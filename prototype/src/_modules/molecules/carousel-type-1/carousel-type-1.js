"use strict";

import slick from "slick-carousel";

export default class CarouselType1 {
  constructor() {
    let $parent = $(".carousel-type-1");

    $parent.each(function(index, item) {
      let $carousel = $(".carousel-slider", item);
      $carousel.slick({
        infinite: true,
        arrows: true,
        nextArrow: $(".next-btn", item),
        prevArrow: $(".prev-btn", item),
        slidesToShow: 3,
        variableWidth: false,
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              dots: false,
              centerMode: true,
              slidesToShow: 2,
              variableWidth: false,
              infinite: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              variableWidth: false,
              dots: false,
              centerMode: true
            }
          }
        ]
      });
    });

    $(".match-height-item", ".carousel-type-1").matchHeight();
  }
}
