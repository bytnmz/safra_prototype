"use strict";

import slick from "slick-carousel";

export default class CarouselBanner {
  constructor() {
    let $parent = $(".carousel-banner");

    $parent.each(function(index, item) {
      let $item = $(item);

      $(".banner-slider", $item).slick({
        dots: false,
        autoplay: true,
        nextArrow: $(".next-btn", $item),
        prevArrow: $(".prev-btn", $item)
      });
    });
  }
}
