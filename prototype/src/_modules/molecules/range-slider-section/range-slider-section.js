"use strict";

export default class RangeSliderSection {
  constructor() {
    let $parent = $(".range-slider-section");

    $parent.each(function(index, item) {
      let $rangeSlider = $("input[type=range]", item);
      $rangeSlider.on("change", function() {
        let $this = $(this);

        let rangeValue = parseInt($this.val()) / 100;
        if (rangeValue === 0.99) {
          $(".listing-section", $parent).hide();
          $("#slider-section-item-4").show();
        } else if (rangeValue === 0.66) {
          $(".listing-section", $parent).hide();
          $("#slider-section-item-3").show();
        } else if (rangeValue === 0.33) {
          $(".listing-section", $parent).hide();
          $("#slider-section-item-2").show();
        } else {
          $(".listing-section", $parent).hide();
          $("#slider-section-item-1").show();
        }
      });
    });
  }
}
