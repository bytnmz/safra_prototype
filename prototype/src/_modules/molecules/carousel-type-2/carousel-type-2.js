"use strict";

export default class CarouselType2 {
  constructor() {
    let $parent = $(".carousel-type-2");

    $parent.each(function(index, item) {
      let $carousel = $(".carousel-slider", item);

      $carousel.slick({
        infinite: false,
        nextArrow: $(".next-btn", item),
        prevArrow: $(".prev-btn", item),
        slidesToShow: 4,
        variableWidth: false,
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              dots: false,
              centerMode: true,
              slidesToShow: 2,
              variableWidth: false,
              infinite: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              arrows: false,
              slidesToShow: 1,
              variableWidth: false,
              dots: false,
              centerMode: true
            }
          }
        ]
      });
    });

    $(".match-height-item", ".carousel-type-2").matchHeight();
  }
}
