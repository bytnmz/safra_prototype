"use strict";

import dot from "dot";
import slick from "slick-carousel";

export default class AjaxCarousel {
  constructor() {
    let $parent = $(".ajax-carousel-section");
    this.endpoint = $(".listing-template", $parent).data("api");

    let templateId = $(".listing-template", $parent).data("template-id");
    let fixedFilter = $(".listing-template", $parent).data("fixed-filter");
    let fixedFilterValue = $(".listing-template", $parent).data(
      "fixed-filter-value"
    );
    this.dotTemplate = dot.template(
      $(`#${templateId}`)
        .html()
        .trim()
    );

    this.targetDiv = $(".carousel-slider", $parent);
    this.parameters = {
      persona: "all",
      location: "all",
      interest: "all"
    };

    this.parameters[fixedFilter] = fixedFilterValue;

    this.getItems();

    window.emitter.on("personalization-trigger", () => {
      let personaDataItems = Object.entries(window.personaData);

      personaDataItems.forEach(personaDataItem => {
        if (this.parameters[personaDataItem[0]] === "all") {
          this.parameters[personaDataItem[0]] = personaDataItem[1];
        }
      });
      let $carousel = $(".carousel-slider", ".ajax-carousel-section");

      $carousel.slick("unslick");
      this.getItems();
    });
  }

  getItems() {
    $.ajax({
      url: this.endpoint,
      method: "GET",
      data: this.parameters,
      dataType: "json",
      success: res => {
        this.processData(res.Data);
      },
      error: err => {
        console.log(err);
      }
    });
  }

  processData(res) {
    let $carousel = $(".carousel-slider", ".ajax-carousel-section");
    let errorMessage = $(".listing-template").data("error-message");

    if (res.length) {
      if (this.targetDiv.parent().hasClass("is-empty"))
        this.targetDiv.parent().removeClass("is-empty");

      this.targetDiv.html(this.dotTemplate(res));
      $carousel.slick({
        infinite: true,
        arrows: true,
        nextArrow: $(".next-btn", ".ajax-carousel-section"),
        prevArrow: $(".prev-btn", ".ajax-carousel-section"),
        slidesToShow: 3,
        variableWidth: false,
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              dots: false,
              centerMode: true,
              slidesToShow: 2,
              variableWidth: false,
              infinite: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              variableWidth: false,
              dots: false,
              centerMode: true
            }
          }
        ]
      });

      $(".match-height-item", ".ajax-carousel-section").matchHeight();
    } else {
      this.targetDiv.html(`<h2>${errorMessage}</h2>`);
      this.targetDiv.parent().addClass("is-empty");
    }
  }
}
