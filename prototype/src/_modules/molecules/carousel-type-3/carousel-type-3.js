"use strict";

import slick from "slick-carousel";
import enquire from "enquire.js";
import matchHeight from "jquery-match-height";

export default class CarouselType3 {
  constructor() {
    let $parent = $(".carousel-type-3");

    $parent.each(function(index, item) {
      let $carousel = $(".carousel-slider", item);

      let $slideIndicator = (".carousel-slide-indicator", item);

      let totalSlides = $(".slide", item).length;

      $(".total", $slideIndicator).text(` /0${totalSlides}`);

      enquire.register("screen and (min-width:768px)", {
        // OPTIONAL
        // If supplied, triggered when a media query matches.
        match: function() {
          $carousel.slick({
            infinite: false,
            arrows: true,
            nextArrow: $(".next-arrow", item),
            prevArrow: $(".prev-arrow", item),
            slidesToShow: 1,
            variableWidth: true,
            responsive: [
              {
                breakpoint: 1023,
                settings: {
                  dots: false,
                  centerMode: false,
                  slidesToShow: 1,
                  variableWidth: true,
                  infinite: false
                }
              },
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 1,
                  variableWidth: false,
                  dots: false
                }
              }
            ]
          });

          $(".match-height-item", ".carousel-type-3").matchHeight();

          $carousel.on("beforeChange", function(
            event,
            slick,
            currentSlide,
            nextSlide
          ) {
            $(".current", $slideIndicator).text(`0${nextSlide + 1}`);
          });
        },

        // OPTIONAL
        // If supplied, triggered when the media query transitions
        // *from a matched state to an unmatched state*.
        unmatch: function() {
          $carousel.slick("unslick");
        },

        // OPTIONAL
        // If supplied, triggered once, when the handler is registered.
        setup: function() {},

        // OPTIONAL, defaults to false
        // If set to true, defers execution of the setup function
        // until the first time the media query is matched
        deferSetup: true,

        // OPTIONAL
        // If supplied, triggered when handler is unregistered.
        // Place cleanup code here
        destroy: function() {}
      });
    });
  }
}
