"use strict";

export default class Accordion {
  constructor() {
    let $accordion = $(".accordion");

    $(".accordion__title", $accordion).on("click", function() {
      let $this = $(this);

      if ($this.hasClass("open")) {
        $this.removeClass("open");
        $this.siblings(".accordion__content").slideUp();
      } else {
        $this.addClass("open");
        $this.siblings(".accordion__content").slideDown();
      }
    });
  }
}
