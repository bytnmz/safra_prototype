"use strict";

import parallax from "materialize-css";

export default class ParallaxSection {
  constructor() {
    $(".parallax").parallax();
  }
}
