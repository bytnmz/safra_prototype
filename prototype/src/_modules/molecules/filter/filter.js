"use strict";

import Pikaday from "pikaday";

export default class Filter {
  constructor() {
    $(".open-filters").on("click", function() {
      //console.log(this);
      let $this = $(this);
      let $filters = $(".filters");
      let $openFilterText = $(".open-filters--text");

      if ($this.hasClass("open")) {
        $this.removeClass("open");
        $filters.removeClass("open");
        $openFilterText.text("Filter");
      } else {
        $this.addClass("open");
        $filters.addClass("open");
        $openFilterText.text("Close");
      }
    });

    $(".date-picker").each(function(index, item) {
      let pickerId = $(item).data("id");

      let picker = new Pikaday({ field: $("#" + pickerId)[0] });
    });

    // let queries = this.getUrlQueries();

    // $('input[name=category]','.filter--checkbox-list').each(function(index, item){
    //   let value = $(item).val()
    //   if ( value === queries.category) {

    //     $(item).prop('checked',true);
    //   }
    // });
  }

  // getUrlQueries() {
  //   let queryString = {};
  //   let query = window.location.search.substring(1);
  //   if (!query.length) {
  //     return queryString;
  //   }
  //   let vars = query.split("&");
  //   for (let i = 0; i < vars.length; i++) {
  //     let pair = vars[i].split("=");

  //     /* If first entry with this name */
  //     if (typeof queryString[pair[0]] === "undefined") {
  //       queryString[pair[0]] = decodeURIComponent(pair[1]);

  //       /* If second entry with this name */
  //     } else if (typeof queryString[pair[0]] === "string") {
  //       let arr = [ queryString[pair[0]], decodeURIComponent(pair[1])];
  //       queryString[pair[0]] = arr;

  //       /* If third or later entry with this name */
  //     } else {
  //       queryString[pair[0]].push(decodeURIComponent(pair[1]));
  //     }
  //   }
  //   return queryString;
  // }
}
