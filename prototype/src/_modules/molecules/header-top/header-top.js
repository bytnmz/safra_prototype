"use strict";
import enquire from "enquire.js";

export default class HeaderTop {
  constructor() {
    let $allHandles = $(".nav-item-group .dropdown-handle"),
      $allItems = $(".nav-item-group .dropdown-item");

    //Desktop
    enquire.register("screen and (min-width:1025px)", {
      match: function() {
        //Focus or click on handle
        $allHandles.on("click", function() {
          let $item = $(this).closest(".dropdown-item");

          if ($item.hasClass("active")) {
            $item.removeClass("active");
          } else {
            $item.addClass("active");
          }
        });

        //Hover
        $allItems.on("mouseenter", function() {
          $(this).addClass("active");
        });
        $allItems.on("mouseleave", function() {
          $(this).removeClass("active");
        });
      },
      unmatch: function() {
        $allHandles.off("click");
        $allItems.off("mouseenter mouseleave");
      },
      deferSetup: true
    });

    //Mobile
    enquire.register("screen and (max-width:1024px)", {
      match: function() {
        //Click on handle
        $allHandles.on("click", function() {
          let $item = $(this).closest(".dropdown-item");

          $allItems.not($item).removeClass("active");

          if ($item.hasClass("active")) {
            $item.removeClass("active");
          } else {
            $item.addClass("active");
          }
        });

        //Click on button text
        $allHandles.prev("span").on("click", function() {
          $(this)
            .next(".dropdown-handle")
            .click();
        });
      },
      unmatch: function() {},
      deferSetup: true
    });
  }
}
