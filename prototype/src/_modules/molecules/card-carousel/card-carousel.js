"use strict";

import slick from "slick-carousel";

export default class CardCarousel {
  constructor() {
    let $parent = $(".card-carousel");
    let $carousel = $(".carousel-slider", $parent);

    $carousel.slick({
      infinite: false,
      prevArrow: $(".announcements__controller .slick-prev"),
      nextArrow: $(".announcements__controller .slick-next"),
      slidesToShow: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1023,
          settings: {
            dots: false,
            centerMode: true,
            slidesToShow: 2,
            variableWidth: false,
            infinite: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            variableWidth: false,
            dots: false
          }
        }
      ]
    });
  }
}
