"use strict";

export default class Card {
  constructor() {
    // var uluru = {lat: -25.344, lng: 131.036};
    // // The map, centered at Uluru
    // var map = new google.maps.Map(document.getElementById('google-map'), {zoom: 10, center: uluru});
    // // The marker, positioned at Uluru
    // var marker = new google.maps.Marker({position: uluru, map: map});

    $(".card__map").each(function(index, item) {
      //   console.log(item);

      let mapID = $(item).data("uid");
      let mapLat = $(item).data("lat");
      let mapLong = $(item).data("long");
      let mapData = {
        lat: mapLat,
        lng: mapLong
      };

      let mapOptions = { zoom: 15, center: mapData, streetViewControl: false };

      console.log(mapID, mapData);

      initMap(mapID, mapOptions, mapData);

      //   var map = new google.maps.Map(document.getElementById(mapID), mapOptions);
      //   // The marker, positioned at Uluru
      //   var marker = new google.maps.Marker({ position: mapData, map: map });

      //   var panorama = map.getStreetView();
      //   panorama.setPosition(mapData);
      //   panorama.setPov({
      //     heading: 45,
      //     pitch: -10
      //   });

      let toggleStreetView = $(item)
        .parent()
        .find(".toggle-street-view");
      $(toggleStreetView).on("click", function() {
        panorama.setVisible(true);
      });
    });
  }
}
