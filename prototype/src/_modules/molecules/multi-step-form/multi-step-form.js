"use strict";

export default class MultiStepForm {
  constructor() {
    $(".next-button").on("click", function() {
      let nextStep = $(this).data("next-step");

      if (nextStep) {
        $(".form").removeClass("active");
        $(`#form-step-${nextStep}`).addClass("active");
        $(".step").removeClass("active");
        $(`#step${nextStep}`).addClass("active");
      }
    });
  }
}
