"use strict";

import slick from "slick-carousel";
import enquire from "enquire.js";
import matchHeight from "jquery-match-height";

export default class CarouselType4 {
  constructor() {
    let $parent = $(".carousel-type-4");

    $parent.each(function(index, item) {
      let $carousel = $(".carousel-slider", item);

      let totalSlides = $(".slide", item).length;

      $carousel.slick({
        infinite: false,
        arrows: true,
        nextArrow: $(".next-arrow", item),
        prevArrow: $(".prev-arrow", item),
        slidesToShow: 1,
        variableWidth: true,
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              dots: false,
              centerMode: false,
              slidesToShow: 1,
              variableWidth: true,
              infinite: false
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              variableWidth: false,
              dots: false
            }
          }
        ]
      });

      $(".match-height-item", ".carousel-type-4").matchHeight();
    });
  }
}
