"use strict";

export default class MobileNav {
  constructor() {
    $(".lvl2handle", ".mobile-nav").on("click", function() {
      let $this = $(this);

      if ($this.hasClass("active")) {
        $this.removeClass("active");
        $this.siblings(".lvl2nav").slideUp();
      } else {
        $this.addClass("active");
        $this.siblings(".lvl2nav").slideDown();
      }
    });
  }
}
