"use strict";

export default class SiteSearch {
  constructor($selector) {
    let $siteSearch = $selector;
    let $form = $(".site-search__form", $siteSearch);
    let $input = $(".site-search__input", $siteSearch);
    let $searchBtn = $(".btn-search", $siteSearch);

    this.action = $form.data("action");
    this.isOpened = false;

    this.$siteSearch = $siteSearch;
    this.$input = $input;

    $searchBtn.on("click", e => {
      this.submit();
    });

    $input.on("keypress", e => {
      if (e.keyCode == 13 || e.which == 13) {
        this.submit();
      }
    });
  }

  open() {
    this.$siteSearch.slideDown();
    this.isOpened = true;
    this.$siteSearch.find("input").focus();
  }

  close() {
    this.$siteSearch.slideUp();
    this.isOpened = false;
  }

  submit() {
    let name = this.$input.attr("name");
    let value = this.$input.val();
    if (value.trim(" ").length == 0) {
      if (!this.$siteSearch.find(".error-msg").length) {
        this.$input.after(
          '<p class="error-msg">Please enter a keyword to search.</p>'
        );
      }
    } else {
      this.$siteSearch.find(".error-msg").remove();
      window.location.href = `${this.action}?${name}=${encodeURIComponent(
        value
      )}`;
    }
  }
}
