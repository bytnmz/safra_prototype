"use strict";

import $ from "jquery";

export default class PaginationAjax {
  constructor(totalPageCount, startingPage, _onPageChange) {
    //Config ==================================================================

    let _this = this;

    //Settings
    //Parent container for pagination components
    this.$pagination = $(".pagination-ajax");

    //maximum number of visible page numbers
    // 3 -> <Prev 1... 4 5 6 ... 12 Next>
    this.visibleNumberOfPages = 5;

    this.addLastPage = true;
    this.addFirstPage = true;
    this.addPrevNextArrows = true;

    this.prevIconClass = "icomoon-chevron-left";
    this.nextIconClass = "icomoon-chevron-right";

    //Dynamic variables
    this.lastNumber = totalPageCount;
    this.currentPage = startingPage;
    this._onPageChange = _onPageChange;

    this.pagesArray = [];
    this.paginationHTML = [];

    //Initialize ==============================================================
    this._updatePagination(totalPageCount, startingPage);

    //Event handlers ==========================================================
    //Emit TO the main organism when page number is clicked
    this.$pagination.on("click", ".pagination-ajax__page", function() {
      _this.currentPage = parseInt($(this).text(), 10);
      _this._onPageChange(_this.currentPage);
    });
    //Emit TO the main organism when next button is clicked
    this.$pagination.on("click", ".next", function() {
      _this.currentPage++;
      _this._onPageChange(_this.currentPage);
    });
    //Emit TO the main organism when prev button is clicked
    this.$pagination.on("click", ".prev", function() {
      _this.currentPage--;
      _this._onPageChange(_this.currentPage);
    });
    this.$pagination.on("click", ".pagination-ajax__last", function() {
      _this.currentPage = _this.lastNumber;
      _this._onPageChange(_this.currentPage);
    });
    this.$pagination.on("click", ".pagination-ajax__first", function() {
      _this.currentPage = 1;
      _this._onPageChange(1);
    });
  }

  //Helper Methods ========================================================
  _updatePagination(totalPageCount, newPageNum) {
    this.lastNumber = totalPageCount;
    this.currentPage = newPageNum;

    //Display pagination only if there is more than one page
    if (this.lastNumber > 1) {
      this._generatePagination();
      this._displayPagination();
    } else {
      //Clear pagination if there is only one page
      this.$pagination.html("");
    }
  }

  //Run all helper functions
  //Determines the additional components to be attached to the pagination
  _generatePagination() {
    //Clears container
    this.pagesArray = [];

    //generate array of page numbers
    this._generatePagesArray();

    if (this.addLastPage) {
      this._addLast();
    }

    if (this.addFirstPage) {
      this._addFirst();
    }

    if (this.addPrevNextArrows) {
      this._addArrows();
    }
    //Convert array to html
    this._convertToHTML();
  }

  //Convert pagesArray to array of HTML tags
  _convertToHTML() {
    this.paginationHTML = this.pagesArray.map(element => {
      if (element === this.currentPage) {
        return `<li class="pagination-ajax__page active" data-value=${element.toString()}>${element.toString()}</li>`;
      } else if (typeof element === "number") {
        return `<li class="pagination-ajax__page" data-value=${element.toString()}><span>${element.toString()}</span></li>`;
      } else if (element === "...") {
        return `<li class="pagination-ajax__dots">${element}</li>`;
      } else if (element === "last") {
        return `<li class="pagination-ajax__last"><i class='icomoon-chevron-right'/></li>`;
      } else if (element === "first") {
        return `<li class="pagination-ajax__first"><i class='icomoon-chevron-left'/></li>`;
      } else if (element === "Next") {
        return `<li class="pagination-ajax__next next"><i class=${this.nextIconClass}/></li>`;
      } else if (element === "Prev") {
        return `<li class="pagination-ajax__prev prev"><i class=${this.prevIconClass}/></li>`;
      } else {
        return element;
      }
    });
  }

  //Add generated html to the DOM
  _displayPagination() {
    this.$pagination.html(this.paginationHTML);
  }

  //Generate an array of page numbers based on the currentPage number and visibleNumberofPages eg. [3,4,5,6,7] or [1,2,3,4,5]
  _generatePagesArray() {
    this.pagesArray.push(this.currentPage);

    let nextNum = this.currentPage + 1;
    let prevNum = this.currentPage - 1;

    while (this.pagesArray.length < this.visibleNumberOfPages) {
      if (nextNum <= this.lastNumber) {
        this.pagesArray.push(nextNum);
        nextNum++;
      }
      if (prevNum >= 1) {
        this.pagesArray.unshift(prevNum);
        prevNum--;
      }
      if (this.pagesArray.length === this.lastNumber) {
        break;
      }
    }
  }

  //Add last page number to pagesArray if valid
  _addLast() {
    let lastDifference =
      this.lastNumber - this.pagesArray[this.pagesArray.length - 1];
    if (lastDifference > 1) {
      this.pagesArray = [...this.pagesArray, "...", "last"];
    } else if (lastDifference === 1) {
      this.pagesArray = [...this.pagesArray, this.lastNumber];
    }
  }

  //Add page 1 to pagesArray if valid
  _addFirst() {
    let startDifference = this.pagesArray[0] - 1;
    if (startDifference > 1) {
      this.pagesArray = ["first", "...", ...this.pagesArray];
    } else if (startDifference === 1) {
      this.pagesArray = [1, ...this.pagesArray];
    }
  }

  //Add Next or Previous arrow if valid
  _addArrows() {
    if (this.currentPage < this.lastNumber) {
      this.pagesArray.push("Next");
    }
    if (this.currentPage > 1) {
      this.pagesArray.unshift("Prev");
    }
  }
}
