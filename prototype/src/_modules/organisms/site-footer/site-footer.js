"use strict";

export default class SiteFooter {
  constructor() {
    $(".back-to-top button").on("click", function() {
      $("html,body").animate({ scrollTop: 0 }, 1000);
    });
  }
}
