"use strict";

export default class FeedCustomizer {
  constructor() {
    $(".button", ".feed-filter-btn").on("click", function() {
      $(".feed-customizer__option input").each(function(index, item) {
        let $item = $(item);

        if ($item.prop("checked")) {
          window.decClient.sentenceClient.writeSentence({
            predicate: `customizefeed`,
            object: $item.val()
          });
        }
      });
      $(".feed-customizer").fadeOut();
      $(".feed-customizer").removeClass("showing");
    });

    $(".feed-curstomizer-trigger").on("click", function() {
      $(".feed-customizer").fadeIn();
      $(".feed-customizer").addClass("showing");
    });

    $(".feed-customizer").on("click", function(e) {
      $(".feed-customizer").fadeOut();
      $(".feed-customizer").removeClass("showing");
    });

    $(".feed-customizer__content").on("click", function(e) {
      e.stopPropagation();
    });
  }
}
