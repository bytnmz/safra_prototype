"use strict";

import slick from "slick-carousel";
import dot from "dot";
import { convertSpeed } from "geolib";

export default class ExploreSafra {
  constructor() {
    let $parent = $(".explore-safra");
    this.endpoint = $(".listing-template", $parent).data("api");
    this.errorMessage = $(".listing-template", $parent).data("error-message");

    let templateId = $(".listing-template", $parent).data("template-id");
    this.dotTemplate = dot.template(
      $(`#${templateId}`)
        .html()
        .trim()
    );
    this.targetDiv = $(".carousel-slider", ".explore-safra");
    this.parameters = {};

    let $filterItem = $(".filter-item select");

    $filterItem.each((index, item) => {
      this.parameters[$(item).attr("name")] = $(item).val();
    });

    this.getItems();

    $filterItem.on("change", e => {
      this.parameters[$(e.currentTarget).attr("name")] = $(
        e.currentTarget
      ).val();
      let $carousel = $(".carousel-slider", ".ajax-carousel-type-2");
      window.decClient.sentenceClient.writeSentence({
        predicate: `filter${$(e.currentTarget).attr("name")}`,
        object: $(e.currentTarget).val()
      });
      $carousel.slick("unslick");
      this.getItems();
    });

    window.emitter.on("personalization-trigger", () => {
      let personaDataItems = Object.entries(window.personaData);

      personaDataItems.forEach(personaDataItem => {
        if (this.parameters[personaDataItem[0]] === "all") {
          this.parameters[personaDataItem[0]] = personaDataItem[1];
        }
      });

      if (window.personaData.interestData.length) {
        let selectedItem = null;
        let interestData = window.personaData.interestData;

        interestData.forEach(item => {
          if (selectedItem === null) {
            selectedItem = item;
          } else {
            if (selectedItem.Score <= item.Score) {
              selectedItem = item;
            }
          }
        });

        this.parameters.interest = selectedItem.CmsName;
      } else {
        this.parameters.interest = "all";
      }

      if (window.personaData.personaItems.length) {
        let selectedItem = null;
        let personaItems = window.personaData.personaItems;

        personaItems.forEach(item => {
          if (selectedItem === null) {
            selectedItem = item;
          } else {
            if (selectedItem.Score <= item.Score) {
              selectedItem = item;
            }
          }
        });

        this.parameters.persona = selectedItem.CmsName;
      } else {
        this.parameters.persona = "all";
      }

      let $carousel = $(".carousel-slider", ".ajax-carousel-type-2");

      $carousel.slick("unslick");
      this.getItems();
      this.updateSelectView("location");
      this.updateSelectView("persona");
      this.updateSelectView("interest");
    });
  }

  getItems() {
    $.ajax({
      url: this.endpoint,
      method: "GET",
      data: this.parameters,
      dataType: "json",
      success: res => {
        this.processData(res.Data);
      },
      error: err => {
        console.log(err);
      }
    });
  }

  updateSelectView(fieldName) {
    let $select = $(`select[name="${fieldName}"]`);
    $select.val(this.parameters[fieldName]);
    $select.niceSelect("update");
  }

  processData(res) {
    // res.cardClass = res.Type.toLowerCase();
    let $carousel = $(".carousel-slider", ".ajax-carousel-type-2");
    if (res.length) {
      res.forEach(item => {
        item.cardClass = `card--type-${item.Type.toLowerCase()}`;
      });
      this.targetDiv.html(this.dotTemplate(res));

      $carousel.on("init", function(instance) {
        $(".loader").hide();
      });

      $carousel.slick({
        infinite: false,
        nextArrow: $(".next-btn", ".ajax-carousel-type-2"),
        prevArrow: $(".prev-btn", ".ajax-carousel-type-2"),
        slidesToShow: 4,
        variableWidth: false,
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              dots: false,
              centerMode: true,
              slidesToShow: 2,
              variableWidth: false,
              infinite: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              arrows: false,
              slidesToShow: 1,
              variableWidth: false,
              dots: false,
              centerMode: true
            }
          }
        ]
      });
    } else {
      $(".prev-btn, .next-btn", $(".explore-safra")).hide();
      $(".loader").hide();
      $carousel.css("visibility", "visible");
      this.targetDiv.html(`<h1 class="text--center">${this.errorMessage}</h1>`);
    }
  }
}
