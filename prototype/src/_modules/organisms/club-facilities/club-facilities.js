"use strict";

export default class ClubFacilities {
  constructor() {
    let $parent = $(".card-carousel", ".club-facilities");
    let $carouselMobile = $(".carousel-slider", ".facilities-slider--mobile");
    let $carouselDesktop = $(".carousel-slider", ".facilities-slider--desktop");

    $carouselDesktop.slick({
      infinite: true,
      arrows: true,
      nextArrow: $(".next-btn", ".facilities-slider--desktop"),
      prevArrow: $(".prev-btn", ".facilities-slider--desktop"),
      slidesToShow: 1,
      variableWidth: false
    });

    $carouselMobile.slick({
      infinite: true,
      arrows: true,
      nextArrow: $(".next-btn", ".facilities-slider--mobile"),
      prevArrow: $(".prev-btn", ".facilities-slider--mobile"),
      slidesToShow: 3,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 1023,
          settings: {
            dots: false,
            centerMode: true,
            slidesToShow: 2,
            variableWidth: false,
            infinite: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            variableWidth: false,
            dots: false,
            centerMode: true
          }
        }
      ]
    });

    $(".match-height-item", ".club-facilities").matchHeight();
  }
}
