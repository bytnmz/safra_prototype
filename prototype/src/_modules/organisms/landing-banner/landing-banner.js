"use strict";

import slick from "slick-carousel";

export default class LandingBanner {
  constructor() {
    // let $landingBanner = $('.landing-banner');

    // const docElm = document.documentElement;
    // const { clientWidth, clientHeight } = docElm;

    // $landingBanner.addClass('animate-in');

    $(".banner-slider", ".landing-banner").slick({
      autoplay: true,
      fade: true,
      dots: true,
      arrows: false
    });

    // $('.banner-slider', '.landing-banner').on('beforeChange', function(event, slick, currentSlide, nextSlide){

    //   $('.slick-slide', '.landing-banner').each(function(index, item){
    //     console.log(item);
    //   });
    // });
  }
}
