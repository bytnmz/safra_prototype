"use strict";

export default class TabbedContent {
  constructor() {
    let $parent = $(".tabbed-content");

    let $activeItem = $(".active", $parent);
    let activeItemwidth = $activeItem.outerWidth();
    let position = $activeItem.position().left;

    let $activeIndicator = $(".active-indicator", $parent);

    $activeIndicator.css("width", activeItemwidth);
    $activeIndicator.css("left", position);

    $(".tab", $parent).on("click", function(e) {
      e.preventDefault();

      let relatedContent = $(this).data("related-content");
      $(".tab").removeClass("active");
      $(this).addClass("active");
      $(".tab-content").addClass("hidden");
      $(`#${relatedContent}`).removeClass("hidden");

      activeItemwidth = $(this).outerWidth();
      position = $(this).position().left;

      $activeIndicator.css("width", activeItemwidth);
      $activeIndicator.css("left", position);
    });
  }
}
