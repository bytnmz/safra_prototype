"use strict";

import slick from "slick-carousel";
import matchHeight from "jquery-match-height";
import dot from "dot";

export default class WhatsonSection {
  constructor() {
    let $parent = $(".whatson-section");
    this.endpoint = $(".listing-template", $parent).data("api");

    let templateId = $(".listing-template", $parent).data("template-id");
    this.dotTemplate = dot.template(
      $(`#${templateId}`)
        .html()
        .trim()
    );
    this.targetDiv = $(".carousel-slider", $parent);
    this.parameters = {
      from: "currentMonth"
    };

    this._getItems();

    $(".btn-currentmonth").on("click", e => {
      e.preventDefault();
      let $parent = $(".card-carousel", ".whatson-section");
      let $carousel = $(".carousel-slider", $parent);
      $(".filter-buttons .button").removeClass("active");
      $(e.currentTarget).addClass("active");

      this.parameters.from = "currentMonth";
      $carousel.slick("unslick");
      this._getItems();
    });

    $(".btn-currentweek").on("click", e => {
      e.preventDefault();
      let $parent = $(".card-carousel", ".whatson-section");
      let $carousel = $(".carousel-slider", $parent);
      $(".filter-buttons .button").removeClass("active");
      $(e.currentTarget).addClass("active");

      this.parameters.from = "currentWeek";
      $carousel.slick("unslick");
      this._getItems();
    });
  }

  _getItems() {
    $.ajax({
      url: this.endpoint,
      method: "GET",
      data: this.parameters,
      dataType: "json",
      success: res => {
        this._processData(res.Data);
      },
      error: err => {
        console.log(err);
      }
    });
  }

  _processData(res) {
    let $parent = $(".card-carousel", ".whatson-section");
    let $carousel = $(".carousel-slider", $parent);
    let errorMessage = $(".listing-template").data("error-message");

    if (res.length) {
      if (
        this.targetDiv
          .parent()
          .parent()
          .hasClass("is-empty")
      )
        this.targetDiv
          .parent()
          .parent()
          .removeClass("is-empty");
      this.targetDiv.html(this.dotTemplate(res));

      $carousel.slick({
        infinite: false,
        arrows: true,
        slidesToShow: 1,
        variableWidth: true,
        nextArrow: $(".next-arrow", ".whatson-section"),
        prevArrow: $(".prev-arrow", ".whatson-section"),
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              dots: false,
              centerMode: true,
              slidesToShow: 2,
              variableWidth: false,
              infinite: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              variableWidth: false,
              dots: false,
              centerMode: true
            }
          }
        ]
      });

      $(".match-height-item", ".whatson-section").matchHeight();
    } else {
      this.targetDiv.html(`<h2>${errorMessage}</h2>`);
      this.targetDiv
        .parent()
        .parent()
        .addClass("is-empty");
    }
  }
}
