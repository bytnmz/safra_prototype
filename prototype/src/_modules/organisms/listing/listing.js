"use strict";

import ListingBase from "../../listing-base/listing-base";
import Pagination from "../../molecules/pagination/pagination";
import matchHeight from "jquery-match-height";
import PaginationAjax from "../../molecules/pagination-ajax/pagination-ajax";
// import Accordion from '../../molecules/accordion/accordion';

export default class Listing extends ListingBase {
  constructor(listing) {
    super();

    let $filterSearch = $(".filter--search");
    let $filterCheckbox = $("input[type=checkbox]", ".filter--checkbox-list");
    let $filterRadio = $("input[type=radio]", ".filter--radio-list");
    let listingTemplateId = $(".listing-template", listing).data("template-id");
    let $pagination = $(".page-number-wrapper", listing);
    let $list = $(".listing-items-container", listing);
    let $filterBtn = $(".apply-filter-button");
    let pageSize = $(".listing-template", listing).data("items-per-page");
    let fixedFilter = $(".listing-template", listing).data("fixed-filter");
    let fixedFilterValue = $(".listing-template", listing).data(
      "fixed-filter-value"
    );

    this.endpoint = $(".listing-template", listing).data("api");

    this.template = $(`#${listingTemplateId}`)
      .html()
      .trim();
    this.loadmore = false;

    let query = this.getUrlQueries();

    this.parameters = query;
    this.parameters.pageSize = parseInt(pageSize);
    this.parameters.page = 1;

    if (fixedFilter) {
      this.parameters[fixedFilter] = fixedFilterValue;
    }

    this.getDataCallback = res => {
      let totalPages = Math.ceil(res.TotalCount / pageSize);

      this.totalPages = totalPages;

      if (this.loadmore) {
        this.renderTemplate(res.Data, $list, "loadmore");
      } else {
        this.renderTemplate(res.Data, $list, "replace");
      }

      /* Replace the URL page parameter with the maximum page available */
      if (this.parameters.page > totalPages) {
        this.parameters.page = totalPages;
        this.updateURL(true);
      }

      if ($(".search-keyword-info").length) {
        $(".keyword-info-message", ".search-keyword-info").text(
          `${res.TotalCount} search results for "${this.parameters.keyword}"`
        );
      }

      this.pagination = new PaginationAjax(
        totalPages,
        this.parameters.page,
        this._onPageChange.bind(this)
      );

      if ($(".pagination").length) {
        if (totalPages > 1) {
          $(".pagination").show();
          //   $pagination.empty();
          //   if (this.parameters.page === totalPages) {
          //     $(".next").hide();
          //   } else {
          //     $(".next").show();
          //   }

          //   if (this.parameters.page === 1) {
          //     $(".prev").hide();
          //   } else {
          //     $(".prev").show();
          //   }
          //   for (let i = 0; i < totalPages; i++) {
          //     if (i + 1 === this.parameters.page) {
          //       $pagination.append(
          //         `<li class="pagination-item active" data-value=${i +
          //           1}><span >${i + 1}</span></li>`
          //       );
          //     } else {
          //       $pagination.append(
          //         `<li class="pagination-item" data-value=${i + 1}><span >${i +
          //           1}</span></li>`
          //       );
          //     }
          //   }
          //   this.paginationInit(this.parameters.page, totalPages);
        } else {
          $(".pagination").hide();
        }
      }

      $(".listing-item").matchHeight();
    };
    this.setupFilters();
    this.getData();

    // window.emitter.on("personalization-trigger", () => {
    //   let personaDataItems = Object.entries(window.personaData);

    //   personaDataItems.forEach(personaDataItem => {
    //     if (this.parameters[personaDataItem[0]] === "all") {
    //       this.parameters[personaDataItem[0]] = personaDataItem[1];
    //     }
    //   });

    //   this.updateURL();
    //   this.queries = this.getUrlQueries();
    //   this.setupFilters();
    //   this.getData();
    // });

    $filterBtn.on("click", e => {
      e.preventDefault();
      this.applyFilter();
    });

    $(".filter--search-btn").on("click", e => {
      e.preventDefault();
      this.applyFilter();
    });

    $(".filter--pill").on("click", e => {
      e.preventDefault();
      $(".filter--pill").removeClass("active");
      $(e.currentTarget).addClass("active");

      this.applyFilter();
    });

    // $(".next", ".pagination").on("click", () => {
    //   this.loadmore = false;
    //   this.parameters.page = this.parameters.page + 1;
    //   this.getData();
    //   this.updateURL();
    // });

    // $(".prev", ".pagination").on("click", () => {
    //   this.parameters.page = this.parameters.page - 1;
    //   this.loadmore = false;
    //   this.getData();
    //   this.updateURL();
    // });

    if ($("[name=sort]").length) {
      $("[name=sort]").on("change", e => {
        this.parameters.sort = $("[name=sort]").val();
        this.getData();
        this.updateURL();
      });
    }

    if ($(".load-more", listing).length) {
      $(".load-more", listing).on("click", e => {
        this.loadmore = true;
        e.preventDefault();
        if (this.parameters.page !== this.totalPages) {
          this.parameters.page = this.parameters.page + 1;
          this.getData();
          this.updateURL();
        } else {
          $(".load-more", listing).hide();
        }
      });
    }
  }

  //   paginationInit(currentPage, totalPages) {
  //     $(".pagination-item").each(function(index, item) {
  //       let actualIndex = index + 1;

  //       if (currentPage > totalPages - 1) {
  //         if (actualIndex < currentPage - 4) {
  //           $(item)
  //             .parent()
  //             .hide();
  //         }
  //       } else if (currentPage === totalPages - 1) {
  //         if (actualIndex < currentPage - 3) {
  //           $(item)
  //             .parent()
  //             .hide();
  //         }
  //       } else {
  //         if (actualIndex < currentPage - 2) {
  //           $(item)
  //             .parent()
  //             .hide();
  //         }
  //       }
  //       if (currentPage > 2) {
  //         if (actualIndex > currentPage + 2) {
  //           $(item)
  //             .parent()
  //             .hide();
  //         }
  //       } else if (currentPage === 2) {
  //         if (actualIndex > currentPage + 3) {
  //           $(item)
  //             .parent()
  //             .hide();
  //         }
  //       } else {
  //         if (actualIndex > currentPage + 4) {
  //           $(item)
  //             .parent()
  //             .hide();
  //         }
  //       }
  //     });
  //     $(".pagination-item").click(e => {
  //       e.preventDefault();
  //       let currentItem = e.currentTarget;
  //       let value = $(currentItem).data("value");
  //       currentPage = value;
  //       this.loadmore = false;
  //       this.parameters.page = value;
  //       this.getData();
  //       this.updateURL();
  //     });
  //   }

  _onPageChange(newPageNum) {
    // console.log("clicked page", newPageNum);

    var checkTimeout;

    this.loadmore = false;
    this.parameters.page = newPageNum;
    // this.getData();
    // this.updateURL();

    clearTimeout(checkTimeout);

    checkTimeout = setTimeout(() => {
      this.getData();
      this.updateURL();
      //After api call success do this
      this.pagination._updatePagination(this.totalPages, newPageNum);
    }, 300);

    // console.log("paginationClick");
  }

  setupFilters() {
    let $filterSearch = $(".filter--search");
    let $filterCheckboxList = $(".filter--checkbox-list");
    let $filterSelect = $(".filter--select");
    let $filterPill = $(".filter--pill");
    $filterCheckboxList.each((index, item) => {
      let fieldName = $(item)
        .find("input")
        .attr("name");
      this.setupCheckboxes(fieldName);
    });

    if ($filterSearch.length) {
      let fieldName = $filterSearch.find("input").attr("name");
      this.setupSearchbar(fieldName);
    }

    $filterSelect.each((index, item) => {
      let fieldName = $(item).attr("name");

      if (fieldName) {
        this.setupSelect(fieldName);
      }
    });
    if ($filterPill.length) {
      $(".filter--pill").removeClass("active");
      $filterPill.each((index, item) => {
        let $item = $(item);

        if (this.parameters[$item.data("name")] !== null) {
          if ($item.data("value") === this.parameters[$item.data("name")]) {
            $item.addClass("active");
          }
        } else {
          if ($item.data("value") === "all") {
            $item.addClass("active");
          }
        }
      });
    }
  }

  applyFilter() {
    let $filterSearch = $(".filter--search");
    let $filterCheckboxList = $(".filter--checkbox-list");
    let $filterSelect = $(".filter--select");
    let $filterPill = $(".filter--pill");

    if ($filterPill.length) {
      $filterPill.each((index, item) => {
        let $item = $(item);

        if ($item.hasClass("active")) {
          this.parameters[$item.data("name")] = $item.data("value");
          this.queries[$item.data("name")] = $item.data("value");
          this.setupSelect($item.data("name"));
        }
      });
    }

    $filterSelect.each((index, item) => {
      let fieldName = $(item).attr("name");
      console.log(fieldName);
      if (fieldName) {
        this.parameters[fieldName] = $(item).val();
        window.decClient.sentenceClient.writeSentence({
          predicate: `filter${fieldName}`,
          object: $(item).val()
        });
      }
    });

    $filterCheckboxList.each((index, item) => {
      let fieldName = $(item)
        .find("input")
        .attr("name");
      let values = this.getCheckboxesValues(fieldName);

      this.parameters[fieldName] = values;
    });

    if ($filterSearch.length) {
      let fieldName = $filterSearch.find("input").attr("name");
      let value = $filterSearch.find("input").val();
      this.parameters[fieldName] = value;
      window.decClient.sentenceClient.writeSentence({
        predicate: `filter${fieldName}`,
        object: value
      });
    }

    this.loadmore = false;
    this.parameters.page = 1;
    this.getData();
    this.updateURL();
  }
}
