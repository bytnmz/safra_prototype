"use strict";

import slick from "slick-carousel";
import dot from "dot";

export default class ClubPersonalizedSection {
  constructor() {
    let $parent = $(".club-personalized-section");
    this.endpoint = $(".listing-template", $parent).data("api");

    let templateId = $(".listing-template", $parent).data("template-id");
    let fixedFilter = $(".listing-template", $parent).data("fixed-filter");
    let fixedFilterValue = $(".listing-template", $parent).data(
      "fixed-filter-value"
    );
    this.dotTemplate = dot.template(
      $(`#${templateId}`)
        .html()
        .trim()
    );
    this.targetDiv = $(".carousel-slider", $parent);
    this.parameters = {};

    this.parameters[fixedFilter] = fixedFilterValue;

    $(".form__item input", ".multi-step-form").each((index, item) => {
      this.parameters[$(item).attr("name")] = "all";
    });

    this._getItems();

    $(".form__item input", ".multi-step-form").on("change", e => {
      let $this = $(e.currentTarget);

      if ($this.attr("type") === "checkbox") {
        let fieldName = $this.attr("name");
        let values = [];
        $(`input[name="${fieldName}"]`)
          .toArray()
          .map(ele => {
            let $this = $(ele);

            if ($this.is(":checked")) {
              values.push($this.val());
            }

            //personalization triggers
          });
        this.parameters[$this.attr("name")] = values.join("|");
      } else {
        this.parameters[$this.attr("name")] = $this.val();
      }
      console.log(this.parameters);
    });

    $(".plan-for-me").on("click", () => {
      let $carousel = $(".carousel-slider", ".club-personalized-section");
      $carousel.slick("unslick");

      let parameters = Object.entries(this.parameters);

      parameters.forEach(parameter => {
        let values = parameter[1].split("|");
        values.forEach(value => {
          window.decClient.sentenceClient.writeSentence({
            predicate: `filter${parameter[0]}`,
            object: value
          });
        });
      });

      this._getItems();
    });
  }
  _getItems() {
    $.ajax({
      url: this.endpoint,
      method: "GET",
      data: this.parameters,
      dataType: "json",
      success: res => {
        this._processData(res.Data);
      },
      error: err => {
        console.log(err);
      }
    });
  }

  _processData(res) {
    let $carousel = $(".carousel-slider", ".club-personalized-section");
    let errorMessage = $(".listing-template").data("error-message");

    if (res.length) {
      if (this.targetDiv.parent().hasClass("is-empty"))
        this.targetDiv.parent().removeClass("is-empty");

      this.targetDiv.html(this.dotTemplate(res));
      $carousel.slick({
        infinite: true,
        arrows: true,
        nextArrow: $(".next-btn", ".club-personalized-section"),
        prevArrow: $(".prev-btn", ".club-personalized-section"),
        slidesToShow: 3,
        variableWidth: false,
        responsive: [
          {
            breakpoint: 1023,
            settings: {
              dots: false,
              centerMode: true,
              slidesToShow: 2,
              variableWidth: false,
              infinite: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              variableWidth: false,
              dots: false,
              centerMode: true
            }
          }
        ]
      });

      $(".match-height-item", ".carousel-type-4").matchHeight();
    } else {
      this.targetDiv.html(`<h2>${errorMessage}</h2>`);
      this.targetDiv.parent().addClass("is-empty");
    }
  }
}
