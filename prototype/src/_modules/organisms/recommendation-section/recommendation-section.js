"use strict";

import enquire from "enquire.js";

export default class RecommendationSection {
  constructor() {
    let $parent = $(".recommendation-section");
    let $carousel = $(".carousel-slider", $parent);

    let $slideIndicator = (".carousel-indicator", $parent);

    let totalSlides = $(".slide", $parent).length;

    $(".indicator", $slideIndicator).css("width", `${100 / totalSlides}%`);

    enquire.register("screen and (max-width:768px)", {
      // OPTIONAL
      // If supplied, triggered when a media query matches.
      match: function() {
        $carousel.slick({
          infinite: false,
          arrows: true,
          nextArrow: $(".next-arrow", $parent),
          prevArrow: $(".prev-arrow", $parent),
          slidesToShow: 1,
          variableWidth: false,
          centerMode: true
        });

        $carousel.on("beforeChange", function(
          event,
          slick,
          currentSlide,
          nextSlide
        ) {
          console.log(currentSlide, nextSlide);
          let leftPercentage = (nextSlide / totalSlides) * 100;
          $(".indicator", $slideIndicator).css("left", `${leftPercentage}%`);
        });
      },

      // OPTIONAL
      // If supplied, triggered when the media query transitions
      // *from a matched state to an unmatched state*.
      unmatch: function() {
        $carousel.slick("unslick");
      },

      // OPTIONAL
      // If supplied, triggered once, when the handler is registered.
      setup: function() {},

      // OPTIONAL, defaults to false
      // If set to true, defers execution of the setup function
      // until the first time the media query is matched
      deferSetup: true,

      // OPTIONAL
      // If supplied, triggered when handler is unregistered.
      // Place cleanup code here
      destroy: function() {}
    });
  }
}
