"use strict";

import dot from "dot";

import imagesLoaded from "imagesloaded";

imagesLoaded.makeJQueryPlugin($);

export default class ClubDeals {
  constructor() {
    let $parent = $(".club-deals");
    this.endpoint = $(".club-deals-template", $parent).data("api");
    let fixedFilter = $(".club-deals-template", $parent).data("fixed-filter");
    let fixedFilterValue = $(".club-deals-template", $parent).data(
      "fixed-filter-value"
    );
    let templateId = $(".club-deals-template", $parent).data("template-id");

    this.parameters = {};

    window.emitter.on("personalization-trigger", () => {
      let personaDataItems = Object.entries(window.personaData);

      personaDataItems.forEach(personaDataItem => {
        if (this.parameters[personaDataItem[0]] === "all") {
          this.parameters[personaDataItem[0]] = personaDataItem[1];
        }
      });
      if (fixedFilter) {
        this.parameters[fixedFilter] = fixedFilterValue;
      }
    });

    this.dotTemplate = dot.template(
      $(`#${templateId}`)
        .html()
        .trim()
    );
    this.targetDiv = $(".listing-items-container", $parent);

    if (fixedFilter) {
      this.parameters[fixedFilter] = fixedFilterValue;
    }

    // console.log(this.parameters);

    this._getItems();

    $(".filter--pill").on("click", e => {
      e.preventDefault();
      $(".filter--pill").removeClass("active");
      $(e.currentTarget).addClass("active");
      let name = $(e.currentTarget).data("name");
      let value = $(e.currentTarget).data("value");
      this.parameters[name] = value;
      this._getItems();
    });
  }

  _getItems() {
    $.ajax({
      url: this.endpoint,
      method: "GET",
      data: this.parameters,
      dataType: "json",
      success: res => {
        this._processData(res.Data);
      },
      error: err => {
        console.log(err);
      }
    });
  }

  _processData(res) {
    this.targetDiv.html(this.dotTemplate(res));

    $(this.targetDiv)
      .imagesLoaded()
      .done(function(instance) {
        $(".match-height-elm").matchHeight();
        $(".match-height-section").matchHeight();
      });
  }
}
