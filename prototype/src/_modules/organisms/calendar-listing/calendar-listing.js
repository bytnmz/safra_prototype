"use strict";

import moment from "moment";
import ListingBase from "../../listing-base/listing-base";

export default class CalendarListing extends ListingBase {
  constructor() {
    super();
    let $parent = $(".calendar-listing");
    let listingTemplateId = $(".listing-template", $parent).data("template-id");
    let dateFormat = "YYYY-MM-DD";
    let query = this.getUrlQueries();
    let $list = $(".event-calendar", $parent);
    let $prevWeekBtn = $(".prev-week", $parent);
    let $nextWeekBtn = $(".next-week", $parent);
    let $filterBtn = $(".filter-btn", $parent);
    let $datePicker = $(".date-picker", $parent);

    this.endpoint = $(".listing-template", $parent).data("api");
    this.template = $(`#${listingTemplateId}`)
      .html()
      .trim();

    this.parameters = query;

    this.parameters.date = moment().format(dateFormat);

    this.getDataCallback = res => {
      this.renderTemplate(res, $list, "replace");
    };

    this.getData();
    this.updateURL();

    $filterBtn.on("click", () => {
      let date = $datePicker.val();
      if (date !== "") {
        this.parameters.date = date;
        this.getData();
        this.updateURL();
      }
    });

    $prevWeekBtn.on("click", () => {
      this.parameters.date = moment(this.parameters.date)
        .day(1 - 6)
        .toDate();
      this.parameters.date = moment(this.parameters.date).format(dateFormat);
      this.getData();
      this.updateURL();
    });

    $nextWeekBtn.on("click", () => {
      this.parameters.date = moment(this.parameters.date)
        .day(1 + 6)
        .toDate();
      this.parameters.date = moment(this.parameters.date).format(dateFormat);
      this.getData();
      this.updateURL();
    });

    // let nextWeek = moment(date)
    //   .day(1 + 6)
    //   .toDate();

    // nextWeek = moment(nextWeek).format(dateFormat);

    // console.log(nextWeek);
  }
}
