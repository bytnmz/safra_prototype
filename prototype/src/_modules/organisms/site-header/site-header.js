"use strict";

import SiteSearch from "../../molecules/site-search/site-search";

export default class SiteHeader {
  constructor() {
    let sitesearch = new SiteSearch($(".site-search"));

    let $banner = $(".banner");

    if ($banner.length) {
      let headerHeight = $(".header-top").height();

      $(".site-wrapper").css("padding-top", headerHeight);

      let scrollPosition = $(window).scrollTop();
      if (scrollPosition > 0) {
        $(".site-nav").addClass("scrolled");
      } else {
        $(".site-nav").removeClass("scrolled");
      }

      $(window).resize(function() {
        let headerHeight = $(".header-top").height();
        $(".site-wrapper").css("padding-top", headerHeight);
      });

      $(window).on("scroll", function() {
        let scrollPosition = $(window).scrollTop();
        if (scrollPosition > 0) {
          $(".site-nav").addClass("scrolled");
        } else {
          $(".site-nav").removeClass("scrolled");
        }
      });
    } else {
      let headerHeight = $(".site-header").height();

      $(".site-wrapper").css("padding-top", headerHeight);

      $(".site-nav").addClass("scrolled");

      $(window).resize(function() {
        let headerHeight = $(".site-header").height();
        $(".site-wrapper").css("padding-top", headerHeight);
      });
    }

    $(".menu-handle").on("click", function(e) {
      let $this = $(this);
      if ($this.hasClass("active")) {
        $this.removeClass("active");
        $(".mobile-nav").slideUp();
      } else {
        $this.addClass("active");
        $(".mobile-nav").slideDown();
      }
    });

    $(".search-toggle").on("click", function() {
      let $this = $(this);
      if ($this.hasClass("active")) {
        $this.removeClass("active");
        sitesearch.close();
      } else {
        $this.addClass("active");
        sitesearch.open();
      }
    });

    $(".notification-toggle").on("click", function() {
      let $this = $(this);
      if ($this.hasClass("active")) {
        $this.removeClass("active");
        $(".notification-wrapper").fadeOut("fast");
      } else {
        $this.addClass("active");
        $(".notification-wrapper").fadeIn("fast");
      }
    });
  }
}
