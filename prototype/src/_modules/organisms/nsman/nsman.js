"use strict";

import slick from "slick-carousel";
import dot from "dot";

import moment from "moment";

export default class Nsman {
  constructor() {
    this.endpoint = $(".listing-template", ".nsman").data("api");

    let templateId = $(".listing-template", ".nsman").data("template-id");
    this.dotTemplate = dot.template(
      $(`#${templateId}`)
        .html()
        .trim()
    );

    this.targetDiv = $(".carousel-slider", ".nsman");

    this._getItems();
  }

  _getItems() {
    $.ajax({
      url: this.endpoint,
      method: "GET",
      contentType: "text/xml",
      data: this.parameters,
      success: res => {
        this._processData(res);
      },
      error: err => {
        console.log(err);
      }
    });
  }

  _processData(res) {
    let rssData = [];
    $(res)
      .find("item")
      .each((index, item) => {
        let $item = $(item);
        let rssItem = {};
        rssItem.Title = $item.find("title").text();
        rssItem.Link = $item.find("link").text();
        let content =
          $item.find("content\\:encoded").text() ||
          $item.find("encoded").text();
        let images = $(content).find("img");
        rssItem.ImageSrc = images[0] ? images[0].src : "";
        rssItem.ImageAlt = images[0] ? images[0].alt : "";
        let pubDate = $item.find("pubDate").text();
        rssItem.Date = moment(pubDate).format("DD/MMM/YYYY");

        if (rssItem.ImageSrc) {
          rssData.push(rssItem);
        }
      });

    this.targetDiv.html(this.dotTemplate(rssData));
    let $carousel = $(".carousel-slider", ".rss-feed-carousel");

    $carousel.slick({
      infinite: true,
      arrows: true,
      nextArrow: $(".next-btn", ".rss-feed-carousel"),
      prevArrow: $(".prev-btn", ".rss-feed-carousel"),
      slidesToShow: 3,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 1023,
          settings: {
            dots: false,
            centerMode: true,
            slidesToShow: 2,
            variableWidth: false,
            infinite: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            variableWidth: false,
            dots: false,
            centerMode: true
          }
        }
      ]
    });
  }
}
