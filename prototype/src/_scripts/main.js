import "../_styles/main.scss";

import matchHeight from "jquery-match-height";
import objectFitImages from "object-fit-images";
import fancybox from "@fancyapps/fancybox";
import "lazysizes";
import Emitter from "tiny-emitter";
import Cookies from "js-cookie";

import niceSelect from "jquery-nice-select";
import TabbedContent from "../_modules/organisms/tabbed-content/tabbed-content";
import CardCarousel from "../_modules/molecules/card-carousel/card-carousel";
import ParallaxSection from "../_modules/molecules/parallax-section/parallax-section";
import WhatsonSection from "../_modules/organisms/whatson-section/whatson-section";
import Nsman from "../_modules/organisms/nsman/nsman";
import InterestGroups from "../_modules/organisms/interest-groups/interest-groups";
import SiteFooter from "../_modules/organisms/site-footer/site-footer";
import SiteHeader from "../_modules/organisms/site-header/site-header";
import MobileNav from "../_modules/molecules/mobile-nav/mobile-nav";
import RecommendationSection from "../_modules/organisms/recommendation-section/recommendation-section";
import ClubFacilities from "../_modules/organisms/club-facilities/club-facilities";
import HeaderTop from "../_modules/molecules/header-top/header-top";
import MultiStepForm from "../_modules/molecules/multi-step-form/multi-step-form";
import CarouselType1 from "../_modules/molecules/carousel-type-1/carousel-type-1";
import SideNav from "../_modules/molecules/side-nav/side-nav";
import CarouselType2 from "../_modules/molecules/carousel-type-2/carousel-type-2";
import SiteNav from "../_modules/molecules/site-nav/site-nav";
import CarouselType3 from "../_modules/molecules/carousel-type-3/carousel-type-3";
import LandingBanner from "../_modules/organisms/landing-banner/landing-banner";
import Filter from "../_modules/molecules/filter/filter";
import CarouselBanner from "../_modules/molecules/carousel-banner/carousel-banner";
import PhotoGallery from "../_modules/molecules/photo-gallery/photo-gallery";
import Notification from "../_modules/molecules/notification/notification";
import Accordion from "../_modules/molecules/accordion/accordion";
import Card from "../_modules/molecules/card/card";
import CarouselType4 from "../_modules/molecules/carousel-type-4/carousel-type-4";
import FeedCustomizer from "../_modules/organisms/feed-customizer/feed-customizer";
import Listing from "../_modules/organisms/listing/listing";
import RangeSliderSection from "../_modules/molecules/range-slider-section/range-slider-section";
import CalendarListing from "../_modules/organisms/calendar-listing/calendar-listing";
import ExploreSafra from "../_modules/organisms/explore-safra/explore-safra";
import AjaxCarousel from "../_modules/molecules/ajax-carousel/ajax-carousel";
import ClubPersonalizedSection from "../_modules/organisms/club-personalized-section/club-personalized-section";
import CookieMessage from "../_modules/molecules/cookie-message/cookie-message";
import LocationNotification from "../_modules/molecules/location-notification/location-notification";
import ClubDeals from "../_modules/organisms/club-deals/club-deals";

$(() => {
  window.emitter = new Emitter();
  var testDataSource = "SAFRA Revamp";
  // window.decClient = new sfDataIntell.Client({
  //   apiKey: "377ed7bd-834b-fc28-d624-29eb2f712551",
  //   authToken: "appauth 4CCF5447-1761-9EDB-E450-E47F5A72AE48",
  //   source: testDataSource,
  //   trackPageVisits: true
  // });

  checkLoginCookie();

  function checkLoginCookie() {
    let loginCookie = Cookies.get("sf-data-intell-subject");
    if (loginCookie) {
      changeLoginUrl(loginCookie);
    } else {
      setTimeout(() => {
        checkLoginCookie();
      }, 300);
    }
  }

  function changeLoginUrl(loginCookie) {
    let url = $(".safra-signin")
      .find("a")
      .attr("href");

    let loginURl = url.split("?");

    let newUrl = `${loginURl[0]}?trackid=${loginCookie}`;

    $(".safra-signin")
      .find("a")
      .attr("href", newUrl);
  }

  var onSuccess = function(results) {
    let personaData = {};
    let location = [];
    let persona = [];
    let interest = [];
    let interestData = [];
    let personaItems = [];

    $.ajax({
      url: "/docs/default-source/personalization/decpersonas.json",
      method: "GET",
      type: "application/json",
      success: res => {
        res = JSON.parse(res);
        if (results.items.length) {
          results.items.forEach(result => {
            console.log("res: ", res);
            res.forEach(item => {
              if (item.Id === result.Id) {
                if (item.Type === "interest") {
                  item.Score = result.Score;
                  interestData.push(item);
                  interest.push(item.CmsName);
                }
                if (item.Type === "persona") {
                  item.Score = result.Score;
                  personaItems.push(item);
                  persona.push(item.CmsName);
                }
                if (item.Type === "location") {
                  item.Score = result.Score;
                  location.push(item);
                }
              }
            });
          });

          if (persona.length) {
            personaData.persona = persona.join("|");
          } else {
            personaData.persona = "all";
          }

          if (interest.length) {
            personaData.interest = interest.join("|");
          } else {
            personaData.interest = "all";
          }

          personaData.interestData = interestData;

          personaData.personaItems = personaItems;

          if (location.length) {
            let selectedLocation = null;

            location.forEach((item, index) => {
              if (selectedLocation === null) {
                selectedLocation = item;
              } else if (selectedLocation.Score <= item.Score) {
                selectedLocation = item;
              }
            });

            personaData.location = selectedLocation.CmsName;
          } else {
            personaData.location = "all";
          }

          window.personaData = personaData;
          window.emitter.emit("personalization-trigger");
        }
      },
      error: err => {
        console.log(err);
      }
    });
  };

  var onFailure = function(error) {
    console.log(error);
  };

  if (window.decClient) {
    window.decClient.personalizationClient.isInPersonas(
      [],
      onSuccess,
      onFailure
    );
  }

  objectFitImages();
  $(".fancybox").fancybox();

  $(".match-height-elm").matchHeight();

  $(".match-height-section").matchHeight();

  if ($(".custom-select").length) {
    $(".custom-select").niceSelect();
  }

  if ($(".parallax-section").length) {
    new ParallaxSection();
  }

  if ($(".card__map").length) {
    new Card();
  }

  if ($(".filters").length) {
    new Filter();
  }

  if ($(".accordion").length) {
    new Accordion();
  }

  if ($(".side-nav").length) {
    new SideNav();
  }

  if ($(".tabbed-content").length) {
    new TabbedContent();
  }

  if ($(".recommendation-section").length) {
    new RecommendationSection();
  }

  if ($(".whatson-section").length) {
    new WhatsonSection();
  }

  if ($(".carousel-type-1").length) {
    new CarouselType1();
  }

  if ($(".carousel-type-2").length) {
    new CarouselType2();
  }

  if ($(".carousel-type-3").length) {
    new CarouselType3();
  }

  if ($(".carousel-type-4").length) {
    new CarouselType4();
  }

  if ($(".nsman").length) {
    new Nsman();
  }

  if ($(".interest-groups").length) {
    new InterestGroups();
  }

  if ($(".club-facilities").length) {
    new ClubFacilities();
  }

  if ($(".multi-step-form").length) {
    new MultiStepForm();
  }

  if ($(".photo-gallery").length) {
    new PhotoGallery();
  }

  if ($(".feed-customizer").length) {
    new FeedCustomizer();
  }

  if ($(".explore-safra").length) {
    new ExploreSafra();
  }

  if ($(".listing").length) {
    $(".listing").each(function(index, item) {
      new Listing(item);
    });
  }

  if ($(".range-slider-section").length) {
    new RangeSliderSection();
  }

  if ($(".calendar-listing").length) {
    new CalendarListing();
  }

  if ($(".ajax-carousel").length) {
    new AjaxCarousel();
  }

  if ($(".club-personalized-section").length) {
    new ClubPersonalizedSection();
  }

  new SiteFooter();

  if ($(".location-notification").length) {
    new LocationNotification();
  }

  if ($(".club-deals").length) {
    new ClubDeals();
  }

  $(window).on("scroll", function() {
    let scrollTop = $(window).scrollTop();
    let docHeight = $(document).height();
    let winHeight = $(window).height();

    let scrollPercentage = scrollTop / (docHeight - winHeight);

    $(".page-scroll-progress").css("width", `${scrollPercentage * 100}%`);

    if ($(".content-section").length) {
      let windowTop = $(window).scrollTop();
      let windowBottom = windowTop + $(window).height() - 100;
      $(".content-section").each(function(index, item) {
        let itemTop = $(item).offset().top;
        if (windowBottom > itemTop) {
          $(item).addClass("animate-in");
        }
      });
    }
  });
});

if (window.personalizationManager) {
  window.personalizationManager.addPersonalizedContentLoaded(function() {
    initPersonalizedWidgets();
  });
} else {
  $(function() {
    initPersonalizedWidgets();
  });
}

function initPersonalizedWidgets() {
  if ($(".landing-banner").length) {
    new LandingBanner();
  }

  if ($(".carousel-banner").length) {
    new CarouselBanner();
  }

  if ($(".cookie-message").length) {
    new CookieMessage();
  }

  new Notification();

  new MobileNav();

  new HeaderTop();

  new SiteNav();
  new SiteHeader();
}
